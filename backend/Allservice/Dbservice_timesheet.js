var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
//var url = "mongodb://206.189.154.242:27017/db100961_1";////
//var url = "mongodb://206.189.154.242:27017/DbTimesheet";
//const url = "mongodb://localhost:27017/backup_timesheet";
let dateandtime = require('date-and-time');
var cors = require('cors');
const jwt = require('jsonwebtoken'); 
const randtoken = require('rand-token');
const SECRET = "00f5vdd5r12fg1g[][dsdd122"; 

var moment = require('moment');
var strtotime = require('strtotime');
var DateDiff = require('date-diff');
let date = require('date-and-time');
var nodedatetime = require('node-datetime');
const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const xoauth2 = require('xoauth2');
//-------------------------------------------
var DATABASEUSERNAME = "tedfund";
var DATABASEPASSWORD = "ayQXyMUAK73Z3mY22kKBPkcrfcAh7NUL";
var DATABASEHOST = "206.189.154.242";
var DATABASEPORT = "27017";
var DATABASENAME = "db120961_1";

var url = 'mongodb://'+DATABASEUSERNAME+':'+DATABASEPASSWORD+'@'+DATABASEHOST+':'+DATABASEPORT+'/'+DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateprofile = async(userid,filename)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const update_filename = await db.collection("users").updateOne(
															{"_id":ObjectId(userid)},
															{$set:{	
																profileimage:filename
															}}
															);
	console.log("data: update profile success.");
	return {data:"update profile success."};
}

exports.deletefile = async(filename)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	//delete filename 
	const delete_data = await db.collection("fileproject").deleteOne({filename:filename});
	console.log("data:delete data base file success.");
	return {data:"delete data base file success.",status:false};
}

exports.addfilename_project = async(projectid,filename)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("projectid: " + projectid);
	console.log("filename: " + filename);
	var obj_file={
		projectid:projectid,
		filename:filename,
		insdate:pastdatetime._created
	}
	const insert_file = await db.collection("fileproject").insertOne(obj_file);
	return filename;
}


exports.getnotification_user = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_notifications={
		notifications:[],
		count_read:0
	}
	//count read 
	const getcountread = await db.collection("notification_users").find({$and:[{projectid:projectid},{read:""}]}).toArray();
	obj_notifications.count_read = getcountread.length;
	//get collection notification_users
	const getnoti_users = await db.collection("notification_users").find({projectid:projectid}).toArray();
	for(var countnoti = (getnoti_users.length-1);countnoti >= 0;countnoti--){
		var obj_noti ={
			noti_id:getnoti_users[countnoti]._id,
			comment_rejectid:getnoti_users[countnoti].comment_rejectid,
			projectid:projectid,
			planid:getnoti_users[countnoti].planid,
			statusaction:getnoti_users[countnoti].statusaction,
			statusstring:getnoti_users[countnoti].statusstring,
			progress_groupplan_id:getnoti_users[countnoti].progress_groupplan_id,
			lasttime:"",
			read:getnoti_users[countnoti].read
	}
		var lastprojecttime = moment(Math.ceil(getnoti_users[countnoti].datetimestamp));
		var thistime = moment(pastdatetime._created);
		console.log("lastprojecttime: "+lastprojecttime);
		console.log("this time: "+pastdatetime._created);
		

		//date project
		var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
		var daypro = moment(lastprojecttime).format('DD');
		var monthpro = moment(lastprojecttime).format('MM');
		var yearpro = moment(lastprojecttime).format('YYYY');
		var hrpro = moment(lastprojecttime).format('HH');
		var minutepro = moment(lastprojecttime).format('mm');
		var sspro = moment(lastprojecttime).format('ss');
		var last_updateproject = moment([yearpro, monthpro, daypro,hrpro,minutepro]);
		console.log("last_updateproject: "+last_updateproject);
		var yearth = parseInt(yearpro) + 543;
		obj_noti.lasttime = daypro+"/"+monthpro+"/"+yearth+"  "+hrpro+":"+minutepro+":"+sspro;

		//date now 
		// var datenow_moment = moment(thistime).format('DD/MM/YYYY');
		// var daynow = moment(thistime).format('DD');
		// var monthnow = moment(thistime).format('MM');
		// var yearnow = moment(thistime).format('YYYY');
		// var hrnow = moment(thistime).format('HH');
		// var minutenow = moment(thistime).format('mm');
		// var ssnow = moment(thistime).format('ss');
		// var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
		// console.log("datenow_action: "+datenow_action);

		// var datepro = new Date(yearpro, monthpro, daypro);
		// var datenow = new Date(yearnow, monthnow, daynow);

		// var diff_minute = datenow_action.diff(last_updateproject, 'minute');
		// var diff_Hr = datenow_action.diff(last_updateproject, 'hour');
		// var diff_day = datenow_action.diff(last_updateproject, 'days');

		// console.log("diff_minute: " + diff_minute);
		// console.log("diff_Hr: " + diff_Hr);
		// console.log("diff_day: " + diff_day);
		// if(diff_day > 0){
		// 	obj_noti.lasttime = diff_day +" "+"วัน";
		// }else if(diff_Hr > 0){
		// 	obj_noti.lasttime = diff_Hr +" "+"ชั่วโมง";
		// }else if(diff_minute > 0){
		// 	obj_noti.lasttime = diff_minute +" "+"นาที";
		// }
		obj_notifications.notifications.push(obj_noti);

		// var diff = new DateDiff(datenow,datepro);
		// console.log("diff days: "+diff.days());
		// console.log("diff minute: "+diff.minutes());
		// console.log("diff hours: "+diff.hours());
		// console.log("--------------------------");
		// if(diff.minutes() >= 1440){
		// 	var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
		// 	console.log("calculate days:" + days);
		// }else if (diff.minutes() >= 60){
		// 	var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
		// 	console.log("calculate hours:" + hours);
		// }else if(diff.minutes() <= 59){
		// 	var minutes = (diff.minutes()) +" "+ "นาที";
		// 	console.log("calculate minutes:" + minutes);
		// }
		console.log("--------------------------");
	}
	return obj_notifications;
}

exports.update_readnoti = async(noti_id)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);

	//update noti read = true 
	const update_notification = await db.collection("notification_users").updateOne(
												{"_id":ObjectId(noti_id)},
												{ $set: {read:true}});
	console.log("update noti_id: "+noti_id +" success.");
	return {data:"update noti_id success.",status:true};
}

exports.getcommentrejectdata = async(comment_rejectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_comment = {
		projectname:"",
		planorder:0,
		planname:"",
		durationorder:0,
		jobvalue:0,
		timelength:0,
		statusstring:"ปรับแก้เอกสาร",
		lasttime:0,
		comment:"",
		pathdocument:""
	}
	//get comment reject data 
	const get_comment = await db.collection("commentreject").find({"_id":ObjectId(comment_rejectid)}).toArray();
	obj_comment.comment = get_comment[0].comment;
	obj_comment.pathdocument = get_comment[0].pahtfile;
	//get plan name 
	const get_planname = await db.collection("planorders").find({"_id":ObjectId(get_comment[0].planid)}).toArray();
	obj_comment.planname = get_planname[0].planname;
	obj_comment.planorder = get_planname[0].planorder;
	obj_comment.durationorder = get_planname[0].durationorder;
	obj_comment.jobvalue = get_planname[0].jobvalue
	obj_comment.timelength = get_planname[0].timelength

	//get detail project 
	const get_detailproject = await db.collection("projects").find({"_id":ObjectId(get_comment[0].projectid)}).toArray();
	obj_comment.projectname = get_detailproject[0].projectname;
	

		var lastprojecttime = moment(Math.ceil(get_comment[0].insertdate));
		var thistime = moment(pastdatetime._created);
		console.log("lastprojecttime: "+lastprojecttime);
		console.log("this time: "+pastdatetime._created);

		//date project
		var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
		var daypro = moment(lastprojecttime).format('DD');
		var monthpro = moment(lastprojecttime).format('MM');
		var yearpro = moment(lastprojecttime).format('YYYY');
		var hrpro = moment(lastprojecttime).format('HH');
		var minutepro = moment(lastprojecttime).format('mm');
		var sspro = moment(lastprojecttime).format('ss');
		var last_updateproject = moment([yearpro, monthpro, daypro,hrpro,minutepro]);
		console.log("last_updateproject: "+last_updateproject);

		//date now 
		var datenow_moment = moment(thistime).format('DD/MM/YYYY');
		var daynow = moment(thistime).format('DD');
		var monthnow = moment(thistime).format('MM');
		var yearnow = moment(thistime).format('YYYY');
		var hrnow = moment(thistime).format('HH');
		var minutenow = moment(thistime).format('mm');
		var ssnow = moment(thistime).format('ss');
		var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
		console.log("datenow_action: "+datenow_action);

		var datepro = new Date(yearpro, monthpro, daypro);
		var datenow = new Date(yearnow, monthnow, daynow);

		var diff_minute = datenow_action.diff(last_updateproject, 'minute');
		var diff_Hr = datenow_action.diff(last_updateproject, 'hour');
		var diff_day = datenow_action.diff(last_updateproject, 'days');

		console.log("diff_minute: " + diff_minute);
		console.log("diff_Hr: " + diff_Hr);
		console.log("diff_day: " + diff_day);
		if(diff_day > 0){
			obj_comment.lasttime = diff_day +" "+"วัน";
		}else if(diff_Hr > 0){
			obj_comment.lasttime = diff_Hr +" "+"ชั่วโมง";
		}else if(diff_minute > 0){
			obj_comment.lasttime = diff_minute +" "+"นาที";
		}
		return obj_comment;
}

exports.getcommentreject_progressweek = async(comment_rejectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_comment = {
		statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
		lasttime:0,
		comment:"",
		pathdocument:"",
		planname:""
	}
	//get all comment where progress_groupplan_id
	const getall_comment = await db.collection("comment_progressweek").find({"_id":ObjectId(comment_rejectid)}).toArray();
	console.log("getall_comment.length: " + getall_comment.length);
	let length_comment = (getall_comment.length - 1);
	if(length_comment < 0){
		return {data:"not have comment reject progress week.",status:false};
	}else{
		obj_comment.comment = getall_comment[length_comment].coment;
		obj_comment.pathdocument = getall_comment[length_comment].documentation;

		var lastprojecttime = moment(Math.ceil(getall_comment[length_comment].datetimestamp));
		var thistime = moment(pastdatetime._created);
		console.log("lastprojecttime: "+lastprojecttime);
		console.log("this time: "+pastdatetime._created);

		//date project
		var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
		var daypro = moment(lastprojecttime).format('DD');
		var monthpro = moment(lastprojecttime).format('MM');
		var yearpro = moment(lastprojecttime).format('YYYY');
		var hrpro = moment(lastprojecttime).format('HH');
		var minutepro = moment(lastprojecttime).format('mm');
		var sspro = moment(lastprojecttime).format('ss');
		var last_updateproject = moment([yearpro, monthpro, daypro,hrpro,minutepro]);
		console.log("last_updateproject: "+last_updateproject);

		//date now 
		var datenow_moment = moment(thistime).format('DD/MM/YYYY');
		var daynow = moment(thistime).format('DD');
		var monthnow = moment(thistime).format('MM');
		var yearnow = moment(thistime).format('YYYY');
		var hrnow = moment(thistime).format('HH');
		var minutenow = moment(thistime).format('mm');
		var ssnow = moment(thistime).format('ss');
		var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
		console.log("datenow_action: "+datenow_action);

		var datepro = new Date(yearpro, monthpro, daypro);
		var datenow = new Date(yearnow, monthnow, daynow);

		var diff_minute = datenow_action.diff(last_updateproject, 'minute');
		var diff_Hr = datenow_action.diff(last_updateproject, 'hour');
		var diff_day = datenow_action.diff(last_updateproject, 'days');

		console.log("diff_minute: " + diff_minute);
		console.log("diff_Hr: " + diff_Hr);
		console.log("diff_day: " + diff_day);
		if(diff_day > 0){
			obj_comment.lasttime = diff_day +" "+"วัน";
		}else if(diff_Hr > 0){
			obj_comment.lasttime = diff_Hr +" "+"ชั่วโมง";
		}else if(diff_minute > 0){
			obj_comment.lasttime = diff_minute +" "+"นาที";
		}
		return obj_comment;
	}
}

exports.getdetailuserprimary = async(userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_user = {
		userid:userid,
		username:"",
		profileimage:""
	}
	//get detailuser primary 
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	obj_user.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
	obj_user.profileimage = getuserprimary[0].profileimage;
	return obj_user;
}

exports.createaccount = async(fristname,lastname,phone,email,token,duedate,batchid,companyname)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//var decode = jwt.verify(token,SECRET);
	var password = token;//decode.password;
   //res.json({token:token,decode:decode.password});

	const getduedate = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var duedate = getduedate[0].duedate;//.format("D/MMMM/YYYY");
	var day = moment().format("D");
	var month = moment().format("MMMM");
	var year = moment().format("YYYY");
	var year_th = parseInt(year) + 543;
	console.log("duedate: " + duedate);
	console.log("thisday: "+ day+"/"+month+"/"+year_th);
	if(duedate < pastdatetime._created){
		console.log("data: not create account becuase is duedate.");
		var respond={data:"not create account becuase is duedate.",status:false};
		return respond;
	}else{
		var userobj={
				batchid:batchid,
				fristname:fristname,
				lastname:lastname,
				phone:phone,
				email:email,
				password:password,
				position:"",
				companyname:companyname,
				profileimage:"",
				Insdate:pastdatetime._created
			};
		var respond ={data:[]}
		//check email 
		const checkemail = await db.collection("users").find({email:email}).toArray();
		if(checkemail.length > 0){
			var resultcheckemail = {data:"email has been used",status:false}
			console.log(resultcheckemail);
			return resultcheckemail;
		}else{
			//create account
			const insertaccount = await db.collection("users").insertOne(userobj)
										.then(function(result){
											respond.data.push({data:"create account sucess.",status:true});
										});
			//get batch detail 
			const getbatchdetail = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
			var batchname = getbatchdetail[0].modelname;

			//send email 									
			var transporter = await nodemailer.createTransport(smtpTransport({
				service: 'gmail',
		  		host: 'smtp.gmail.com',
				auth:{
					xoauth2:xoauth2.createXOAuth2Generator({
						user:'tedfund.app@gmail.com',
						clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
						clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
						refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
					})
				},
			}));

			 const output = `
			 	<h3 style="color:#128553">Create Account Success</h3>
			    <h4 style="color:#128553;margin-left:20px">${batchname}</h4>
				<ul>  
					<li style="margin-bottom:5px">Name:  ${fristname}  ${lastname}</li>
					<li style="margin-bottom:5px">Email: ${email}</li>
					<li style="margin-bottom:5px">Phone: ${phone}</li>
				</ul>
				<hr color='128553'>
			  `;
			//set mail options
			var mailOptions = {
				from:'tedfund.app <tedfund.app@gmail.com>',
				to:email,
				subject:'Create account succcess.',
				text:'Register TEDfund.',
				html:output
			}
			// sent email
			var sentemail = await transporter.sendMail(mailOptions,function(err,res){
				if(err){
					console.log(err);
				}else{
					console.log('Email sent.');
					console.log(mailOptions);
				}
			});
			return respond;
		}		
	}
}

exports.loginuser = async(email,token)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var decode_front = jwt.verify(token,SECRET);
	var password_front = decode_front.password;
	console.log("password_front: "+ password_front);

	//check json token 
	const getpassjwt = await db.collection("users").find({email:email}).toArray();
	//check email 
	if(getpassjwt.length == 0){
		var respond = {data:"invalid username or password.",status:false};
		return respond;
	}else{
		var decode_back = jwt.verify(getpassjwt[0].password,SECRET);
		var password_back = decode_back.password;
		console.log("password_back:"+password_back);
		if(password_front == password_back){
			var password = password_front;
		    console.log("userid: "+ getpassjwt[0]._id);
				var userid = getpassjwt[0]._id;
				const getprojectid = await db.collection("projects").find({Primaryresponsibility:userid.toString()}).toArray();
				if (getprojectid.length == 0){
					var resultdata={
					 	data:"login pass",
					 	userid:getpassjwt[0]._id,
					 	fristname:getpassjwt[0].fristname,
					 	lastname:getpassjwt[0].lastname,
					 	phone:getpassjwt[0].phone,
					 	batchid:getpassjwt[0].batchid,
					 	projectid:"",
					 	position:getpassjwt[0].position,
					 	status:true
					}
					console.log(resultdata);
					return resultdata;
				}else{
					var resultdata={
					 	data:"login pass",
					 	userid:getpassjwt[0]._id,
					 	fristname:getpassjwt[0].fristname,
					 	lastname:getpassjwt[0].lastname,
					 	phone:getpassjwt[0].phone,
					 	batchid:getprojectid[0].batchid,
					 	projectid:getprojectid[0]._id,
					 	projectname:getprojectid[0].projectname,
					 	position:getpassjwt[0].position,
					 	status:true,
					 	statusproject:"",
					 	Fill_in:""
					}
					//check status project
					if(getprojectid[0].status == 1){
						resultdata.statusproject = "กำลังดำเนินการ";
						resultdata.Fill_in = "กำลังกรอกข้อมูล";
					}else if(getprojectid[0].status == 2){
						resultdata.statusproject = "รอดำเนินการ";
						resultdata.Fill_in = "ส่งเอกสารแล้ว";
					}else if(getprojectid[0].status == 3){
						resultdata.statusproject = "ปรับแก้เอกสาร";
						resultdata.Fill_in = "ปรับแก้เอกสาร";
					}else if(getprojectid[0].status == 4){
						resultdata.statusproject = "อนุมัติ Timesheet";
						resultdata.Fill_in = "อนุมัติ Timesheet";
					}

					console.log(resultdata);
					return resultdata;
				}
			
		}else{
			var respond = {data:"invalid username or password.",status:false};
			return respond;
		}
	}
	
}

exports.fillin_userprimary = async(userid,batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_project = {
		firstname:"",
		lastname:"",
		phone:"",
		batchid:"",
		batchname:""
	}

	//get user detail 
	const getuser = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	obj_project.firstname = getuser[0].fristname;
	obj_project.lastname = getuser[0].lastname;
	obj_project.phone = getuser[0].phone;
	obj_project.batchid = batchid;

	//get batch name 
	const getbatchname = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	obj_project.batchname = getbatchname[0].modelname;
	console.log(obj_project);
	return obj_project;
}

exports.dashboard = async(userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const getproject = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	if(getproject.length == 0){
		console.log("data: no have projects");
		var result = {data:" no have project",status:true}
		return result;
	}else{
		console.log("data: have projects");
		var result = {data:"have project",status:true}
		return result;
	}
}

exports.dashboard_percent = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objproject = {
		projectname:"",
		projectvalue:0,
		statusproject:"",
		Fill_in:"",
		partdocument:0,
		partplan:0,
		partteam:0,
		partsending:0,
		percent1to3:0,
		percentcircle:0
	}
	//documentation 
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	const getalldocumetation = await db.collection("fileproject").find({projectid:projectid}).toArray();
	if (getalldocumetation.length == 0){
		objproject.partdocument = 0;
	}else{
		objproject.partdocument = 100;
	}
	console.log("percent part 1: "+objproject.partdocument);
	var percent1 = objproject.partdocument;
	objproject.partdocument = percent1;
	console.log("---------------------------------------");

	//plan order
	console.log("amount plan: "+ getdocumetation[0].amountplan);
	var amountplan = getdocumetation[0].amountplan;
	const getallplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	var plans = getallplanorder.length;
	console.log("plans: "+plans);
	var percentplan = 100/amountplan;
	var calculate_percent_plan = percentplan * plans;
	console.log("percent part 2: "+calculate_percent_plan);
	var percent2 = calculate_percent_plan;
	objproject.partplan = percent2;
	console.log("---------------------------------------");

	//team 
	const get_amount_team = await  db.collection("users").find({projectid:projectid}).toArray();
	var amountusers = get_amount_team.length;//team_added
	var userprimary = 1;
	console.log("amount team: "+ (amountusers+userprimary));
	var allusers = (amountusers+userprimary);
	const getamount_setteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var setamount = getamount_setteam[0].amountteam;
	var percentteam = 100/setamount;
	console.log("percent amount users: "+ percentteam);
	var percent3 = (percentteam * allusers);
	objproject.partteam = percent3;
	console.log("percent part 3:"+ percentteam * allusers);
	console.log("---------------------------------------");

	//percent1to3
	objproject.percent1to3 = ((percent1 + percent2 + percent3) * 50) /300;
	
	//part sending
	const getstatusproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var status = getstatusproject[0].status;
	if(status == 1){
		console.log("part 4 sending: 0%");
		console.log("---------------------------------------");
		objproject.partsending = objproject.percent1to3;
	}else{
		console.log("part 4 sending: 100%");
		console.log("---------------------------------------");
		objproject.partsending = 100;
	}
	//percent all 
	var percentall = (((percent1 + percent2 + percent3) + objproject.partsending) * 100)/400 ;
	//var totalpercent = (percentall*100) / 300;
	objproject.percentcircle = percentall;
	console.log("percentcircle: "+ percentall);
	console.log("---------------------------------------");
	console.log(objproject);

	//get detail project 
	// projectname:"",
	// projectvalue:0,
	// statusproject:"",
	// Fill_in:"",
	objproject.projectname = getstatusproject[0].projectname;
	objproject.projectvalue = getstatusproject[0].projectvalue;
	if(getstatusproject[0].status == 1){
		objproject.statusproject = "กำลังดำเนินการ";
		objproject.Fill_in = "กำลังกรอกข้อมูล";
	}else if(getstatusproject[0].status == 2){
		objproject.statusproject = "รอดำเนินการ";
		objproject.Fill_in = "ส่งเอกสารแล้ว";
	}else if(getstatusproject[0].status == 3){
		objproject.statusproject = "ปรับแก้เอกสาร";
		objproject.Fill_in = "ปรับแก้เอกสาร";
	}else if(getstatusproject[0].status == 4){
		objproject.statusproject = "อนุมัติ Timesheet";
		objproject.Fill_in = "อนุมัติ Timesheet";
	}
	
	return objproject;
}

exports.createproject = async(userid,projectname,projectvalue,batchid)=>{
	console.log("userid: "+userid);
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var porjectobj={
		batchid:batchid,
		projectname:projectname,
		projectvalue:projectvalue,
		Primaryresponsibility:userid,
		Documentation:"",
		Insdate:pastdatetime._created,
		updateproject:"",
		senddatetime:"",
		amountteam:0,
		status:1,
		active:1
	};
	var projectobjres = {}
	var batchbsm = {}
	//insert project 
	const insertproject = await db.collection("projects").insertOne(porjectobj);
	//find _id project 
	const getproject = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	projectobjres = {
						data:"new project sucess",
						userid:userid,
						projectid:getproject[0]._id,
						projectname:getproject[0].projectname,
						status:true
					}
	batchbsm = {
					batchid:batchid,
					projectid:getproject[0]._id,
					Insdate:pastdatetime._created,
					updatetime:"",
					status:"1",
					active:"1"
				}
	//insert project in bsm 
	const insertbsm = await db.collection("batchbusinessman").insertOne(batchbsm);
	//update aleady businessman 
	const findbsminbatch =  await db.collection("projects").find({batchid:batchid}).toArray();
	const updatealready = await db.collection("projects").updateOne(
												{"_id":ObjectId(batchid)},
												{ $set: {alreadybusinessman:findbsminbatch.length}});
	//safe draft 2 สร้างโครงการ
	const getidactivity = await db.collection("activity").find({activityorder:"2"}).toArray();
	console.log("get activity _id: "+ getidactivity[0]._id);
	var objactivity = {
		userid:userid,
		activityid:getidactivity[0]._id,
		datetime:pastdatetime._created
	}
	console.log("---------insert activity---------");
	console.log(objactivity);
	console.log("---------------------------------");
	const insertactivity = await db.collection("activityusers").insertOne(objactivity); 

	return projectobjres;
}

exports.getuserprimary = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getiduserprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("userprimary: "+ getiduserprimary[0].Primaryresponsibility);
	const getdatauserprimary = await db.collection("users").find({"_id":ObjectId(getiduserprimary[0].Primaryresponsibility)}).toArray();
	console.log(getdatauserprimary);
	return getdatauserprimary;
}

exports.updateuserprimary = async(userid,fristname,lastname,phone,position,profileimage)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var objuser = {data:"update user success.",user:[],status:true};
	//var path_profileimage = "../../fontend/src/assets/upload/profil"+profileimage;
	//update user primary 
	const updateuserprimary = await db.collection("users").updateOne(
					{"_id":ObjectId(userid)},
					{ $set: { fristname: fristname,
							  lastname: lastname,
							  phone:phone,
							  position:position,
							  profileimage:profileimage,
							  updatetime:pastdatetime._created
					}});
	var user = {
		fristname: fristname,
		lastname: lastname,
		phone:phone,
		position:position,
		profileimage:profileimage
	}
	objuser.user.push(user);
	return objuser;
}

exports.adddocument = async(userid,projectid,pathdocument)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const updatedocument = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set: {Documentation: pathdocument}});
	//get documentation 
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("documenttaion: "+ getdocumetation[0].Documentation);
	//get activity_id  
		var activityorder = "1"; // สร้างแผนงาน
		const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
		console.log("activity _id: " + getactivity_id[0]._id);
		console.log("update document success.");
		console.log("projectid: "+ projectid);
		console.log("---------------------------");
		//insert activityusers
		const insert_activity_user = await db.collection("activityusers").insert({
										userid:userid,
										activityid:getactivity_id[0]._id,
										datetime:pastdatetime._created
								  });
	//check document for draft 
	if(getdocumetation[0].Documentation == ""){
		var draftat = "1";
		console.log("draft at: "+ draftat);
		return {data:"no have document.",draft:draftat,status:true};
	}else{
		var draftat = "2";
		console.log("draft at: "+ draftat);
		return {data:"update document sucess.",status:true};
	}
}

exports.addplanorder = async(userid,projectid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var totalhours = timelength * 8
	let countplanorder
	const get_countplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get_countplan.length: "+ get_countplan.length);
	let amount_plan = get_countplan.length+1;
	const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
	var planobj={
			projectid:projectid,
			planorder:amount_plan,
			planname:planname,
			jobvalue:jobvalue,
			moneydifference:moneydifference,
			enforceemployee:enforceemployee,
			durationid:durationid,
			durationorder:getdurationnumber[0].durationorder,
			averagewages:averagewages,
			timelength:timelength,
			totalhours:totalhours,
			averagehours:0,
			Insdate:pastdatetime._created,
			status:1,
			active:1
	};
	//calculate average hour/man
	var averageHR = ((jobvalue/timelength)/enforceemployee)/8;
	planobj.averagehours = averageHR;

	const insertplan = await db.collection("planorders").insertOne(planobj);
	//get activity_id  
	var activityorder = "2.3"; // สร้างแผนงาน
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });

	// get amount plan 
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("amountplan: "+getamountplan[0].amountplan);
	var amount = getamountplan[0].amountplan;
	// get plan where projectid 
	const getplanfromproject = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get plan: "+getplanfromproject.length);
	var getplan = getplanfromproject.length;
	var calpercent = 100/amount;
	console.log("percent unit:"+ calpercent + "%");
	var calplanpercent = calpercent * getplan;
	console.log("percent plan: "+calplanpercent +"%");
	console.log("---------------------------");
	if(calplanpercent < 100){
		var draftat = "2.3";
		console.log("draft at: "+ draftat);
		return {data:"insert plan success.",draft:draftat,status:true};
	}else{
		var draftat = "3.1";
		console.log("draft at: "+ draftat);
		//get activity_id  
		var activityorder = draftat; //เพิ่มจำนวนคนทำงาน
		const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
		console.log("activity _id: " + getactivity_id[0]._id);
		console.log("update amount plan success.");
		console.log("projectid: "+ projectid);
		console.log("---------------------------");
		//insert activityusers
		const insert_activity_user = await db.collection("activityusers").insert({
										userid:userid,
										activityid:getactivity_id[0]._id,
										datetime:pastdatetime._created
								  });
		return {data:"insert plan success.",draft:draftat,status:true};
	}
}

exports.getpallplanorder = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var new_datestring = moment().format("dddd");
	console.log("new_datestring: "+ new_datestring);
	var new_datenow = moment().format('D/MMMM/YYYY');
	console.log("new_datenow: "+ new_datenow);
	var datapaln={
				project:{
					lateweek:"",
					projectname:"",
					projectvalue:0,
					valueused:0,
					amountplan:0,
					planadded:0,
					plan:[],
					amountsetteam:0,
					teamadded:0,
					activity:"",
					duration:0,
					durationnumber:0,
					weeknumber:0,
					daynumber:0,
					statusproject:0,
					statusprojectstring:"",
					
			}
		}

	if(new_datestring == "Sunday"){
		var day6 = moment().add(-1,'days').format("D/MMMM/YYYY");
		console.log("day6: "+day6);
		//get week id 
		const getweekid = await db.collection("days").find({$and:[{datestring:day6},{projectid:projectid}]}).toArray();
		//get week number 
		//const getweeknumber = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
		const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(getweekid[0].weekid)}).toArray();
		datapaln.project.weeknumber = weeknumber;
		datapaln.project.daynumber = 7;
		var status_late = getweeknumber[0].status_late;
		if(status_late == true){
			datapaln.project.lateweek = "true";
			console.log("************************************************");
			console.log("getweeknumber[0].status_late:" + getweeknumber[0].status_late);
			console.log("datapaln.project.lateweek=true");
			console.log("************************************************");
		}else{
			datapaln.project.lateweek = "false";
			console.log("************************************************");
			console.log("datapaln.project.lateweek=false");
			console.log("************************************************");
		}
		

		//get duration number 
		const get_durationnumber = await db.collection("duration").find({"_id":ObjectId(getweeknumber[0].durationid)}).toArray();
		datapaln.project.durationnumber = get_durationnumber[0].durationorder;
	}else{
		//get daynumber 
		const getdaynumber = await db.collection("days").find({$and:[{datestring:new_datenow},{projectid:projectid}]}).toArray();
		if(getdaynumber.length == 0){
			console.log("data: no have day on timesheet this week.");
		}else{
			datapaln.project.daynumber = getdaynumber[0].daynumber;
			var weekid = getdaynumber[0].weekid;

			///get weeknumber and daynumber 
			//const getweeknumber = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
			const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
			console.log("getweeknumber:"+ getweeknumber);
			datapaln.project.weeknumber = getweeknumber[0].weeknumber;
			var status_late = getweeknumber[0].status_late;
			if(status_late == true){
				datapaln.project.lateweek = "true";
				console.log("************************************************");
				console.log("getweeknumber[0].status_late:" + status_late);
				console.log("datapaln.project.lateweek=true");
				console.log("************************************************");
			}else{
				datapaln.project.lateweek = "false";
				console.log("************************************************");
				console.log("getweeknumber[0].status_late:" + status_late);
				console.log("datapaln.project.lateweek=false");
				console.log(getweeknumber);
				console.log("************************************************");
			}
			
			//get duration number 
			const get_durationnumber = await db.collection("duration").find({"_id":ObjectId(getweeknumber[0].durationid)}).toArray();
			datapaln.project.durationnumber = get_durationnumber[0].durationorder;
		}
	}	
		

	//check duration on project 
	const check_duration = await db.collection("duration").find({projectid:projectid}).toArray();
	datapaln.project.duration = check_duration.length;
	console.log("check duration: "+check_duration.length);
	//find total jobvalue = sum(jobvalud in plan)
	var totalprojectvalue = 0;

	// //find detail project 
	var getallplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	datapaln.project.amountplan = getallplan[0].amountplan;
	datapaln.project.projectname = getallplan[0].projectname;
	datapaln.project.statusproject = getallplan[0].status;
	if(getallplan[0].status == 4){
		datapaln.project.statusprojectstring = "อนุมัติ Timesheet";
	}else{
		datapaln.project.statusprojectstring = "รอการอนุมัติ";
	}

	//sum total project value used 
	var sumtotal_valueused = 0;
	const get_projectvalue_used = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var countproject =0;countproject < get_projectvalue_used.length;countproject++){
		sumtotal_valueused += get_projectvalue_used[countproject].totalvalue;
	}
	datapaln.project.valueused = sumtotal_valueused;

	//find planorder
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get all plan: "+getallplan.length);
	var totalplanadded = 0;
	for(var i = 0; i < getallplan.length;i++){
		totalprojectvalue += getallplan[i].jobvalue;
		console.log(getallplan[i].durationid);
		
		//sum job value used 
		var total_valueused = 0;
		var planid_find = getallplan[i]._id.toString();
		const get_sumvalue_used = await db.collection("timesheet").find({planid:planid_find}).toArray();
		for(var count = 0;count < get_sumvalue_used.length;count++){
			total_valueused += get_sumvalue_used[count].totalvalue;
		}

		//status plan 
		var statusplan_string = "";
		if(getallplan[i].statusplan == ""){
			statusplan_string = "";
		}else if(getallplan[i].statusplan == true){
			statusplan_string = true;
		}else if(getallplan[i].statusplan == false){
			statusplan_string = false;
		}

		//find duration 
		var getduration = await db.collection("duration").find({"_id":ObjectId(getallplan[i].durationid)}).toArray();
		var durationnumber = getduration[0].durationorder;
		var dataorder = {
			planid:getallplan[i]._id,
			planname:getallplan[i].planname,
			duration:durationnumber,
			jobvalue:getallplan[i].jobvalue,
			moneydifference:getallplan[i].moneydifference,
			timelength:getallplan[i].timelength,
			valueused:total_valueused,
			statusplan:statusplan_string
		}
		datapaln.project.plan.push(dataorder);
		totalplanadded += 1;
	}
	datapaln.project.planadded = totalplanadded;
	// datapaln.project.planadded = getallplan.length;
	datapaln.project.projectvalue = totalprojectvalue;


	//get amount set team 
	const getamountsetteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getamountsetteam.length == 0){
		datapaln.project.amountsetteam = 0;
	}else{
		datapaln.project.amountsetteam = getamountsetteam[0].amountteam;
	}

	//get team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if(getteamadded.length == 0){
		datapaln.project.teamadded = 0;
	}else{	
		datapaln.project.teamadded = (getteamadded.length +1);
	}

	//set next activity 
	if(datapaln.project.amountsetteam == 0){
		datapaln.project.activity = "3.1";
	}else{
		datapaln.project.activity = "3.2";
	}
	
	console.log(datapaln.project);
	return datapaln.project;
}


exports.getpallplanorder_report = async(projectid,weeknumber)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var new_datestring = moment().format("dddd");
	console.log("new_datestring: "+ new_datestring);
	var new_datenow = moment().format('D/MMMM/YYYY');
	console.log("new_datenow: "+ new_datenow);
	var datapaln={
				project:{
					lateweek:"",
					projectname:"",
					projectvalue:0,
					valueused:0,
					amountplan:0,
					planadded:0,
					plan:[],
					amountsetteam:0,
					teamadded:0,
					activity:"",
					duration:0,
					durationnumber:0,
					weeknumber:0,
					daynumber:0,
					statusproject:0,
					statusprojectstring:"",
					
			}
		}

	if(new_datestring == "Sunday"){
		var day6 = moment().add(-1,'days').format("D/MMMM/YYYY");
		console.log("day6: "+day6);
		//get week id 
		const getweekid = await db.collection("days").find({$and:[{datestring:day6},{projectid:projectid}]}).toArray();
		//get week number 
		const getweeknumber = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
		//const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(getweekid[0].weekid)}).toArray();
		datapaln.project.weeknumber = weeknumber;
		datapaln.project.daynumber = 7;
		var status_late = getweeknumber[0].status_late;
		if(status_late == true){
			datapaln.project.lateweek = "true";
			console.log("************************************************");
			console.log("getweeknumber[0].status_late:" + getweeknumber[0].status_late);
			console.log("datapaln.project.lateweek=true");
			console.log("************************************************");
		}else{
			datapaln.project.lateweek = "false";
			console.log("************************************************");
			console.log("datapaln.project.lateweek=false");
			console.log("************************************************");
		}
		

		//get duration number 
		const get_durationnumber = await db.collection("duration").find({"_id":ObjectId(getweeknumber[0].durationid)}).toArray();
		datapaln.project.durationnumber = get_durationnumber[0].durationorder;
	}else{
		//get daynumber 
		const getdaynumber = await db.collection("days").find({$and:[{datestring:new_datenow},{projectid:projectid}]}).toArray();
		if(getdaynumber.length == 0){
			console.log("data: no have day on timesheet this week.");
		}else{
			datapaln.project.daynumber = getdaynumber[0].daynumber;
			var weekid = getdaynumber[0].weekid;

			///get weeknumber and daynumber 
			const getweeknumber = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
			//const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
			console.log("getweeknumber:"+ getweeknumber);
			datapaln.project.weeknumber = getweeknumber[0].weeknumber;
			var status_late = getweeknumber[0].status_late;
			if(status_late == true){
				datapaln.project.lateweek = "true";
				console.log("************************************************");
				console.log("getweeknumber[0].status_late:" + status_late);
				console.log("datapaln.project.lateweek=true");
				console.log("************************************************");
			}else{
				datapaln.project.lateweek = "false";
				console.log("************************************************");
				console.log("getweeknumber[0].status_late:" + status_late);
				console.log("datapaln.project.lateweek=false");
				console.log(getweeknumber);
				console.log("************************************************");
			}
			//get duration number 
			const get_durationnumber = await db.collection("duration").find({"_id":ObjectId(getweeknumber[0].durationid)}).toArray();
			datapaln.project.durationnumber = get_durationnumber[0].durationorder;
		}
	}	
		

	//check duration on project 
	const check_duration = await db.collection("duration").find({projectid:projectid}).toArray();
	datapaln.project.duration = check_duration.length;
	console.log("check duration: "+check_duration.length);
	//find total jobvalue = sum(jobvalud in plan)
	var totalprojectvalue = 0;

	// //find detail project 
	var getallplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	datapaln.project.amountplan = getallplan[0].amountplan;
	datapaln.project.projectname = getallplan[0].projectname;
	datapaln.project.statusproject = getallplan[0].status;
	if(getallplan[0].status == 4){
		datapaln.project.statusprojectstring = "อนุมัติ Timesheet";
	}else{
		datapaln.project.statusprojectstring = "รอการอนุมัติ";
	}

	//sum total project value used 
	var sumtotal_valueused = 0;
	const get_projectvalue_used = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var countproject =0;countproject < get_projectvalue_used.length;countproject++){
		sumtotal_valueused += get_projectvalue_used[countproject].totalvalue;
	}
	datapaln.project.valueused = sumtotal_valueused;

	//find planorder
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get all plan: "+getallplan.length);
	var totalplanadded = 0;
	for(var i = 0; i < getallplan.length;i++){
		totalprojectvalue += getallplan[i].jobvalue;
		console.log(getallplan[i].durationid);
		
		//sum job value used 
		var total_valueused = 0;
		var planid_find = getallplan[i]._id.toString();
		const get_sumvalue_used = await db.collection("timesheet").find({planid:planid_find}).toArray();
		for(var count = 0;count < get_sumvalue_used.length;count++){
			total_valueused += get_sumvalue_used[count].totalvalue;
		}

		//status plan 
		var statusplan_string = "";
		if(getallplan[i].statusplan == ""){
			statusplan_string = "";
		}else if(getallplan[i].statusplan == true){
			statusplan_string = true;
		}else if(getallplan[i].statusplan == false){
			statusplan_string = false;
		}

		//find duration 
		var getduration = await db.collection("duration").find({"_id":ObjectId(getallplan[i].durationid)}).toArray();
		var durationnumber = getduration[0].durationorder;
		var dataorder = {
			planid:getallplan[i]._id,
			planname:getallplan[i].planname,
			duration:durationnumber,
			jobvalue:getallplan[i].jobvalue,
			moneydifference:getallplan[i].moneydifference,
			timelength:getallplan[i].timelength,
			valueused:total_valueused,
			statusplan:statusplan_string
		}
		datapaln.project.plan.push(dataorder);
		totalplanadded += 1;
	}
	datapaln.project.planadded = totalplanadded;
	// datapaln.project.planadded = getallplan.length;
	datapaln.project.projectvalue = totalprojectvalue;


	//get amount set team 
	const getamountsetteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getamountsetteam.length == 0){
		datapaln.project.amountsetteam = 0;
	}else{
		datapaln.project.amountsetteam = getamountsetteam[0].amountteam;
	}

	//get team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if(getteamadded.length == 0){
		datapaln.project.teamadded = 0;
	}else{	
		datapaln.project.teamadded = (getteamadded.length +1);
	}

	//set next activity 
	if(datapaln.project.amountsetteam == 0){
		datapaln.project.activity = "3.1";
	}else{
		datapaln.project.activity = "3.2";
	}
	
	console.log(datapaln.project);
	return datapaln.project;
}

exports.getallplanorder_before_approve =async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var new_datestring = moment().format("dddd");
	console.log("new_datestring: "+ new_datestring);
	var new_datenow = moment().format('D/MMMM/YYYY');
	console.log("new_datenow: "+ new_datenow);
	var datapaln={
				project:{
				projectname:"",
				projectvalue:0,
				valueused:0,
				amountplan:0,
				planadded:0,
				plan:[],
				amountsetteam:0,
				teamadded:0,
				activity:"",
				duration:0,
				weeknumber:0,
				daynumber:0,
				statusproject:0,
				statusprojectstring:""
			}
		}

	// if(new_datestring == "Sunday"){
	// 	var day6 = moment().add(-1,'days').format("D/MMMM/YYYY");
	// 	console.log("day6: "+day6);
	// 	//get week id 
	// 	const getweekid = await db.collection("days").find({$and:[{datestring:day6},{projectid:projectid}]}).toArray();
	// 	//get week number 
	// 	const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(getweekid[0].weekid)}).toArray();
	// 	datapaln.project.weeknumber = getweeknumber[0].weeknumber;
	// 	datapaln.project.daynumber = 7;
	// }else{
	// 	//get daynumber 
	// 	const getdaynumber = await db.collection("days").find({$and:[{datestring:new_datenow},{projectid:projectid}]}).toArray();
	// 	datapaln.project.daynumber = getdaynumber[0].daynumber;
	// 	var weekid = getdaynumber[0].weekid;

	// 	///get weeknumber and daynumber 
	// 	const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
	// 	datapaln.project.weeknumber = getweeknumber[0].weeknumber;
	// }	
		

	//check duration on project 
	const check_duration = await db.collection("duration").find({projectid:projectid}).toArray();
	datapaln.project.duration = check_duration.length;
	console.log("check duration: "+check_duration.length);
	//find total jobvalue = sum(jobvalud in plan)
	var totalprojectvalue = 0;

	// //find detail project 
	var getallplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getallplan[0].amountplan == ""){
		datapaln.project.amountplan = 0;
	}else if(getallplan[0].amountplan > 0){
		datapaln.project.amountplan = getallplan[0].amountplan;
	}else{
		datapaln.project.amountplan = 0;
	}
	
	datapaln.project.projectname = getallplan[0].projectname;
	datapaln.project.statusproject = getallplan[0].status;
	if(getallplan[0].status == 4){
		datapaln.project.statusprojectstring = "อนุมัติ Timesheet";
	}else{
		datapaln.project.statusprojectstring = "รอการอนุมัติ";
	}

	//sum total project value used 
	var sumtotal_valueused = 0;
	const get_projectvalue_used = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var countproject =0;countproject < get_projectvalue_used.length;countproject++){
		sumtotal_valueused += get_projectvalue_used[countproject].totalvalue;
	}
	datapaln.project.valueused = sumtotal_valueused;

	//find planorder
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get all plan: "+getallplan.length);
	var totalplanadded = 0;
	for(var i = 0; i < getallplan.length;i++){
		totalprojectvalue += getallplan[i].jobvalue;
		console.log(getallplan[i].durationid);
		//find duration 
		var getduration = await db.collection("duration").find({"_id":ObjectId(getallplan[i].durationid)}).toArray();
		var durationnumber = getduration[0].durationorder;
		var dataorder = {
			planid:getallplan[i]._id,
			planname:getallplan[i].planname,
			duration:durationnumber,
			jobvalue:getallplan[i].jobvalue,
			timelength:getallplan[i].timelength
		}
		datapaln.project.plan.push(dataorder);
		totalplanadded += 1;
	}
	datapaln.project.planadded = totalplanadded;
	// datapaln.project.planadded = getallplan.length;
	datapaln.project.projectvalue = totalprojectvalue;


	//get amount set team 
	const getamountsetteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getamountsetteam.length == 0){
		datapaln.project.amountsetteam = 0;
	}else{
		datapaln.project.amountsetteam = getamountsetteam[0].amountteam;
	}

	//get team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if(getteamadded.length == 0){
		datapaln.project.teamadded = 0;
	}else{	
		datapaln.project.teamadded = (getteamadded.length +1);
	}

	//set next activity 
	if(datapaln.project.amountsetteam == 0){
		datapaln.project.activity = "3.1";
	}else{
		datapaln.project.activity = "3.2";
	}
	
	console.log(datapaln.project);
	return datapaln.project;
}

exports.getplanorder = async(planid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var dataplan={
		planadded:0,
		amountplan:0,
		plandetial:[
			// planname:getplan[0].planname,
			// jobvalue:getplan[0].jobvalue,
			// duration:getduration[0].durationorder,
			// timelength:getplan[0].timelength,
			// averagewage:getplan[0].averagewages,
			// enforceemployee:getplan[0].enforceemployee
			// duration:
			 
		],
		duration:[]
	}
	const getplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	const getduration = await  db.collection("duration").find({"_id":ObjectId(getplan[0].durationid)}).toArray();
	const getallduration = await  db.collection("duration").find({projectid:getplan[0].projectid}).toArray();
	//count plan in project 
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(getplan[0].projectid)}).toArray();
	dataplan.amountplan = parseInt(getamountplan[0].amountplan);
	//find plan added 
	const findplanadded = await db.collection("planorders").find({projectid:getplan[0].projectid}).toArray();
	dataplan.planadded = findplanadded.length;
	var dataplandetail = {
		planname:getplan[0].planname,
		jobvalue:getplan[0].jobvalue,
		duration:getduration[0].durationorder,
		durationid:getduration[0]._id,
		timelength:getplan[0].timelength,
		averagewage:getplan[0].averagewages,
		moneydifference:getplan[0].moneydifference,
		enforceemployee:getplan[0].enforceemployee
	}
	for(var i = 0; i < getallduration.length; i++){
		dataplan.duration.push(getallduration[i]);
	}
	dataplan.plandetial.push(dataplandetail);
	console.log(dataplan);
	console.log("---------------------------------");
	return dataplan;
} 

exports.setamountorder = async(amountorder,projectid,userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const setamountplan = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set: {amountplan: amountorder}});
	//get activity_id  
	var activityorder = "2.2";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });  
	return {data:"set amount plan success.",draft:"2.3",status:true};
}

exports.getsetamountorder = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getamountorder = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray(); 
	return {projectid:projectid,amountplan:getamountorder[0].amountplan};
}

exports.updateplanorder = async(planid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
	const updateplanorder = await db.collection("planorders").updateOne(
									{"_id":ObjectId(planid)},
									{ $set: { planname: planname,
											  jobvalue: jobvalue,
											  durationid: durationid,
											  durationorder:getdurationnumber[0].durationorder,
											  timelength: timelength,
											  averagewages: averagewages,
											  moneydifference:moneydifference,
											  enforceemployee:enforceemployee,
											  updatetime:pastdatetime._created}});
	return {data:"update plan sucess.",planid:planid,status:true};
}

exports.addamounteam = async(userid,projectid,amount)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//add amount user in projects
	const addamountteam = await db.collection("projects").updateOne(
							{"_id":ObjectId(projectid)},
							{ $set: {
								amountteam:amount
							}});
	console.log("add amount team in project: "+ amount);
	//add activity 
	var activityorder = "3.1"; // เพิ่มจำนวนคนทำงาน
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("userid: "+ userid);
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });
	return {data:"add amount success.",status:true};
}

exports.addteamwork = async(userid,projectid,fristname,lastname,position,profileimage,email,phone)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const checkemail = await db.collection("users").find({email:email}).toArray();
	if(checkemail.length > 0){
		var respond = {data:"data: email has been used.",status:false};
		return respond;
	}else{
		//var path_profileimage = "../../fontend/src/assets/upload/profile"+profileimage;
		var profileimg = ""
		if(profileimage == "data: invalid file."){

		}else{

		}
		var userobj={
			projectid:projectid,
			fristname:fristname,
			lastname:lastname,
			position:position,
			email:email,
			phone:phone,
			profileimage:profileimage,
			Insdate:pastdatetime._created,
			active:1
		};
	//add teamwork tp project 
	const insertteam = await db.collection("users").insertOne(userobj);
	//get activity_id  
	var activityorder = "3.2";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });
	//get amount team 
	// const getamountteam = await db.collection("users").find({projectid:activityorder}).toArray();
	// console.log("get team in project: "+ getamountteam.length);

	return {data:"insert team success.",status:true};
	}
}

exports.getteamwork = async(projectid,userprimarykeyid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var teamwork={
		projectid:projectid,
		amountset:0,
		addedteam:0,
		team:[
			//fristname
			//lastname
			//position
			]
	}

	//get set amount team 
	const getsetamountteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var get_setamountteam = getsetamountteam[0].amountteam;
	if(get_setamountteam == ""){
		console.log("not set amount team.");
	}else{
		teamwork.amountset = getsetamountteam[0].amountteam;
	}
	

	//find user primary
	const userprimary = await db.collection("users").find({"_id":ObjectId(userprimarykeyid)}).toArray();
	var objuser_primary = {
		userid:userprimary[0]._id,
		fristname:userprimary[0].fristname,
		lastname:userprimary[0].lastname,
		position:userprimary[0].position,
		profileimage:userprimary[0].profileimage
	}
	teamwork.team.push(objuser_primary);
	const getteamwork = await db.collection("users").find({"projectid":projectid}).toArray();
	teamwork.addedteam = (getteamwork.length +1);//+1 is userpirmary
	for(var i = 0; i < getteamwork.length; i++){
		var objuser = {
			userid:getteamwork[i]._id,
			fristname:getteamwork[i].fristname,
			lastname:getteamwork[i].lastname,
			position:getteamwork[i].position,
			profileimage:getteamwork[i].profileimage
		}
		teamwork.team.push(objuser);
	}
	console.log("-------------getteamwork------------------");
	console.log(teamwork);
	console.log("--------------------------------");
	return teamwork;
}

exports.getteamwhereid = async(userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var typeuser = "";
	var password = "not password";
	//get type user 
	const gettypeuser = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	//get users
	const getdetailuser = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	if(gettypeuser.length == 0){
		typeuser = "normal user";
	}else{
		typeuser = "primary user";
		password = getdetailuser[0].password;
	}
	var datauser={
		userid:userid,
		detail:{
			fristname:getdetailuser[0].fristname,
			lastname:getdetailuser[0].lastname,
			position:getdetailuser[0].position,
			profileimage:getdetailuser[0].profileimage,
			email:getdetailuser[0].email,
			password:password,
			phone:getdetailuser[0].phone,
			typeuser:typeuser
		}
	}
	return datauser;
}

exports.updatedatauser = async(userid,fristname,lastname,position,profileimage,email,phone)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(profileimage == ""){
		const updateuser = await db.collection("users").updateOne(
							{"_id":ObjectId(userid)},
							{ $set: { fristname:fristname,
									  lastname:lastname,
									  position:position,
									  email:email,
									  phone:phone,
									  updatetime:pastdatetime._created
							}});
		return {data:"update user success.",userid:userid,status:true};
	}else{
		const updateuser = await db.collection("users").updateOne(
							{"_id":ObjectId(userid)},
							{ $set: { fristname:fristname,
									  lastname:lastname,
									  position:position,
									  profileimage:profileimage,
									  email:email,
									  phone:phone,
									  updatetime:pastdatetime._created
							}});
		return {data:"update user success.",userid:userid,status:true};
	}
}

exports.updatedocumentproject = async(projectid,documentname)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	const updatedoc = await db.collection("projects").updateOne(
								{"_id":ObjectId(projectid)},
								{ $set: {Documentation:documentname}});
	return {data:"update documentation success.",projectid:projectid,status:true};
}


exports.sendproject = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var status = 2 //send peoject is pending not update or edit project (bsm)
	const updateststusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{ $set: {
												 status:status,
												 senddatetime:pastdatetime._created
										}});
	//update status project in batchbusiinessman = 2 
	const updatebsm = await db.collection("batchbusinessman").updateOne(
								{"projectid":ObjectId(projectid)},
								{$set: {
										 updatetime:pastdatetime._created,
										 status:"2"//send and pending.
										}
								});

	//insert collection notification_admin 
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:projectid,
		planid:"",
		comment_rejectid:"",
		statusaction:2,
		statusstring:"ส่งเอกสารแล้ว",
		read:"",
		progress_groupplan_id:"",
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_admin = await db.collection("notification_admin").insertOne(obj_notification);

	//insert activity status
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var objlog={
		projectid:projectid,
		projectname:getdetailproject[0].projectname,
		userid:getdetailproject[0].Primaryresponsibility,
		status:3,
		statusstring:"ส่งเอกสารแล้ว",
		datetimestamp:pastdatetime._created
	}
	const insertlog =  await db.collection("logactivityproject").insertOne(objlog); 
	return {data:"sending project success.",projectid:projectid,statusproject:"sending",status:true}; 
}

//add duration(งวดงาน)
exports.addduration = async(projectid,durationorder,userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("duration index:"+durationorder.length);
	console.log(durationorder);
	var countdurationorder = 1;
	//insert duration
	for(var i = 0; i < durationorder.length; i++){
		const durationmonth = await db.collection("duration").insert({
									durationorder:countdurationorder, 
									month: durationorder[i].month,
									projectid:projectid
							  });
		console.log("durationorder: "+durationorder[i].durationorder);
		console.log("month: "+durationorder[i].month);
		console.log("-----------------------------");
		countdurationorder += 1;
	}
	
	//get activity_id  
	var activityorder = "2.1";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });  
	return {data:"insert duration success.",draft:"2.2",status:true};
} 

exports.testcountdate = async()=>{
	// 7 days in week 
	// 7 = sunday at this week 
	// 6 = saturday at this week
	//------------------------
	// -7 = sunday at last weeked.
	// -6 = monday at last weeked.
	// -1 = saturday at last weeked.

	// set default start Timesheet at maonday next week.
	var days = moment().day(7+1);
	console.log("days: "+ days);
	console.log("day: "+ days.format("dddd"));
	console.log("at date: "+ days.format("D"));
	console.log("month: "+days.format("MMMM"));
	console.log("year: "+ days.format("YYYY"));
	console.log("------------------------------");

	//get todays date
   var today = moment()
	//get birth date
   var birthday = moment(1993+"/"+6+"/"+8, 'YYYY-MM-DD');
   var result = moment.duration(8).months();
   console.log(today.diff(birthday, 'day'));
   console.log("-----------------------------");

   //add days 
   var date = moment().add(7, 'days');
   var datelocate = date.locale('th');
   console.log("add days: " + datelocate.format('DD/MM/YY'));

}


exports.getamount_duration_order = async(userid,projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objduration_order = {
		projectid:projectid,
		userid:userid,
		duration:[],
		planadded:0,
		amountplan:0
	}
	//get duration 
	const getduration = await db.collection("duration").find({projectid:projectid}).toArray();
	console.log("projectid: "+ projectid);
	console.log("userid: "+ userid);
	console.log("---------------------");
	console.log("duratoin");
	console.log(getduration);
	for(var i =0; i < getduration.length; i++){
		var durationid = getduration[i]._id;
		var durationnumber = getduration[i].durationorder;
		var month = getduration[i].month;
		objduration_order.duration.push({durationid:durationid,durationnumber:durationnumber,month:month});
	}

	console.log("---------------------");
	//get amount plan  : amountplan
	const getamountlan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	objduration_order.amountplan = getamountlan[0].amountplan
	console.log("get amount plan in project: "+ getamountlan[0].amountplan)
	//get plan 
	const getplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("-------------getplan---------------");
	console.log("plan: "+ getplan.length);
	console.log("-----------------------------------");
	var planadded = getplan.length;
	objduration_order.planadded = planadded;
	return objduration_order;
}

exports.getallduration = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	//get duration 
	const getallduration = await db.collection("duration").find({projectid:projectid}).toArray();
	console.log("-------getallduration------");
	console.log(getallduration);
	console.log("-------------------");
	return getallduration;
}

exports.updateduration = async(duration,projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	for(var i = 0; i < duration.length;i++){
		if(duration[i].durationid == ""){
			//insert duration 
			var objduration = {
				durationorder:duration[i].durationorder,
				month:duration[i].month,
				projectid:projectid
			};
			const insertduration =  await db.collection("duration").insertOne(objduration);
			console.log("insert duration: "+ i);

			console.log("----------------------------------------------");
		}else{
			//check duraion in db 
			const getduration = await db.collection("duration").find({"_id":ObjectId(duration[i].durationid)}).toArray();
			console.log(duration[i].durationid);
			var durationorder = duration[i].durationorder;
			var month = duration[i].month;
			// update duration
			const updateduration = await db.collection("duration").updateOne(
												{"_id":ObjectId(duration[i].durationid)},
												{ $set: { 
													durationorder:duration[i].durationorder,
													month:duration[i].month
												}});
			console.log("update duration: "+i);
			console.log("-----------------------------------------------");
		}
	}
	return {data:"update all duration success.",status:true};
}

exports.delete_user =async(userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	//check userid is primary user.
	const checkuserid = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	console.log("length: " + checkuserid.length);
	if(checkuserid.length == 0){
		var objuser = {"_id":ObjectId(userid)};
		const deleteuser = await db.collection("users").deleteOne(objuser);
		console.log("data: delete user success.");
		return {data:"delete user success.",status:true};
	}else{
		console.log("projectid: "+checkuserid[0]._id);
		console.log("data: this userid it's primary user.");
		return {data:"this userid it's primary user.",status:false};
	}
}

exports.delete_plan = async(planid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objplan = {"_id":ObjectId(planid)};
	const deleteplan = await db.collection("planorders").deleteOne(objplan);
	console.log("data:delete plan sucess.");
	return {data:"delete plan sucess.",status:true};
}

exports.getdetailproject = async(projectid,userid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objdetail_project = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		userprimary:"",
		documentation:[],
		plans:[],
		team:[],
		percentproject:0,
		statusproject:"",
		Fill_in:""
	};

	//get datadetail project 
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getdetail_project[0].status == 1){
		objdetail_project.statusproject = "กำลังดำเนินการ";
		objdetail_project.Fill_in = "กำลังกรอกข้อมูล";
	}else if(getdetail_project[0].status == 2){
		objdetail_project.statusproject = "รอดำเนินการ";
		objdetail_project.Fill_in = "ส่งเอกสารแล้ว";
	}else if(getdetail_project[0].status == 3){
		objdetail_project.statusproject = "ปรับแก้เอกสาร";
		objdetail_project.Fill_in = "ปรับแก้เอกสาร";
	}else if(getdetail_project[0].status == 4){
		objdetail_project.statusproject = "อนุมัติ Timesheet";
		objdetail_project.Fill_in = "อนุมัติ Timesheet";
	}

	//get userprimary 
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	var objuserprimary = {
		userid:getuserprimary[0]._id,
		fristname:getuserprimary[0].fristname,
		lastname:getuserprimary[0].lastname,
		position:getuserprimary[0].position,
		profileimage:getuserprimary[0].profileimage
	}
	objdetail_project.team.push(objuserprimary);

	console.log("------------------------------");

	console.log("projectid: "+ projectid);
	objdetail_project.projectid = projectid;

	console.log("projectname: "+ getdetail_project[0].projectname);
	objdetail_project.projectname = getdetail_project[0].projectname;

	console.log("projectvalue: "+ getdetail_project[0].projectvalue);
	objdetail_project.projectvalue = getdetail_project[0].projectvalue;

	console.log("userprimary: "+ getuserprimary[0].fristname + " " + getuserprimary[0].lastname);
	objdetail_project.userprimary = getuserprimary[0].fristname + " " + getuserprimary[0].lastname;

	// console.log("documentation: "+getdetail_project[0].Documentation);
	// objdetail_project.documentation = getdetail_project[0].Documentation;
	const getallfile = await db.collection("fileproject").find({projectid:projectid}).toArray();
	 for(var count = 0;count < getallfile.length;count++){
		  var obj={
		   	filename:getallfile[count].filename
		  }
		  objdetail_project.documentation.push(obj);
	 }


	//get plan in project 
	const getplaninproject = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var i = 0; i < getplaninproject.length; i++){
		const getdurationorders = await db.collection("duration").find({"_id":ObjectId(getplaninproject[i].durationid)}).toArray();
		var durationorder = getdurationorders[0].durationorder;
		var objplan = {
			planid:getplaninproject[i]._id,
			planname:getplaninproject[i].planname,
			duration:durationorder,
			jobvalue:getplaninproject[i].jobvalue,
			timelength:getplaninproject[i].timelength
		}
		objdetail_project.plans.push(objplan);
		console.log("plans: ");
		console.log(objplan);
	}

	//documentation 
	var partdocument =0;
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	const getalldocumetation = await db.collection("fileproject").find({projectid:projectid}).toArray();
	if (getalldocumetation.length == 0){
		partdocument = 0;
	}else{
		partdocument = 100;
	}
	console.log("percent part 1: "+partdocument);
	var percent1 = partdocument;
	console.log("---------------------------------------");

	//plan order
	console.log("amount plan: "+ getdocumetation[0].amountplan);
	var amountplan = getdocumetation[0].amountplan;
	const getallplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	var plans = getallplanorder.length;
	console.log("plans: "+plans);
	var percentplan = 100/amountplan;
	var calculate_percent_plan = percentplan * plans;
	console.log("percent part 2: "+calculate_percent_plan);
	var percent2 = calculate_percent_plan;
	console.log("---------------------------------------");

	//team 
	const get_amount_team = await  db.collection("users").find({projectid:projectid}).toArray();
	var amountusers = get_amount_team.length;//team_added
	var userprimary = 1;
	console.log("amount team: "+ (amountusers+userprimary));
	var allusers = (amountusers+userprimary);
	const getamount_setteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var setamount = getamount_setteam[0].amountteam;
	var percentteam = 100/setamount;
	console.log("percent amount users: "+ percentteam);
	var percent3 = (percentteam * allusers);
	console.log("percent part 3:"+ percentteam * allusers);
	console.log("---------------------------------------");

	//percent1to3
	objdetail_project.percentproject = ((percent1 + percent2 + percent3) * 100) /300;

	//get team 
	const get_team = await db.collection("users").find({projectid:projectid}).toArray();
	if(get_team.length == 0){
		console.log("team: "+ objdetail_project.team[0].fristname + " " +objdetail_project.team[0].lastname);
		console.log("-----------------------------");
		return objdetail_project;
	}else{
		console.log("team: ");
		for(var i = 0; i < get_team.length; i++){
			var objuser ={
				userid:get_team[i]._id,
				fristname:get_team[i].fristname,
				lastname:get_team[i].lastname,
				position:get_team[i].position,
				profileimage:get_team[i].profileimage
			}
			console.log(objuser);
			objdetail_project.team.push(objuser);
		}
		console.log("-----------------------------");
		return objdetail_project;
	}
}

exports.updatepassword = async(userid,token)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const updatepassword = await db.collection("users").updateOne(
												{"_id":ObjectId(userid)},
												{ $set:{password:token}});
	return {data:"update password success.",status:true};
}

exports.getamountteam =async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	
	var objamountteam = {
		amountset:0,
		amountadded:0
	}
	//get amount 
	const getamountteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	objamountteam.amountset = getamountteam[0].amountteam;
	//get amount team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	objamountteam.amountadded = (getteamadded.length +1);
	console.log("-----------getamountteam-------------");
	console.log(objamountteam);
	return objamountteam;
}

exports.updateamountteam = async(projectid,amount) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	//update amount team 
	const updateamountteam = await db.collection("projects").updateOne(
												{"_id":ObjectId(projectid)},
												{ $set: {amountteam:amount}});
	return {data:"update success.",status:true};
}

exports.checklastdraft = async(userid,projectid) =>{
	console.log("-----------Check last draft----------");
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);

	//check part 1 : upload document 
	const checkdocument = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkdocument[0].Documentation == ""){
		console.log("draft: last activity upload documentation.");
		var respond = {data:"last activity upload documentation.",activity:"1",activityname:"เอกสารประกอบโครงการ"};
		return respond;
	}else{
		console.log("draft: activity upload documentation success.");
	}

	//check part 2 : create project
	const checkcreateproject = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	if(checkcreateproject[0].projectname == "" || checkcreateproject[0].projectvalue == ""){
		console.log("draft: last activity create project.");
		var respond = {data:"last activity create project.",activity:"2",activityname:"แผนการใช้งบประมาณ"};
		return respond;
	}else{
		console.log("draft: activity create project success.");
	}

	//check part 2.1 : add amount duration
	const checkamountduration = await db.collection("duration").find({projectid:projectid}).toArray();
	if(checkamountduration.length == 0){
		console.log("draft: last activity create duration.");
		var respond = {data:"last activity create duration.",activity:"2.1",activityname:"สร้างงวดงาน"};
		return respond;
	}else{
		console.log("draft: activity create duration success.");
	}

	//check part 2.2 create amount plan 
	const checkamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkamountplan[0].amountplan == 0){
		console.log("draft: last activity create amount plan.");
		var respond = {data:"last activity create amount plan.",activity:"2.2",activityname:"สร้างจำนวนแผนงาน"};
		return respond;
	}else{
		console.log("draft: activity create amount plan success.");
	}

	//check part 2.3 create plan 
	const checkplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var planadded = checkplanorder.length;
	var amountplan = getamountplan[0].amountplan;
	if(planadded < amountplan){
		console.log("draft: last activity create plan order.");
		var respond = {data:"last activity create plan order.",activity:"2.3",activityname:"สร้างแผนงาน"};
		return respond;
	}else{
		console.log("draft: activity create plan order success.");
	}

	//check part 3.1 create amount team
	const checkamountteam =  await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkamountteam[0].amountteam == 0){
		console.log("draft: last activity create amount team.");
		var respond = {data:"last activity create amount team.",activity:"3.1",activityname:"เพิ่มจำนวนคนทำงาน"};
		return respond;
	}else{
		console.log("draft: activity create amount team success.");
	}

	//check part 3.2 create normal users 
	var amountteam = checkamountteam[0].amountteam;
	const checkteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if((checkteamadded.length + 1) < amountteam){
		console.log("draft: last activity create normal users.");
		console.log("users added: " + (checkteamadded.length + 1));
		var respond = {data:"last activity create normal users.",activity:"3.2",activityname:"เพิ่มข้อมูลรายละเอียดสมาชิก"};
		return respond;
	}else{
		console.log("draft: activity create normal success.");
		var respond = {data:"activity create normal success.",activity:"4",activityname:"สรุปและส่งข้อมูลโครงการ"};
		return respond;
	}
}	

exports.getplandashboard = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objproject = {
		projectid:"",
		projectname:"",
		statusproject:"",
		projectvalue:0,
		valueused:0,
		fristname:"",
		lastname:"",
		week:0,
		day:0,
		planorders:[]
	}
	const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	const getuserdetail =  await db.collection("users").find({"_id":ObjectId(getprojectdetail[0].Primaryresponsibility)}).toArray();
	objproject.projectid = projectid;
	objproject.projectname = getprojectdetail[0].projectname;
	objproject.statusproject = getprojectdetail[0].status;
	objproject.projectvalue = getprojectdetail[0].projectvalue;
	objproject.fristname = getuserdetail[0].fristname;
	objproject.lastname = getuserdetail[0].lastname;
	var datenow = moment().format("D/MMMM/YYYY");
	console.log("this day: "+datenow);
	const check_date_on_week = await db.collection("days").find({datestring:datenow}).toArray();
	if(check_date_on_week.length == 0){
		console.log("no have day on week.");
		return {data:"no have day on week.",status:false};
	}else{
		console.log("have day on week.");
		//get plan on this week.
		var weekid = check_date_on_week[0].weekid;
		const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
		var weeknumber = getweeknumber[0].weeknumber;
		objproject.week = weeknumber;
		objproject.day = check_date_on_week[0].daynumber;
		const getplanonweek = await db.collection("weeksplanorder").find({weekid:weekid}).toArray();
		for(var i = 0; i < getplanonweek.length;i++){
			var planid = getplanonweek[i].planid;
			const getplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
			var durationid = getplan[0].durationid;
			const getduration = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
			var objplan = {
				planid:planid,
				planname:getplan[0].planname,
				planvalue:getplan[0].jobvalue,
				valueused:0,
				duration:getduration[0].durationorder,
				timelength:getplan[0].timelength,
				timeused:0
			}
			objproject.planorders.push(objplan);
		}
		return objproject;
	}

}

//weekly progress
exports.getallweeksofplan = async(projectid,planid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = moment().format("D/MMMM/YYYY");
	var datestring_now =  moment().format("dddd");
	console.log("this day: "+datenow);
	console.log("projectid: "+projectid);
	console.log("planid: " + planid);
	var obj_weekly = {
		projectid:projectid,
		projectname:"",
		projectvalue:0,
		totalvalue:0,
		weekid:"",
		weeknumber:0,
		dayid:"",
		daynumber:0,
		weeks:[],
		status:true
	}

	//get project value 
	const getprojectvalue = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var projectvalue = getprojectvalue[0].projectvalue;
	obj_weekly.projectvalue = projectvalue;
	obj_weekly.projectname = getprojectvalue[0].projectname;

	//Sum total value timsheet from timesheet => totalvalueonday
	var totalvalue = 0;
	const get_total_value_timesheet = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var project = 0; project < get_total_value_timesheet.length;project++){
		totalvalue += get_total_value_timesheet[project].totalvalueonday;
	}
	obj_weekly.totalvalue = totalvalue;

	// codition sunday 
	if(datestring_now == "Sunday"){
		var newdatenow = moment().add(-1,'days').format("D/MMMM/YYYY");
		//find days on weeks 
		const getdayonweek = await db.collection("days").find({datestring:newdatenow},{projectid:projectid}).toArray();
		console.log("datenow: " + datenow);
		console.log("get day on week: " +getdayonweek[0].weekid);
		if(getdayonweek.length == 0){
			console.log("data: no have this day on week.");
			return {data:"no have this day on week.",status:false};	
		}else{
			//find week of plan 
			const getweekofplan = await db.collection("weeks").find({planid:ObjectId(planid)}).toArray();
			if(getweekofplan.length == 0){
				return obj_weekly;
			}else{
				for(var week = 0;week < getweekofplan.length; week++){
					console.log(getweekofplan[week]);
					obj_weekly.weeks.push(getweekofplan[week]);
				}

				console.log("weekid: "+getdayonweek[0].weekid);	
				obj_weekly.weekid = getdayonweek[0].weekid;
				//find detail week.
				const getweekdetail = await db.collection("weeks").find({"_id":ObjectId(getdayonweek[0].weekid)}).toArray();
				//$and:[{"_id":ObjectId(getdayonweek[0].weekid)},{projectid:projectid}]
				obj_weekly.weeknumber = getweekdetail[0].weeknumber;
				obj_weekly.dayid = getdayonweek[0]._id;
				obj_weekly.daynumber = 7;//getdayonweek[0].daynumber;
				console.log("week number: "+ obj_weekly.weeknumber);
				//console.log("day id: "+obj_weekly.dayid);
				console.log("day number" + obj_weekly.daynumber);
				
				return obj_weekly;
			}
			
		}
	}else{
		//find days on weeks 
		const getdayonweek = await db.collection("days").find({$and:[{datestring:datenow},{projectid:projectid}]}).toArray();
		console.log("datenow: " + datenow);
		if(getdayonweek.length == 0){
			console.log("data: no have this day on week.");
			return {data:"no have this day on week.",status:false};	
		}else{
			//find week of plan 
			console.log("get day on week: " +getdayonweek[0].weekid);
			const getweekofplan = await db.collection("weeks").find({planid:ObjectId(planid)}).toArray();
			for(var week = 0;week < getweekofplan.length; week++){
				console.log(getweekofplan[week]);
				obj_weekly.weeks.push(getweekofplan[week]);
			}

			console.log("weekid: "+getdayonweek[0].weekid);	
			obj_weekly.weekid = getdayonweek[0].weekid;
			//find detail week.
			const getweekdetail = await db.collection("weeks").find({"_id":ObjectId(getdayonweek[0].weekid)}).toArray();
			//$and:[{"_id":ObjectId(getdayonweek[0].weekid)},{projectid:projectid}]
			obj_weekly.weeknumber = getweekdetail[0].weeknumber;
			obj_weekly.dayid = getdayonweek[0]._id;
			obj_weekly.daynumber = getdayonweek[0].daynumber;
			console.log("week number: "+ obj_weekly.weeknumber);
			console.log("day id: "+obj_weekly.dayid);
			console.log("day number" + obj_weekly.daynumber);
			
			return obj_weekly;
		}
	}
}

//Daily progress
exports.dailyprogress = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = moment().format("D/MMMM/YYYY");
	console.log("this day: "+datenow);
	var obj_project={
		projectid:projectid,
		projectname:"",
		projectvalue:0,
		valueused:0,
		weekid:"",
		weeknumber:0,
		dayid:"",
		daynumber:0,
		plans:[]
	}

	//Sum total use value
	var sumtotal =0;
	const sumusedvalue = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var i =0; i < sumusedvalue.length; i++){
		sumtotal += sumusedvalue[i].totalvalueonday;
	}
	obj_project.valueused = sumtotal;
	
	//getdetail project
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	obj_project.projectname = getdetailproject[0].projectname;
	obj_project.projectvalue = getdetailproject[0].projectvalue;

	//get weekid , dayid
	const getday_detail = await db.collection("days").find({datestring:datenow}).toArray();
	obj_project.dayid = getday_detail[0]._id;
	obj_project.daynumber = getday_detail[0].daynumber;
	const getweekdetail = await db.collection("weeks").find({$and:[{projectid:projectid},{"_id":ObjectId(getday_detail[0].weekid)}]}).toArray();
	obj_project.weekid = getweekdetail[0]._id;
	obj_project.weeknumber = getweekdetail[0].weeknumber;

	//get all plans in project
	const getallplans = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var plan = 0;plan < getallplans.length; plan++){
		var obj_plan ={
			planid:"",
			planname:"",
			duraionid:"",
			durationorder:"",
			jobvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			status_timesheet:false
		}
		const check_planonweek = await db.collection("weeks").find({$and:[{planid:ObjectId(getallplans[plan]._id)},{weeknumber:getweekdetail[0].weeknumber}]}).toArray();

		if(check_planonweek.length > 0){
			obj_plan.status_timesheet = true;
		}else{
			obj_plan.status_timesheet = false;
		}

		obj_plan.planid = getallplans[plan]._id;
		obj_plan.planname = getallplans[plan].planname;
		obj_plan.durationid = getallplans[plan].durationid;
		obj_plan.jobvalue = getallplans[plan].jobvalue;
		obj_plan.timelength = getallplans[plan].timelength;

		//Sum total value used on plan 
		var TotalValueUsedOnplan = 0;
		var TotalTimeUsed = 0;
		const getvalue_used_onplan = await db.collection("timesheet").find({planid:getallplans[plan]._id}).toArray();
		for(var value = 0; value < getvalue_used_onplan.length; value++){
			TotalValueUsedOnplan += getvalue_used_onplan[value].totalvalueonday;
			TotalTimeUsed +=getvalue_used_onplan[value].hours;
		}
		obj_plan.valueused = TotalValueUsedOnplan;
	
		//Sum total timeused
		obj_plan.timeused = TotalTimeUsed / 8;


		//get duration order 
		const getduration = await db.collection("duration").find({"_id":ObjectId(getallplans[plan].durationid)}).toArray();
		obj_plan.durationorder = getallplans[plan].durationorder;
		obj_project.plans.push(obj_plan);
	}
	console.log(obj_project);
	return obj_project;
}

exports.getweektimesheet = async(dayid,projectid,planid,weeknumber) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var Int_weeknumber = parseInt(weeknumber);
	var getweekid = await db.collection("weeks").find({$and:[{weeknumber:Int_weeknumber},{planid:ObjectId(planid)}]}).toArray();
	console.log("weekid: " + getweekid[0]._id);
	var weekid = getweekid[0]._id.toString();
	
	var obj_thisweek = {
		projectid:projectid,
		planid:planid,
		planname:"",
		jobvalue:0,
		usedvalue:0,
		timelength:0,
		timeused:0,
		weekid:weekid,
		weeknumber:0,
		dayid:dayid,
		daynumber:0,
		team:[],
		day1:[],
		day1_id:"",
		statusday1:false,
		day2:[],
		day2_id:"",
		statusday2:false,
		day3:[],
		day3_id:"",
		statusday3:false,
		day4:[],
		day4_id:"",
		statusday4:false,
		day5:[],
		day5_id:"",
		statusday5:false,
		day6:[],
		day6_id:"",
		statusday6:false
	}


	//get day number 
	const getdetailday = await db.collection("days").find({"_id":ObjectId(dayid)}).toArray();
	obj_thisweek.daynumber = getdetailday[0].daynumber;

	//get detail plan
	const getdetailplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	obj_thisweek.planname = getdetailplan[0].planname;
	obj_thisweek.jobvalue = getdetailplan[0].jobvalue;
	obj_thisweek.timelength = getdetailplan[0].timelength;

	//get sum timeused
	var sumtotal = 0;
	var sumhours = 0;
	const gettimeused = await db.collection("timesheet").find({planid:planid}).toArray();
	for(var day =0; day < gettimeused.length; day++){
		sumtotal += gettimeused[day].totalvalue;
		sumhours += gettimeused[day].hours;
	}
	obj_thisweek.usedvalue = sumtotal;
	obj_thisweek.timeused = sumhours;

	//get week detail 
	const getweekdetail = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
	obj_thisweek.weeknumber = getweekdetail[0].weeknumber;

	//get team (primary user)
	const getteam_primary_user = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	obj_primaryuser ={
		userid:getteam_primary_user[0].Primaryresponsibility,
		username:"",
		profileimage:""
	}
	const getusername_primaryuser = await db.collection("users").find({"_id":ObjectId(getteam_primary_user[0].Primaryresponsibility)}).toArray();
	obj_primaryuser.username = getusername_primaryuser[0].fristname+" "+getusername_primaryuser[0].lastname;
	obj_primaryuser.profileimage = getusername_primaryuser[0].profileimage;
	obj_thisweek.team.push(obj_primaryuser);

	//get user in team 
	const getallteam = await db.collection("users").find({projectid:projectid}).toArray();
	for(var user =0;user < getallteam.length;user++){
		obj_user = {
			userid:getallteam[user]._id,
			username:getallteam[user].fristname +" "+getallteam[user].lastname,
			profileimage:getallteam[user].profileimage
		}
		obj_thisweek.team.push(obj_user);
	}

	//set on this day dtms get open and close day.
	var onthisday_dtms = 0;//moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]);
	const get_onthisday = await db.collection("days").find({"_id":ObjectId(dayid)}).toArray();
	onthisday_dtms = moment(get_onthisday[0].datetimestamp._d,["YYYY-MM-DD"]).add(1, 'days');
	//moment(startdate).add(days, 'days');
	console.log("on this day: " + onthisday_dtms);
	console.log("on this day string: "+onthisday_dtms.format("YYYY-MM-DD"));
	console.log("---------------------------");

	//get days on weeks
	var sort = {daynumber:1};
	const getdays_onweek = await db.collection("days").find({weekid:ObjectId(weekid)}).sort(sort).toArray();
	console.log("weekid: "+obj_thisweek.weekid);
	console.log("week number: "+obj_thisweek.weeknumber);
	for(var days = 0;days < getdays_onweek.length;days++){
		console.log("dayid: " + getdays_onweek[days]._id);
		console.log("daynumber: " + getdays_onweek[days].daynumber);
		console.log("datetimestamp: "+ moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]));
		console.log("on this day: "+onthisday_dtms.format("YYYY-MM-DD"));
		var convert_getdate_dtsm = moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]);
		console.log("convert_getdate_dtsm: "+convert_getdate_dtsm.format("YYYY-MM-DD"));
		
		if(getdays_onweek[days].daynumber == 1){
			obj_thisweek.day1_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday1 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday1 = false;
				console.log("open day: false");
			}
		}else if(getdays_onweek[days].daynumber == 2){
			obj_thisweek.day2_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday2 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday2 = false;
				console.log("open day: false");
			}
		}else if(getdays_onweek[days].daynumber == 3){
			obj_thisweek.day3_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday3 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday3 = false;
				console.log("open day: false");
			}
		}else if(getdays_onweek[days].daynumber == 4){
			obj_thisweek.day4_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday4 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday4 = false;
				console.log("open day: fasle");
			}
		}else if(getdays_onweek[days].daynumber == 5){
			obj_thisweek.day5_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday5 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday5 = false;
				console.log("open day: false");
			}
		}else if(getdays_onweek[days].daynumber == 6){
			obj_thisweek.day6_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm < onthisday_dtms){
				obj_thisweek.statusday6 = true;
				console.log("open day: true");
			}else{
				obj_thisweek.statusday6 = false;
				console.log("open day: fasle");
			}
		}else{

		}

		console.log("count team: " + obj_thisweek.team.length);
		console.log("---------------------------");
		for(var i = 0;i < obj_thisweek.team.length;i++){
			var dayid_find = getdays_onweek[days]._id.toString();
			var userid_find = obj_thisweek.team[i].userid.toString();
			console.log("dayid: " + dayid_find);
			console.log("userid: "+ userid_find);
			const gettimesheet = await db.collection("timesheet").find({$and:[{userid:userid_find},{dayid:dayid_find}]}).toArray();
			console.log("gettimesheet.length: " + gettimesheet.length);
			if(gettimesheet.length == 0){
				console.log("hours: " + 0);
				var obj_user_timesheet_onday = {
					userid:userid_find,
					dayid:dayid_find,
					hours:0
				}
				if(getdays_onweek[days].daynumber == 1){
					obj_thisweek.day1.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 2){
					obj_thisweek.day2.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 3){
					obj_thisweek.day3.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 4){
					obj_thisweek.day4.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 5){
					obj_thisweek.day5.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 6){
					obj_thisweek.day6.push(obj_user_timesheet_onday);
				}
			}else{
				console.log("hours: " + gettimesheet[0].hours);	
				var obj_user_timesheet_onday = {
					userid:userid_find,
					dayid:dayid_find,
					hours:gettimesheet[0].hours
				}
				if(getdays_onweek[days].daynumber == 1){
					obj_thisweek.day1.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 2){
					obj_thisweek.day2.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 3){
					obj_thisweek.day3.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 4){
					obj_thisweek.day4.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 5){
					obj_thisweek.day5.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 6){
					obj_thisweek.day6.push(obj_user_timesheet_onday);
				}
			}
			console.log("---------------------------");
		}
		
	}// for days
	console.log(obj_thisweek);
	return obj_thisweek;
}

exports.addtimesheet = async(projectid,planid,weekid,day1,day2,day3,day4,day5,day6,day1_id,day2_id,day3_id,day4_id,day5_id,day6_id) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("day1: " + day1.length);
	console.log("day2: " + day2.length);
	console.log("day3: " + day3.length);
	console.log("day4: " + day4.length);
	console.log("day5: " + day5.length);
	console.log("day6: " + day6.length);
	console.log("------------------------------------");
	//get average value on day.
	const getaverage = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	var cal_average = getaverage[0].averagewages;
	console.log("cal_average: "+cal_average);

	//day1
	console.log("-------------day1--------------");
	for(var d1 =0; d1 < day1.length;d1++){
		//console.log(day1[d1]);
		var obj_day_timesheet = {
			userid:day1[d1].userid,
			dayid:day1[d1].dayid,
			planid:planid,
			projectid:projectid,
			hours:day1[d1].hours,
			totalvalue:(parseInt(cal_average) / 8) * day1[d1].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);

		const checkday = await db.collection("timesheet").find({$and:[{dayid:day1_id},{userid:day1[d1].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day1[d1].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day2
	console.log("-------------day2--------------");
	for(var d2 =0; d2 < day2.length;d2++){
		//console.log(day2[d2]);
		var obj_day_timesheet = {
			userid:day2[d2].userid,
			dayid:day2[d2].dayid,
			planid:planid,
			projectid:projectid,
			hours:day2[d2].hours,
			totalvalue:(parseInt(cal_average) / 8) * day2[d2].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day2_id},{userid:day2[d2].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day2[d2].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}	
	}
	
	//day3
	console.log("-------------day3--------------");
	for(var d3 =0; d3 < day3.length;d3++){
		//console.log(day3[d3]);
		var obj_day_timesheet = {
			userid:day3[d3].userid,
			dayid:day3[d3].dayid,
			planid:planid,
			projectid:projectid,
			hours:day3[d3].hours,
			totalvalue:(parseInt(cal_average) / 8) * day3[d3].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day3_id},{userid:day3[d3].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day3[d3].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}	
	}
	
	//day4
	console.log("-------------day4--------------");
	for(var d4 =0; d4 < day4.length;d4++){
		//console.log(day4[d4]);
		var obj_day_timesheet = {
			userid:day4[d4].userid,
			dayid:day4[d4].dayid,
			planid:planid,
			projectid:projectid,
			hours:day4[d4].hours,
			totalvalue:(parseInt(cal_average) / 8) * day4[d4].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day4_id},{userid:day4[d4].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day4[d4].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day5
	console.log("-------------day5--------------");
	for(var d5 =0; d5 < day5.length;d5++){
		//console.log(day5[d5]);
		var obj_day_timesheet = {
			userid:day5[d5].userid,
			dayid:day5[d5].dayid,
			planid:planid,
			projectid:projectid,
			hours:day5[d5].hours,
			totalvalue:(parseInt(cal_average) / 8) * day5[d5].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day5_id},{userid:day5[d5].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day5[d5].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day6
	console.log("-------------day6--------------");
	for(var d6 =0; d6 < day6.length;d6++){
		//console.log(day6[d6]);
		var obj_day_timesheet = {
			userid:day6[d6].userid,
			dayid:day6[d6].dayid,
			planid:planid,
			projectid:projectid,
			hours:day6[d6].hours,
			totalvalue:(parseInt(cal_average) / 8) * day6[d6].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day6_id},{userid:day6[d6].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day6[d6].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	return {data:"add timesheet success.",status:true};
	
}

exports.getbatchname = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = moment();
	console.log("batchid: " + batchid);
	const getbatchname = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	console.log(getbatchname[0]);
	var duedate = getbatchname[0].duedate;//moment().format('DD/MMMM/YYYY');
	console.log("duedate: "+ duedate);
	console.log("datenow: "+ datenow);
	var statusDueDate = false;
	if(duedate >= datenow){
		statusDueDate = true;
	}else{
		statusDueDate = false;
	}
	var objbatch = {
		batchname:getbatchname[0].modelname,
		duedate:statusDueDate
	};
	return objbatch;
}

exports.getdetail_progress_week = async(projectid,weeknumber)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = moment().format('D/MMMM/YYYY');
	//var pastdatetime = nodedatetime.create(datenow);
	//get detail project 
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	//sum total value used
	var totalproject_valueused = 0;
	const get_total_valueused = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var counttotal = 0;counttotal < get_total_valueused.length;counttotal++){
		totalproject_valueused+=get_total_valueused[counttotal].totalvalue;
	}

	var obj_progress_week = {
		projectid:projectid,
		projectname:getdetailproject[0].projectname,
		projectvalue:getdetailproject[0].projectvalue,
		usedvalueproject:totalproject_valueused,
		weeknumber:weeknumber,
		weeknow:0,
		plans:[]
	}

	//get week now 
	console.log("datenow: "+datenow);
	console.log("-----------------------------");
	const get_daynow = await db.collection("days").find({$and:[{projectid:projectid},{datestring:datenow}]}).toArray();
	const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(get_daynow[0].weekid)}).toArray();
	obj_progress_week.weeknow = getweeknumber[0].weeknumber;
	//get plan on week 
	const getplan_onweek = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	console.log("getplan_onweek: "+getplan_onweek.length);
	for(var plan = 0;plan < getplan_onweek.length;plan++){
		console.log("planid: " + getplan_onweek[plan].planid);

		var obj_plan = {
			planid:getplan_onweek[plan].planid,
			planname:"",
			durationid:"",
			durationorder:"",
			jobvalue:0,
			usevalue:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			statusplan:""
		} 

		//get plan detail 
		const getplan_detail  = await db.collection("planorders").find({"_id":ObjectId(getplan_onweek[plan].planid)}).toArray(); 
		
		console.log("planname: " + getplan_detail[0].planname);
		obj_plan.planname = getplan_detail[0].planname;

		console.log("durationid: " + getplan_detail[0].durationid);
		obj_plan.durationid = getplan_detail[0].durationid;

		console.log("durationorder: " + getplan_detail[0].durationorder);
		obj_plan.durationorder = getplan_detail[0].durationorder;

		console.log("jobvalue: " + getplan_detail[0].moneydifference);
		obj_plan.jobvalue = getplan_detail[0].moneydifference;

		var sum_usedvalue = 0;
		var sum_timeused = 0;
		var planid_find = getplan_onweek[plan].planid.toString();
		const get_sumvaluesuse = await db.collection("timesheet").find({planid:planid_find}).toArray();
		for(var i = 0; i < get_sumvaluesuse.length; i++){
			sum_usedvalue += get_sumvaluesuse[i].totalvalue;
			sum_timeused += get_sumvaluesuse[i].hours;
		}
		console.log("usevalue: " + sum_usedvalue);
		obj_plan.usevalue = sum_usedvalue;

		console.log("timelength: " + getplan_detail[0].timelength)
		obj_plan.timelength = getplan_detail[0].timelength;

		console.log("timeused: " + (sum_timeused/8));
		obj_plan.timeused =sum_timeused / 8;

		
		//get primary user 
		var obj_userprimary = {
			userid:"",
			username:"",
			hours:0,
			comment:""
		}
		const get_userprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid_primary = get_userprimary[0].Primaryresponsibility.toString();
		const get_detail_user_primary = await db.collection("users").find({"_id":ObjectId(userid_primary)}).toArray();
		console.log("username: " + get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname);
		obj_userprimary.username = get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname;
		obj_userprimary.userid = userid_primary;
		//Sum hours
		var sum_totaluser_hour = 0;
		const get_sum_hours = await db.collection("timesheet").find({$and:[{planid:planid_find},{userid:userid_primary}]}).toArray();
		for(var a =0;a < get_sum_hours.length;a++){
			sum_totaluser_hour += get_sum_hours[a].hours;
		}
		console.log("total hours: "+ sum_totaluser_hour);
		obj_userprimary.hours = sum_totaluser_hour;
		obj_plan.team.push(obj_userprimary);
		console.log("-------------------------------------");


		//get all user 
		const getalluser_onproject = await db.collection("users").find({projectid:projectid}).toArray();
		console.log("getalluser_onproject.length: " +getalluser_onproject.length);
		for(var user =0; user < getalluser_onproject.length;user++){
			var obj_user= {
				userid:"",
				username:"",
				hours:0,
				comment:""
			}
			console.log("userid: "+ getalluser_onproject[user]._id);
			var userid = getalluser_onproject[user]._id.toString();
			console.log("username: " + getalluser_onproject[user].fristname +" "+ getalluser_onproject[user].lastname);
			obj_user.userid = getalluser_onproject[user]._id;
			obj_user.username = getalluser_onproject[user].fristname +" "+ getalluser_onproject[user].lastname;
			//Sum hours
			var sum_totaluser = 0;
			const get_sum_hours = await db.collection("timesheet").find({$and:[{planid:planid_find},{userid:userid}]}).toArray();
			for(var j =0;j < get_sum_hours.length;j++){
				sum_totaluser += get_sum_hours[j].hours;
			}
			console.log("total hours: "+ sum_totaluser);
			obj_user.hours = sum_totaluser;
			obj_plan.team.push(obj_user);
			console.log("-------------------------------------");
		}
		console.log("-----------------------------------");	
		obj_progress_week.plans.push(obj_plan);
	}
	console.log(obj_progress_week);
	return obj_progress_week;
}


exports.get_timesheet_allusers = async(projectid,weeknumber) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_progress_timesheet_user = {
		projectid:projectid,
		weeknumber:weeknumber,
		team:[]
	}
	var weekid = "";

	//get weekid 
	const getweekid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	weekid = getweekid[0]._id;
	console.log("getweekid[0]._id"+getweekid[0]._id);
	//console.log("getweekid[1]._id"+getweekid[1]._id);
	//get userprimary 
	var obj_userprimary = {
		userid:"",
		username:"",
		totalhours:0,
		plans:[]
	}
	const getuserprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var userprimary_id = getuserprimary[0].Primaryresponsibility.toString();

	const get_detail_user_primary = await db.collection("users").find({"_id":ObjectId(userprimary_id)}).toArray();
	console.log("userid: " + userprimary_id);
	console.log("username: " + get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname);
	obj_userprimary.userid = userprimary_id;
	obj_userprimary.username = get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname;


	//get plan on week 
	var totalhours_on_week = 0;
	const get_plan = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var plan = 0; plan < get_plan.length; plan++){
		var obj_plan={
			planid: get_plan[plan]._id,
			planname:get_plan[plan].planname,
			durationid:"",
			durationorder:0,
			totalhours:0,
			valueused:0
		}
		console.log("planid: " + get_plan[plan]._id);
		var planid_find = get_plan[plan]._id.toString();
		const getplan_onweek = await db.collection("weeks").find({$and:[{planid:ObjectId(planid_find)},{weeknumber:weeknumber}]}).toArray();
		if(getplan_onweek.length == 0){
			console.log("getplan_onweek.length: "+getplan_onweek.length);
			console.log("----------------------------------------------");
		}else{

			console.log("getplan_onweek.length: "+getplan_onweek.length);
			console.log("durationid: "+getplan_onweek[0].durationid);
			obj_plan.durationid = getplan_onweek[0].durationid;
			const get_durationorder = await db.collection("duration").find({"_id":ObjectId(getplan_onweek[0].durationid)}).toArray();
			console.log("durationorder: " + get_durationorder[0].durationorder);
			obj_plan.durationorder = get_durationorder[0].durationorder;

			var sumtotal_hours = 0;
			var sumtotal_value = 0;
			const get_day_onplan = await db.collection("days").find({weekid:ObjectId(getplan_onweek[0]._id)}).toArray();
			for(var days = 0; days < get_day_onplan.length;days++){
				const get_hours = await db.collection("timesheet").find({dayid:get_day_onplan[days]._id.toString()}).toArray();
				if(get_hours.length == 0){
					console.log("dayid "+ days+": "+get_day_onplan[days]._id+" Hours: "+0);
				}else{
					console.log("dayid "+ days+": "+get_day_onplan[days]._id+" Hours: " + get_hours[0].hours);
					sumtotal_hours += get_hours[0].hours;
					sumtotal_value += get_hours[0].totalvalue;
					totalhours_on_week += get_hours[0].hours;
				}
			}


			console.log("totalhours: " + sumtotal_hours);
			obj_plan.totalhours = sumtotal_hours;

			//get average plan manday
			const get_average = await db.collection("planorders").find({"_id":ObjectId(get_plan[plan]._id)}).toArray();
			var average = get_average[0].averagewages;
			obj_plan.valueused = sumtotal_value;
			console.log("----------------------------------------------");
			obj_userprimary.plans.push(obj_plan);
		}
	}//for plan	
	obj_userprimary.totalhours = totalhours_on_week;
	obj_progress_timesheet_user.team.push(obj_userprimary);
	
	//get all user 
	console.log("----------------get users---------------------");
	const getallusers = await db.collection("users").find({projectid:projectid}).toArray();
	for(var users = 0; users < getallusers.length;users++){
		var total_hours_on_weekusers = 0;
		var obj_user = {
			userid:"",
			username:"",
			totalhours:0,
			plans:[]
		}
		console.log("userid: " + getallusers[users]._id);
		console.log("username: " + getallusers[users].fristname +" "+ getallusers[users].lastname);
		obj_user.userid = getallusers[users]._id;
		obj_user.username = getallusers[users].fristname +" "+ getallusers[users].lastname;
		const getplan_onweek = await db.collection("weeks").find({$and:[{projectid:projectid},{weeknumber:weeknumber}]}).toArray();
		console.log("getplan_onweek.length: " + getplan_onweek.length);
		for(var countplan=0;countplan < getplan_onweek.length;countplan++){
			
			//get plan detail 
			const get_detail_plan = await db.collection("planorders").find({"_id":ObjectId(getplan_onweek[countplan].planid)}).toArray();
			var obj_plan={
				planid: getplan_onweek[countplan].planid,
				planname:get_detail_plan[0].planname,
				durationid:get_detail_plan[0].durationid,
				durationorder:get_detail_plan[0].durationorder,
				totalhours:0,
				valueused:0
			}

			console.log("planid: " + getplan_onweek[countplan].planid);
			console.log("weekid: " + getplan_onweek[countplan]._id);
			const get_day_onweek = await db.collection("days").find({weekid:ObjectId(getplan_onweek[countplan]._id)}).toArray();
			var SumTotalHours = 0;
			var SumTotalvalue = 0;
			for(var days = 0;days < get_day_onweek.length;days++){
				//console.log("dayid: " + days + " "+ get_day_onweek[days]._id);
				var dayid_find = get_day_onweek[days]._id.toString();
				var userid_find = getallusers[users]._id.toString();
				const get_hours_value = await db.collection("timesheet").find({$and:[{dayid:dayid_find},{userid:userid_find}]}).toArray();
				if(get_hours_value.length == 0){
					SumTotalHours += 0;
					SumTotalvalue += 0;
				}else{
					SumTotalHours += get_hours_value[0].hours;
					SumTotalvalue += get_hours_value[0].totalvalue;
					total_hours_on_weekusers +=get_hours_value[0].hours;
				}
			}
			obj_plan.totalhours = SumTotalHours;
			obj_plan.valueused = SumTotalvalue;
			console.log("SumTotalHours: "+ SumTotalHours);
			console.log("SumTotalvalue: "+ SumTotalvalue);
			obj_user.plans.push(obj_plan);
		}
		obj_user.totalhours = total_hours_on_weekusers;
		obj_progress_timesheet_user.team.push(obj_user);
		console.log("----------------------------------------------");
	}
	//console.log(obj_progress_timesheet_user);
	return obj_progress_timesheet_user;
}

exports.send_progress_week = async(projectid,weeknumber,plans,late)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	//get detail week 
	const get_detail_week = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	console.log("get_detail_week.length: " +get_detail_week.length);
	console.log("projectid: "+ projectid);
	console.log("count plan: " + plans.length);

	//get batchid 
	const get_batchid = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();

	//insert progress_groupplan
	var obj_progress_groupplan={
		projectid:projectid,
		batchid:get_batchid[0].batchid,
		durationid:get_detail_week[0].durationid,
		status:1,
		statusstring:"ส่งความก้าวหน้า",
		updatetime:pastdatetime._created
	}
	if(late == "ส่งล่าช้า"){
		obj_progress_groupplan.status = 5;
		obj_progress_groupplan.statusstring = "ส่งล่าช้า";
	}else{
		obj_progress_groupplan.status = 1;
		obj_progress_groupplan.statusstring = "ส่งความก้าวหน้า";
	}

	const insert_progress_groupplan = await db.collection("progress_groupplan").insertOne(obj_progress_groupplan);
	const get_last_progress_groupplan_id = await db.collection("progress_groupplan").find({updatetime:pastdatetime._created}).toArray();
	console.log("last rogress_groupplan_id: " + get_last_progress_groupplan_id[0]._id);
	var progress_groupplan_id =  get_last_progress_groupplan_id[0]._id;
	
	for(var countplan = 0; countplan < plans.length;countplan++){

		console.log("***************************************************************");
		console.log("***************************************************************");
		console.log("plans[count].statusplan: "+ plans[countplan].statusplan);
		console.log("***************************************************************");
		console.log("***************************************************************");
		if(plans[countplan].statusplan == true){
			console.log("data: this plan is success.");
			const update_statusplan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countplan].planid)},
																	{$set:{
																		statusplan:true
																	}});
		}else if(plans[countplan].statusplan == false || plans[countplan].statusplan == ""){
			console.log("data: this plan not success.");
			const update_statusplan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countplan].planid)},
																	{$set:{
																		statusplan:false
																	}});
		}else{
			console.log("data: this plan status undefind.");
		}


		//get weekid 
		const get_weekid_atplanid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{planid:ObjectId(plans[countplan].planid)}]}).toArray();

		var updateweek_status = 1;
		var updateweek_statusweek = 1;
		var status_late = false;
		if(late == "ส่งล่าช้า"){
			updateweek_status = 5;
			updateweek_statusweek = "ส่งล่าช้า";
			status_late = true;
		}else{
			updateweek_status = 1;
			updateweek_statusweek = "ส่งความก้าวหน้า";
			status_late = false;
		}
		//update status week = 1 sending , 2 = missing , 3 = reject ,4 = approve "_id":ObjectId(get_weekid_atplanid[0]._id)
		const update_statusweek = await db.collection("weeks").updateOne(
												{$and:[{weeknumber:weeknumber},{projectid:projectid}]},
												{$set:{
													statusweek:updateweek_status,
													status:updateweek_status,
													statusstring:updateweek_statusweek,
													status_late:status_late
												}});
		var obj_progress_week={
			progress_groupplan_id:progress_groupplan_id,
			weekid:get_weekid_atplanid[0]._id,
			weeknumber:weeknumber,
			batchid:get_batchid[0].batchid,
			projectid:projectid,
			planid:plans[countplan].planid,
			planname:plans[countplan].planname,
			durationid:plans[countplan].durationid,
			durationorder:plans[countplan].durationorder,
			planvalue:plans[countplan].jobvalue,
			valueused:plans[countplan].usevalue,
			timelength:plans[countplan].timelength,
			timeused:plans[countplan].timeused,
			documentation:plans[countplan].document,
			barries:plans[countplan].barrier,
			updatetime:pastdatetime._created,
			status:1,
			statusstring:"ส่งความก้าวหน้าของงาน"
		}

		if(late == "ส่งล่าช้า"){
			obj_progress_week.status = 5;
			obj_progress_week.statusstring = "ส่งล่าช้า";
		}else{
			obj_progress_week.status = 1;
			obj_progress_week.statusstring = "ส่งความก้าวหน้าของงาน";
		}

		console.log(obj_progress_week);
		const insert_obj_progress_week = await db.collection("progress_weeks").insertOne(obj_progress_week);

		const getlast_progressid = await db.collection("progress_weeks").find({$and:[{weekid:ObjectId(get_weekid_atplanid[0]._id)},{planid:plans[countplan].planid}]}).toArray();
		console.log("count team: "+ plans[countplan].team.length);
		var progress_weekid = getlast_progressid[0]._id;
		for(var countteam = 0;countteam < plans[countplan].team.length; countteam++){
			var obj_user = {
				progress_weekid:progress_weekid,
				userid:plans[countplan].team[countteam].userid,
				username:plans[countplan].team[countteam].username,
				totalhours:plans[countplan].team[countteam].hours,
				comment:plans[countplan].team[countteam].comment
			}
			console.log(obj_user);
			const insert_obj_detailprogress = await db.collection("detailprogress_weeks").insertOne(obj_user);
		}
		console.log("------------------------------");
	}

	//insert collection notification_admin 
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:projectid,
		planid:"",
		comment_rejectid:"",
		statusaction:1,
		statusstring:"ส่งความก้าวหน้า",
		read:"",
		progress_groupplan_id:progress_groupplan_id,
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_admin = await db.collection("notification_admin").insertOne(obj_notification);


	return {data:"send progress week success.",status:true};
}

exports.update_progress_week = async(projectid,weeknumber,plans,late)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var status = 1;
	var statusstring = "ส่งความก้าวหน้า";

	console.log("projectid: "+projectid);
	console.log("weeknumber: "+weeknumber);
	console.log("plans.length: "+plans.length);

	//get progress_groupplan_id
	const get_progress_groupplan_id = await db.collection("progress_weeks").find({$and:[{projectid:projectid},{weeknumber:weeknumber}]}).toArray();
	console.log("progress_groupplan_id: "+ get_progress_groupplan_id[0].progress_groupplan_id);
	var progress_groupplan_id = get_progress_groupplan_id[0].progress_groupplan_id;

	//update progress groupplan 
	var updateweek_progressgroup_status = 1;
	var updateweek_progressgroup_statusweek = 1;
	var status_progressgroup_late = false;
		if(late == "ส่งล่าช้า"){
			updateweek_progressgroup_status = 5;
			updateweek_progressgroup_statusweek = "ส่งล่าช้า";
			status_progressgroup_late = true;
		}else{
			updateweek_progressgroup_status = 1;
			updateweek_progressgroup_statusweek = "ส่งความก้าวหน้า";
			status_progressgroup_late = false;
		}
	const update_progress_groupplan = await db.collection("progress_groupplan").update({"_id":ObjectId(progress_groupplan_id)},
																					  {$set:{

																					  	status:updateweek_progressgroup_status,
																					  	statusstring:updateweek_progressgroup_statusweek}});
	console.log("***************************************************************");
	console.log("count plans:"+plans.length);
	//update progress week
	for(var countplan=0;countplan < plans.length;countplan++){
		var planid = plans[countplan].planid;
		var usevalue = plans[countplan].usevalue;
		var timeused = plans[countplan].timeused;
		var documentation = plans[countplan].document;
		var barrier = plans[countplan].barrier;
		var weeknumber = weeknumber;
		console.log("***************************************************************");
		console.log("***************************************************************");
		console.log("plans[count].statusplan: "+ plans[countplan].statusplan);
		console.log("***************************************************************");
		console.log("***************************************************************");
		if(plans[countplan].statusplan == true){
			console.log("data: this plan is success.");
			const update_statusplan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countplan].planid)},
																	{$set:{
																		statusplan:true
																	}});
		}else if(plans[countplan].statusplan == false || plans[countplan].statusplan == ""){
			console.log("data: this plan not success.");
			const update_statusplan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countplan].planid)},
																	{$set:{
																		statusplan:false
																	}});
		}else{
			console.log("data: this plan status undefind.");
		}

		//get weekid 
		const get_weekid_atplanid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{planid:ObjectId(planid)}]}).toArray();

		var updateweek_status = 1;
		var updateweek_statusweek = 1;
		var status_late = false;
		if(late == "ส่งล่าช้า"){
			updateweek_status = 5;
			updateweek_statusweek = "ส่งล่าช้า";
			status_late = true;
		}else{
			updateweek_status = 1;
			updateweek_statusweek = "ส่งความก้าวหน้า";
			status_late = false;
		}
		//update status week = 1 sending , 2 = missing , 3 = reject ,4 = approve {"_id":ObjectId(get_weekid_atplanid[0]._id)
		const update_statusweek = await db.collection("weeks").updateOne(
												{$and:[{weeknumber:weeknumber},{projectid:projectid}]},
												{$set:{
													statusweek:updateweek_status,
													status:updateweek_status,
													statusstring:updateweek_statusweek,
													status_late:status_late
												}});

		const update_status_progressweek = await db.collection("progress_weeks").updateOne({planid:planid,weeknumber:weeknumber},
																					  {$set:{status:updateweek_status,
																					  		 statusstring:updateweek_statusweek,
																					  		 valueused:usevalue,
																					  		 timeused:timeused,
																					  		 documentation:documentation,
																					  		 barries:barrier
																					  		}});
		const get_progress_weekid = await db.collection("progress_weeks").find({$and:[{planid:planid},{weeknumber:weeknumber}]}).toArray();
		var progress_weekid_find = get_progress_weekid[0]._id;
		console.log("progress weekid: " +progress_weekid_find);
		console.log("team.length: "+plans[countplan].team.length);
		for(var countteam = 0;countteam < plans[countplan].team.length;countteam++){
			var userid = plans[countplan].team[countteam].userid;
			var hours =  plans[countplan].team[countteam].hours;
			var comment = plans[countplan].team[countteam].comment;
			console.log("progress_weekid: "+progress_weekid_find);
			console.log("userid: "+userid);
			console.log("hours: "+hours);
			console.log("comment: "+comment);
			const update_data_useronprogressweek = await db.collection("detailprogress_weeks").update({progress_weekid:ObjectId(progress_weekid_find),userid:userid},
																					  {$set:{totalhours:hours,
																					  		 comment:comment
																					  		}});
		}
		console.log("--------------------------------");
	}	
	//insert collection notification_admin 
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:projectid,
		planid:"",
		comment_rejectid:"",
		statusaction:1,
		statusstring:"ส่งความก้าวหน้า",
		read:"",
		progress_groupplan_id:progress_groupplan_id,
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
}
	const add_notification_admin = await db.collection("notification_admin").insertOne(obj_notification);

	console.log("data: update status progress weeks success.");
	return {data:"update status progress weeks success.",status:true};
}

exports.getplan_on_now_week = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("datenow: "+ datenow);
	
	var obj_data = {
		daynumber:0,
		projectid:projectid,
		projectname:"",
		projectvalue:0,
		totalvalue:0,
		weeknumber:0,
		plans:[]

	}
	//get detail project
	const get_detailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	obj_data.projectname = get_detailproject[0].projectname;
	obj_data.projectvalue = get_detailproject[0].projectvalue;

	//get detail day 
	const getdetail_day = await db.collection("days").find({$and:[{datestring:datenow},{projectid:projectid}]}).toArray();
	console.log("getdetail_day: "+ getdetail_day.length);

	console.log("-------------------------------------------------------");
	//get week number 
	for(var countday = 0;countday < getdetail_day.length;countday++){
		
		var weekid = getdetail_day[countday].weekid;
		const getweeknumber = await db.collection("weeks").find({$and:[{"_id":ObjectId(weekid)},{projectid:projectid}]}).toArray();	
		console.log("weekid: "+ getweeknumber[0]._id);
		console.log("weeknumber: "+ getweeknumber[0].weeknumber);
		console.log("planid: " + getweeknumber[0].planid);
		

		//get detail plan 
		const getdetailplan = await db.collection("planorders").find({"_id":ObjectId(getweeknumber[0].planid)}).toArray();
		console.log("planname: " + getdetailplan[0].planname);
		console.log("planorder: "+ getdetailplan[0].planorder);
		console.log("-------------------------------------------------------");
		var obj_plan ={
			dayid:getdetail_day[countday]._id,
			daynumber:getdetail_day[countday].daynumber,
			weekid:weekid,
			weeknumber:getweeknumber[0].weeknumber,
			planid:getdetailplan[0]._id,
			planorder:getdetailplan[0].planorder,
			planname:getdetailplan[0].planname,
			jobvalue:getdetailplan[0].jobvalue,
			durationorder:getdetailplan[0].durationorder,
			timelength:getdetailplan[0].timelength
		}
		obj_data.plans.push(obj_plan);
	}
	// if(getdetail_day.length > 0){
	// 	const getweeknumber = await db.collection("weeks").find({$and:[{"_id":ObjectId(getdetail_day[0].weekid)},{projectid:projectid}]}).toArray();	
	// 	obj_data.daynumber = getdetail_day[0].daynumber;
	// 	obj_data.weeknumber = getweeknumber[0].weeknumber;
	// }else{
	// 	console.log("data: no plans on this week.");
	// }
	console.log("-----------------------------------");
	console.log(obj_data);
	return obj_data;
} 

exports.get_reject_file = async(projectid,comment_rejectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_reject = {
		comment:"",
		filename:"",
		filename_reject:""
	}

	const get_rejectfile = await db.collection("commentreject_file").find({$and:[{"_id":ObjectId(comment_rejectid)},{projectid:projectid}]}).toArray();
	obj_reject.comment = get_rejectfile[0].comment;
	obj_reject.filename = get_rejectfile[0].filename;
	obj_reject.filename_reject = get_rejectfile[0].filename_reject;
	console.log(obj_reject);
	console.log("-----------------------");
	return obj_reject;
}

////////////////////////////////////////ADMIN//////////////////////////////////////////////////////////

exports.updatepassword = async(userid,token)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//update password 
	const update_data = await db.collection("accountadmin").updateOne({"_id":ObjectId(userid)},
														{$set:{
															password:token
														}});
	console.log("data: update password success.");
	return {data:"update password success.",status:false};
}

exports.reject_file =async(projectid,comment,filename,filename_reject)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	///add Table:commentreject_file
	var obj_rejectfile = {
		projectid:projectid,
		filename:filename,
		filename_reject:filename_reject,
		comment:comment,
		insertdate:pastdatetime._created,
		read:"",
		status:3,
		statusstring:"ปรับแก้เอกสารโครงการ"
	}
	const insert_rejectfile = await db.collection("commentreject_file").insertOne(obj_rejectfile);
	console.log(obj_rejectfile);
	console.log("----------------------------");
	//add notification_users
	const get_commentreject_file_id = await db.collection("commentreject_file").find({$and:[{projectid:projectid},{filename:filename},{insertdate:pastdatetime._created}]}).toArray();
	var obj_noti = {
		projectid:projectid,
		planid:"",
		comment_rejectid:get_commentreject_file_id[0]._id,
		statusaction:get_commentreject_file_id[0].status,
		statusstring:get_commentreject_file_id[0].statusstring,
		read:"",
		progress_groupplan_id:"",
		datetimestamp:pastdatetime._created,
		datetimestring:moment(pastdatetime._created).format('DD/MM/YYYY HH:mm:ss')
	}
	const insert_notification = await db.collection("notification_users").insertOne(obj_noti);
	console.log(obj_noti);
	console.log("----------------------------");


	//get plan_id
	const get_planid = await db.collection("planorders").find({projectid:projectid}).toArray();
	var planid = get_planid[0]._id;
	//update comment and status , updatetime 
	const update_reject_plan = await db.collection("planorders").updateOne(
								{"_id":ObjectId(planid)},
								{$set: {
									comment:"",
									status:3,
									updatetime:pastdatetime._created
								}
								});
	const getprojectid = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	//update comment and status , updatetime 
	const update_reject_project = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set:{status:3}});
	//update comment and status , updatetime 
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:"3",
											   updatetime:pastdatetime._created}});
	return {data:"reject file success.",status:true}
}

exports.get_allusers = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_users = {
		users:[]
	}
	const getallproject = await db.collection("projects").find({batchid:batchid}).toArray();
	for(var count = 0;count < getallproject.length;count++){
		const getuser = await db.collection("users").find({"_id":ObjectId(getallproject[count].Primaryresponsibility)}).toArray();
		var obj={
			projectid:getallproject[count]._id,
			projectname:getallproject[count].projectname,
			username:getuser[0].fristname+" "+getuser[0].lastname,
			companyname:getuser[0].companyname
		}
		obj_users.users.push(obj);
	}
	console.log(obj_users);
	console.log("-------------------------------");
	return obj_users;
}

exports.get_commented = async(projectid,planid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_comment = {
		all_comment:[]
	}
	const get_comment = await db.collection("commentreject").find({$and:[{projectid:projectid},{planid:planid}]}).toArray();
	for(var count=0;count < get_comment.length;count++){
		var time = moment(get_comment[count].insertdate).format("D/MM/YYYY HH:mm:ss");
		var obj_reject = {
			comment:get_comment[count].comment,
			filename:get_comment[count].pahtfile,
			insertdate:time
		}
		obj_comment.all_comment.push(obj_reject);
	}
	console.log("data: get all comment reject.");
	console.log(obj_comment);
	console.log("-----------------------------");
	return obj_comment;
}

exports.getnotification_admin = async(adminid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//check adminid 
	const getadmin = await db.collection("accountadmin").find({"_id":ObjectId(adminid)}).toArray();
	if(getadmin.length > 0){
		var obj_notifications={
			notifications:[],
			count_read:0
		}
		//count read 
		const getcountread = await db.collection("notification_admin").find({read:""}).toArray();
		obj_notifications.count_read = getcountread.length;
		//get collection notification_users
		const getnoti_admin = await db.collection("notification_admin").find({}).toArray();
		console.log("getnoti_admin.length : "+ getnoti_admin.length);
		for(var countnoti = (getnoti_admin.length - 1);countnoti >= 0;countnoti--){
			var obj_noti ={
				noti_id:getnoti_admin[countnoti]._id,
				companyname:"",
				projectid:getnoti_admin[countnoti].projectid,
				planid:getnoti_admin[countnoti].planid,
				statusaction:getnoti_admin[countnoti].statusaction,
				statusstring:getnoti_admin[countnoti].statusstring,
				progress_groupplan_id:getnoti_admin[countnoti].progress_groupplan_id,
				read:getnoti_admin[countnoti].read,
				lasttime:""
			}

			//get company 
			const get_detailproject = await db.collection("projects").find({"_id":ObjectId(getnoti_admin[countnoti].projectid)}).toArray();
			const getcompanyname = await db.collection("users").find({"_id":ObjectId(get_detailproject[0].Primaryresponsibility)}).toArray();
			obj_noti.companyname = getcompanyname[0].companyname;

			var lastprojecttime = moment(Math.ceil(getnoti_admin[countnoti].datetimestamp));
			var thistime = moment(pastdatetime._created);
			console.log("lastprojecttime: "+lastprojecttime);
			console.log("this time: "+pastdatetime._created);

			//date project
			var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
			var daypro = moment(lastprojecttime).format('DD');
			var monthpro = moment(lastprojecttime).format('MM');
			var yearpro = moment(lastprojecttime).format('YYYY');
			var hrpro = moment(lastprojecttime).format('HH');
			var minutepro = moment(lastprojecttime).format('mm');
			var sspro = moment(lastprojecttime).format('ss');
			var last_updateproject = moment([yearpro, monthpro, daypro,hrpro,minutepro]);
			console.log("last_updateproject: "+last_updateproject);
			var yearth = parseInt(yearpro) + 543;
			obj_noti.lasttime =  daypro+"/"+monthpro+"/"+yearpro+"  "+hrpro+":"+minutepro+":"+sspro

			//date now 
			// var datenow_moment = moment(thistime).format('DD/MM/YYYY');
			// var daynow = moment(thistime).format('DD');
			// var monthnow = moment(thistime).format('MM');
			// var yearnow = moment(thistime).format('YYYY');
			// var hrnow = moment(thistime).format('HH');
			// var minutenow = moment(thistime).format('mm');
			// var ssnow = moment(thistime).format('ss');
			// var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
			// console.log("datenow_action: "+datenow_action);

			// var datepro = new Date(yearpro, monthpro, daypro);
			// var datenow = new Date(yearnow, monthnow, daynow);

			// var diff_minute = datenow_action.diff(last_updateproject, 'minute');
			// var diff_Hr = datenow_action.diff(last_updateproject, 'hour');
			// var diff_day = datenow_action.diff(last_updateproject, 'days');

			// console.log("diff_minute: " + diff_minute);
			// console.log("diff_Hr: " + diff_Hr);
			// console.log("diff_day: " + diff_day);
			// if(diff_day > 0){
			// 	obj_noti.lasttime = diff_day +" "+"วัน";
			// }else if(diff_Hr > 0){
			// 	obj_noti.lasttime = diff_Hr +" "+"ชั่วโมง";
			// }else if(diff_minute > 0){
			// 	obj_noti.lasttime = diff_minute +" "+"นาที";
			// }
			obj_notifications.notifications.push(obj_noti);

			// var diff = new DateDiff(datenow,datepro);
			// console.log("diff days: "+diff.days());
			// console.log("diff minute: "+diff.minutes());
			// console.log("diff hours: "+diff.hours());
			// console.log("--------------------------");
			// if(diff.minutes() >= 1440){
			// 	var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
			// 	console.log("calculate days:" + days);
			// }else if (diff.minutes() >= 60){
			// 	var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
			// 	console.log("calculate hours:" + hours);
			// }else if(diff.minutes() <= 59){
			// 	var minutes = (diff.minutes()) +" "+ "นาที";
			// 	console.log("calculate minutes:" + minutes);
			// }
			console.log("--------------------------");
		}
		return obj_notifications;
	}else{
		return {data:"invalid adminid.",status:false};
	}
}

exports.update_readnoti_admin = async(noti_id)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);

	//update noti read = true 
	const update_notification = await db.collection("notification_admin").updateOne(
												{"_id":ObjectId(noti_id)},
												{ $set: {read:true}});
	console.log("update noti_id: "+noti_id +" success.");
	return {data:"update noti_id success.",status:true};
}


//login admin
exports.getuser = async(username,password)=>{
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	console.log("data: login admin");
	console.log("username: " + username);
	console.log("password: " + password);

	//check username and password
     const res = await db.collection("accountadmin").find({username:username}).toArray();
     var decode_password = jwt.verify(res[0].password,SECRET);
     var decode = jwt.verify(password,SECRET);
	 var password = decode.password;
	 console.log("decode_password: "+ decode_password.pass);
	 console.log("password: "+ password);
	 if(decode_password.password == password){
	 	try {
	 	//update login time 
	 	  const updatetimelogin = await db.collection("accountadmin").updateOne(
								{username:username},
								{ $set: {lastlogin:pastdatetime._created}}, 
								function(err, res) {
								    if (err) throw err;
								    console.log("data: update lastlogin success");
								    console.log("-------------------");
								 });
	 	
          if(res.length == 0){
          	response = {data:"invalid username or password",status:false};
          	return response;
          }else{
          	response = res;
          	return response;
          }
        }
        catch(err) {
        	console.log(err);
            client.close();
        }
	 }else{
	 	response = {data:"invalid username or password",status:false};
        return response;
	 }
}

exports.addbatch = async(admininsert,adminupdate,modelname,piscalyear,numberofoperators,startingdate,dts_startingdate,duedate,dts_duedate,alreadybusinessman,datenow,pastdatetime,updatetime,status,active)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
		//Create batch
		var userobj={
			modelname:modelname,
			piscalyear:piscalyear,
			numberofoperators:numberofoperators,
			alreadybusinessman:alreadybusinessman,
			startingdate:dts_startingdate,
			duedate:dts_duedate,
			Insdate:pastdatetime._created,
			updatetime:updatetime,
			admininsert:admininsert,
			adminupdate:adminupdate,
			status:status,
			active:active
		}; 
		const createbatch = await db.collection("batch").insertOne(userobj, function(err, res) {
			if (err) throw err;
				console.log("data: create batch success.");
				console.log("-------------------");
		});
		var resultreturn = {data:"create batch success.",status:true}
		return resultreturn;
}

exports.getallbatch = async()=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getbatchs = await db.collection("batch").find().toArray();
	return getbatchs;
}

exports.createlink = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getlink = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var resultjson={
		data:"create link success.",
		link:"http://ted.integreat.in.th/register/"+getlink[0]._id,
		status:true
	}
	return resultjson;
}




exports.getdetailbatch = async(batchid)=>{
	var countbsm;
	var projectpending;
	var projectreject;
	var projectsuccess;
	var respond ={
		countbsm:0,
		projectpending:"",
		percentpending:0,
		projectreject:"",
		percentreject:0,
		projectsuccess:"",
		percentsucess:0,
		duedate:"",
		differancetime:0
	}
	
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	const get_countbsm = await db.collection("projects").find({batchid:batchid}).toArray();
	countbsm = getdetailbatch[0].numberofoperators;
	respond.countbsm = get_countbsm.length;
	console.log("count bsm: "+countbsm);

	//get todays date
	var today = moment();
	var duedate = getdetailbatch[0].duedate;//moment(getdetailbatch[0].duedate).format("D/MM/YYYY");
	respond.duedate = duedate;
	console.log("duedate: "+duedate);
	console.log("Day: "+ moment(getdetailbatch[0].duedate).format("D"));
	var day =  moment(getdetailbatch[0].duedate).format("D");
	console.log("Month: "+ moment(getdetailbatch[0].duedate).format("MM"));
	var month = moment(getdetailbatch[0].duedate).format("MM")
	console.log("Year: " + moment(getdetailbatch[0].duedate).format("YYYY"));
	var year = moment(getdetailbatch[0].duedate).format("YYYY");
	var cal_year = year-543;
	var set_duedate = moment(cal_year+"/"+month+"/"+day, 'YYYY-MM-DD');
	var diffdate = set_duedate.diff(today, 'day');
	console.log("diff date: "+diffdate);
	respond.differancetime = diffdate;

	//project status sending
	const getprojectpending = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"2"}]}).toArray();
	projectpending = getprojectpending.length;
	respond.projectpending = projectpending;
	respond.percentpending = (projectpending * 100) / countbsm;
	console.log("project pending: "+projectpending);

	//project status reject 
	const getprojectreject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"3"}]}).toArray();
	projectreject = getprojectreject.length;
	respond.projectreject = projectreject;
	respond.percentreject = (projectreject * 100)/countbsm;
	console.log("project reject: "+projectreject);

	//project status success 
	const getprojectsuccess = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"4"}]}).toArray();
	projectsuccess = getprojectsuccess.length;
	respond.projectsuccess = projectsuccess;
	respond.percentsucess = (projectsuccess * 100) / countbsm;
	console.log("project sucess: "+projectsuccess);
	

	return respond;
}

exports.updateduedatetime = async(batchid,duedate) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var obj_update_duedate = {
			data:"update duedate success.",
			batchid:batchid,
			duedate:"",
			duedate_amount:0,
			status:true
	}
	const updateduedate =  db.collection("batch").updateOne(
										{"_id":ObjectId(batchid)},
										{$set:{duedate:duedate}});
	console.log("duedate update: "+ duedate);
	console.log("batchid :" + batchid);

	//get detail batch 
	const getdetail = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var startdate_dtsm = pastdatetime._created;
	var duedate_dtsm = getdetail[0].duedate;

		//get days from date now count down
			var startday = moment(startdate_dtsm).format("DD");
			var startmonth = moment(startdate_dtsm).format("MM");
			var startyear = moment(startdate_dtsm).format("YYYY");
			var start = moment([startyear, startmonth, startday]);
			
			var dueday = moment(duedate_dtsm).format("DD");
			var duemonth =  moment(duedate_dtsm).format("MM");
			var dueyear =  moment(duedate_dtsm).format("YYYY");
			var due = moment([dueyear, duemonth, dueday]);
			var diff_day = due.diff(start, 'days');

	var daystring = moment(duedate_dtsm).format("DD");
	var monthstring = moment(duedate_dtsm).format("MM");
	var yearstring = moment(duedate_dtsm).format("YYYY");
	var yearth = parseInt(yearstring) + 543;
	var duedatestring = daystring+"/"+monthstring+"/"+ yearth;
	obj_update_duedate.duedate_amount = diff_day;
	obj_update_duedate.duedate = duedatestring;
	console.log(obj_update_duedate);
	return obj_update_duedate;
}

exports.getdata_detailbatch = async(batchid) =>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	const getdetail = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var obj_batch = {
		batchname:"",
		piscalyear:"",
		amount_bsm:"",
		duedate:"",
		duedate_amount:0
	}

	if(getdetail.length == 0){
		var respond = {data:"invalid batchid.",status:false};
		return respond;
	}else{
		var startdate_dtsm = pastdatetime._created;//getdetail[0].startingdate;
		var duedate_dtsm = getdetail[0].duedate;
		if(startdate_dtsm < duedate_dtsm){
			//get days from date now count down
				var startday = moment(startdate_dtsm).format("DD");
				var startmonth = moment(startdate_dtsm).format("MM");
				var startyear = moment(startdate_dtsm).format("YYYY");
				var start = moment([startyear, startmonth, startday]);
				
				var dueday = moment(duedate_dtsm).format("DD");
				var duemonth =  moment(duedate_dtsm).format("MM");
				var dueyear =  moment(duedate_dtsm).format("YYYY");
				var due = moment([dueyear, duemonth, dueday]);
				var diff_day = due.diff(start, 'days');

			obj_batch.batchname = getdetail[0].modelname;
			obj_batch.piscalyear = getdetail[0].piscalyear;
			obj_batch.amount_bsm = getdetail[0].numberofoperators;

			var daystring = moment(duedate_dtsm).format("DD");
			var monthstring = moment(duedate_dtsm).format("MM");
			var yearstring = moment(duedate_dtsm).format("YYYY");
			var yearth =  parseInt(yearstring) + 543;
			var duedatestring = daystring+"/"+monthstring+"/"+yearth;
			obj_batch.duedate = duedatestring;
			obj_batch.duedate_amount = diff_day;
			console.log(duedatestring);
			return obj_batch;
		}else{
			obj_batch.batchname = getdetail[0].modelname;
			obj_batch.piscalyear = getdetail[0].piscalyear;
			obj_batch.amount_bsm = getdetail[0].numberofoperators;

			var daystring = moment(duedate_dtsm).format("DD");
			var monthstring = moment(duedate_dtsm).format("MM");
			var yearstring = moment(duedate_dtsm).format("YYYY");
			var yearth =  parseInt(yearstring) + 543;
			var duedatestring = daystring+"/"+monthstring+"/"+yearth;
			obj_batch.duedate = duedatestring;
			obj_batch.duedate_amount = 0;
			console.log(duedatestring);
			return obj_batch;
		}
		
	}
}

exports.getprojectsending = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var status = "2";//reject 
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status}]}).toArray();
	for(var i = 0; i < getproject.length; i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:2,
			statusstring:"ส่งข้อมูล",
			updatetime:""
		};
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("-------------------------");
	return objprojects;
}


exports.getdetailprpoject = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var projectobj ={
		batchname:"",
		piscalyear:"",
		numberofoperators:"",
		startingdate:0,
		duedate:0,
		projectname:"",
		primaryuser:"",
		datetime:0,
		documentation:[],
		projectvalue:0,
		planorder:[],
		team:[],
		lastupdatetime:"",
		statusstring:""
	}
	//Primaryresponsibility
	const project = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var status_project = project[0].status.toString();
	//status project 
	if(status_project == "0"){
		projectobj.statusstring = "กำลังกรอกข้อมูล";
	}else if(status_project == "1"){
		projectobj.statusstring = "ส่งข้อมูลแล้ว";
	}else if(status_project == "3"){
		projectobj.statusstring = "ปรับแก้เอกสาร";
	}else if(status_project == "4"){
		projectobj.statusstring = "อนุมัติการใช้ Timsheet";
	}
	//get bacth name 
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(project[0].batchid)}).toArray();
	projectobj.batchname = getdetailbatch[0].modelname;
	projectobj.piscalyear = getdetailbatch[0].piscalyear;
	projectobj.numberofoperators = getdetailbatch[0].numberofoperators;
	projectobj.startingdate = getdetailbatch[0].startingdate;
	projectobj.duedate = getdetailbatch[0].duedate;

	console.log("batchname: "+ projectobj.batchname);
	console.log("piscalyear: "+ projectobj.piscalyear);
	console.log("numberofoperators: " + projectobj.numberofoperators);
	console.log("startingdate: "+ projectobj.startingdate);
	console.log("duedate: "+projectobj.duedate);

	//cal between time 
	const gettimeproject = await db.collection("batchbusinessman").find({"projectid":ObjectId(projectid)}).toArray();
	var lastprojecttime = moment(Math.ceil(gettimeproject[0].updatetime));
	var thistime = moment(pastdatetime._created);
	console.log("lastprojecttime: "+lastprojecttime);
	console.log("this time: "+pastdatetime._created);

	//date project
	var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
	var daypro = moment(lastprojecttime).format('DD');
	var monthpro = moment(lastprojecttime).format('MM');
	var yearpro = moment(lastprojecttime).format('YYYY');
	var hrpro = moment(lastprojecttime).format('HH');
	var minutepro = moment(lastprojecttime).format('mm');
	var sspro = moment(lastprojecttime).format('ss');

	//date now 
	var datenow_moment = moment(thistime).format('DD/MM/YYYY');
	var daynow = moment(thistime).format('DD');
	var monthnow = moment(thistime).format('MM');
	var yearnow = moment(thistime).format('YYYY');
	var hrnow = moment(thistime).format('HH');
	var minutenow = moment(thistime).format('mm');
	var ssnow = moment(thistime).format('ss');

	var datepro = new Date(yearpro, monthpro, daypro);
	var datenow = new Date(yearnow, monthnow, daynow);
	var diff = new DateDiff(datenow,datepro);
	console.log("diff days: "+diff.days());
	console.log("diff minute: "+diff.minutes());
	console.log("diff hours: "+diff.hours());
	console.log("--------------------------");
	if(diff.minutes() >= 1440){
		var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
		projectobj.lastupdatetime = days;
		console.log("calculate days:" + days);
	}else if (diff.minutes() >= 60){
		var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
		projectobj.lastupdatetime = days;
		console.log("calculate hours:" + hours);
	}else if(diff.minutes() <= 59){
		var minutes = (diff.minutes()) +" "+ "นาที";
		projectobj.lastupdatetime = minutes;
		console.log("calculate minutes:" + minutes);
	}
	console.log("--------------------------");
	// diff.years(); // ===> 1.9
	// diff.months(); // ===> 23
	// diff.days(); // ===> 699


	// diff.weeks(); // ===> 99.9
	// diff.hours(); // ===> 16776
	// diff.minutes(); // ===> 1006560
	// diff.seconds(); // ===> 60393600
	

	const userprimary = await db.collection("users").find({"_id":ObjectId(project[0].Primaryresponsibility)}).toArray();
	//add user primary to frist person in team 
	for(var i = 0; i < userprimary.length; i++){
		var userobj = userprimary[i];
		projectobj.team.push(userobj);
	}
	console.log("project name: "+ project[0].projectname);
	projectobj.projectname = project[0].projectname;
	console.log("user: "+ userprimary[0].fristname +" "+userprimary[0].lastname);
	projectobj.primaryuser = userprimary[0].fristname +" "+userprimary[0].lastname;
	console.log("datetime: "+ project[0].senddatetime);
	projectobj.datetime = project[0].senddatetime;

	// console.log("documentation: "+project[0].Documentation);
	// projectobj.documentation = project[0].Documentation;
	const getallfile = await db.collection("fileproject").find({projectid:projectid}).toArray();
	for(var count = 0;count < getallfile.length;count++){
		var obj={
			filename:getallfile[count].filename
		}
		projectobj.documentation.push(obj);
	}

	console.log("project value: "+project[0].projectvalue);
	projectobj.projectvalue = project[0].projectvalue;
	//plan order
	const planorders = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("-----planorders: "+ planorders.length +"-----");
	for(var i = 0; i < planorders.length; i++){
		var planobj = {
			planid:planorders[i]._id,
			planname:planorders[i].planname,
			durationid:planorders[i].durationid,
			duration:0,
			planvalue:planorders[i].jobvalue,
			timelength:planorders[i].timelength,
			jobvalue:planorders[i].jobvalue,
			moneydifference:planorders[i].moneydifference,
			enforceemployee:planorders[i].enforceemployee,
			averagewages:planorders[i].averagewages
		};
		const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(planorders[i].durationid)}).toArray();
		planobj.duration = getdurationnumber[0].durationorder;
		console.log(planobj);
		projectobj.planorder.push(planobj);
	}
	console.log("---------------------");
	//team work 
	const team = await db.collection("users").find({projectid:projectid}).toArray();
	console.log("----------team----------");
	for(var i = 0;i < team.length; i++){
		var teamobj = team[i];
		console.log(teamobj);
		projectobj.team.push(teamobj);
	}
	console.log("--------------------");
	return projectobj;
}

exports.update_status_projectinvest = async(projectid,status)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var respond = {stausupdate:[]};
	console.log("status: "+status);
	console.log("projectid: "+projectid);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	//update status project collection BatchBusinessman
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:status,
											   updatetime:pastdatetime._created}})
										.then(function(result){
											respond.stausupdate.push({data:"update batchbusinessman.",status:true});
										});

	//update status project collection project 
	const updatestatusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{$set:{status:status}}).then(function(result){
											respond.stausupdate.push({data:"update projects.",status:true});
										});
	 return respond;
}

exports.insertcommentreject = async(projectid,comment,documentcomment)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var respond = {data:[]};
	//update comment and document to BatchBusinessman collection
	const updatecomment = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{commentreject:comment,
											   documentreject:documentcomment}})
										.then(function(result){
											respond.data.push({data:"update comment reject.",status:true});
										});
	return respond;									

}

exports.approveproject = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var respond = {stausupdate:[]};
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	//approve status project collection BatchBusinessman
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:"4",
											   updatetime:pastdatetime._created}})
										.then(function(result){
											respond.stausupdate.push({data:"approve batchbusinessman.",status:true});
										});

	//approve status project collection project 
	const updatestatusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{$set:{status:"4"}}).then(function(result){
											respond.stausupdate.push({data:"approve projects.",status:true});
										});

	//get detail project 
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("getdetail_project.length: "+getdetail_project.length);
	var projectname = getdetail_project[0].projectname;
	
	//get batch name 
	const getbatchname = await db.collection("batch").find({"_id":ObjectId(getdetail_project[0].batchid)}).toArray();
	console.log("getbatchname.length: "+getbatchname.length);
	var batchname = getbatchname[0].modelname;

	//get user detail 
	const getusers = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	console.log("getusers.length: "+getusers.length);
	var username = getusers[0].fristname+"  "+getusers[0].lastname;
	var email = getusers[0].email;
	var phone = getusers[0].phone;

	//insert collection notification_users
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:projectid,
		planid:"",
		comment_rejectid:"",
		statusaction:4,
		statusstring:"อนุมัติ Timsheet",
		read:"",
		progress_groupplan_id:"",
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_users = await db.collection("notification_users").insertOne(obj_notification);
	return respond;									
}

// get all project status reproject 
exports.getprojectstatusreject = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var status = "3";//reject 
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status}]}).toArray();
	for(var i = 0; i < getproject.length; i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:3,
			statusstring:"ปรับแก้",
			updatetime:""
		};
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("-------------------------");
	return objprojects;
}

//get project status approve
exports.getproject_approve = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var status_approve = "4";
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status_approve}]}).toArray();
	for(var i=0; i < getproject.length;i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:4,
			statusstring:"อนุมัติโครงการ",
			updatetime:""
		};
		//find fristname-lastname 
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("------------------------");
	return objprojects;
}

//create weeks 
exports.createweeks_test = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	const getplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var i = 0;i < getplan.length; i++){
		//get month from duration 
		const getduraion = await db.collection("duration").find({"_id":ObjectId(getplan[i].durationid)}).toArray();
		console.log("plan name:"+ getplan[i].planname);
		console.log("duration: "+ getduraion[0].durationorder);
		console.log("month: "+ getduraion[0].month);
		console.log("weeks: "+ (getduraion[0].month * 4));
		var week = (getduraion[0].month * 4);
		var daysunday = week; //sunday on all week.
		var days = ((getduraion[0].month * 4) * 6) + daysunday;
		var dayinweek = (days / week);
		console.log("days add sunday: " + days);
		console.log("days not sunday: " + ((getduraion[0].month * 4) * 6));
		var daynotsunday = ((getduraion[0].month * 4) * 6);
		//start date
		var startday = moment().day(7+1);
		console.log("start date timestamp : "+ startday);
		console.log("start day: "+ startday.format("dddd"));
		console.log("start at date: "+ startday.format("D"));
		console.log("start at month: "+startday.format("MMMM"));
		console.log("start at year: "+ startday.format("YYYY"));
		console.log("-----------------------------------------");
		var projectsuccess = moment(startday).add(days, 'days');
		var countdays = days;
		var countweek = 1;
		for(var d = 0; d <= days; d++){
			var modnumber = d % 7;
			if(modnumber == 0){
				console.log("week :"+ countweek);
				if(countweek > week){
					console.log("week finish.");
				}else{
					//insert week 
					var objweek = {
						durationid:"",
						projectid:"",
						weeknumber:countweek,
						monthnumber:0,
						daysinweek:6,
						weekstartdate:0,
						weekenddatetimestamp:0,
						weekvalue:0,
						updatetime:pastdatetime._created,
						statusweek:1
					}
					objweek.durationid = getduraion[0]._id;
					objweek.projectid  = projectid;
					const insertweek = await db.collection("weeks").insertOne(objweek);
					const getweekid = await db.collection("weeks").find({$and:[
											{durationid:getduraion[0]._id},
											{projectid:projectid},
											{weeknumber:countweek}
											]}).toArray();
					//insert weeksplanorder
					var objdayplan = {
						weekid:getweekid[0]._id,
						planid:getplan[i]._id
					}
					const insertdayplanorder = await db.collection("weeksplanorder").insertOne(objdayplan);
					for(var j = 1;j<=7;j++){

						var startdate = moment().day(7+d);
						

						var date = moment(startdate).add(j, 'days');
						if(date.format("dddd") == "Sunday"){
							console.log("Holiday.");
						}else{
							console.log(date.format("dddd D/MMMM/YYYY"));
							var objday = {
								weekid:"",
								daynumber:0,
								datetimestamp:0,
								datestring:"",
								daystring:"",
								statusopen:0	
							}
							objday.weekid = getweekid[0]._id;
							objday.daynumber = j;
							objday.datetimestamp = date;
							objday.datestring = date.format("D/MMMM/YYYY");
							objday.daystring = date.format("dddd");
							//insert day 
							const insertday = await db.collection("days").insertOne(objday); 
							countdays +=1;
						}
					}
				}
				countweek += 1;
				console.log("-----------------------------");
			}else{
				//not equal 0
			}
		}
		console.log("project success: " + projectsuccess.format("dddd D/MMMM/YYYY"));
	}
}

exports.createweeks = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	//check double 
	const checkweekofprojectid = await db.collection("weeks").find({projectid:projectid}).toArray();
	if(checkweekofprojectid.length > 0){
		return {data:"this project approved.",status:false};
	}

	var startdate_duration = "";
	//get sort array  
	var sort = {durationid:1};
	const getsort = await db.collection("planorders").find({projectid:projectid}).sort(sort).toArray();
	for(var z =0; z < getsort.length;z++){
		console.log("sort plan of durationid: "+ getsort[z]._id);
	}
	console.log("--------------------");
	//get planorders from projectid 
	const getplanorders = await db.collection("planorders").find({projectid:projectid}).sort(sort).toArray();
	//await db.collection("planorders").find({projectid:projectid}).toArray();
	var countweeks =0;
	var durationorder = 0;
	var last_startdate_duration = 0;
	var startdate_incodition ="";
	for(var plan =0;plan < getplanorders.length;plan++){
		console.log("planid: "+ getplanorders[plan]._id);
		console.log("time length: " + getplanorders[plan].timelength);
		const getdaysof_duration = await db.collection("duration").find({"_id":ObjectId(getplanorders[plan].durationid)}).toArray();
		var timelength = getdaysof_duration[0].month * 30;//getplanorders[plan].timelength;
		var cal_weeks = timelength / 6;
		console.log("durationorders: " + getplanorders[plan].durationorder);
		console.log("cal_weeks float :"+cal_weeks);
		console.log("cal_weeks: " + Math.floor(cal_weeks));
		console.log("cal_weeks Math ceil: " + Math.ceil(cal_weeks));
		var cal_weeks_ceil = Math.ceil(cal_weeks);//ปัดขึ้นหมด
		var days_not_decimal = Math.floor(cal_weeks) * 6;
		var days_decimal = timelength - days_not_decimal;
		console.log("days_not_decimal: " + days_not_decimal);
		console.log("days_decimal: "+ days_decimal);
		var startdate = "";
		if(startdate_duration == ""){
			startdate = moment().day(7+1);
			startdate_incodition = startdate;
		}else if( durationorder == getplanorders[plan].durationorder){
			startdate = last_startdate_duration;
			startdate_incodition = startdate;
			countweeks =0;
		}
		else if(startdate_duration != ""){
			startdate = startdate_duration;
			startdate_incodition = startdate;
		}
		console.log("start frist date on duration: "+ startdate.format("D dddd/MM/YYYY"));
		console.log("---------------------------");
		var partdate = "";
		for(var i =1; i <= cal_weeks_ceil;i++){
			countweeks+=1;
			var obj_week = {
				durationid:getplanorders[plan].durationid,
				projectid:getplanorders[plan].projectid,
				planid:getplanorders[plan]._id,
				weeknumber:countweeks,
				monthnumber:0,
				daysinweek:6,
				weekdatetimestamp:0,
				weekvalue:0,
				updatetime:pastdatetime._created,
				statusweek:0
			}
			//insert weeks 
			const insertweek = await db.collection("weeks").insertOne(obj_week);
			//get new weekid 
			const getweekid = await db.collection("weeks").find({$and:[
															{durationid:getplanorders[plan].durationid},
															{projectid:getplanorders[plan].projectid},
															{planid:getplanorders[plan]._id},
															{weeknumber:countweeks}
															]}).toArray();
			if(timelength < 6){
				console.log("weekid: "+getweekid[0]._id);
				console.log("week: " + countweeks + " at days: " + timelength);
				var daynumber_array = 0;
				for(var j=0;j < timelength;j++){// add days
					var date = moment(startdate).add(j, 'days');
					daynumber_array = j;
					var daynumber =0;
					console.log(date.format("D dddd/MMMM/YYYY")+ " day number: "+ j);
					if(daynumber_array == 0){
						daynumber=1;
					}else if(daynumber_array == 1){
						daynumber=2;
					}else if(daynumber_array == 2){
						daynumber=3;
					}else if(daynumber_array == 3){
						daynumber=4;
					}else if(daynumber_array == 4){
						daynumber=5;
					}else if(daynumber_array == 5){
						daynumber=6;
					}
					//insert day 
					var obj_day = {
						projectid:projectid,
						weekid:getweekid[0]._id,
						daynumber:daynumber,
						datetimestamp:date,
						datestring:date.format("D/MMMM/YYYY"),
						daystring:date.format("dddd"),
						statusopen:0
					}
					const insertday = await db.collection("days").insertOne(obj_day);
				}
			}else{
				timelength = timelength - 6;
				console.log("weekid: "+getweekid[0]._id);
				console.log("week: " + countweeks + " at days: 6");
				var daynumber_array = 0;
				//create days 
				for(var days = 0;days < 6;days++){//add days
					daynumber_array = days;
					var daynumber =0;
					var date = moment(startdate).add(days, 'days');
					console.log(date.format("D dddd/MM/YYYY")+" day number: "+ days);
					if(daynumber_array == 0){
						daynumber=1;
					}else if(daynumber_array == 1){
						daynumber=2;
					}else if(daynumber_array == 2){
						daynumber=3;
					}else if(daynumber_array == 3){
						daynumber=4;
					}else if(daynumber_array == 4){
						daynumber=5;
					}else if(daynumber_array == 5){
						daynumber=6;
					}
					//insert day 
					var obj_day = {
						projectid:projectid,
						weekid:getweekid[0]._id,
						daynumber:daynumber,
						datetimestamp:date,
						datestring:date.format("D/MMMM/YYYY"),
						daystring:date.format("dddd"),
						statusopen:0
					}
					const insertday = await db.collection("days").insertOne(obj_day);
					if(days == 5){
						console.log("set new start date:"+ moment(date).day(7+1).format("D dddd/MMMM/YYYY"));
						startdate = moment(date).day(7+1);
					}

				}//days
				if(i == cal_weeks_ceil){
						//start date next dutaion
						last_startdate_duration = startdate_incodition;
						startdate_duration = moment(date).day(7+1);
						durationorder = getplanorders[plan].durationorder;
						console.log("next date duration: " + startdate_duration.format("D dddd/MMMM/YYYY"));
						console.log("last startdate: " + last_startdate_duration.format("D dddd/MMMM/YYYY"));
						console.log("durationorder: "+durationorder);
					}
				console.log("---------------------------");
			}
		}
	}//plan

	//get start project 
	var weekid_start = "";
	const get_start_week = await db.collection("weeks").find({projectid:projectid}).toArray();
	for(var countweek = 0;countweek < get_start_week.length;countweek++){
		if(get_start_week[countweek].weeknumber == 1){
			//get frist week 
			weekid_start = get_start_week[countweek]._id;
		}
	}

	//get frist day 
	var fristday_string = "";
	var daystring = "";
	var datestring = "";
	const get_fristday = await db.collection("days").find({weekid:ObjectId(weekid_start)}).toArray();
	//day string TH
	if(get_fristday[0].daystring == "Monday"){
		daystring = "วันจันทร์";
	}else if(get_fristday[0].daystring == "Tuesday"){
		daystring = "วันอังคาร";
	}else if(get_fristday[0].daystring == "Wednesday"){
		daystring = "วันพุธ";
	}else if(get_fristday[0].daystring == "Thursday"){
		daystring = "วันพฤหัสบดี";
	}else if(get_fristday[0].daystring == "Friday"){
		daystring = "วันศุกร์";
	}else if(get_fristday[0].daystring == "Saturday"){
		daystring = "วันเสาร์";
	}

	var dd = moment(get_fristday[0].datetimestamp._d).locale('th').format("DD");
	var MM = moment(get_fristday[0].datetimestamp._d).locale('th').format("MMMM");
	var YY = moment(get_fristday[0].datetimestamp._d).locale('th').format("YYYY");
	var YY_th = parseInt(YY) + 543;

	datestring = dd+"/"+MM+"/"+YY_th;
	console.log("daystring "+ daystring);
	console.log("datestring: "+ datestring);
	fristday_string = daystring +"  "+ datestring;

	//get batchname 
	const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getprojectdetail[0].Primaryresponsibility)}).toArray();
	const getbatchname = await db.collection("batch").find({"_id":ObjectId(getprojectdetail[0].batchid)}).toArray();
	var batchname = getbatchname[0].modelname;
	var projectname = getprojectdetail[0].projectname;
	var username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
	var email = getuserprimary[0].email;
	var phone = getuserprimary[0].phone;

	//send email 									
			var transporter = nodemailer.createTransport(smtpTransport({
				service: 'gmail',
		  		host: 'smtp.gmail.com',
				auth:{
					xoauth2:xoauth2.createXOAuth2Generator({
						user:'tedfund.app@gmail.com',
						clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
						clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
						refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
					})
				},
			}));

			 const output = `
			 	<h3 style="color:#128553">Approve project.</h3>
			    <h4 style="color:#128553;margin-left:20px"><b>${batchname}</b></h4>
				<ul> 
					<li style="margin-bottom:5px"><b>ชื่อโครงการ:</b>  ${projectname}</li>
					<li style="margin-bottom:5px"><b>เริ่มลงเวลา:</b>  ${fristday_string}</li>
					<li style="margin-bottom:5px"><b>ชื่อผู้ประสานงานหลัก:</b>  ${username}</li>
					<li style="margin-bottom:5px"><b>Email:</b> ${email}</li>
					<li style="margin-bottom:5px"><b>เบอร์โทรศัพท์:</b> ${phone}</li>
				</ul>
				<hr color='128553'>
			  `;
			//set mail options
			var mailOptions = {
				from:'tedfund.app <tedfund.app@gmail.com>',
				to:email,
				subject:'Approve project.',
				text:'Approve project.',
				html:output
			}
			// sent email
			transporter.sendMail(mailOptions,function(err,res){
				if(err){
					console.log(err);
				}else{
					console.log('Email sent.');
					console.log(mailOptions);
				}
			});

	return {data:"create weeks success.",status:true};
}

exports.createweeks_test2 =async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getplanorders = await db.collection("planorders").find({projectid:projectid}).toArray();
	var month = 0;
	var weeks = 0;
	//sum all weeks of duration.
	for(var i =0;i < getplanorders.length; i++){
		const getmonth = await db.collection("duration").find({"_id":ObjectId(getplanorders[i].durationid)}).toArray(); 
		month += getmonth[0].month;

	}
	console.log("total month :" + month);
	weeks = month * 4;
	console.log("weeks: "+weeks);
	
} 

exports.getallweeks = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	const getweeks = await db.collection("weeks").find({projectid:projectid}).toArray();
	var objweeks = {
		weeks:[]
	}
	for(var week=0; week < getweeks.length; week++){
		var statusweek = "";
		var statuseng = "";
		if(getweeks[week].statusweek == 0){
			statusweek="ยังไม่ดำเนินการ";
			statuseng = "not used.";
		}else if(getweeks[week].statusweek == 1){
			statusweek="บันทึกความก้าวหน้า";
			statuseng = "Record progress.";
		}else if(getweeks[week].statusweek == 2){
			statusweek="ปรับแก้";
			statuseng = "Reject.";
		}else if(getweeks[week].statusweek == 3){
			statusweek="ตรวจสอบแล้ว";
			statuseng = "Checked success.";
		}
		var objweek = {
			week:getweeks[week].weeknumber,
			status:statuseng
		}
		objweeks.weeks.push(objweek);
		console.log("weeknumber: " + getweeks[week].weeknumber +" "+statuseng);
	}
	return objweeks;
}

exports.rejectproject = async(planid,comment,pahtfile)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//update comment and status , updatetime 
	const update_reject_plan = await db.collection("planorders").updateOne(
								{"_id":ObjectId(planid)},
								{$set: {
									comment:comment,
									status:3,
									updatetime:pastdatetime._created
								}
								});
	const getprojectid = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	var projectid = getprojectid[0].projectid;
	//update comment and status , updatetime 
	const update_reject_project = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set:{status:3}});
	//update comment and status , updatetime 
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:"3",
											   updatetime:pastdatetime._created}});
	//insert to collection comment 
	var obj_comment = {
		projectid:projectid,
		planid:planid,
		comment:comment,
		pahtfile:pahtfile,
		insertdate:pastdatetime._created,
		read:"",
		status:3,
		statusstring:"ปรับแก้เอกสาร"
	};
	const insertcomment = await db.collection("commentreject").insertOne(obj_comment);
	const get_commentreject_id = await db.collection("commentreject").find({insertdate:pastdatetime._created}).toArray();	

	//insert collection notification_users
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:projectid,
		planid:planid,
		comment_rejectid:get_commentreject_id[0]._id,
		statusaction:3,
		statusstring:"ปรับแก้เอกสาร",
		read:"",
		progress_groupplan_id:"",
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_users = await db.collection("notification_users").insertOne(obj_notification);
	return {data:"reject plan order success.",statuproject:"ปรับแก้เอกสาร",status:true};
}

exports.getreport_dashboard = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);

	var obj_dashboard = {
		batchid:batchid,
		modelname:"",
		piscalyear:"",
		allvalueused_batch:0,
		alltimeused:0,
		numberof_operators:0,
		sending_progress:0,
		missing_progress:0,
		reject_progress:0,
		now_operators:0
	}

	var SumTotalAllValueused = 0;
	var SumTotalAllTimeused = 0;
	var SetFirstDayOnproject = 0;
	var DateNowString_day = "";
	//get detail batch 
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	obj_dashboard.modelname = getdetailbatch[0].modelname;
	obj_dashboard.piscalyear = getdetailbatch[0].piscalyear;
	obj_dashboard.numberof_operators = getdetailbatch[0].numberofoperators;

	//get all project on batch
	const getallproject = await db.collection("projects").find({batchid:batchid}).toArray();
	console.log("project on batch: "+ getallproject.length);

	for(var countproject = 0; countproject < getallproject.length; countproject++){
		console.log("projectid: "+getallproject[countproject]._id);
		var projectid_find = getallproject[countproject]._id.toString();
		//get sum total used value on batch.
		const gettotal_value = await db.collection("progress_weeks").find({projectid:projectid_find}).toArray();
		if(gettotal_value.length == 0){
			SumTotalAllValueused += 0;
			SumTotalAllTimeused += 0;
		}else{
			SumTotalAllValueused += gettotal_value[0].valueused;	
			SumTotalAllTimeused += gettotal_value[0].timeused;
		}
	}
	console.log("SumTotalAllValueused: " + SumTotalAllValueused +" Bath.");
	console.log("SumTotalAllTimeused: " + SumTotalAllTimeused +" days.");
	obj_dashboard.allvalueused_batch = SumTotalAllValueused;
	obj_dashboard.alltimeused = SumTotalAllTimeused;
	obj_dashboard.now_operators = getallproject .length

	//get bsm sending progress week.
	const get_sending = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:1}]}).toArray();
	console.log("bsm sending: "+get_sending.length);
	//obj_dashboard.sending_progress = get_sending.length;

	//get bsm sending progress week late.
	const get_sending_late = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:5}]}).toArray();
	console.log("bsm sending: "+get_sending_late.length);
	obj_dashboard.sending_progress = get_sending_late.length+get_sending.length;


	//get bsm reject progress week.
	const get_reject = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:3}]}).toArray();
	console.log("bsm reject: " + get_reject.length);
	obj_dashboard.reject_progress = get_reject.length;

	//get bsm missing
	console.log("----------------missing---------------------");
	var count_missing = 0;
	const getproject_onbatch = await db.collection("projects").find({batchid:batchid}).toArray();

	//all project 
	for(var countproject_missing =0;countproject_missing < getproject_onbatch.length;countproject_missing++){
		//check week on project 
		var projectid_get = getproject_onbatch[countproject_missing]._id.toString();
		//var sort = {weeknumber:1};
		console.log("projectid: "+projectid_get);
		const check_week = await db.collection("weeks").find({projectid:projectid_get}).toArray();
		console.log("check_week.length: " +check_week.length);
	
			//get day on this datenow 
			const getday_on_datenow = await db.collection("days").find({$and:[{datestring:datenow},{projectid:projectid_get}]}).toArray();
			if(getday_on_datenow.length == 0){
			}
			else{
				console.log("datestring now: "+ getday_on_datenow[0].datestring);
				console.log("weekid: "+getday_on_datenow[0].weekid);
				//get week on project 
				const getweek_on_datenow = await db.collection("weeks").find({"_id":ObjectId(getday_on_datenow[0].weekid)}).toArray();
				console.log("weeknumber: " + getweek_on_datenow[0].weeknumber);
				if(getweek_on_datenow[0].weeknumber <= 1){
					count_missing +=0;
				}else{
					for(var count = 1;count < getweek_on_datenow[0].weeknumber;count++){
						//get less than week now on collection progress weeks
						const get_lessthan_week = await db.collection("progress_weeks").find({$and:[
																								{weeknumber:count},
																								{projectid:projectid_get}
																								]}).toArray();
						if(get_lessthan_week.length > 0){
							count_missing += 0;
						}else{
							//console.log("planid: "+getweek_on_datenow[count].planid);
							console.log("at week: "+ count);
							console.log("get_lessthan_week.length: "+get_lessthan_week.length);
							console.log("---------------------------------------------------");
							count_missing += 1;
						}
					}
				}
			}
			console.log("--------------------");
		
	console.log("-------------------------------------");
	obj_dashboard.missing_progress = count_missing;
	}
	console.log(obj_dashboard);
	return obj_dashboard;
}

exports.get_progress_sending = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var objsending={
		sending:[]
	}
	//get bsm sending progress week.
	const get_sending = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:1}]}).toArray();
	console.log("bsm sending: "+get_sending.length);
	for(var countsending = 0;countsending < get_sending.length;countsending++){
		var obj_progress_week = {
			progress_groupplan_id:"",
			projectid:"",
			projectname:"",
			username:"",
			updatetime:0,
			weekid:"",
			weeknumber:0,
			statusweek:0
		}
		const getdetail_project = await db.collection("projects").find({"_id":ObjectId(get_sending[countsending].projectid)}).toArray();
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
		obj_progress_week.progress_groupplan_id = get_sending[countsending]._id;
		obj_progress_week.projectid = get_sending[countsending].projectid;
		obj_progress_week.projectname =  getdetail_project[0].projectname;
		obj_progress_week.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		obj_progress_week.statusweek = get_sending[countsending].status;

		var day = moment(get_sending[countsending].updatetime).format("DD");
		var month = moment(get_sending[countsending].updatetime).format("MM");
		var year = moment(get_sending[countsending].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		var time = moment(get_sending[countsending].updatetime).format("HH:mm:ss");
		obj_progress_week.updatetime = day+"/"+month+"/"+year_th+" "+time; 

		//get weeknumber
		const get_progressweek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(get_sending[countsending]._id)}).toArray();
		obj_progress_week.weeknumber = get_progressweek[0].weeknumber;
		objsending.sending.push(obj_progress_week);
	}

	//get bsm sending progress week late .
	const get_sending_late = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:5}]}).toArray();
	console.log("bsm sending: "+get_sending_late.length);
	for(var countsending = 0;countsending < get_sending_late.length;countsending++){
		var obj_progress_week = {
			progress_groupplan_id:"",
			projectid:"",
			projectname:"",
			username:"",
			updatetime:0,
			weekid:"",
			weeknumber:0,
			statusweek:0
		}
		const getdetail_project = await db.collection("projects").find({"_id":ObjectId(get_sending_late[countsending].projectid)}).toArray();
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
		obj_progress_week.progress_groupplan_id = get_sending_late[countsending]._id;
		obj_progress_week.projectid = get_sending_late[countsending].projectid;
		obj_progress_week.projectname =  getdetail_project[0].projectname;
		obj_progress_week.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		obj_progress_week.statusweek = get_sending_late[countsending].status;

		var day = moment(get_sending_late[countsending].updatetime).format("DD");
		var month = moment(get_sending_late[countsending].updatetime).format("MM");
		var year = moment(get_sending_late[countsending].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		var time = moment(get_sending_late[countsending].updatetime).format("HH:mm:ss");
		obj_progress_week.updatetime = day+"/"+month+"/"+year_th+" "+time; 

		//get weeknumber
		const get_progressweek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(get_sending_late[countsending]._id)}).toArray();
		obj_progress_week.weeknumber = get_progressweek[0].weeknumber;

		objsending.sending.push(obj_progress_week);
	}

	console.log(objsending);
	return objsending;
}

exports.get_detailprogress_sending = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[],
		status_late:""
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	obj_detail_progress.status_late = getdetail_progress_groupplan[0].statusstring;
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.reject_progressplan = async(progress_groupplan_id,comment_reject,document_reject)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);
	//update status progressgroup_plan 
	const update_progressgroup_plan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progress_groupplan_id)},
														{$set: {status:3,
																statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
																comment_reject:comment_reject,
																document_reject:document_reject
																}
													});

	//update status progressweeks
	const update_progressgroup_weeks = await db.collection("progress_weeks").updateOne({progress_groupplan_id:ObjectId(progress_groupplan_id)},
														{$set:{status:3,statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์"}});
	//get projectid from progress_weeks
	const getprojectid = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	let projectid_find = getprojectid[0].projectid;

	//update status weeks
	const getprogress_weeks = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	for(var countprogress = 0;countprogress < getprogress_weeks.length;countprogress++){
		console.log("update weekid: "+ getprogress_weeks[countprogress].weekid);
		const update_status_weeks = await db.collection("weeks").updateOne({"_id":ObjectId(getprogress_weeks[countprogress].weekid)},
														{$set:{status:3,
															   statusweek:3,
															   statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
															}});
	}
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	//insert comment_progressweek
	var obj_reject_commentprogressweek = {
		progress_groupplan_id:progress_groupplan_id,
		coment:comment_reject,
		documentation:"",
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const insert_comment_progressweek = await db.collection("comment_progressweek").insertOne(obj_reject_commentprogressweek);
	//get comment_id 
	const get_commentid = await db.collection("comment_progressweek").find({$and:[{progress_groupplan_id:progress_groupplan_id},{datetimestamp:dtsmp}]}).toArray();
	console.log("get_commentid: "+ get_commentid[0]._id);
	//insert collection notification_users
	var obj_notification={
		projectid:projectid_find,
		planid:"",
		comment_rejectid:get_commentid[0]._id,
		statusaction:3,
		statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
		read:"",
		progress_groupplan_id:progress_groupplan_id,
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_admin = await db.collection("notification_users").insertOne(obj_notification);

	console.log("data:reject progress week sucess.");
	return {data:"reject progress week sucess.",status:true};
}


exports.getmissing_progress = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_progress_missing = {
		progress_weeks:[]
	}
	//get bsm missing
	console.log("----------------missing---------------------");
	var count_missing = 0;
	const getproject_onbatch = await db.collection("projects").find({batchid:batchid}).toArray();

	//all project 
	for(var countproject =0;countproject < getproject_onbatch.length;countproject++){
		//check week on project 
		var projectid_get = getproject_onbatch[countproject]._id.toString();
		//var sort = {weeknumber:1};
		console.log("projectid: "+projectid_get);
		const check_week = await db.collection("weeks").find({projectid:projectid_get}).toArray();
		console.log("check_week.length: " +check_week.length);
		var obj_progressweek = {
			projectid:"",
			projectname:"",
			comapnyname:"",
			userprimary:"",
			weekid:"",
			weeknumber:0
		}
			//get day on this datenow 
			const getday_on_datenow = await db.collection("days").find({$and:[{datestring:datenow},{projectid:projectid_get}]}).toArray();
			if(getday_on_datenow.length == 0){
			}
			else{
				console.log("datestring now: "+ getday_on_datenow[0].datestring);
				console.log("weekid: "+getday_on_datenow[0].weekid);
				//get week on project 
				const getweek_on_datenow = await db.collection("weeks").find({"_id":ObjectId(getday_on_datenow[0].weekid)}).toArray();
				console.log("weeknumber: " + getweek_on_datenow[0].weeknumber);
				if(getweek_on_datenow[0].weeknumber <= 1){
					count_missing +=0;
				}else{
					//get less than week now on collection progress weeks
					//const get_lessthan_week = await db.collection("progress_weeks").find({$and:[{weeknumber:{$lt:getweek_on_datenow[0].weeknumber}},{projectid:projectid_get}]}).toArray();

					for(var count = 1;count < getweek_on_datenow[0].weeknumber;count++){
						//get less than week now on collection progress weeks
						const get_lessthan_week = await db.collection("progress_weeks").find({$and:[
																								{weeknumber:count},
																								{projectid:projectid_get}
																								]}).toArray();
						if(get_lessthan_week.length > 0){
							count_missing += 0;
						}else{
							//console.log("planid: "+getweek_on_datenow[count].planid);
							console.log("at week: "+ count);
							obj_progressweek.weeknumber = count;
							console.log("get_lessthan_week.length: "+get_lessthan_week.length);
							console.log("---------------------------------------------------");
							const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid_get)}).toArray();
							const get_userprimary = await db.collection("users").find({"_id":ObjectId(getdetailproject[0].Primaryresponsibility)}).toArray();
							obj_progressweek.projectid = projectid_get;
							obj_progressweek.projectname = getdetailproject[0].projectname;
							obj_progressweek.comapnyname = get_userprimary[0].companyname;
							obj_progressweek.userprimary = get_userprimary[0].fristname+" "+get_userprimary[0].lastname;
							obj_progressweek.weekid = "";
							obj_progress_missing.progress_weeks.push(obj_progressweek);
							//count_missing += 1;
						}
					}

					//console.log("get_lessthan_week.length: "+get_lessthan_week.length);
					//count_missing += 1;
					// for(var countprogress =0; countprogress < get_lessthan_week.length;countprogress++){
					// 	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid_get)}).toArray();
					// 	obj_progressweek.projectid = projectid_get;
					// 	obj_progressweek.projectname = getdetailproject[0].projectname;
					// 	obj_progressweek.comapnyname = getdetailproject[0].companyname;
					// 	obj_progressweek.weekid = "";
					// 	obj_progressweek.weeknumber = getweek_on_datenow[0].weeknumber;
					// 	obj_progress_missing.progress_weeks.push(obj_progressweek);
					// }
				}
			}
			console.log("--------------------");
		
	console.log("-------------------------------------");
	}
	console.log(obj_progress_missing);
	return obj_progress_missing;
}

exports.getprogress_week_reject = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_progress_reject = {
		progress:[]
	}
	//get progress week.
	const getprogressweek = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:3}]}).toArray();
	console.log("getprogressweek.length: "+getprogressweek.length);
	console.log("--------------------------------------------");
	for(var countweek =0;countweek < getprogressweek.length;countweek++){
		var obj_progress = {
			progress_groupplan_id:getprogressweek[countweek]._id,
			projectname:"",
			username:"",
			weeknumber:0,
			updatetime:""
		}
		//get detail project 
		const getdetailproject = await db.collection("projects").find({"_id":ObjectId(getprogressweek[countweek].projectid)}).toArray();
		//get users primary 
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetailproject[0].Primaryresponsibility)}).toArray();
		//get weeknumber 
		const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(getprogressweek[countweek]._id)}).toArray();
		console.log("getweeknumber.length: "+ getweeknumber.length);
		console.log("weeknumber: "+getweeknumber[0].weeknumber);
		obj_progress.weeknumber = getweeknumber[0].weeknumber;
		
		console.log("progress_groupid: "+getprogressweek[countweek]._id);
		console.log("username: "+getuserprimary[0].fristname+" "+getuserprimary[0].lastname);
		
		obj_progress.projectname = getdetailproject[0].projectname;
		obj_progress.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		var updatetime = getprogressweek[countweek].updatetime;
		obj_progress.updatetime = moment(updatetime).format("DD/MM/YYYY");
		console.log("updatetime: "+moment(updatetime).format("DD/MM/YYYY"));
		console.log("--------------------------------------------");
		obj_progress_reject.progress.push(obj_progress);
	}
	return obj_progress_reject;
}

exports.get_detail_progress_week_reject = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);

	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[]
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getweeknumber.length: "+ getweeknumber.length);
	console.log("weeknumber: "+getweeknumber[0].weeknumber);
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.approve_progress_week = async(progress_groupplan_id,late)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var status = 4;
	var statusstring = "ตรวจสอบแล้ว";
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);

	if(late == "ส่งล่าช้า"){
		status = 6;
		statusstring = "ตรวจแล้วล่าช้า";
	}else{
		 status = 4;
		 statusstring = "ตรวจสอบแล้ว";
	}

	//update status progress_groupplan
	const update_status_groupplan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progress_groupplan_id)},
																					  {$set:{status:status,
																					  		 statusstring:statusstring}});

	//upate status progress week
	const update_ststus_progressweek = await db.collection("progress_weeks").update({progress_groupplan_id:ObjectId(progress_groupplan_id)},
																					  {$set:{status:status,
																					  		 statusstring:statusstring}},
																					   {upsert:true,
																					   	multi:true});

	//get weeks 
	const getweeks_id = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	for(var countweek = 0;countweek < getweeks_id.length;countweek++){
		//update status week
		const update_status_week = await db.collection("weeks").updateOne({"_id":ObjectId(getweeks_id[countweek].weekid)},
																		   {$set:{status:status,
																		   		  statusstring:statusstring,
																		   		  statusweek:status}});
	}

	//insert collection notification_users
	var dtsmp = pastdatetime._created;
	var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
	var obj_notification={
		projectid:getweeks_id[0].projectid,
		planid:"",
		comment_rejectid:"",
		statusaction:4,
		statusstring:"ตรวจสอบแล้ว",
		read:"",
		progress_groupplan_id:progress_groupplan_id,
		datetimestamp:dtsmp,
		datetimestring:dtsmp_string
	}
	const add_notification_users = await db.collection("notification_users").insertOne(obj_notification);

	console.log("data:update status approve success.,progress_groupplan_id:"+progress_groupplan_id+",status:true");
	return {data:"update status approve success.",progress_groupplan_id:progress_groupplan_id,status:true};
}

exports.getall_bsm = async(batchid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_all_project = {
		projects:[]
	}

	//get project on batch 
	const getall_project = await db.collection("projects").find({batchid:batchid}).toArray();
	for(var countproject =0;countproject < getall_project.length;countproject++){
		var obj_project={
			projectid:"",
			projectname:"",
			companyname:"",
			userprimary:""
		}
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getall_project[countproject].Primaryresponsibility)}).toArray();

		console.log("projectid: "+getall_project[countproject]._id);
		console.log("projectname: "+getall_project[countproject].projectname);
		console.log("companyname: "+getuserprimary[0].companyname);
		console.log("userprimary: "+getuserprimary[0].fristname+" "+getuserprimary[0].lastname);

		obj_project.projectid = getall_project[countproject]._id;
		obj_project.projectname = getall_project[countproject].projectname;
		obj_project.companyname = getuserprimary[0].companyname;
		obj_project.userprimary = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		console.log("-----------------------------------------------------");
		obj_all_project.projects.push(obj_project);
	}

	console.log(obj_all_project.projects);
	return obj_all_project;
}

exports.get_progressweek_fromproject = async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var obj_weeks={
		weeknumbernow:0,
		weeks:[]
	}
	var date_nowfind = moment().format('DD/MMMM/YYYY');
	const getdaynow = await db.collection("days").find({$and:[{datestring:date_nowfind},{projectid:projectid}]}).toArray();
	if(getdaynow.length == 0){
		console.log("data: no have days on timesheet.");
		console.log("--------------------------------");
	}else{
		const getweeknow = await db.collection("weeks").find({"_id":ObjectId(getdaynow[0].weekid)}).toArray();
		obj_weeks.weeknumbernow = getweeknow[0].weeknumber;
	}
	const getallweek = await db.collection("weeks").find({projectid:projectid}).toArray();
	for(var count=0;count < getallweek.length;count++){
		var objweek={
			weeknumber:0,
			statusweek:0,
			progress_groupplan_id:""
		}
		const get_progress_groupplan_id = await db.collection("progress_weeks").find({weekid:ObjectId(getallweek[count]._id)}).toArray();
		if(get_progress_groupplan_id.length == 0){
			objweek.progress_groupplan_id = "";
		}else{
			objweek.progress_groupplan_id = get_progress_groupplan_id[0].progress_groupplan_id;
			console.log("progress week group plan id:"+ get_progress_groupplan_id[0].progress_groupplan_id);
		}
		console.log("week id:"+ getallweek[count]._id);
		console.log("at week:"+getallweek[count].weeknumber);
		console.log("-------------------------------------");
		objweek.weeknumber = getallweek[count].weeknumber;
		objweek.statusweek = getallweek[count].statusweek;

		obj_weeks.weeks.push(objweek);
	}
	// //get progress_groupplan_id
	// const get_progress_groupplan_id = await db.collection("progress_groupplan").find({$and:[{projectid:projectid},{status:4}]}).toArray();
	// console.log("get_progress_groupplan_id.length: "+get_progress_groupplan_id.length);
	// for(var countplan =0;countplan < get_progress_groupplan_id.length;countplan++){
	// 	var objweek = {
	// 		progress_groupplan_id:get_progress_groupplan_id[countplan]._id,
	// 		weeknumber:0,
	// 		status:0,
	// 		statusstring:""
	// 	}
	// 	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(get_progress_groupplan_id[countplan]._id)}).toArray();
	// 	objweek.weeknumber = getweeknumber[0].weeknumber;
	// 	objweek.status = getweeknumber[0].status;
	// 	objweek.statusstring = getweeknumber[0].statusstring;
	// 	obj_weeks.weeks.push(objweek);
	// }
	// console.log(obj_weeks);
	return obj_weeks;
}

exports.get_detail_progressweek_approve = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[]
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.getoverall_progressproject =async(projectid)=>{
	const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var obj_overall_project = {
		projectid:projectid,
		projectname:"",
		userprimary:"",
		projectvalue:0,
		plans:[],
		team:[],
		document:""
	}

	//get detail project 
	var getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	//get user primary 
	var getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetailproject[0].Primaryresponsibility)}).toArray();
	var obj_userprimary = {
		username:getuserprimary[0].fristname +" "+getuserprimary[0].lastname,
		position:getuserprimary[0].position,
		profileimage:getuserprimary[0].profileimage
	}
	obj_overall_project.team.push(obj_userprimary);
	obj_overall_project.projectname = getdetailproject[0].projectname;
	obj_overall_project.userprimary = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
	obj_overall_project.projectvalue = getdetailproject[0].projectvalue;
	obj_overall_project.document = getdetailproject[0].Documentation;


	//get all plan on project 
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("getallplan.length: " + getallplan.length);
	for(var countplan =0;countplan < getallplan.length;countplan++){
		var obj_plan={
			planname:getallplan[countplan].planname,
			durationorder:0,
			jobvalue:0,
			usedvalue:0,
			timelength:getallplan[countplan].timelength,
			usedtime:0,
			moneydifference:0,
			enforceemployee:0,
			averagewages:0,
			statusplan:""
		}
		//get durationid 
		const get_durationorder = await db.collection("duration").find({"_id":ObjectId(getallplan[countplan].durationid)}).toArray();
		//Sum total 
		var sumhours = 0;
		var sumtotalvalue = 0;
		var planid_find = getallplan[countplan]._id.toString();
		const getsumtotal = await db.collection("timesheet").find({planid:planid_find}).toArray();
		for(var counttimesheet = 0;counttimesheet < getsumtotal.length;counttimesheet++){
			sumhours += getsumtotal[counttimesheet].hours;
			sumtotalvalue += getsumtotal[counttimesheet].totalvalue;
		}

		console.log("planname: " + getallplan[countplan].planname);
		console.log("valueproject: " + getallplan[countplan].jobvalue);
		console.log("timelength: " + getallplan[countplan].timelength);
		console.log("duration: " + get_durationorder[0].durationorder);
		console.log("usetime: "+sumhours);
		console.log("usedvalue: "+sumtotalvalue);

		obj_plan.planname =  getallplan[countplan].planname;
		obj_plan.jobvalue = getallplan[countplan].jobvalue;
		obj_plan.durationorder = get_durationorder[0].durationorder;
		obj_plan.timelength =  getallplan[countplan].timelength;
		obj_plan.usedvalue = sumtotalvalue;
		obj_plan.usedtime = sumhours;
		obj_plan.moneydifference = getallplan[countplan].moneydifference;
		obj_plan.averagewages = getallplan[countplan].averagewages;
		obj_plan.enforceemployee = getallplan[countplan].enforceemployee;
		obj_plan.statusplan = getallplan[countplan].statusplan;
		obj_overall_project.plans.push(obj_plan);
		console.log("------------------------------------------------");
	}

	//get team 
	const getteam = await db.collection("users").find({projectid:projectid}).toArray();
	for(var countteam=0;countteam < getteam.length;countteam++){
		var obj_user = {
			username:getteam[countteam].firstname +" "+getteam[countteam].lastname,
			position:getteam[countteam].position,
			profileimage:getteam[countteam].profileimage
		}
		obj_overall_project.team.push(obj_user);
	}
	console.log(obj_overall_project);
	return obj_overall_project;
}



