const express = require('express');
const app = express();
//const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken'); 
const randtoken = require('rand-token');
var cors = require('cors');
const SECRET = "00f5vdd5r12fg1g[][dsdd122"; 

const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const xoauth2 = require('xoauth2');

var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var mime = require('mime');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
//var url = "mongodb://localhost:27017/DbTimesheet";
//var Dbname = "DbTimesheet";
let date = require('date-and-time');
var moment = require('moment');
var nodedatetime = require('node-datetime');
const multer = require('multer');
const dbservice = require('./Dbservice_timesheet.js');
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
var http = require('http');
var server = http.createServer(app);

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype ==='application/pdf') {
    cb(null, true);
  }else{
    cb(null, false);
  }
};
const fileFilter_doc = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype ==='application/pdf' || file.mimetype ==='application/msword' ||  file.mimetype==='application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.mimetype ==='application/vnd.openxmlformats-officedocument.wordprocessingml.template' || file.mimetype === 'application/vnd.ms-word.document.macroEnabled.12' || file.mimetype === 'application/vnd.ms-word.template.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.template' || file.mimetype === 'application/vnd.ms-excel.sheet.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel.template.macroEnabled.12' || file.mimetype ==='application/vnd.ms-excel.addin.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel.sheet.binary.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.presentation' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.template' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.slideshow' || file.mimetype === 'application/vnd.ms-powerpoint.addin.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint.presentation.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint.template.macroEnabled.12' || file.mimetype ==='application/vnd.ms-powerpoint.slideshow.macroEnabled.12'){
    cb(null, true);
  }else{
  	console.log(file);
    cb(null, false);
  }
};

////upload image 
const storage_image = multer.diskStorage({
	destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname, '../upload/profile'))
  },
	filename: function (req, file, cb) {
	var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	//var pastdatetime = nodedatetime.create(datenow);
	cb(null,datenow+"_"+file.originalname)
	}
});
const upload_image = multer({
 storage:storage_image,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

////upload file 
const storage_file = multer.diskStorage({
	destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname, '../upload/project'))//src/assets/upload/project
  },
	filename: function (req, file, cb) {
	var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	cb(null,"document"+datenow+"_"+file.originalname)
  }
});

const upload_file = multer({
 storage:storage_file,
 fileFilter: fileFilter_doc
});


const check_auth = (req,res,next) =>{
	console.log("token: "+req.body.authorization);
	console.log("userid: "+req.body.userid);
	var token = req.body.authorization;
	var userid = req.body.userid;
	const decoded = jwt.verify(token,SECRET);
	console.log("decoded: "+ decoded.userid);
	if(decoded.userid == userid){
		console.log("data: authorization success.");
		next();
	}else{
		res.json({
			data:"authorization falied.",
			status:false
		});
	}
}

app.listen(3000, () => {
  console.log('Start server at port 3000.');
  console.log("----------------");
});

app.post('/send', (req, res) => {
		//send email 	
			var email = 'atirach.in.dev@gmail.com';								
			var transporter = nodemailer.createTransport(smtpTransport({
				service: 'gmail',
		  		host: 'smtp.gmail.com',
				auth:{
					xoauth2:xoauth2.createXOAuth2Generator({
						user:'tedfund.app@gmail.com',
						clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
						clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
						refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
					})
				},
			}));

			 const output = `
			 	<h3 style="color:#128553">Create Account Success</h3>
			    <h4 style="color:#128553;margin-left:20px">bacthname</h4>
				<ul>  
					<li style="margin-bottom:5px">Name:fristname  lastname</li>
					<li style="margin-bottom:5px">Email: email</li>
					<li style="margin-bottom:5px">Phone: phone</li>
				</ul>
				<hr color='128553'>
			  `;
			//set mail options
			var mailOptions = {
				from:'tedfund.app <tedfund.app@gmail.com>',
				to:email,
				subject:'Create account succcess.',
				text:'Register TEDfund.',
				html:output
			}
			// sent email
			transporter.sendMail(mailOptions,function(err,res){
				if(err){
					console.log(err);
				}else{
					console.log('Email sent.');
					console.log(mailOptions);
				}
			});
	var result = {from:'tedfund.app <tedfund.app@gmail.com>',to:email,status:'success'}
	res.json(result);
});


app.get('/download/:filename', function(req, res){
  var filename = req.params.filename;
  var path = '../../frontend/src/assets/upload/project/';
  //var filename = path.basename(file);
  //var mimetype = mime.lookup(file);
  res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

  // var filestream = fs.createReadStream(file);
  // filestream.pipe(res);
  console.log("filename: "+path+filename);
  res.download(path+filename);
});

//update profile user 
app.post('/update/profile/user',upload_image.single('profileimage'),(req,res)=>{
	var userid = req.body.userid;
	if(req.file && userid){ 
		console.log(req.file);
		var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
		console.dir("path : " + datenow+"_"+req.file.originalname);
		var filename = datenow+"_"+req.file.originalname; 
		dbservice.updateprofile(userid,filename)
		.then(function(result){
			return res.end(datenow+"_"+req.file.originalname); 	
		});
	}else{
		console.log(req.file);
		console.log("data no file: " + req.file);
		return res.end('data: invalid file.');
	} 
});

//delete file 
app.post('/delete/file',(req,res)=>{
	var filename = req.body.filename;
	if(filename == ""){
		console.log("data: invalid filename.");
		res.json({
			data:"invalid filename.",
			status:false
		});
	}else{
		//\\work\\tedtimesheet\\backend\\Allservice\\image\\
		fs.unlink('./upload/project/'+filename, function(error) {
	    if (error) {
	        throw error;
		    }
		    console.log('data: delete '+filename);
		    dbservice.deletefile(filename)
		    .then(function(result){
		    	res.json(result);
		    });
		});
	}
});

//path upload file 
app.post("/upload/user/image",upload_image.single('profileimage'),(req,res)=>{
	var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	if(req.file){ 
		console.log(req.file);
		console.dir("path : " + datenow+"_"+req.file.originalname); 
		return res.end(datenow+"_"+req.file.originalname); 
	}else{
		console.log(req.file);
		console.log("data no file: " + req.file);
		return res.end('data: invalid file.');
	} 
});

app.use('/images', express.static(`${__dirname}/../upload`));

//path upload file 
app.post("/upload/user/project",upload_file.single('projectdoc'),(req,res)=>{
	if(req.file){ 
		var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
		console.dir("path : " + "document"+datenow+"_"+req.file.originalname); 
		return res.end("document"+datenow+"_"+req.file.originalname); 	
	}else{
		console.log("data no file: " + req.file);
		return res.end('');
	} 
});

//path upload multi file
app.post("/upload/multi/file/project",upload_file.single('projectdoc'),(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		console.log("data: invalid projectid.");
		res.end('data: invalid projectid.');
	}else{
		if(req.file){ 
			console.log("projectid: "+ projectid);
			var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
			console.dir("projectid: "+ projectid);
			console.dir("path : " + "document"+datenow+"_"+req.file.originalname); 
			var filename = "document"+datenow+"_"+req.file.originalname;
			dbservice.addfilename_project(projectid,filename)
			.then(function(result){
				res.end(result);
			});
			//return res.end("document"+datenow+"_"+req.file.originalname); 	
		}else{
			console.log("data no file: " + req.file);
			return res.end('data: invalid file.');
		}
	}
});

///first microservice

//path notification_users
app.post("/get/noti/users",check_auth,(req,res)=>{
	let projectid = req.body.projectid;
	if(projectid == ""){
		console.log("data: invalid projectid.");
		res.json({data: "invalid projectid."});
	}else{
		dbservice.getnotification_user(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/read/noti',check_auth,(req,res)=>{
	let noti_id = req.body.noti_id;
	if(noti_id == ""){
		console.log("data: invalid notification id.");
		res.json({	
			data:"invalid notification id.",
			status:false
		});
	}else{
		dbservice.update_readnoti(noti_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/comment/reject/data',check_auth,(req,res)=>{
	let comment_rejectid = req.body.comment_rejectid;
	if(comment_rejectid == ""){
		res.json({
			data:"invalid comment_rejectid.",
			status:false
		});
	}else{
		dbservice.getcommentrejectdata(comment_rejectid)
		.then(function(result){
			res.json(result);
		});
	}
});



app.post('/get/comment/reject/progress/week',check_auth,(req,res)=>{
	let comment_rejectid = req.body.comment_rejectid;
	if(comment_rejectid == ""){
		res.json({
			data:"invalid progress_groupplan_id.",
			status:false
		});
	}else{
		dbservice.getcommentreject_progressweek(comment_rejectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/notification/user',check_auth,(req,res)=>{
	let projectid = req.body.projectid;
	let planid = req.body.planid;
	let comment_rejectid = req.body.comment_rejectid;
	let statusaction = req.body.statusaction;
	let progress_groupplan_id = req.body.progress_groupplan_id;
	let status_string = req.body.statusstring;
	let noti_id = req.body.noti_id;
	console.log("req.body.statusstring: "+ req.body.statusstring);
	if(status_string == ""){
		console.log("data: invalid status string.");
		res.json({data:"invalid status string.",status:false});
	}else{
		if(status_string == "อนุมัติ Timsheet"){ 
			dbservice.update_readnoti(noti_id)
			.then(function(result){
				res.json({
					path:"/sme/dashboard",
					status:true
				});
			});
		}else if(status_string == "ปรับแก้เอกสาร"){
			dbservice.update_readnoti(noti_id)
			.then(function(result){
				res.json({
					path:"/sme/view-notification/",
					id:comment_rejectid,
					status:true
				});
			});
			
		}else if(status_string == "ปรับแก้ความก้าวหน้ารายสัปดาห์"){
			dbservice.update_readnoti(noti_id)
			.then(function(result){
				res.json({
				path:"/timesheet/view-notification/",
					id:comment_rejectid,
					status:true
				});
			});
		}else if(status_string == "ตรวจสอบแล้ว"){
			dbservice.update_readnoti(noti_id)
			.then(function(result){
				res.json({
					path:"/timesheet/dashboard",
					//id:progress_groupplan_id,
					status:true
				});
			});
		}else if(status_string == "ปรับแก้เอกสารโครงการ"){
			dbservice.update_readnoti(noti_id)
			.then(function(result){
				res.json({
					path:"/sme/view-notification/reject/file/",
					id:comment_rejectid,
					status:true
				});
			});
		}else{
			console.log("data: invalid status string.");
			res.json({data:"invalid status string.",status:false});
		}
	}
});

app.post('/get/reject/file',(req,res)=>{
	let projectid = req.body.projectid;
	let comment_rejectid = req.body.comment_rejectid;
	if(projectid == "" || comment_rejectid == ""){
		console.log("data: invalid projectid or comment_rejectid.");
		res.json({
			data:"invalid projectid or comment_rejectid.",
			status:false
		});
	}else{
		dbservice.get_reject_file(projectid,comment_rejectid)
		.then(function(result){
			res.json(result);
		});
	}
});

//get detail userprimary 
app.post('/get/detail/user/primary',check_auth,(req,res)=>{
	let userid = req.body.userid;
	if(userid == ""){
		console.log("data: invalid userid.");
		res.json({data:"invalid userid."});
	}else{
		dbservice.getdetailuserprimary(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

//create account
app.post('/createaccount',(req,res)=>{
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var phone = req.body.phone;
	var email = req.body.email;
	var password = req.body.password;
	var duedate = req.body.duedate;
	var batchid = req.body.batchid;
	var companyname = req.body.companyname;
	if (fristname == "" || lastname == "" || phone == "" || email == "" || password =="" || companyname == ""){
		var result ={
			data:"data invalid",
			status:false
		}
		res.json(result);
	}else{
		// var obj_pass ={
		// 	password:password
		// }
		var token = password;//jwt.sign(obj_pass, SECRET, { expiresIn: 300 });
		//var decode = jwt.verify(token,SECRET);
    	//res.json({token:token,decode:decode.password});
		dbservice.createaccount(fristname,lastname,phone,email,token,duedate,batchid,companyname)
		.then(function(result){
			res.json(result);
		});
	}
});

//login 
app.post('/login',(req,res)=>{
	var email = req.body.email;
	var password = req.body.password;
	if(email == "" || password == ""){
		var respond ={data:"invalid email or password."};
		res.json(respond);
	}else{
		console.log("password:" + password);
		var token = password;
		dbservice.loginuser(email,token)
		.then(function(result){
			res.json(result);
		});
	}
});

//first login 
app.post('/fill/in/userprimary',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	var userid = req.body.userid;
	if(batchid == "" || userid == ""){
		var respond ={data:"invalid batchid or userid."};
		res.json(respond);
	}else{
		dbservice.fillin_userprimary(userid,batchid)
		.then(function(result){
			res.json(result);
		});

	}
});

//dashboard
app.post('/dashboard',check_auth,(req,res)=>{
	var userid = req.body.userid;
	if (userid == ""){
		var result = {
			data:"invalid userid"
		}
		console.log("invalid userid");
		res.json(result);
	}else{
		dbservice.dashboard(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

//create project
app.post('/createproject',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectname = req.body.projectname;
	var projectvalue = req.body.projectvalue;
	var batchid = req.body.batchid;
	if(userid == "" || projectname == "" || projectvalue == "" || batchid == ""){
		console.log(userid);
		var result = {
			data:"data invalid",
			status:false
		}
		res.json(result);
	}else{
		dbservice.createproject(userid,projectname,projectvalue,batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

//updateuserprimary
app.post('/updateuserprimary',check_auth,(req,res)=>{
	//upload_image
	// if(req.file){ 
	// 	console.dir(req.file); 
	// 	return res.end('Thank you for the file'); 
	// } 
	// res.end('Missing file');
	var userid = req.body.userid;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var phone = req.body.phone;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	if(fristname == "" || lastname == "" || phone == "" || position == ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.updateuserprimary(userid,fristname,lastname,phone,position,profileimage)
		.then(function(result){
			res.json(result);
		});
	}
});

//get userprimary 
app.post('/getdatauserprimary',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid."};
		res.json(respond);
	}else{
		dbservice.getuserprimary(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

//add and update document project 
app.post('/updatedocumentproject',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var pathdocument = req.body.documentname;
	if (userid == "" || pathdocument == "" || projectid == ""){
		console.log("data: path document invalid");
		var result={data:"path document invalid",status:false}
		res.json(result);
	}else{
		dbservice.adddocument(userid,projectid,pathdocument)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/addplanorder',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid
	var planname = req.body.planname
	var jobvalue = req.body.jobvalue
	var durationid = req.body.durationid
	var timelength = req.body.timelength //days
	var averagewages = req.body.averagewages
	var moneydifference = req.body.moneydifference
	var enforceemployee = req.body.enforceemployee
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(userid == "" || planname == "" || jobvalue == "" || durationid == "" || timelength == "" || averagewages == "" || enforceemployee == "" || projectid == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.addplanorder(userid,projectid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getpallplanorder',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.getpallplanorder(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/plan/report',(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	if(projectid == "" || weeknumber == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.getpallplanorder_report(projectid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});


app.post('/getplanorder',check_auth,(req,res)=>{
	var planid = req.body.planid;
	if(planid == ""){
		var result={data:"invalid planid",status:false}
		res.json(result);
	}else{
		dbservice.getplanorder(planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/setamountorder',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var amountorder = req.body.amount;
	var projectid = req.body.projectid;
	if(amountorder == "" || userid == "" || projectid == ""){
		var respond = {data:"projectid or amount order invalid.",status:false};
		res.json(respond);
	}else{
		dbservice.setamountorder(amountorder,projectid,userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getsetamountorder',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respone ={data:"invalid projectid.",status:false};
		res.json(respone);
	}else{
		dbservice.getsetamountorder(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/updateplanorder',check_auth,(req,res)=>{
	var planid = req.body.planid;
	var planname = req.body.planname;
	var jobvalue = req.body.jobvalue;
	var durationid = req.body.durationid;
	var timelength = req.body.timelength;
	var averagewages = req.body.averagewages;
	var moneydifference = req.body.moneydifference;
	var enforceemployee = req.body.enforceemployee;
	if (planname == "" || jobvalue == "" || durationid == "" || timelength == "" || averagewages == "" || planid == "" || moneydifference == "" || enforceemployee == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.updateplanorder(planid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/add/amount/team',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var amount = req.body.amount;
	if(userid == "" || projectid == "" || amount == ""){
		var respond = {data:"invalid userid or projectid , amount.",status:false};
		res.json(respond);
	}else{
		dbservice.addamounteam(userid,projectid,amount)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/addteamwork',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	var email = req.body.email;
	var phone = req.body.phone;
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(userid == "" || fristname == "" || lastname == "" || position == "" || email == "" || phone == "" ||profileimage == ""){
		var result ={data:"data invalid",status:false};
		res.json(result);
	}else{
		dbservice.addteamwork(userid,projectid,fristname,lastname,position,profileimage,email,phone)
		.then(function(result){
			res.json(result);
		});
	}
});

//get all teamwork 
app.post('/getteamwork',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var userprimarykeyid = req.body.userid;
	if(projectid == ""){
		var result={data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.getteamwork(projectid,userprimarykeyid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getteamwhereid',check_auth,(req,res)=>{
	var userid = req.body.userid_update;
	if(userid == ""){
		var respond = {data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.getteamwhereid(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/updatedatauser',check_auth,(req,res)=>{
	var userid = req.body.userid_update;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	var email = req.body.email;
	var phone = req.body.phone;
	var password = req.body.password;
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if (userid == "" || fristname == "" || lastname == "" || position == ""){
		var result={data: "data invalid",status:false};
		res.json(result);
	}else{
		if(password == "not password"){
				dbservice.updatedatauser(userid,fristname,lastname,position,profileimage,email,phone)
				.then(function(result){
				res.json(result);
			});
		}else{
			dbservice.updatedatauser(userid,fristname,lastname,position,profileimage,email,phone)
				.then(function(result){
				res.json(result);
			});
		}
		
	}
});

//update document
app.post('/updatedocumentproject',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var documentname = req.body.documentname;
	if (projectid == "" || documentname == ""){
		var result={data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.updatedocumentproject(projectid,documentname)
		.then(function(result){
			res.json(result);
		});
	}
});

//sending project 
app.post('/sendproject',check_auth,(req,res)=>{
	var projectid = req.body.projectid
	if (projectid == ""){
		var result={data:"invalid projectid",status:false}
		res.json(result);
	}else{
		dbservice.sendproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

//add duration (งวดงาน)
app.post('/add/duration',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var durationorder = req.body.duration;
	console.log(durationorder);
	if (durationorder == ""){
		var respond = {data:"duration or month invalid.",status:false};
		res.json(respond);
	}else{
		dbservice.addduration(projectid,durationorder,userid)
		.then(function(result){
			res.json(result);
		});
	}

});

app.post('/get/amount/duration/order',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	if(userid == "" || projectid == ""){
		var respond = {data:"invalid userid and projectid."};
		res.json(respond);
	}else{
		dbservice.getamount_duration_order(userid,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/duration',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	//var userid = req.body.userid;
	if(projectid == ""){
		var respond = {data:"invalid projectid or userid."};
		res.json(respond);
	}else{
		dbservice.getallduration(projectid)
		.then(function(result){
			res.json(result);
		});
	}	
});


app.post('/update/duration',check_auth,(req,res)=>{
	var duration = req.body.duration;
	var projectid = req.body.projectid;
	if(duration == "" || projectid == ""){
		var respond = {data:"invalid duration.",status:false};
		res.json(respond);
	}else{
		dbservice.updateduration(duration,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});
app.post('/count/day',check_auth,(req,res)=>{
	dbservice.testcountdate()
	.then(function(result){
		//res.json();
	});
});

app.post('/dashboard/percent',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respone = {data:"invalid project."};
		res.json(respone);
	}else{
		dbservice.dashboard_percent(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/delete/user',check_auth,(req,res)=>{
	var del_userid = req.body.del_userid;
	if(del_userid == ""){
		var respond ={data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.delete_user(del_userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/delete/plan/',check_auth,(req,res)=>{
	var planid = req.body.planid;
	if(planid == ""){
		var respond = {data:"invalid planid."};
		res.json(respond);
	}else{
		dbservice.delete_plan(planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getdetailproject',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var userid = req.body.userid;
	if(projectid == "" || userid == ""){
		var respond = {data:"",status:false};
		res.json(respond);
	}else{
		dbservice.getdetailproject(projectid,userid)
		.then(function(result){
			res.json(result);
		});	
	}
});	

app.post('/update/password',check_auth,(req,res)=>{
	var userid = req.body.userid
	var password = req.body.password;
	if(userid == "" || password == ""){
		var respond = {data:"invalid userid or password."};
		res.json(respond);
	}else{
		dbservice.updatepassword(userid,password)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/amount/team',check_auth,(req,res)=>{
	var amount = req.body.amount;
	var projectid = req.body.projectid;
	if(amount == "" || projectid == ""){
		var respond = {data:"invalid amount or projectid."};
		res.json(respond);
	}else{
		dbservice.updateamountteam(projectid,amount)
		.then(function(result){
			res.json(result);
		});
	}
});
//
app.post('/get/amount/team',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid amount or projectid."};
		res.json(respond);
	}else{
		dbservice.getamountteam(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/last/activity/:userid',check_auth,(req,res)=>{
	var userid = req.body.userid;
	if(userid == ""){
		var respond ={data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.getlastactivity()
		.then(function(result){
			//res.json();
		});
	}
});

app.post('/check/last/activity',check_auth,(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	if(userid == "" || projectid == ""){
		var respond = {data:"invalid userid or projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.checklastdraft(userid,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/planorder/dashboard',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getplandashboard(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/weeks/ofplan',check_auth,(req,res)=>{
	var planid = req.body.planid;
	var projectid = req.body.projectid;
	if(planid == "" || projectid == ""){
		var respond = {data:"invalid planid or projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getallweeksofplan(projectid,planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/daily/progress',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid"};
		res.json(respond);
	}else{
		dbservice.dailyprogress(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/days/onweek/timesheet',check_auth,(req,res)=>{
	var weekid = req.body.weekid;
	var dayid = req.body.dayid;
	var projectid = req.body.projectid;
	var planid = req.body.planid;
	var weeknumber = req.body.weeknumber;
	if(weekid =="" || dayid == "" || projectid == "" || planid == ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.getweektimesheet(dayid,projectid,planid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/add/timesheet/days',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var planid = req.body.planid;
	var weekid = req.body.weekid;
	var day1_id = req.body.day1_id;
	var day2_id = req.body.day2_id;
	var day3_id = req.body.day3_id;
	var day4_id = req.body.day4_id;
	var day5_id = req.body.day5_id;
	var day6_id = req.body.day6_id;
	var day1 = req.body.day1;
	var day2 = req.body.day2;
	var day3 = req.body.day3;
	var day4 = req.body.day4;
	var day5 = req.body.day5;
	var day6 = req.body.day6;
	if(projectid == "" || planid == "" || weekid == ""){
		var respond = {data:"invalid data."};
		res.json(respond);
	}else{
		dbservice.addtimesheet(projectid,planid,weekid,day1,day2,day3,day4,day5,day6,day1_id,day2_id,day3_id,day4_id,day5_id,day6_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/get/batchname/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid",status:false};
		res.json(respond);
	}else{
		dbservice.getbatchname(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	if(projectid == "" || weeknumber == ""){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getdetail_progress_week(projectid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/timesheet/all/users',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	if(projectid == "" || weeknumber == ""){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_timesheet_allusers(projectid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/send/progress/week',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	var plans = req.body.plans;
	var late = req.body.late;
	if(projectid == "" || weeknumber == "" || plans.length == 0){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.send_progress_week(projectid,weeknumber,plans,late)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/progress/week/after/reject',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	var plans = req.body.plans;
	var late = req.body.late;
	if(projectid == "" || weeknumber == "" || plans.length == 0){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.update_progress_week(projectid,weeknumber,plans,late)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/allplan/order/before/approve',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getallplanorder_before_approve(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/plan/on/now/week',check_auth,(req,res)=>{
	let projectid = req.body.projectid;
	if(projectid == ""){
		res.json({
			data:"invalid projectid.",
			status:false
		});
	}else{
		dbservice.getplan_on_now_week(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

/////////////////////////////////ADMIN////////////////////////////////////////////

//update password admin 
app.post('/update/password/admin',check_auth,(req,res)=>{
	let userid = req.body.userid;
	let pass = req.body.password;
	if(pass == ""){
		console.log("data: invalid userid.");
		return {data:"invalid userid.",status:false}
	}else{
		var objpass = {
			password:pass
		}
		var token = jwt.sign(objpass,SECRET);
		dbservice.updatepassword(userid,token)
		.then(function(result){
			res.json(result);
		});
	}
});

//reject file to user 
app.post('/reject/comment/file',check_auth,(req,res)=>{
	let projectid = req.body.projectid;
	let comment = req.body.comment;
	let filename = req.body.filename;
	let filename_reject = req.body.filename_reject;
	if(projectid == ""){
		console.log("data: invalid projectid.");
		res.json({
			data:"invalid projectid.",
			status:false
		});
	}else{
		dbservice.reject_file(projectid,comment,filename,filename_reject)
		.then(function(result){
			res.json(result);
		});
	}
});

// get all user 
app.post('/get/all/users',check_auth,(req,res)=>{
	let batchid = req.body.batchid;
	if(batchid == ""){
		console.log("data: invalid batchid.");
		res.json({
			data:"invalid batchid.",
			status:false
		});
	}else{
		dbservice.get_allusers(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

//path notification_admin
app.post("/get/noti/admin",check_auth,(req,res)=>{
	let adminid = req.body.adminid;
	if(adminid == ""){
		console.log("data: invalid adminid.");
		res.json({data: "invalid adminid."});
	}else{
		dbservice.getnotification_admin(adminid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/commented/reject',(req,res)=>{
	let projectid = req.body.projectid;
	let planid = req.body.planid;
	if(projectid == "" || planid ==""){
		console.log("data: invalid data.");
		res.json({
			data:"invalid data.",
			status:false
		});
	}else{
		dbservice.get_commented(projectid,planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/loginadmin',(req,res)=>{
	var username = req.body.username;
	var password = req.body.password;
	if(username == "" || password == ""){
		console.log("data: invalid username or password.");
		var response = {data:"invalid username or password"};
		res.json(response);
	}else{
		dbservice.getuser(username,password)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/notification/admin',check_auth,(req,res)=>{
	let projectid = req.body.projectid;
	let progress_groupplan_id = req.body.progress_groupplan_id;
	let statusstring = req.body.statusstring;
	let noti_id = req.body.noti_id;
	if(statusstring == "ส่งความก้าวหน้า"){
		dbservice.update_readnoti_admin(noti_id).
		then(function(result){
			res.json({
				path:"/admintimesheet/sme-detail/success/",
				id:progress_groupplan_id
			});
		});
	}else if(statusstring == "ส่งเอกสารแล้ว"){
		dbservice.update_readnoti_admin(noti_id).
		then(function(result){
			res.json({
				path:"/admin/project-detail/pending/",
				id:projectid
			});
		});
	}else{
		res.json({
			data:"invalid statusstring.",
			status:false
		});
	}
});

app.post('/createbatch',check_auth,(req,res)=>{
	var admininsert = req.body.userid
	var adminupdate = ""
	var modelname = req.body.modelname
	var piscalyear = req.body.piscalyear
	var numberofoperators = req.body.numberofoperators

	var startingdate = req.body.startingdate
	var dts_startingdate = Date.parse(startingdate);

	var duedate = req.body.duedate
	var dts_duedate = Date.parse(duedate);

	var alreadybusinessman = 0
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var updatetime = 0
	var status = "1"
	var active = "1" 
	if (modelname == "" || piscalyear == "" || numberofoperators == "" || duedate == "" || startingdate == ""){
		var result={data:"data invalid.",status:false}
		res.json(result);
	}else{
		dbservice.addbatch(admininsert,adminupdate,modelname,piscalyear,numberofoperators,startingdate,dts_startingdate,duedate,dts_duedate,alreadybusinessman,datenow,pastdatetime,updatetime,status,active)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getallbatch',check_auth,(req,res)=>{
	dbservice.getallbatch()
	.then(function(result){
		res.json(result);
	});
});

app.post('/createlink',check_auth,(req,res)=>{ 
	var batchid = req.body.batchid
	dbservice.createlink(batchid)
	.then(function(result){
		res.json(result);
	});
});

app.post('/getdetail',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var response = {data:"invalid batchid",status:false};
		res.json(response);
	}else{
		dbservice.getdetailbatch(batchid)
		.then(function(result){
			res.json(result);	
		});
	}
});

app.post('/update/duedate',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	var duedate = req.body.duedate;
	if(duedate == ""){
		var respond = {data:"invalid duedate",status:false};
		res.json(respond);
	}else{
		var dts_duedate = Date.parse(duedate);
		dbservice.updateduedatetime(batchid,dts_duedate)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getprojectsending',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var response = {data:"invalid batchid",status:false};
		res.json(response);
	}else{
		dbservice.getprojectsending(batchid)
		.then(function(result){
			res.json(result);	
		});
	}
});

//check project status sending 
app.post('/investprojectsending',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond={data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getdetailprpoject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/status/projectinvest',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var status = req.body.status;
	if(projectid == "" || status == ""){
		var respond = {data:"invalid projectid or status.",status:false};
		res.json(respond);
	}else{
		dbservice.update_status_projectinvest(projectid,status)
		.then(function(result){
			res.json(result);
		});	
	}
});


app.post('/rejectproject',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	var comment = req.body.comment;
	var documentcomment = req.body.document;
	if(projectid === "" || comment === "" || documentcomment === ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.updatecommentproject(projectid,comment,documentcomment)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/approvepeoject',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid === ""){
		var respond = {data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.approveproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});


app.post('/getproject/status/reject',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		res.json();
	}else{
		dbservice.getprojectstatusreject(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/create/weeks',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks(projectid)
		.then(function(result){
			//res.json();
		});
	}
});


app.post('/get/all/weeks',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false}
		res.json(respond);
	}else{	
		dbservice.getallweeks(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/reject/planorder',check_auth,(req,res)=>{
	var planid = req.body.planid;
	var comment = req.body.comment;
	var pahtfile = req.body.pahtfile;
	if(planid == ""){
		var respond = {data:"invalid planid."};
		res.json(respond);
	}else{
		dbservice.rejectproject(planid,comment,pahtfile)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/data/detail/batch',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"incalid batchid.",status:false};
		res.json(respond);	
	}else{
		dbservice.getdata_detailbatch(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/project/status/approve',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid",status:false};
		res.json(respond);
	}else{
		dbservice.getproject_approve(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/create/weeks/test2',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks_test(projectid)
		.then(function(result){
			//res.json(result);
		});
	}
});

app.post('/create/weeks',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/report/dashboard',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getreport_dashboard(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/week/sending',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_progress_sending(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week/sending',check_auth,(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detailprogress_sending(progress_groupplan_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/reject/progressplan/',check_auth,(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	var comment_reject = req.body.comment_reject;
	var document_reject = req.body.document_reject;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.reject_progressplan(progress_groupplan_id,comment_reject,document_reject)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/missing',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getmissing_progress(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/week/reject',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getprogress_week_reject(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});	

app.post('/get/detail/progress/week/reject',check_auth,(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detail_progress_week_reject(progress_groupplan_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/approve/progress/week',check_auth,(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	var late = req.body.late;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.approve_progress_week(progress_groupplan_id,late)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/bsm',check_auth,(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getall_bsm(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/weeks/from/project',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_progressweek_fromproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week/approve',check_auth,(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detail_progressweek_approve(progress_groupplan_id)
		.then(function(result){
			res.json(result);	
		});
	}
});

app.post('/get/overall/progress/project',check_auth,(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getoverall_progressproject(projectid)
		.then(function(result){
			res.json(result);	
		});
	}
});
