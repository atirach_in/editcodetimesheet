var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
let dateandtime = require('date-and-time');
var moment = require('moment');
var strtotime = require('strtotime');
var DateDiff = require('date-diff');
let date = require('date-and-time');
var nodedatetime = require('node-datetime');
var dbname = "DbTimesheet";
exports.getuser = async(username,password)=>{
	var username = username;
	var password = password;
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	 try {
	 	//update login time 
	 	  const updatetimelogin = await db.collection("accountadmin").updateOne(
								{username:username},
								{ $set: {lastlogin:pastdatetime._created}}, 
								function(err, res) {
								    if (err) throw err;
								    console.log("data: update lastlogin success");
								    console.log("-------------------");
								 });
	 	//check username and password
          const res = await db.collection("accountadmin").find({$and:[{username:username},{password:password}]}).toArray();
          if(res.length == 0){
          	response = {data:"invalid username or password",status:false};
          	return response;
          }else{
          	response = res;
          	return response;
          }
        }
        catch(err) {
        	console.log(err);
            client.close();
        }
}

exports.addbatch = async(admininsert,adminupdate,modelname,piscalyear,numberofoperators,startingdate,dts_startingdate,duedate,dts_duedate,alreadybusinessman,datenow,pastdatetime,updatetime,status,active)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
		//Create batch
		var userobj={
			modelname:modelname,
			piscalyear:piscalyear,
			numberofoperators:numberofoperators,
			alreadybusinessman:alreadybusinessman,
			startingdate:dts_startingdate,
			duedate:dts_duedate,
			Insdate:pastdatetime._created,
			updatetime:updatetime,
			admininsert:admininsert,
			adminupdate:adminupdate,
			status:status,
			active:active
		}; 
		const createbatch = await db.collection("batch").insertOne(userobj, function(err, res) {
			if (err) throw err;
				console.log("data: create batch success.");
				console.log("-------------------");
		});
		var resultreturn = {data:"create batch success.",status:true}
		return resultreturn;
}

exports.getallbatch = async()=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	const getbatchs = await db.collection("batch").find().toArray();
	return getbatchs;
}

exports.createlink = async(batchid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	const getlink = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var resultjson={
		data:"create link success.",
		link:"localhost:3000/register/"+getlink[0]._id,
		status:true
	}
	return resultjson;
}




exports.getdetailbatch = async(batchid)=>{
	var countbsm;
	var projectpending;
	var projectreject;
	var projectsuccess;
	var respond ={
		countbsm:0,
		projectpending:"",
		percentpending:0,
		projectreject:"",
		percentreject:0,
		projectsuccess:"",
		percentsucess:0,
		duedate:"",
		differancetime:0
	}
	
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	countbsm = getdetailbatch[0].numberofoperators;
	respond.countbsm = countbsm;
	console.log("count bsm: "+countbsm);

	//get todays date
	var today = moment();
	var duedate = moment(getdetailbatch[0].duedate).format("D/MM/YYYY");
	respond.duedate = duedate;
	console.log("duedate: "+duedate);
	console.log("Day: "+ moment(getdetailbatch[0].duedate).format("D"));
	var day =  moment(getdetailbatch[0].duedate).format("D");
	console.log("Month: "+ moment(getdetailbatch[0].duedate).format("MM"));
	var month = moment(getdetailbatch[0].duedate).format("MM")
	console.log("Year: " + moment(getdetailbatch[0].duedate).format("YYYY"));
	var year = moment(getdetailbatch[0].duedate).format("YYYY");
	var cal_year = year-543;
	var set_duedate = moment(cal_year+"/"+month+"/"+day, 'YYYY-MM-DD');
	var diffdate = set_duedate.diff(today, 'day');
	console.log("diff date: "+diffdate);
	respond.differancetime = diffdate;

	//project status sending
	const getprojectpending = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"2"}]}).toArray();
	projectpending = getprojectpending.length;
	respond.projectpending = projectpending;
	respond.percentpending = (projectpending * 100) / countbsm;
	console.log("project pending: "+projectpending);

	//project status reject 
	const getprojectreject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"3"}]}).toArray();
	projectreject = getprojectreject.length;
	respond.projectreject = projectreject;
	respond.percentreject = (projectreject * 100)/countbsm;
	console.log("project reject: "+projectreject);

	//project status success 
	const getprojectsuccess = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"4"}]}).toArray();
	projectsuccess = getprojectsuccess.length;
	respond.projectsuccess = projectsuccess;
	respond.percentsucess = (projectsuccess * 100) / countbsm;
	console.log("project sucess: "+projectsuccess);
	//count down days

	return respond;
}

exports.updateduedatetime = async(batchid,duedate) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	const updateduedate =  db.collection("batch").updateOne(
										{"_id":ObjectId(batchid)},
										{$set:{duedate:duedate}});
	console.log("duedate update: "+ duedate);
	console.log("batchid :" + batchid);
	return {data:"update duedate success.",batchid:batchid,status:true};
}

exports.getdata_detailbatch = async(batchid) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	const getdetail = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	if(getdetail.length == 0){
		var respond = {data:"invalid batchid.",status:false};
		return respond;
	}else{
		console.log(getdetail[0]);
		return getdetail[0];
	}
}

exports.getprojectsending = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var status = "2";//reject 
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status}]}).toArray();
	for(var i = 0; i < getproject.length; i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:2,
			statusstring:"ส่งข้อมูล",
			updatetime:""
		};
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("-------------------------");
	return objprojects;
}


exports.getdetailprpoject = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(dbname);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var projectobj ={
		batchname:"",
		piscalyear:"",
		numberofoperators:"",
		startingdate:0,
		duedate:0,
		projectname:"",
		primaryuser:"",
		datetime:0,
		documentation:"",
		projectvalue:0,
		planorder:[],
		team:[],
		lastupdatetime:""
	}
	//Primaryresponsibility
	const project = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();

	//get bacth name 
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(project[0].batchid)}).toArray();
	projectobj.batchname = getdetailbatch[0].modelname;
	projectobj.piscalyear = getdetailbatch[0].piscalyear;
	projectobj.numberofoperators = getdetailbatch[0].numberofoperators;
	projectobj.startingdate = getdetailbatch[0].startingdate;
	projectobj.duedate = getdetailbatch[0].duedate;

	console.log("batchname: "+ projectobj.batchname);
	console.log("piscalyear: "+ projectobj.piscalyear);
	console.log("numberofoperators: " + projectobj.numberofoperators);
	console.log("startingdate: "+ projectobj.startingdate);
	console.log("duedate: "+projectobj.duedate);

	//cal between time 
	const gettimeproject = await db.collection("batchbusinessman").find({"projectid":ObjectId(projectid)}).toArray();
	var lastprojecttime = moment(Math.ceil(gettimeproject[0].updatetime));
	var thistime = moment(pastdatetime._created);
	console.log("lastprojecttime: "+lastprojecttime);
	console.log("this time: "+pastdatetime._created);

	//date project
	var dateproject_moment = moment(lastprojecttime).format('DD/MM/YYYY');
	var daypro = moment(lastprojecttime).format('DD');
	var monthpro = moment(lastprojecttime).format('MM');
	var yearpro = moment(lastprojecttime).format('YYYY');
	var hrpro = moment(lastprojecttime).format('HH');
	var minutepro = moment(lastprojecttime).format('mm');
	var sspro = moment(lastprojecttime).format('ss');

	//date now 
	var datenow_moment = moment(thistime).format('DD/MM/YYYY');
	var daynow = moment(thistime).format('DD');
	var monthnow = moment(thistime).format('MM');
	var yearnow = moment(thistime).format('YYYY');
	var hrnow = moment(thistime).format('HH');
	var minutenow = moment(thistime).format('mm');
	var ssnow = moment(thistime).format('ss');

	var datepro = new Date(yearpro, monthpro, daypro);
	var datenow = new Date(yearnow, monthnow, daynow);
	var diff = new DateDiff(datenow,datepro);
	console.log("diff days: "+diff.days());
	console.log("diff minute: "+diff.minutes());
	console.log("diff hours: "+diff.hours());
	console.log("--------------------------");
	if(diff.minutes() >= 1440){
		var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
		projectobj.lastupdatetime = days;
		console.log("calculate days:" + days);
	}else if (diff.minutes() >= 60){
		var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
		projectobj.lastupdatetime = days;
		console.log("calculate hours:" + hours);
	}else if(diff.minutes() <= 59){
		var minutes = (diff.minutes()) +" "+ "นาที";
		projectobj.lastupdatetime = minutes;
		console.log("calculate minutes:" + minutes);
	}
	console.log("--------------------------");
	// diff.years(); // ===> 1.9
	// diff.months(); // ===> 23
	// diff.days(); // ===> 699


	// diff.weeks(); // ===> 99.9
	// diff.hours(); // ===> 16776
	// diff.minutes(); // ===> 1006560
	// diff.seconds(); // ===> 60393600
	

	const userprimary = await db.collection("users").find({"_id":ObjectId(project[0].Primaryresponsibility)}).toArray();
	//add user primary to frist person in team 
	for(var i = 0; i < userprimary.length; i++){
		var userobj = userprimary[i];
		projectobj.team.push(userobj);
	}
	console.log("project name: "+ project[0].projectname);
	projectobj.projectname = project[0].projectname;
	console.log("user: "+ userprimary[0].fristname +" "+userprimary[0].lastname);
	projectobj.primaryuser = userprimary[0].fristname +" "+userprimary[0].lastname;
	console.log("datetime: "+ project[0].senddatetime);
	projectobj.datetime = project[0].senddatetime;
	console.log("documentation: "+project[0].Documentation);
	projectobj.documentation = project[0].Documentation;
	console.log("project value: "+project[0].projectvalue);
	projectobj.projectvalue = project[0].projectvalue;
	//plan order
	const planorders = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("-----planorders: "+ planorders.length +"-----");
	for(var i = 0; i < planorders.length; i++){
		var planobj = {
			planid:planorders[i]._id,
			planname:planorders[i].planname,
			durationid:planorders[i].durationid,
			duration:0,
			planvalue:planorders[i].jobvalue,
			timelength:planorders[i].timelength
		};
		const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(planorders[i].durationid)}).toArray();
		planobj.duration = getdurationnumber[0].durationorder;
		console.log(planobj);
		projectobj.planorder.push(planobj);
	}
	console.log("---------------------");
	//team work 
	const team = await db.collection("users").find({projectid:projectid}).toArray();
	console.log("----------team----------");
	for(var i = 0;i < team.length; i++){
		var teamobj = team[i];
		console.log(teamobj);
		projectobj.team.push(teamobj);
	}
	console.log("--------------------");
	return projectobj;
}

exports.update_status_projectinvest = async(projectid,status)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var respond = {stausupdate:[]};
	console.log("status: "+status);
	console.log("projectid: "+projectid);
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	//update status project collection BatchBusinessman
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:status,
											   updatetime:pastdatetime._created}})
										.then(function(result){
											respond.stausupdate.push({data:"update batchbusinessman.",status:true});
										});

	//update status project collection project 
	const updatestatusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{$set:{status:status}}).then(function(result){
											respond.stausupdate.push({data:"update projects.",status:true});
										});
	 return respond;
}

exports.insertcommentreject = async(projectid,comment,documentcomment)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var respond = {data:[]};
	//update comment and document to BatchBusinessman collection
	const updatecomment = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{commentreject:comment,
											   documentreject:documentcomment}})
										.then(function(result){
											respond.data.push({data:"update comment reject.",status:true});
										});
	return respond;									

}

exports.approveproject = async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var respond = {stausupdate:[]};
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	//approve status project collection BatchBusinessman
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:"4",
											   updatetime:pastdatetime._created}})
										.then(function(result){
											respond.stausupdate.push({data:"approve batchbusinessman.",status:true});
										});

	//approve status project collection project 
	const updatestatusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{$set:{status:"4"}}).then(function(result){
											respond.stausupdate.push({data:"approve projects.",status:true});
										});
	return respond;									
}

// get all project status reproject 
exports.getprojectstatusreject = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var status = "3";//reject 
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status}]}).toArray();
	for(var i = 0; i < getproject.length; i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:3,
			statusstring:"ปรับแก้",
			updatetime:""
		};
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("-------------------------");
	return objprojects;
}

//get project status approve
exports.getproject_approve = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var status_approve = "4";
	var objprojects={
		batchid:batchid,
		project:[]
	}
	//get project 
	const getproject = await db.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:status_approve}]}).toArray();
	for(var i=0; i < getproject.length;i++){
		var objproject = {
			batchid:batchid,
			projectid:getproject[i].projectid,
			projectname:"",
			userprimary:"",
			status:4,
			statusstring:"อนุมัติโครงการ",
			updatetime:""
		};
		//find fristname-lastname 
		var projectid = getproject[i].projectid;
		const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid = getprojectdetail[0].Primaryresponsibility;
		const getusername = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
		var username = getusername[0].fristname +" "+getusername[0].lastname;
		objproject.projectname = getprojectdetail[0].projectname;
		objproject.userprimary = username;
		var day = moment(getproject[i].updatetime).format("DD");
		var month = moment(getproject[i].updatetime).format("MM");
		var year = moment(getproject[i].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		objproject.updatetime = day+"/"+month+"/"+year_th+" "+moment(getproject[i].updatetime).format("HH:mm:ss");
		objprojects.project.push(objproject);
	}
	console.log(objprojects);
	console.log("------------------------");
	return objprojects;
}

//create weeks 
exports.createweeks_test = async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	const getplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var i = 0;i < getplan.length; i++){
		//get month from duration 
		const getduraion = await db.collection("duration").find({"_id":ObjectId(getplan[i].durationid)}).toArray();
		console.log("plan name:"+ getplan[i].planname);
		console.log("duration: "+ getduraion[0].durationorder);
		console.log("month: "+ getduraion[0].month);
		console.log("weeks: "+ (getduraion[0].month * 4));
		var week = (getduraion[0].month * 4);
		var daysunday = week; //sunday on all week.
		var days = ((getduraion[0].month * 4) * 6) + daysunday;
		var dayinweek = (days / week);
		console.log("days add sunday: " + days);
		console.log("days not sunday: " + ((getduraion[0].month * 4) * 6));
		var daynotsunday = ((getduraion[0].month * 4) * 6);
		//start date
		var startday = moment().day(7+1);
		console.log("start date timestamp : "+ startday);
		console.log("start day: "+ startday.format("dddd"));
		console.log("start at date: "+ startday.format("D"));
		console.log("start at month: "+startday.format("MMMM"));
		console.log("start at year: "+ startday.format("YYYY"));
		console.log("-----------------------------------------");
		var projectsuccess = moment(startday).add(days, 'days');
		var countdays = days;
		var countweek = 1;
		for(var d = 0; d <= days; d++){
			var modnumber = d % 7;
			if(modnumber == 0){
				console.log("week :"+ countweek);
				if(countweek > week){
					console.log("week finish.");
				}else{
					//insert week 
					var objweek = {
						durationid:"",
						projectid:"",
						weeknumber:countweek,
						monthnumber:0,
						daysinweek:6,
						weekstartdate:0,
						weekenddatetimestamp:0,
						weekvalue:0,
						updatetime:pastdatetime._created,
						statusweek:1
					}
					objweek.durationid = getduraion[0]._id;
					objweek.projectid  = projectid;
					const insertweek = await db.collection("weeks").insertOne(objweek);
					const getweekid = await db.collection("weeks").find({$and:[
											{durationid:getduraion[0]._id},
											{projectid:projectid},
											{weeknumber:countweek}
											]}).toArray();
					//insert weeksplanorder
					var objdayplan = {
						weekid:getweekid[0]._id,
						planid:getplan[i]._id
					}
					const insertdayplanorder = await db.collection("weeksplanorder").insertOne(objdayplan);
					for(var j = 1;j<=7;j++){

						var startdate = moment().day(7+d);
						

						var date = moment(startdate).add(j, 'days');
						if(date.format("dddd") == "Sunday"){
							console.log("Holiday.");
						}else{
							console.log(date.format("dddd D/MMMM/YYYY"));
							var objday = {
								weekid:"",
								daynumber:0,
								datetimestamp:0,
								datestring:"",
								daystring:"",
								statusopen:0	
							}
							objday.weekid = getweekid[0]._id;
							objday.daynumber = j;
							objday.datetimestamp = date;
							objday.datestring = date.format("D/MMMM/YYYY");
							objday.daystring = date.format("dddd");
							//insert day 
							const insertday = await db.collection("days").insertOne(objday); 
							countdays +=1;
						}
					}
				}
				countweek += 1;
				console.log("-----------------------------");
			}else{
				//not equal 0
			}
		}
		console.log("project success: " + projectsuccess.format("dddd D/MMMM/YYYY"));
	}
}

exports.createweeks = async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	//check double 
	const checkweekofprojectid = await db.collection("weeks").find({projectid:projectid}).toArray();
	if(checkweekofprojectid.length > 0){
		return {data:"this project approved.",status:false};
	}

	var startdate_duration = "";
	//get sort array  
	var sort = {durationid:1};
	const getsort = await db.collection("planorders").find({projectid:projectid}).sort(sort).toArray();
	for(var z =0; z < getsort.length;z++){
		console.log("sort plan of durationid: "+ getsort[z]._id);
	}
	console.log("--------------------");
	//get planorders from projectid 
	const getplanorders = await db.collection("planorders").find({projectid:projectid}).sort(sort).toArray();
	//await db.collection("planorders").find({projectid:projectid}).toArray();
	var countweeks =0;
	var durationorder = 0;
	var last_startdate_duration = 0;
	var startdate_incodition ="";
	for(var plan =0;plan < getplanorders.length;plan++){
		console.log("planid: "+ getplanorders[plan]._id);
		console.log("time length: " + getplanorders[plan].timelength);
		const getdaysof_duration = await db.collection("duration").find({"_id":ObjectId(getplanorders[plan].durationid)}).toArray();
		var timelength = getdaysof_duration[0].month * 30;//getplanorders[plan].timelength;
		var cal_weeks = timelength / 6;
		console.log("durationorders: " + getplanorders[plan].durationorder);
		console.log("cal_weeks float :"+cal_weeks);
		console.log("cal_weeks: " + Math.floor(cal_weeks));
		console.log("cal_weeks Math ceil: " + Math.ceil(cal_weeks));
		var cal_weeks_ceil = Math.ceil(cal_weeks);//ปัดขึ้นหมด
		var days_not_decimal = Math.floor(cal_weeks) * 6;
		var days_decimal = timelength - days_not_decimal;
		console.log("days_not_decimal: " + days_not_decimal);
		console.log("days_decimal: "+ days_decimal);
		var startdate = "";
		if(startdate_duration == ""){
			startdate = moment().day(7+1);
			startdate_incodition = startdate;
		}else if( durationorder == getplanorders[plan].durationorder){
			startdate = last_startdate_duration;
			startdate_incodition = startdate;
			countweeks =0;
		}
		else if(startdate_duration != ""){
			startdate = startdate_duration;
			startdate_incodition = startdate;
		}
		console.log("start frist date on duration: "+ startdate.format("D dddd/MM/YYYY"));
		console.log("---------------------------");
		var partdate = "";
		for(var i =1; i <= cal_weeks_ceil;i++){
			countweeks+=1;
			var obj_week = {
				durationid:getplanorders[plan].durationid,
				projectid:getplanorders[plan].projectid,
				planid:getplanorders[plan]._id,
				weeknumber:countweeks,
				monthnumber:0,
				daysinweek:6,
				weekdatetimestamp:0,
				weekvalue:0,
				updatetime:pastdatetime._created,
				statusweek:0
			}
			//insert weeks 
			const insertweek = await db.collection("weeks").insertOne(obj_week);
			//get new weekid 
			const getweekid = await db.collection("weeks").find({$and:[
															{durationid:getplanorders[plan].durationid},
															{projectid:getplanorders[plan].projectid},
															{planid:getplanorders[plan]._id},
															{weeknumber:countweeks}
															]}).toArray();
			if(timelength < 6){
				console.log("weekid: "+getweekid[0]._id);
				console.log("week: " + countweeks + " at days: " + timelength);
				var daynumber_array = 0;
				for(var j=0;j < timelength;j++){// add days
					var date = moment(startdate).add(j, 'days');
					daynumber_array = j;
					var daynumber =0;
					console.log(date.format("D dddd/MMMM/YYYY")+ " day number: "+ j);
					if(daynumber_array == 0){
						daynumber=1;
					}else if(daynumber_array == 1){
						daynumber=2;
					}else if(daynumber_array == 2){
						daynumber=3;
					}else if(daynumber_array == 3){
						daynumber=4;
					}else if(daynumber_array == 4){
						daynumber=5;
					}else if(daynumber_array == 5){
						daynumber=6;
					}
					//insert day 
					var obj_day = {
						weekid:getweekid[0]._id,
						daynumber:daynumber,
						datetimestamp:date,
						datestring:date.format("D/MMMM/YYYY"),
						daystring:date.format("dddd"),
						statusopen:0
					}
					const insertday = await db.collection("days").insertOne(obj_day);
				}
			}else{
				timelength = timelength - 6;
				console.log("weekid: "+getweekid[0]._id);
				console.log("week: " + countweeks + " at days: 6");
				var daynumber_array = 0;
				//create days 
				for(var days = 0;days < 6;days++){//add days
					daynumber_array = days;
					var daynumber =0;
					var date = moment(startdate).add(days, 'days');
					console.log(date.format("D dddd/MM/YYYY")+" day number: "+ days);
					if(daynumber_array == 0){
						daynumber=1;
					}else if(daynumber_array == 1){
						daynumber=2;
					}else if(daynumber_array == 2){
						daynumber=3;
					}else if(daynumber_array == 3){
						daynumber=4;
					}else if(daynumber_array == 4){
						daynumber=5;
					}else if(daynumber_array == 5){
						daynumber=6;
					}
					//insert day 
					var obj_day = {
						weekid:getweekid[0]._id,
						daynumber:daynumber,
						datetimestamp:date,
						datestring:date.format("D/MMMM/YYYY"),
						daystring:date.format("dddd"),
						statusopen:0
					}
					const insertday = await db.collection("days").insertOne(obj_day);
					if(days == 5){
						console.log("set new start date:"+ moment(date).day(7+1).format("D dddd/MMMM/YYYY"));
						startdate = moment(date).day(7+1);
					}

				}//days
				if(i == cal_weeks_ceil){
						//start date next dutaion
						last_startdate_duration = startdate_incodition;
						startdate_duration = moment(date).day(7+1);
						durationorder = getplanorders[plan].durationorder;
						console.log("next date duration: " + startdate_duration.format("D dddd/MMMM/YYYY"));
						console.log("last startdate: " + last_startdate_duration.format("D dddd/MMMM/YYYY"));
						console.log("durationorder: "+durationorder);
					}
				console.log("---------------------------");
			}
		}
	}//plan
	return {data:"create weeks success.",status:true};
}

exports.createweeks_test2 =async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	const getplanorders = await db.collection("planorders").find({projectid:projectid}).toArray();
	var month = 0;
	var weeks = 0;
	//sum all weeks of duration.
	for(var i =0;i < getplanorders.length; i++){
		const getmonth = await db.collection("duration").find({"_id":ObjectId(getplanorders[i].durationid)}).toArray(); 
		month += getmonth[0].month;

	}
	console.log("total month :" + month);
	weeks = month * 4;
	console.log("weeks: "+weeks);
	
} 

exports.getallweeks = async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	const getweeks = await db.collection("weeks").find({projectid:projectid}).toArray();
	var objweeks = {
		weeks:[]
	}
	for(var week=0; week < getweeks.length; week++){
		var statusweek = "";
		var statuseng = "";
		if(getweeks[week].statusweek == 0){
			statusweek="ยังไม่ดำเนินการ";
			statuseng = "not used.";
		}else if(getweeks[week].statusweek == 1){
			statusweek="บันทึกความก้าวหน้า";
			statuseng = "Record progress.";
		}else if(getweeks[week].statusweek == 2){
			statusweek="ปรับแก้";
			statuseng = "Reject.";
		}else if(getweeks[week].statusweek == 3){
			statusweek="ตรวจสอบแล้ว";
			statuseng = "Checked success.";
		}
		var objweek = {
			week:getweeks[week].weeknumber,
			status:statuseng
		}
		objweeks.weeks.push(objweek);
		console.log("weeknumber: " + getweeks[week].weeknumber +" "+statuseng);
	}
	return objweeks;
}

exports.rejectproject = async(planid,comment)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	dateandtime.locale('th');
	var datenow = dateandtime.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//update comment and status , updatetime 
	const update_reject_plan = await db.collection("planorders").updateOne(
								{"_id":ObjectId(planid)},
								{$set: {
									comment:comment,
									status:3,
									updatetime:pastdatetime._created
								}
								});
	const getprojectid = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	var projectid = getprojectid[0].projectid;
	//update comment and status , updatetime 
	const update_reject_project = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set:{status:3}});
	//update comment and status , updatetime 
	const updatestatusbatchbsm = await db.collection("batchbusinessman").updateOne(
										{projectid:ObjectId(projectid)},
										{$set:{status:"3",
											   updatetime:pastdatetime._created}});
	//insert to collection comment 
	var obj_comment = {
		projectid:projectid,
		planid:planid,
		comment:comment,
		insertdate:pastdatetime._created,
		read:"",
		status:3,
		statusstring:"ปรับแก้เอกสาร"
	};
	const insertcomment = await db.collection("commentreject").insertOne(obj_comment);

	return {data:"reject plan order success.",statuproject:"ปรับแก้เอกสาร",status:true};
}

exports.getreport_dashboard = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);

	var obj_dashboard = {
		batchid:batchid,
		modelname:"",
		piscalyear:"",
		allvalueused_batch:0,
		alltimeused:0,
		numberof_operators:0,
		sending_progress:0,
		missing_progress:0,
		reject_progress:0,
		now_operators:0
	}

	var SumTotalAllValueused = 0;
	var SumTotalAllTimeused = 0;
	var SetFirstDayOnproject = 0;
	var DateNowString_day = "";
	//get detail batch 
	const getdetailbatch = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	obj_dashboard.modelname = getdetailbatch[0].modelname;
	obj_dashboard.piscalyear = getdetailbatch[0].piscalyear;
	obj_dashboard.numberof_operators = getdetailbatch[0].numberofoperators;

	//get all project on batch
	const getallproject = await db.collection("projects").find({batchid:batchid}).toArray();
	console.log("project on batch: "+ getallproject.length);

	for(var countproject = 0; countproject < getallproject.length; countproject++){
		console.log("projectid: "+getallproject[countproject]._id);
		var projectid_find = getallproject[countproject]._id.toString();
		//get sum total used value on batch.
		const gettotal_value = await db.collection("progress_weeks").find({projectid:projectid_find}).toArray();
		if(gettotal_value.length == 0){
			SumTotalAllValueused += 0;
			SumTotalAllTimeused += 0;
		}else{
			SumTotalAllValueused += gettotal_value[0].valueused;	
			SumTotalAllTimeused += gettotal_value[0].timeused;
		}
	}
	console.log("SumTotalAllValueused: " + SumTotalAllValueused +" Bath.");
	console.log("SumTotalAllTimeused: " + SumTotalAllTimeused +" days.");
	obj_dashboard.allvalueused_batch = SumTotalAllValueused;
	obj_dashboard.alltimeused = SumTotalAllTimeused;
	obj_dashboard.now_operators = getdetailbatch .length

	//get bsm sending progress week.
	const get_sending = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:1}]}).toArray();
	console.log("bsm sending: "+get_sending.length);
	obj_dashboard.sending_progress = get_sending.length;

	//get bsm reject progress week.
	const get_reject = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:3}]}).toArray();
	console.log("bsm reject: " + get_reject.length);
	obj_dashboard.reject_progress = get_reject.length;

	//get bsm missing
	console.log("----------------missing---------------------");
	const getproject_onbatch = await db.collection("projects").find({batchid:batchid}).toArray();
	for(var countproject=0; countproject < getproject_onbatch.length; countproject++){
		console.log("projectid: " + getproject_onbatch[countproject]._id);
		console.log("date now:" + datenow);
		console.log("--------------------------------------------");
		var projectid_find = getproject_onbatch[countproject]._id.toString();
		
		//get first day project 
		const get_firstweek = await db.collection("weeks").find({$and:[{projectid:projectid_find},{weeknumber:1}]}).toArray();
		if(get_firstweek.length == 0){
			console.log("data: not have week.");
		}else{
			console.log("get first week:"+ get_firstweek[0]._id);
			const get_firstday = await db.collection("days").find({$and:[{weekid:ObjectId(get_firstweek[0]._id)},{daynumber:1}]}).toArray();
			console.log("get first day: "+ get_firstday[0].datestring);
			var dts_firstday = moment(get_firstday[0].datetimestamp._d);
			console.log("get first datetimestamp: "+ dts_firstday);
			SetFirstDayOnproject = dts_firstday;
		}
		
		//get plan on projectid
		const planon_project = await db.collection("planorders").find({projectid:projectid_find}).toArray();
		for(var countplan = 0; countplan < planon_project.length; countplan++){
			console.log("planid: " + planon_project[countplan]._id);
			var planid_find = planon_project[countplan]._id.toString();

			//get days from date now count down
			var lastday = moment(SetFirstDayOnproject).format("DD");
			var lastmonth = moment(SetFirstDayOnproject).format("MM");
			var lastyear = moment(SetFirstDayOnproject).format("YYYY");
			var lastdaay = moment([lastyear, lastmonth, lastday]);
			
			var datenow_string = moment(pastdatetime._created).format("dddd");
			DateNowString_day = datenow_string;
			console.log("datenow_string: "+datenow_string);
			var daynow = moment(pastdatetime._created).format("DD");
			var monthnow =  moment(pastdatetime._created).format("MM");
			var yearnow =  moment(pastdatetime._created).format("YYYY");
			var daynow = moment([yearnow, monthnow, daynow]);

			var diff_day = daynow.diff(lastdaay, 'days');
			console.log("diff_day: "+diff_day);
			for(var countdown=1; countdown <= diff_day;countdown++){
				console.log("countdown: "+ countdown);
				var at_day = moment().subtract(countdown,'days').format("DD/MMMM/YYYY");
				console.log("at day: "+at_day);

				//get week 
				const getweek_id = await db.collection("days").find({datestring:at_day}).toArray();
				console.log("get weekid from colec days: "+ getweek_id.length);
				for(var count_getweek = 0;count_getweek < getweek_id.length;count_getweek++){
					console.log("weekid: "+getweek_id[count_getweek].weekid);
					const getstatus_week = await db.collection("weeks").find({"_id":ObjectId(getweek_id[count_getweek].weekid)}).toArray();
					const checkthisday = await db.collection("days").find({$and:[{weekid:ObjectId(getweek_id[count_getweek].weekid)},{datestring:datenow}]}).toArray();
					console.log("checkthisday.length: "+checkthisday.length);
					var statusweek = getstatus_week[0].statusweek;
					if(checkthisday.length == 0){
						obj_dashboard.missing_progress +=1;
					}else{
						obj_dashboard.missing_progress +=0;
					}
				}
			}
			console.log("--------------------------------------------");
		}
		console.log("--------------------------------------------");

	}
	console.log("-------------------------------------");
	//console.log(obj_dashboard);
	return obj_dashboard;
}

exports.get_progress_sending = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var objsending={
		sending:[]
	}
	//get bsm sending progress week.
	const get_sending = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:1}]}).toArray();
	console.log("bsm sending: "+get_sending.length);
	for(var countsending = 0;countsending < get_sending.length;countsending++){
		var obj_progress_week = {
			progress_groupplan_id:"",
			projectid:"",
			projectname:"",
			username:"",
			updatetime:0,
			weekid:"",
			weeknumber:0,
		}
		const getdetail_project = await db.collection("projects").find({"_id":ObjectId(get_sending[countsending].projectid)}).toArray();
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
		obj_progress_week.progress_groupplan_id = get_sending[countsending]._id;
		obj_progress_week.projectid = get_sending[countsending].projectid;
		obj_progress_week.projectname =  getdetail_project[0].projectname;
		obj_progress_week.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;

		var day = moment(get_sending[countsending].updatetime).format("DD");
		var month = moment(get_sending[countsending].updatetime).format("MM");
		var year = moment(get_sending[countsending].updatetime).format("YYYY");
		var year_th = parseInt(year) + 543;
		var time = moment(get_sending[countsending].updatetime).format("HH:mm:ss");
		obj_progress_week.updatetime = day+"/"+month+"/"+year_th+" "+time; 

		//get weeknumber
		const get_progressweek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(get_sending[countsending]._id)}).toArray();
		obj_progress_week.weeknumber = get_progressweek[0].weeknumber;
		objsending.sending.push(obj_progress_week);
	}
	console.log(objsending);
	return objsending;
}

exports.get_detailprogress_sending = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[]
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.reject_progressplan = async(progress_groupplan_id,comment_reject,document_reject)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);

	//update status progressgroup_plan 
	const update_progressgroup_plan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progress_groupplan_id)},
														{$set: {status:3,
																statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
																comment_reject:comment_reject,
																document_reject:document_reject
																}
													});

	//update status progressweeks
	const update_progressgroup_weeks = await db.collection("progress_weeks").updateOne({progress_groupplan_id:ObjectId(progress_groupplan_id)},
														{$set:{status:3,statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์"}});

	//update status weeks
	const getprogress_weeks = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	for(var countprogress = 0;countprogress < getprogress_weeks.length;countprogress++){
		console.log("update weekid: "+ getprogress_weeks[countprogress].weekid);
		const update_status_weeks = await db.collection("weeks").updateOne({"_id":ObjectId(getprogress_weeks[countprogress].weekid)},
														{$set:{statusweek:3,statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์"}});
	}
	console.log("data:reject progress week sucess.");
	return {data:"reject progress week sucess.",status:true};
}


exports.getmissing_progress = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var datenow = dateandtime.format(new Date(), 'D/MMMM/YYYY');
	var pastdatetime = nodedatetime.create(datenow);
	var obj_progress_missing = {
		progress_weeks:[]
	}
	//get bsm missing
	console.log("----------------missing---------------------");
	const getproject_onbatch = await db.collection("projects").find({batchid:batchid}).toArray();
	console.log("batchid: "+batchid);
	console.log("getproject_onbatch.length: "+getproject_onbatch.length);
	for(var countproject=0; countproject < getproject_onbatch.length; countproject++){
		console.log("projectid: " + getproject_onbatch[countproject]._id);
		console.log("date now:" + datenow);
		console.log("--------------------------------------------");
		var projectid_find = getproject_onbatch[countproject]._id.toString();
		
		//get first day project 
		const get_firstweek = await db.collection("weeks").find({$and:[{projectid:projectid_find},{weeknumber:1}]}).toArray();
		if(get_firstweek.length == 0){
			console.log("data: not have week.");
		}else{
			console.log("get first week:"+ get_firstweek[0]._id);
			const get_firstday = await db.collection("days").find({$and:[{weekid:ObjectId(get_firstweek[0]._id)},{daynumber:1}]}).toArray();
			console.log("get first day: "+ get_firstday[0].datestring);
			var dts_firstday = moment(get_firstday[0].datetimestamp._d);
			console.log("get first datetimestamp: "+ dts_firstday);
			SetFirstDayOnproject = dts_firstday;
		}
		
		//get plan on projectid
		const planon_project = await db.collection("planorders").find({projectid:projectid_find}).toArray();
		for(var countplan = 0; countplan < planon_project.length; countplan++){
			console.log("planid: " + planon_project[countplan]._id);
			var planid_find = planon_project[countplan]._id.toString();

			//get days from date now count down
			var lastday = moment(SetFirstDayOnproject).format("DD");
			var lastmonth = moment(SetFirstDayOnproject).format("MM");
			var lastyear = moment(SetFirstDayOnproject).format("YYYY");
			var lastdaay = moment([lastyear, lastmonth, lastday]);
			
			var datenow_string = moment(pastdatetime._created).format("dddd");
			DateNowString_day = datenow_string;
			console.log("datenow_string: "+datenow_string);
			var daynow = moment(pastdatetime._created).format("DD");
			var monthnow =  moment(pastdatetime._created).format("MM");
			var yearnow =  moment(pastdatetime._created).format("YYYY");
			var daynow = moment([yearnow, monthnow, daynow]);

			var diff_day = daynow.diff(lastdaay, 'days');
			console.log("diff_day: "+diff_day);
			for(var countdown=1; countdown <= diff_day;countdown++){
				console.log("countdown: "+ countdown);
				var at_day = moment().subtract(countdown,'days').format("DD/MMMM/YYYY");
				console.log("at day: "+at_day);

				//get week 
				const getweek_id = await db.collection("days").find({datestring:at_day}).toArray();
				console.log("get weekid from colec days: "+ getweek_id.length);
				for(var count_getweek = 0;count_getweek < getweek_id.length;count_getweek++){
					console.log("weekid: "+getweek_id[count_getweek].weekid);
					const getstatus_week = await db.collection("weeks").find({"_id":ObjectId(getweek_id[count_getweek].weekid)}).toArray();
					const checkthisday = await db.collection("days").find({$and:[{weekid:ObjectId(getweek_id[count_getweek].weekid)},{datestring:datenow}]}).toArray();
					console.log("checkthisday.length: "+checkthisday.length);
					var statusweek = getstatus_week[0].statusweek;
					var obj_week_missing={
						projectname:"",
						username:"",
						updatetime:0
					}
					if(checkthisday.length == 0){
						//obj_dashboard.missing_progress +=1;
						obj_week_missing.projectname = getproject_onbatch[countproject].projectname;
						const getusername = await db.collection("users").find({"_id":ObjectId(getproject_onbatch[countproject].Primaryresponsibility)}).toArray(); 
						obj_week_missing.username = getusername[0].fristname +" "+ getusername[0].lastname;
						obj_week_missing.updatetime = "";
						obj_progress_missing.progress_weeks.push(obj_week_missing);
					}else{
						//obj_dashboard.missing_progress +=0;
					}
				}
			}
			console.log("--------------------------------------------");
		}
		console.log("--------------------------------------------");
	}
	console.log(obj_progress_missing);
	return obj_progress_missing;
}

exports.getprogress_week_reject = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_progress_reject = {
		progress:[]
	}
	//get progress week.
	const getprogressweek = await db.collection("progress_groupplan").find({$and:[{batchid:batchid},{status:3}]}).toArray();
	console.log("getprogressweek.length: "+getprogressweek.length);
	console.log("--------------------------------------------");
	for(var countweek =0;countweek < getprogressweek.length;countweek++){
		var obj_progress = {
			progress_groupplan_id:getprogressweek[countweek]._id,
			projectname:"",
			username:"",
			weeknumber:0,
			updatetime:""
		}
		//get detail project 
		const getdetailproject = await db.collection("projects").find({"_id":ObjectId(getprogressweek[countweek].projectid)}).toArray();
		//get users primary 
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetailproject[0].Primaryresponsibility)}).toArray();
		//get weeknumber 
		const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(getprogressweek[countweek]._id)}).toArray();
		console.log("getweeknumber.length: "+ getweeknumber.length);
		console.log("weeknumber: "+getweeknumber[0].weeknumber);
		obj_progress.weeknumber = getweeknumber[0].weeknumber;
		
		console.log("progress_groupid: "+getprogressweek[countweek]._id);
		console.log("username: "+getuserprimary[0].fristname+" "+getuserprimary[0].lastname);
		
		obj_progress.projectname = getdetailproject[0].projectname;
		obj_progress.username = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		var updatetime = getprogressweek[countweek].updatetime;
		obj_progress.updatetime = moment(updatetime).format("DD/MM/YYYY");
		console.log("updatetime: "+moment(updatetime).format("DD/MM/YYYY"));
		console.log("--------------------------------------------");
		obj_progress_reject.progress.push(obj_progress);
	}
	return obj_progress_reject;
}

exports.get_detail_progress_week_reject = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);

	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[]
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getweeknumber.length: "+ getweeknumber.length);
	console.log("weeknumber: "+getweeknumber[0].weeknumber);
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.approve_progress_week = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var status = 4;
	var statusstring = "ตรวจสอบแล้ว";

	//update status progress_groupplan
	const update_status_groupplan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progress_groupplan_id)},
																					  {$set:{status:status,statusstring:statusstring}});

	//upate status progress week
	const update_ststus_progressweek = await db.collection("progress_weeks").update({progress_groupplan_id:ObjectId(progress_groupplan_id)},
																					  {$set:{status:status,statusstring:statusstring}},
																					   {upsert:true,multi:true});

	//get weeks 
	const getweeks_id = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	for(var countweek = 0;countweek < getweeks_id.length;countweek++){
		//update status week
		const update_status_week = await db.collection("weeks").updateOne({"_id":ObjectId(getweeks_id[countweek].weekid)},
																		   {$set:{status:status,statusstring:statusstring}});
	}
	console.log("data:update status approve success.,progress_groupplan_id:progress_groupplan_id,status:true");
	return {data:"update status approve success.",progress_groupplan_id:progress_groupplan_id,status:true};
}

exports.getall_bsm = async(batchid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_all_project = {
		projects:[]
	}

	//get project on batch 
	const getall_project = await db.collection("projects").find({batchid:batchid}).toArray();
	for(var countproject =0;countproject < getall_project.length;countproject++){
		var obj_project={
			projectid:"",
			projectname:"",
			companyname:"",
			userprimary:""
		}
		const getuserprimary = await db.collection("users").find({"_id":ObjectId(getall_project[countproject].Primaryresponsibility)}).toArray();

		console.log("projectid: "+getall_project[countproject]._id);
		console.log("projectname: "+getall_project[countproject].projectname);
		console.log("companyname: ");
		console.log("userprimary: "+getuserprimary[0].fristname+" "+getuserprimary[0].lastname);

		obj_project.projectid = getall_project[countproject]._id;
		obj_project.projectname = getall_project[countproject].projectname;
		obj_project.companyname = getall_project[countproject].companyname;
		obj_project.userprimary = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
		console.log("-----------------------------------------------------");
		obj_all_project.projects.push(obj_project);
	}

	console.log(obj_all_project.projects);
	return obj_all_project;
}

exports.get_progressweek_fromproject = async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_weeks={
		weeks:[]
	}

	//get progress_groupplan_id
	const get_progress_groupplan_id = await db.collection("progress_groupplan").find({$and:[{projectid:projectid},{status:4}]}).toArray();
	console.log("get_progress_groupplan_id.length: "+get_progress_groupplan_id.length);
	for(var countplan =0;countplan < get_progress_groupplan_id.length;countplan++){
		var objweek = {
			progress_groupplan_id:get_progress_groupplan_id[countplan]._id,
			weeknumber:0,
			status:0,
			statusstring:""
		}
		const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(get_progress_groupplan_id[countplan]._id)}).toArray();
		objweek.weeknumber = getweeknumber[0].weeknumber;
		objweek.status = getweeknumber[0].status;
		objweek.statusstring = getweeknumber[0].statusstring;
		obj_weeks.weeks.push(objweek);
	}
	console.log(obj_weeks);
	return obj_weeks;
}

exports.get_detail_progressweek_approve = async(progress_groupplan_id)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_detail_progress = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		valueused:0,
		userprimary:"",
		weeknumber:0,
		plans:[]
	}
	const getdetail_progress_groupplan = await db.collection("progress_groupplan").find({"_id":ObjectId(progress_groupplan_id)}).toArray();
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(getdetail_progress_groupplan[0].projectid)}).toArray();
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetail_project[0].Primaryresponsibility)}).toArray();
	obj_detail_progress.projectid = getdetail_progress_groupplan[0].projectid;
	obj_detail_progress.projectname = getdetail_project[0].projectname;
	obj_detail_progress.userprimary = getuserprimary[0].fristname+" "+ getuserprimary[0].lastname;
	obj_detail_progress.projectvalue = getdetail_project[0].projectvalue;

	//sum total value used 
	var project_find_sumvalueused = getdetail_progress_groupplan[0].projectid.toString();
	const get_sumvalue = await db.collection("timesheet").find({projectid:project_find_sumvalueused}).toArray();
	for(var sumvalueused = 0; sumvalueused < get_sumvalue.length;sumvalueused++){
		obj_detail_progress.valueused += get_sumvalue[sumvalueused].totalvalue;
	}

	console.log("projectid: "+getdetail_progress_groupplan[0].projectid);
	console.log("projectname: "+getdetail_project[0].projectname);
	console.log("username: " + getuserprimary[0].fristname+" "+ getuserprimary[0].lastname);

	//get weeknumber 
	const getweeknumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	obj_detail_progress.weeknumber = getweeknumber[0].weeknumber;

	const getprogress_week = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progress_groupplan_id)}).toArray();
	console.log("getprogress_week.length: " + getprogress_week.length);
	for(var countprogress = 0;countprogress < getprogress_week.length;countprogress++){
		var progress_weekid = getprogress_week[countprogress]._id;
		var obj_plan = {
			planname:"",
			durationorder:0,
			planvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:"",
			totalhours:0
		}
		var totalhours_plan = 0;
		console.log("planname: "+getprogress_week[countprogress].planname);
		console.log("durationorder: "+getprogress_week[countprogress].durationorder);
		console.log("planvalue: "+getprogress_week[countprogress].planvalue);
		console.log("valueused: "+getprogress_week[countprogress].valueused);
		console.log("timelength: "+getprogress_week[countprogress].timelength);
		console.log("timeused: "+getprogress_week[countprogress].timeused);
		console.log("document: "+getprogress_week[countprogress].documentation);
		console.log("barrier: "+getprogress_week[countprogress].barries);

		obj_plan.planname = getprogress_week[countprogress].planname;
		obj_plan.durationorder = getprogress_week[countprogress].durationorder;
		obj_plan.planvalue = getprogress_week[countprogress].planvalue;
		obj_plan.valueused = getprogress_week[countprogress].valueused;
		obj_plan.timelength = getprogress_week[countprogress].timelength;
		obj_plan.timeused = getprogress_week[countprogress].timeused;
		obj_plan.document = getprogress_week[countprogress].documentation;
		obj_plan.barrier = getprogress_week[countprogress].barries;

		const getusertimesheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progress_weekid)}).toArray(); 
		console.log("getusertimesheet.length: "+getusertimesheet.length);
		for(var countuser =0;countuser < getusertimesheet.length;countuser++){
			var obj_user={
				username:"",
				hours:0,
				comment:""
			}
			console.log("username: "+getusertimesheet[countuser].username);
			console.log("hours: "+getusertimesheet[countuser].totalhours);
			console.log("comment: "+getusertimesheet[countuser].comment);
			obj_user.username = getusertimesheet[countuser].username;
			obj_user.hours = getusertimesheet[countuser].totalhours;
			obj_user.comment = getusertimesheet[countuser].comment;
			totalhours_plan += getusertimesheet[countuser].totalhours;
			obj_plan.team.push(obj_user);
		}
		obj_plan.totalhours = totalhours_plan;
		obj_detail_progress.plans.push(obj_plan);
		console.log("------------------------------------------");
	}
	return obj_detail_progress;
}

exports.getoverall_progressproject =async(projectid)=>{
	const client = await MongoClient.connect(url,{useNewUrlParser: true });
	const db = client.db(dbname);
	var obj_overall_project = {
		projectid:projectid,
		projectname:"",
		userprimary:"",
		projectvalue:0,
		plans:[]
	}
	//get detail project 
	var getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	//get user primary 
	var getuserprimary = await db.collection("users").find({"_id":ObjectId(getdetailproject[0].Primaryresponsibility)}).toArray();
	obj_overall_project.projectname = getdetailproject[0].projectname;
	obj_overall_project.userprimary = getuserprimary[0].fristname+" "+getuserprimary[0].lastname;
	obj_overall_project.projectvalue = getdetailproject[0].projectvalue;

	//get all plan on project 
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("getallplan.length: " + getallplan.length);
	
}


