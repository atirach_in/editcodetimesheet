const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var mp = require('mongodb-promise');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
var Dbname = "DbTimesheet";
let date = require('date-and-time');
var moment = require('moment');
var nodedatetime = require('node-datetime');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(3000, () => {
  console.log('Start server at port 3000.');
  console.log("----------------");
});

app.post('/loginadmin',(req,res)=>{
	var username = req.body.username
	var password = req.body.password 
	//logindate
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	if (username == "" || password == ""){
		var result={
			data:"username or password invalid.",
			status:false
		}
		res.status(201).json(result);
	}else{
		//update lastlogin
		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  	 var dbo = db.db(Dbname); 
			dbo.collection("accountadmin").updateOne(
								{username:username},
								{ $set: {lastlogin:pastdatetime._created}}, 
								function(err, res) {
								    if (err) throw err;
								    console.log("data: update lastlogin success");
								    console.log("-------------------");
								    db.close();

			});
		});
		//check username and password
		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  	 var dbo = db.db(Dbname);
			 dbo.collection("accountadmin").find({$and:[{username:username},{password:password}]}).toArray(function(err,result){
			 	if (err){
			 		console.log("Error:"+err);
			 		db.close();
			 	}else{
			 		db.close();
			 		if (result.length > 0){
			 			console.log("login pass");
			 			console.log("---------------");
			 			console.log(result);
			 			console.log("---------------");
			 			var resultdata={
				 				data:"login pass",
				 				userid:result[0]._id,
				 				typeaccount:result[0].typeaccount,
				 				status:true
				 			}
				 			res.status(201).json(resultdata);
			 		}else if(result.length == 0){
			 			console.log("login not pass");
			 			var resultdata={
			 				data:"login not pass",
			 				status:true
			 			}
			 			res.status(201).json(resultdata);
			 		}
			 	}
			 });
		});
	}
});

app.post('/createbatch',(req,res)=>{
	var admininsert = req.body.userid
	var adminupdate = ""
	var modelname = req.body.modelname
	var piscalyear = req.body.piscalyear
	var numberofoperators = req.body.numberofoperators

	var startingdate = req.body.startingdate
	var dts_startingdate = Date.parse(startingdate);

	var duedate = req.body.duedate
	var dts_duedate = Date.parse(duedate);

	var alreadybusinessman = 0
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var updatetime = 0
	var status = "1"
	var active = "1"  
	if (modelname == "" || piscalyear == "" || numberofoperators == "" || duedate == "" || startingdate == ""){
		var result={
			data:"data invalid.",
			status:false
		}
		res.status(201).json(result);
	}else{
		var checkformat = moment(duedate, "DD/MM/YYYY").isValid(); 
		if(checkformat != true){
			var result={
				data:"due date format invalid.",
				status:false
			}
			res.status(201).json(result);
		}else{
			MongoClient.connect(url, function(err, db) {
						 	 var dbo = db.db(Dbname);
							 var userobj={
									modelname:modelname,
									piscalyear:piscalyear,
									numberofoperators:numberofoperators,
									alreadybusinessman:alreadybusinessman,
									startingdate:dts_startingdate,
									duedate:dts_duedate,
									Insdate:pastdatetime._created,
									updatetime:updatetime,
									admininsert:admininsert,
									adminupdate:adminupdate,
									status:status,
									active:active
								};
							    //Create batch 
								  dbo.collection("batch").insertOne(userobj, function(err, res) {
									if (err) throw err;
										console.log("data: create batch success.");
										console.log("-------------------");
										db.close();
								});
								var resultreturn = {
								data:"create batch success.",
								status:true
								}
								res.status(201).json(resultreturn);
			});
		}
	}
});

//get all batch
app.post('/getallbatch',(req,res)=>{
	var allbatch={
		batch:[
			// batchid:"",
			// modelname:"",
			// piscalyear:"",
			// numberofoperators:"",
			// startingdate:"",
			// duedate:"",
			// Insdate:"",
		]
	}
	MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  	 var dbo = db.db(Dbname);
			 dbo.collection("batch").find().toArray(function(err,result){
			 	if (err){
			 		console.log("Error:"+err);
			 		db.close();
			 	}else{
			 		if(result.length == 0){
			 			console.log("data: batch is no have created.");
			 			console.log("-------------------------------");
			 			var resultjson = {
			 				data:" batch is no have created.",
			 				status:false
			 			}
			 			res.status(201).json(resultjson);
			 		}else{
			 			console.log("data: batch is created.");
			 			console.log("-------------------------------");
			 			for(var i in result){
			 				var databatch = {
									batchid:result[i]._id,
									modelname:result[i].modelname,
									piscalyear:result[i].piscalyear,
									numberofoperators:result[i].numberofoperators,
									startingdate:result[i].startingdate,
									duedate:result[i].duedate,
									Insdate:result[i].Insdate
							}
							allbatch.batch.push(databatch);
			 			}
			 			res.status(201).json(allbatch);
			 		}
			 	}
			 });
		});
});

//create link from batch 
app.post('/createlink',(req,res)=>{
	var batchid = req.body.batchid
	if (batchid == ""){
		console.log("data: batch invalid.");
		console.log("--------------------");
		var result = {
			data:" batch invalid.",
			status:false
		}
		res.status(201).json(result);
	}else{
		//check batchid 
		MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("batch").find({"_id":ObjectId(batchid)}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		db.close();
				 		if(result.length == 0){
				 			console.log("data: invalid batch.");
				 			console.log("-------------------");
				 			var resultjson={
				 				data:"invalid batch.",
				 				status:false
				 			}
				 			res.status(201).json(resultjson);
				 		}else{
				 			console.log(result);
				 			console.log("-------------------");
				 			var resultjson={
				 				data:"create link success.",
				 				link:"localhost:3000/createaccount/"+result[0]._id,
				 				status:true
				 			}
				 			res.status(201).json(resultjson);
				 		}
				 	}
				});
		});
	}
});

//get all project from batchid 
app.post('/detailbatch/:batchid',(req,res)=>{
	var batchid = req.params.batchid
	if(batchid == ""){
		var result = {
			data:"invalid batchid",
			status:false
		}
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
			var batchproject ={
					batchid:batchid,
					totalnumber:0,
					sending:0,
					reject:0,
					success:0
			}
		  if (err) throw err;
		  	 var dbo = db.db(Dbname);
		  	 //count totalnumber businessman 
		  	  dbo.collection("batch").find({"_id":ObjectId(batchid)}).toArray(function(err,resultstotalnumber){
		  	  		if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		var amountnumber = resultstotalnumber[0].numberofoperators
				 		console.log("data: total number: "+ amountnumber +".");
						console.log("---------------------------------");
						batchproject.totalnumber = amountnumber
						//count status = 2 (sending)
						 dbo.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"2"}]}).toArray(function(err,resultsending){
						 	if (err){
						 		console.log("Error:"+err);
						 		db.close();
						 	}else{
						 		if(resultsending.length == 0){
						 			console.log("data: project sending is not row.");
						 			console.log("---------------------------------");
						 		}else{
						 			console.log("data: project sending is have row.");
						 			console.log("---------------------------------");
						 			batchproject.sending = resultsending.length
							 			//count status = 3 (reject)
										dbo.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"3"}]}).toArray(function(err,resultreject){
										 	if (err){
										 		console.log("Error:"+err);
										 		db.close();
										 	}else{
										 		if(resultreject.length == 0){
										 			console.log("data: project reject  is not row.");
										 			console.log("---------------------------------");
										 		}else{

										 			console.log("data: project reject is have row.");
										 			console.log("---------------------------------");
										 			batchproject.reject = resultreject.length
										 		}
										 	}
										 });
										//count status = 4 (approve)
										dbo.collection("batchbusinessman").find({$and:[{batchid:batchid},{status:"4"}]}).toArray(function(err,resultsuccess){
										 	if (err){
										 		console.log("Error:"+err);
										 		db.close();
										 	}else{
										 		if(resultsuccess.length == 0){
										 			console.log("data: project approve  is not row.");
										 			console.log("---------------------------------");
										 		}else{
										 			console.log("data: project approve is have row.");
										 			console.log("---------------------------------");
										 			batchproject.success = resultsuccess.length
										 		}
										 		res.status(201).json(batchproject);
										 	}
										 });
								 }
						 	 }
						 });
				 	}
		  	  });
		});
	}
});

//get all project from batchid and status = 2
// app.post('/getprojectsending/admin',(req,res)=>{
// 	var respond = {
// 	  	 data:{
// 	  	 	project:[]
// 	  	 }
// 	  }
// 	var batchid = req.body.batchid
// 	///function
// 	 async function Findproject() {
// 	    await MongoClient.connect(url).then(function(db) {
// 	      //var collection = dbo.collection('batch');
// 	     dbo = db.db(Dbname);
// 	     return dbo.collection('batchbusinessman').find({$and:[{batchid:batchid},{status:"2"}]}).toArray(function(err,resultstotalnumber){
// 		  	  	if (err){

// 		  	  	}else{
// 		  	  		//respond.data.project.push(resultstotalnumber)
// 		  	  		console.log(resultstotalnumber);
// 					for (var i in resultstotalnumber){
// 						console.log("projectid: "+resultstotalnumber[i]['projectid']);
// 						console.log("----------------------------------------------");
// 						dbo.collection('projects').find({"_id":ObjectId(resultstotalnumber[i]['projectid'])}).toArray(function(err,resutlproject){
// 							var projectid = resutlproject[0]['_id'];
// 							dbo.collection('users').find({"_id":ObjectId(resutlproject[0]['Primaryresponsibility'])}).toArray(function(err,resultusers){
// 								// console.log("projectname: "+resutlproject[0]['projectname']);
// 								// console.log("userid: "+resutlproject[0]['Primaryresponsibility']);
// 								// console.log("username: "+resultusers[0]['fristname']);
// 								var resultdata={
// 									projectid:projectid,
// 									projectname:resutlproject[0]['projectname'],
// 									username:resultusers[0]['fristname']+" "+resultusers[0]['lastname'],
// 								}
// 								respond.data.project.push(resultdata);
// 							});//find users
// 						});//find project
// 					}//for
// 					console.log(respond.data.project);
// 					console.log("----------------------------------------------");
// 				  }//else
// 			  });//find batchbusinessman
// 	     });//MongoClient
// 	}//async
// 	async function main(){
// 		let a = await Findproject();
// 		let b = await res.status(201).json(respond)
// 	}
// 	main();
// });


app.post('/testawait',(req,res)=>{

	var respond = {data:[]}
	var user = {datauser:[]} 
	var batchid = req.body.batchid

	///function
	function Findproject() {
	      return MongoClient.connect(url).then(function(db) {
	      //var collection = dbo.collection('batch');
	      dbo = db.db(Dbname);
	      return dbo.collection('projects').find({batchid:batchid}).toArray();
	    }).then(function(items) {
	      //console.log(items);
	      return items;
	    });
	}

	function Findusername(userid) {
		return MongoClient.connect(url).then(function(db) {
	     			dbo = db.db(Dbname);
		     		return dbo.collection('users').find({"_id":ObjectId(userid)}).toArray();//find users
		 }).then(function(items2) {
		 	  // console.log("---------Findusername-------------");
		    //   console.log(items2);
		    //   console.log("----------------------");
		      return items2;
		});
	}

	function call(){
		return Findproject().then(function(items) {//1
				console.log("step 1");
				for (var i in items){
				 	 var projectpush={
				 	 		userid:items[i].Primaryresponsibility,
					 	 	username:"",
					 	 	projectid:items[i]._id,
					 	 	projectname:items[i].projectname,
				 	 		timesend:items[i].senddatetime
				 	 } 
				 	 respond.data.push(projectpush);
				 }//for
				 return respond;
			}).then(function(result){//then step 1
				console.log("step 2");
				for (var i in result.data){
						console.log("userid: "+ respond.data[i]['userid']);
						Findusername(respond.data[i]['userid']).then(function(result){
						respond.data[i].username = result[0]['fristname'];
						console.log("username: "+ result[0]['fristname']);
					});
				}
				res.json(respond);
				return respond;
			});//then step 2 
	}
	call();
});	

// app.post('/getallplancheckapprove',(req,res)=>{
// 	var projectid = req.body.projectid
// 	var respond = {
// 		project:{
// 			Documentation:"",
// 			//{
// 			//documentproject:[document]
// 			planorder:[],
// 			// 	{planname:"",installment:"",jobvalue:"",timelength:""},
			
// 			team:[
// 			// 	{fristname:"",lastname:"",position:""},
// 			]
// 			//}
// 		}
// 	}
// 	MongoClient.connect(url, function(err, db) {
// 		var dbo = db.db(Dbname);
// 		dbo.collection("projects").find({"_id":ObjectId(projectid)}).toArray(function(err,resultproject){
// 			//Documentation
// 			respond.project.Documentation = resultproject[0]['Documentation'];
// 			dbo.collection("projects").find({projectid:projectid}).toArray(function(err,resultplanorder){
// 				//plan
// 				for(var i in resultplanorder){
// 					var detailplan ={
// 						planname:resultplanorder[i]['planname'],
// 						installment:resultplanorder[i]['installment'],
// 						jobvalue:resultplanorder[i]['jobvalue'],
// 						timelength:resultplanorder[i]['timelength']
// 					}
// 					respond.project.planorder.push(detailplan);
// 				}
// 				console.log(respond);
// 				res.status(201).json(respond);
// 			});
			
// 			// // //user
// 			// for(var j = 1; j <= 5 ;j++){
// 			// 	console.log(j);
// 			// 	var detailuser ={fristname:"atirach",lastname:"inthrasuk",position:"CEO"}
// 			// 	respond.project.team.push(detailuser);
// 			// }
			
// 		});//find project
// 	});//MongoClient
// });

app.post('/testmp',(req,res)=>{
	MongoClient.connect("mongodb://localhost:27017/DbTimesheet")
    .then(function(db){
            return db.collection('users')
                .then(function(col) {
                    return col.find({fristname : "atirach"}).toArray()
                        .then(function(items) {
                            console.log(items);
                            db.close().then(console.log('success'));
                        })
            })
	})
	.fail(function(err) {console.log(err)});
});
