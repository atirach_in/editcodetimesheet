const express = require('express');
const app = express();

var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
let date = require('date-and-time');
var moment = require('moment');
var nodedatetime = require('node-datetime');
const dbservice = require('./DBServiceAdmin.js');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(3000, () => {
  console.log('Start server at port 3000.');
  console.log("----------------");
});

app.post('/loginadmin',(req,res)=>{
	var username = req.body.username;
	var password = req.body.password;
	if(username == "" || password == ""){
		console.log("data: invalid username or password.");
		var response = {data:"invalid username or password"};
		res.json(response);
	}else{
		dbservice.getuser(username,password).then(function(result){
			res.json(result);
		});
	}
});

app.post('/createbatch',(req,res)=>{
	var admininsert = req.body.userid
	var adminupdate = ""
	var modelname = req.body.modelname
	var piscalyear = req.body.piscalyear
	var numberofoperators = req.body.numberofoperators

	var startingdate = req.body.startingdate
	var dts_startingdate = Date.parse(startingdate);

	var duedate = req.body.duedate
	var dts_duedate = Date.parse(duedate);

	var alreadybusinessman = 0
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var updatetime = 0
	var status = "1"
	var active = "1" 
	if (modelname == "" || piscalyear == "" || numberofoperators == "" || duedate == "" || startingdate == ""){
		var result={data:"data invalid.",status:false}
		res.json(result);
	}else{
		dbservice.addbatch(admininsert,adminupdate,modelname,piscalyear,numberofoperators,startingdate,dts_startingdate,duedate,dts_duedate,alreadybusinessman,datenow,pastdatetime,updatetime,status,active)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getallbatch',(req,res)=>{
	dbservice.getallbatch()
	.then(function(result){
		res.json(result);
	});
});

app.post('/createlink',(req,res)=>{ 
	var batchid = req.body.batchid
	dbservice.createlink(batchid)
	.then(function(result){
		res.json(result);
	});
});

app.get('/getdetail/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var response = {data:"invalid batchid",status:false};
		res.json(response);
	}else{
		dbservice.getdetailbatch(batchid)
		.then(function(result){
			res.json(result);	
		});
	}
});

app.post('/update/duedate',(req,res)=>{
	var batchid = req.body.batchid;
	var duedate = req.body.duedate;
	if(duedate == ""){
		var respond = {data:"invalid duedate",status:false};
		res.json(respond);
	}else{
		var dts_duedate = Date.parse(duedate);
		dbservice.updateduedatetime(batchid,dts_duedate)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getprojectsending',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var response = {data:"invalid batchid",status:false};
		res.json(response);
	}else{
		dbservice.getprojectsending(batchid)
		.then(function(result){
			res.json(result);	
		});
	}
});

//check project status sending 
app.get('/investprojectsending/:projectid',(req,res)=>{
	var projectid = req.params.projectid;
	if(projectid == ""){
		var respond={data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getdetailprpoject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/update/status/projectinvest/:projectid/:status',(req,res)=>{
	var projectid = req.params.projectid;
	var status = req.params.status;
	if(projectid == "" || status == ""){
		var respond = {data:"invalid projectid or status.",status:false};
		res.json(respond);
	}else{
		dbservice.update_status_projectinvest(projectid,status)
		.then(function(result){
			res.json(result);
		});	
	}
});


app.post('/rejectproject',(req,res)=>{
	var projectid = req.body.projectid;
	var comment = req.body.comment;
	var documentcomment = req.body.document;
	if(projectid === "" || comment === "" || documentcomment === ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.updatecommentproject(projectid,comment,documentcomment)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/approvepeoject/:projectid',(req,res)=>{
	var projectid = req.params.projectid;
	if(projectid === ""){
		var respond = {data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.approveproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});


app.get('/getproject/status/reject/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		res.json();
	}else{
		dbservice.getprojectstatusreject(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/create/weeks',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks(projectid)
		.then(function(result){
			//res.json();
		});
	}
});


app.post('/get/all/weeks',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false}
		res.json(respond);
	}else{	
		dbservice.getallweeks(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/reject/planorder',(req,res)=>{
	var planid = req.body.planid;
	var comment = req.body.comment;
	if(planid == ""){
		var respond = {data:"invalid planid."};
		res.json(respond);
	}else{
		dbservice.rejectproject(planid,comment)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/get/data/detail/batch/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var respond = {data:"incalid batchid.",status:false};
		res.json(respond);	
	}else{
		dbservice.getdata_detailbatch(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/get/project/status/approve/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid",status:false};
		res.json(respond);
	}else{
		dbservice.getproject_approve(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/create/weeks/test2',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks_test(projectid)
		.then(function(result){
			//res.json(result);
		});
	}
});

app.post('/create/weeks',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid",status:false};
		res.json(respond);
	}else{
		dbservice.createweeks(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/report/dashboard',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getreport_dashboard(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/week/sending',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_progress_sending(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week/sending',(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detailprogress_sending(progress_groupplan_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/reject/progressplan/',(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	var comment_reject = req.body.comment_reject;
	var document_reject = req.body.document_reject;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.reject_progressplan(progress_groupplan_id,comment_reject,document_reject)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/missing',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getmissing_progress(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/week/reject',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getprogress_week_reject(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});	

app.post('/get/detail/progress/week/reject',(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detail_progress_week_reject(progress_groupplan_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/approve/progress/week',(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.approve_progress_week(progress_groupplan_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/bsm',(req,res)=>{
	var batchid = req.body.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getall_bsm(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/progress/weeks/from/project',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_progressweek_fromproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week/approve',(req,res)=>{
	var progress_groupplan_id = req.body.progress_groupplan_id;
	if(progress_groupplan_id == ""){
		var respond = {data:"invalid progress_groupplan_id.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detail_progressweek_approve(progress_groupplan_id)
		.then(function(result){
			res.json(result);	
		});
	}
});

app.post('/get/overall/progress/project',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_detail_progressweek_approve(projectid)
		.then(function(result){
			res.json(result);	
		});
	}
});