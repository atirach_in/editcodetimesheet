var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
let date = require('date-and-time');
var moment = require('moment');
var nodedatetime = require('node-datetime');
const DbTimesheet = "DbTimesheet";

exports.createaccount = async(fristname,lastname,phone,email,password,duedate,batchid,companyname)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const getduedate = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	var duedate = moment(getduedate[0].duedate).format("D/MMMM/YYYY");
	var day = moment().format("D");
	var month = moment().format("MMMM");
	var year = moment().format("YYYY");
	var year_th = parseInt(year) + 543;
	console.log("duedate: " + duedate);
	console.log("thisday: "+ day+"/"+month+"/"+year_th);
	if(duedate == day+"/"+month+"/"+year_th){
		var respond={data:"not create account becuase is duedate.",status:false};
		return respond;
	}else{
		var userobj={
				batchid:batchid,
				fristname:fristname,
				lastname:lastname,
				phone:phone,
				email:email,
				password:password,
				position:"",
				companyname:companyname,
				profileimage:"",
				Insdate:pastdatetime._created
			};
		var respond ={data:[]}
		//check email 
		const checkemail = await db.collection("users").find({email:email}).toArray();
		if(checkemail.length > 0){
			var resultcheckemail = {data:"email has been used",status:false}
			console.log(resultcheckemail);
			return resultcheckemail;
		}else{
			//create account
			const insertaccount = await db.collection("users").insertOne(userobj)
										.then(function(result){
											respond.data.push({data:"create account sucess.",status:true});
										});
			return respond;
		}		
	}
}

exports.loginuser = async(email,password)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//check username and password
	const checkuser = await db.collection("users").find({$and:[{email:email},{password:password}]}).toArray();
	if (checkuser.length == 0){
		var respond = {data:"invalid username or password.",status:false};
		return respond;
	}else{
		console.log("userid: "+ checkuser[0]._id);
		var userid = checkuser[0]._id;
		const getprojectid = await db.collection("projects").find({Primaryresponsibility:userid.toString()}).toArray();
		if (getprojectid.length == 0){
			var resultdata={
			 	data:"login pass",
			 	userid:checkuser[0]._id,
			 	fristname:checkuser[0].fristname,
			 	lastname:checkuser[0].lastname,
			 	phone:checkuser[0].phone,
			 	batchid:checkuser[0].batchid,
			 	projectid:"",
			 	position:checkuser[0].position,
			 	status:true
			}
			console.log(resultdata);
			return resultdata;
		}else{
			var resultdata={
			 	data:"login pass",
			 	userid:checkuser[0]._id,
			 	fristname:checkuser[0].fristname,
			 	lastname:checkuser[0].lastname,
			 	phone:checkuser[0].phone,
			 	batchid:getprojectid[0].batchid,
			 	projectid:getprojectid[0]._id,
			 	projectname:getprojectid[0].projectname,
			 	position:checkuser[0].position,
			 	status:true,
			 	statusproject:"",
			 	Fill_in:""
			}
			//check status project
			if(getprojectid[0].status == 1){
				resultdata.statusproject = "กำลังดำเนินการ";
				resultdata.Fill_in = "กำลังกรอกข้อมูล";
			}else if(getprojectid[0].status == 2){
				resultdata.statusproject = "รอดำเนินการ";
				resultdata.Fill_in = "ส่งเอกสารแล้ว";
			}else if(getprojectid[0].status == 3){
				resultdata.statusproject = "ปรับแก้เอกสาร";
				resultdata.Fill_in = "ปรับแก้เอกสาร";
			}else if(getprojectid[0].status == 4){
				resultdata.statusproject = "อนุมัติ Timesheet";
				resultdata.Fill_in = "อนุมัติ Timesheet";
			}

			console.log(resultdata);
			return resultdata;
		}
	}
}

exports.dashboard = async(userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const getproject = await dbo.collection("projects").find({Primaryresponsibility:userid}).toArray();
	if(getproject.length == 0){
		console.log("data: no have projects");
		var result = {data:" no have project",status:true}
		return result;
	}else{
		console.log("data: have projects");
		var result = {data:"have project",status:true}
		return result;
	}
}

exports.dashboard_percent = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objproject = {
		projectname:"",
		projectvalue:0,
		statusproject:"",
		Fill_in:"",
		partdocument:0,
		partplan:0,
		partteam:0,
		partsending:0,
		percent1to3:0,
		percentcircle:0
	}
	//documentation 
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if (getdocumetation[0].Documentation == ""){
		objproject.partdocument = 0;
	}else{
		objproject.partdocument = 100;
	}
	console.log("percent part 1: "+objproject.partdocument);
	var percent1 = objproject.partdocument;
	objproject.partdocument = percent1;
	console.log("---------------------------------------");

	//plan order
	console.log("amount plan: "+ getdocumetation[0].amountplan);
	var amountplan = getdocumetation[0].amountplan;
	const getallplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	var plans = getallplanorder.length;
	console.log("plans: "+plans);
	var percentplan = 100/amountplan;
	var calculate_percent_plan = percentplan * plans;
	console.log("percent part 2: "+calculate_percent_plan);
	var percent2 = calculate_percent_plan;
	objproject.partplan = percent2;
	console.log("---------------------------------------");

	//team 
	const get_amount_team = await  db.collection("users").find({projectid:projectid}).toArray();
	var amountusers = get_amount_team.length;//team_added
	var userprimary = 1;
	console.log("amount team: "+ (amountusers+userprimary));
	var allusers = (amountusers+userprimary);
	const getamount_setteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var setamount = getamount_setteam[0].amountteam;
	var percentteam = 100/setamount;
	console.log("percent amount users: "+ percentteam);
	var percent3 = (percentteam * allusers);
	objproject.partteam = percent3;
	console.log("percent part 3:"+ percentteam * allusers);
	console.log("---------------------------------------");

	//percent1to3
	objproject.percent1to3 = ((percent1 + percent2 + percent3) * 50) /300;
	
	//part sending
	const getstatusproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var status = getstatusproject[0].status;
	if(status == 1){
		console.log("part 4 sending: 0%");
		console.log("---------------------------------------");
		objproject.partsending = objproject.percent1to3;
	}else{
		console.log("part 4 sending: 100%");
		console.log("---------------------------------------");
		objproject.partsending = 100;
	}
	//percent all 
	var percentall = (((percent1 + percent2 + percent3) + objproject.partsending) * 100)/400 ;
	//var totalpercent = (percentall*100) / 300;
	objproject.percentcircle = percentall;
	console.log("percentcircle: "+ percentall);
	console.log("---------------------------------------");
	console.log(objproject);

	//get detail project 
	// projectname:"",
	// projectvalue:0,
	// statusproject:"",
	// Fill_in:"",
	objproject.projectname = getstatusproject[0].projectname;
	objproject.projectvalue = getstatusproject[0].projectvalue;
	if(getstatusproject[0].status == 1){
		objproject.statusproject = "กำลังดำเนินการ";
		objproject.Fill_in = "กำลังกรอกข้อมูล";
	}else if(getstatusproject[0].status == 2){
		objproject.statusproject = "รอดำเนินการ";
		objproject.Fill_in = "ส่งเอกสารแล้ว";
	}else if(getstatusproject[0].status == 3){
		objproject.statusproject = "ปรับแก้เอกสาร";
		objproject.Fill_in = "ปรับแก้เอกสาร";
	}else if(getstatusproject[0].status == 4){
		objproject.statusproject = "อนุมัติ Timesheet";
		objproject.Fill_in = "อนุมัติ Timesheet";
	}
	
	return objproject;
}

exports.createproject = async(userid,projectname,projectvalue,batchid)=>{
	console.log("userid: "+userid);
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var porjectobj={
		batchid:batchid,
		projectname:projectname,
		projectvalue:projectvalue,
		Primaryresponsibility:userid,
		Documentation:"",
		Insdate:pastdatetime._created,
		updateproject:"",
		senddatetime:"",
		status:1,
		active:1
	};
	var projectobjres = {}
	var batchbsm = {}
	//insert project 
	const insertproject = await db.collection("projects").insertOne(porjectobj);
	//find _id project 
	const getproject = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	projectobjres = {
						data:"new project sucess",
						userid:userid,
						projectid:getproject[0]._id,
						projectname:getproject[0].projectname,
						status:true
					}
	batchbsm = {
					batchid:batchid,
					projectid:getproject[0]._id,
					Insdate:pastdatetime._created,
					updatetime:"",
					status:"1",
					active:"1"
				}
	//insert project in bsm 
	const insertbsm = await db.collection("batchbusinessman").insertOne(batchbsm);
	//update aleady businessman 
	const findbsminbatch =  await db.collection("projects").find({batchid:batchid}).toArray();
	const updatealready = await db.collection("projects").updateOne(
												{"_id":ObjectId(batchid)},
												{ $set: {alreadybusinessman:findbsminbatch.length}});
	//safe draft 2 สร้างโครงการ
	const getidactivity = await db.collection("activity").find({activityorder:"2"}).toArray();
	console.log("get activity _id: "+ getidactivity[0]._id);
	var objactivity = {
		userid:userid,
		activityid:getidactivity[0]._id,
		datetime:pastdatetime._created
	}
	console.log("---------insert activity---------");
	console.log(objactivity);
	console.log("---------------------------------");
	const insertactivity = await db.collection("activityusers").insertOne(objactivity); 

	return projectobjres;
}

exports.getuserprimary = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	const getiduserprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("userprimary: "+ getiduserprimary[0].Primaryresponsibility);
	const getdatauserprimary = await db.collection("users").find({"_id":ObjectId(getiduserprimary[0].Primaryresponsibility)}).toArray();
	console.log(getdatauserprimary);
	return getdatauserprimary;
}

exports.updateuserprimary = async(userid,fristname,lastname,phone,position,profileimage)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var objuser = {data:"update user success.",user:[],status:true};
	//update user primary 
	const updateuserprimary = await db.collection("users").updateOne(
					{"_id":ObjectId(userid)},
					{ $set: { fristname: fristname,
							  lastname: lastname,
							  phone:phone,
							  position:position,
							  profileimage:profileimage,
							  updatetime:pastdatetime._created
					}});
	var user = {
		fristname: fristname,
		lastname: lastname,
		phone:phone,
		position:position,
		profileimage:profileimage
	}
	objuser.user.push(user);
	return objuser;
}

exports.adddocument = async(userid,projectid,pathdocument)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const updatedocument = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set: {Documentation: pathdocument}});
	//get documentation 
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("documenttaion: "+ getdocumetation[0].Documentation);
	//get activity_id  
		var activityorder = "1"; // สร้างแผนงาน
		const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
		console.log("activity _id: " + getactivity_id[0]._id);
		console.log("update document success.");
		console.log("projectid: "+ projectid);
		console.log("---------------------------");
		//insert activityusers
		const insert_activity_user = await db.collection("activityusers").insert({
										userid:userid,
										activityid:getactivity_id[0]._id,
										datetime:pastdatetime._created
								  });
	//check document for draft 
	if(getdocumetation[0].Documentation == ""){
		var draftat = "1";
		console.log("draft at: "+ draftat);
		return {data:"no have document.",draft:draftat,status:true};
	}else{
		var draftat = "2";
		console.log("draft at: "+ draftat);
		return {data:"update document sucess.",status:true};
	}
}

exports.addplanorder = async(userid,projectid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var totalhours = timelength * 8
	let countplanorder
	const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
	var planobj={
			projectid:projectid,
			planorder:"",
			planname:planname,
			jobvalue:jobvalue,
			moneydifference:moneydifference,
			enforceemployee:enforceemployee,
			durationid:durationid,
			durationorder:getdurationnumber[0].durationorder,
			averagewages:averagewages,
			timelength:timelength,
			totalhours:totalhours,
			averagehours:0,
			Insdate:pastdatetime._created,
			status:1,
			active:1
	};
	//calculate average hour/man
	var averageHR = ((jobvalue/timelength)/enforceemployee)/8;
	planobj.averagehours = averageHR;

	const insertplan = await db.collection("planorders").insertOne(planobj);
	//get activity_id  
	var activityorder = "2.3"; // สร้างแผนงาน
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });

	// get amount plan 
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	console.log("amountplan: "+getamountplan[0].amountplan);
	var amount = getamountplan[0].amountplan;
	// get plan where projectid 
	const getplanfromproject = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("get plan: "+getplanfromproject.length);
	var getplan = getplanfromproject.length;
	var calpercent = 100/amount;
	console.log("percent unit:"+ calpercent + "%");
	var calplanpercent = calpercent * getplan;
	console.log("percent plan: "+calplanpercent +"%");
	console.log("---------------------------");
	if(calplanpercent < 100){
		var draftat = "2.3";
		console.log("draft at: "+ draftat);
		return {data:"insert plan success.",draft:draftat,status:true};
	}else{
		var draftat = "3.1";
		console.log("draft at: "+ draftat);
		//get activity_id  
		var activityorder = draftat; //เพิ่มจำนวนคนทำงาน
		const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
		console.log("activity _id: " + getactivity_id[0]._id);
		console.log("update amount plan success.");
		console.log("projectid: "+ projectid);
		console.log("---------------------------");
		//insert activityusers
		const insert_activity_user = await db.collection("activityusers").insert({
										userid:userid,
										activityid:getactivity_id[0]._id,
										datetime:pastdatetime._created
								  });
		return {data:"insert plan success.",draft:draftat,status:true};
	}
}

exports.getpallplanorder = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var datapaln={
				project:{
				projectvalue:0,
				amountplan:0,
				planadded:0,
				plan:[
					//planid:"1",
					// planname:"plan1",
					// installment:"1",
					// jobvalue:"1",
					// timelength:"20"
				],
				amountsetteam:0,
				teamadded:0,
				activity:""
			}
		}
	//find total jobvalue = sum(jobvalud in plan)
	var totalprojectvalue = 0;
	//find all plan order
	var getallplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	datapaln.project.amountplan = getallplan[0].amountplan;
	//find planorder
	var getallplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var i = 0; i < getallplan.length;i++){
		totalprojectvalue += getallplan[i].jobvalue;
		console.log(getallplan[i].durationid);
		//find duration 
		var getduration = await db.collection("duration").find({"_id":ObjectId(getallplan[i].durationid)}).toArray();
		var durationnumber = getduration[0].durationorder;
		var dataorder = {
			planid:getallplan[i]._id,
			planname:getallplan[i].planname,
			duration:durationnumber,
			jobvalue:getallplan[i].jobvalue,
			timelength:getallplan[i].timelength
		}
		datapaln.project.plan.push(dataorder);
	}
	datapaln.project.planadded = getallplan.length;
	datapaln.project.projectvalue = totalprojectvalue;


	//get amount set team 
	const getamountsetteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getamountsetteam.length == 0){
		datapaln.project.amountsetteam = 0;
	}else{
		datapaln.project.amountsetteam = getamountsetteam[0].amountteam;
	}

	//get team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if(getteamadded.length == 0){
		datapaln.project.teamadded = 0;
	}else{	
		datapaln.project.teamadded = (getteamadded.length +1);
	}

	//set next activity 
	if(datapaln.project.amountsetteam == 0){
		datapaln.project.activity = "3.1";
	}else{
		datapaln.project.activity = "3.2";
	}
	
	console.log(datapaln.project);
	return datapaln.project;
}

exports.getplanorder = async(planid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var dataplan={
		planadded:0,
		amountplan:0,
		plandetial:[
			// planname:getplan[0].planname,
			// jobvalue:getplan[0].jobvalue,
			// duration:getduration[0].durationorder,
			// timelength:getplan[0].timelength,
			// averagewage:getplan[0].averagewages,
			// enforceemployee:getplan[0].enforceemployee
			// duration:
			 
		],
		duration:[]
	}
	const getplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	const getduration = await  db.collection("duration").find({"_id":ObjectId(getplan[0].durationid)}).toArray();
	const getallduration = await  db.collection("duration").find({projectid:getplan[0].projectid}).toArray();
	//count plan in project 
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(getplan[0].projectid)}).toArray();
	dataplan.amountplan = parseInt(getamountplan[0].amountplan);
	//find plan added 
	const findplanadded = await db.collection("planorders").find({projectid:getplan[0].projectid}).toArray();
	dataplan.planadded = findplanadded.length;
	var dataplandetail = {
		planname:getplan[0].planname,
		jobvalue:getplan[0].jobvalue,
		duration:getduration[0].durationorder,
		durationid:getduration[0]._id,
		timelength:getplan[0].timelength,
		averagewage:getplan[0].averagewages,
		moneydifference:getplan[0].moneydifference,
		enforceemployee:getplan[0].enforceemployee
	}
	for(var i = 0; i < getallduration.length; i++){
		dataplan.duration.push(getallduration[i]);
	}
	dataplan.plandetial.push(dataplandetail);
	console.log(dataplan);
	console.log("---------------------------------");
	return dataplan;
} 

exports.setamountorder = async(amountorder,projectid,userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const setamountplan = await db.collection("projects").updateOne(
									{"_id":ObjectId(projectid)},
									{$set: {amountplan: amountorder}});
	//get activity_id  
	var activityorder = "2.2";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });  
	return {data:"set amount plan success.",draft:"2.3",status:true};
}

exports.getsetamountorder = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	const getamountorder = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray(); 
	return {projectid:projectid,amountplan:getamountorder[0].amountplan};
}

exports.updateplanorder = async(planid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const getdurationnumber = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
	const updateplanorder = await db.collection("planorders").updateOne(
									{"_id":ObjectId(planid)},
									{ $set: { planname: planname,
											  jobvalue: jobvalue,
											  durationid: durationid,
											  durationorder:getdurationnumber[0].durationorder,
											  timelength: timelength,
											  averagewages: averagewages,
											  moneydifference:moneydifference,
											  enforceemployee:enforceemployee,
											  updatetime:pastdatetime._created}});
	return {data:"update plan sucess.",planid:planid,status:true};
}

exports.addamounteam = async(userid,projectid,amount)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	//add amount user in projects
	const addamountteam = await db.collection("projects").updateOne(
							{"_id":ObjectId(projectid)},
							{ $set: {
								amountteam:amount
							}});
	console.log("add amount team in project: "+ amount);
	//add activity 
	var activityorder = "3.1"; // เพิ่มจำนวนคนทำงาน
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("userid: "+ userid);
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });
	return {data:"add amount success.",status:true};
}

exports.addteamwork = async(userid,projectid,fristname,lastname,position,profileimage,email,phone)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const checkemail = await db.collection("users").find({email:email}).toArray();
	if(checkemail.length > 0){
		var respond = {data:"data: email has been used.",status:false};
		return respond;
	}else{
		var userobj={
			projectid:projectid,
			fristname:fristname,
			lastname:lastname,
			position:position,
			email:email,
			phone:phone,
			profileimage:profileimage,
			Insdate:pastdatetime._created,
			active:1
		};
	//add teamwork tp project 
	const insertteam = await db.collection("users").insertOne(userobj);
	//get activity_id  
	var activityorder = "3.2";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	console.log("update amount plan success.");
	console.log("projectid: "+ projectid);
	console.log("---------------------------");
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });
	//get amount team 
	// const getamountteam = await db.collection("users").find({projectid:activityorder}).toArray();
	// console.log("get team in project: "+ getamountteam.length);

	return {data:"insert team success.",status:true};
	}
}

exports.getteamwork = async(projectid,userprimarykeyid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var teamwork={
		projectid:projectid,
		amountset:0,
		addedteam:0,
		team:[
			//fristname
			//lastname
			//position
			]
	}
	//get set amount team 
	const getsetamountteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	teamwork.amountset = getsetamountteam[0].amountteam;
	//find user primary
	const userprimary = await db.collection("users").find({"_id":ObjectId(userprimarykeyid)}).toArray();
	var objuser_primary = {
		userid:userprimary[0]._id,
		fristname:userprimary[0].fristname,
		lastname:userprimary[0].lastname,
		position:userprimary[0].position
	}
	teamwork.team.push(objuser_primary);
	const getteamwork = await db.collection("users").find({"projectid":projectid}).toArray();
	teamwork.addedteam = (getteamwork.length +1);//+1 is userpirmary
	for(var i = 0; i < getteamwork.length; i++){
		var objuser = {
			userid:getteamwork[i]._id,
			fristname:getteamwork[i].fristname,
			lastname:getteamwork[i].lastname,
			position:getteamwork[i].position
		}
		teamwork.team.push(objuser);
	}
	console.log("-------------getteamwork------------------");
	console.log(teamwork);
	console.log("--------------------------------");
	return teamwork;
}

exports.getteamwhereid = async(userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var typeuser = "";
	var password = "not password";
	//get type user 
	const gettypeuser = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	//get users
	const getdetailuser = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	if(gettypeuser.length == 0){
		typeuser = "normal user";
	}else{
		typeuser = "primary user";
		password = getdetailuser[0].password;
	}
	var datauser={
		userid:userid,
		detail:{
			fristname:getdetailuser[0].fristname,
			lastname:getdetailuser[0].lastname,
			position:getdetailuser[0].position,
			profileiamge:getdetailuser[0].profileiamge,
			email:getdetailuser[0].email,
			password:password,
			phone:getdetailuser[0].phone,
			typeuser:typeuser
		}
	}
	return datauser;
}

exports.updatedatauser = async(userid,fristname,lastname,position,profileimage,email,phone)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(profileimage == ""){
		const updateuser = await db.collection("users").updateOne(
							{"_id":ObjectId(userid)},
							{ $set: { fristname:fristname,
									  lastname:lastname,
									  position:position,
									  email:email,
									  phone:phone,
									  updatetime:pastdatetime._created
							}});
		return {data:"update user success.",userid:userid,status:true};
	}else{
		const updateuser = await db.collection("users").updateOne(
							{"_id":ObjectId(userid)},
							{ $set: { fristname:fristname,
									  lastname:lastname,
									  position:position,
									  profileimage:profileimage,
									  email:email,
									  phone:phone,
									  updatetime:pastdatetime._created
							}});
		return {data:"update user success.",userid:userid,status:true};
	}
}

exports.updatedocumentproject = async(projectid,documentname)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	const updatedoc = await db.collection("projects").updateOne(
								{"_id":ObjectId(projectid)},
								{ $set: {Documentation:documentname}});
	return {data:"update documentation success.",projectid:projectid,status:true};
}


exports.sendproject = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	var status = 2 //send peoject is pending not update or edit project (bsm)
	const updateststusproject = await db.collection("projects").updateOne(
										{"_id":ObjectId(projectid)},
										{ $set: {
												 status:status,
												 senddatetime:pastdatetime._created
										}});
	//update status project in batchbusiinessman = 2 
	const updatebsm = await db.collection("batchbusinessman").updateOne(
								{"projectid":ObjectId(projectid)},
								{$set: {
										 updatetime:pastdatetime._created,
										 status:"2"//send and pending.
										}
								});
	//insert activity status
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var objlog={
		projectid:projectid,
		projectname:getdetailproject[0].projectname,
		userid:getdetailproject[0].Primaryresponsibility,
		status:3,
		statusstring:"ส่งเอกสารแล้ว",
		datetimestamp:pastdatetime._created
	}
	const insertlog =  await db.collection("logactivityproject").insertOne(objlog); 
	return {data:"sending project success.",projectid:projectid,statusproject:"sending",status:true}; 
}

//add duration(งวดงาน)
exports.addduration = async(projectid,durationorder,userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("duration index:"+durationorder.length);
	console.log(durationorder);
	//insert duration
	for(var i = 0; i < durationorder.length; i++){
		const durationmonth = await db.collection("duration").insert({
									durationorder:durationorder[i].durationorder, 
									month: durationorder[i].month,
									projectid:projectid
							  });
		console.log("durationorder: "+durationorder[i].durationorder);
		console.log("month: "+durationorder[i].month);
		console.log("-----------------------------");
	}
	
	//get activity_id  
	var activityorder = "2.1";
	const getactivity_id = await db.collection("activity").find({activityorder:activityorder}).toArray();
	console.log("activity _id: " + getactivity_id[0]._id);
	//insert activityusers
	const insert_activity_user = await db.collection("activityusers").insert({
									userid:userid,
									activityid:getactivity_id[0]._id,
									datetime:pastdatetime._created
							  });  
	return {data:"insert duration success.",draft:"2.2",status:true};
} 

exports.testcountdate = async()=>{
	// 7 days in week 
	// 7 = sunday at this week 
	// 6 = saturday at this week
	//------------------------
	// -7 = sunday at last weeked.
	// -6 = monday at last weeked.
	// -1 = saturday at last weeked.

	// set default start Timesheet at maonday next week.
	var days = moment().day(7+1);
	console.log("days: "+ days);
	console.log("day: "+ days.format("dddd"));
	console.log("at date: "+ days.format("D"));
	console.log("month: "+days.format("MMMM"));
	console.log("year: "+ days.format("YYYY"));
	console.log("------------------------------");

	//get todays date
   var today = moment()
	//get birth date
   var birthday = moment(1993+"/"+6+"/"+8, 'YYYY-MM-DD');
   var result = moment.duration(8).months();
   console.log(today.diff(birthday, 'day'));
   console.log("-----------------------------");

   //add days 
   var date = moment().add(7, 'days');
   var datelocate = date.locale('th');
   console.log("add days: " + datelocate.format('DD/MM/YY'));

}


exports.getamount_duration_order = async(userid,projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objduration_order = {
		projectid:projectid,
		userid:userid,
		duration:[],
		planadded:0,
		amountplan:0
	}
	//get duration 
	const getduration = await db.collection("duration").find({projectid:projectid}).toArray();
	console.log("projectid: "+ projectid);
	console.log("userid: "+ userid);
	console.log("---------------------");
	console.log("duratoin");
	console.log(getduration);
	for(var i =0; i < getduration.length; i++){
		var durationid = getduration[i]._id;
		var durationnumber = getduration[i].durationorder;
		var month = getduration[i].month;
		objduration_order.duration.push({durationid:durationid,durationnumber:durationnumber,month:month});
	}

	console.log("---------------------");
	//get amount plan  : amountplan
	const getamountlan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	objduration_order.amountplan = getamountlan[0].amountplan
	console.log("get amount plan in project: "+ getamountlan[0].amountplan)
	//get plan 
	const getplan = await db.collection("planorders").find({projectid:projectid}).toArray();
	console.log("-------------getplan---------------");
	console.log("plan: "+ getplan.length);
	console.log("-----------------------------------");
	var planadded = getplan.length;
	objduration_order.planadded = planadded;
	return objduration_order;
}

exports.getallduration = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	//get duration 
	const getallduration = await db.collection("duration").find({projectid:projectid}).toArray();
	console.log("-------getallduration------");
	console.log(getallduration);
	console.log("-------------------");
	return getallduration;
}

exports.updateduration = async(duration,projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	for(var i = 0; i < duration.length;i++){
		if(duration[i].durationid == ""){
			//insert duration 
			var objduration = {
				durationorder:duration[i].durationorder,
				month:duration[i].month,
				projectid:projectid
			};
			const insertduration =  await db.collection("duration").insertOne(objduration);
			console.log("insert duration: "+ i);

			console.log("----------------------------------------------");
		}else{
			//check duraion in db 
			const getduration = await db.collection("duration").find({"_id":ObjectId(duration[i].durationid)}).toArray();
			console.log(duration[i].durationid);
			var durationorder = duration[i].durationorder;
			var month = duration[i].month;
			// update duration
			const updateduration = await db.collection("duration").updateOne(
												{"_id":ObjectId(duration[i].durationid)},
												{ $set: { 
													durationorder:duration[i].durationorder,
													month:duration[i].month
												}});
			console.log("update duration: "+i);
			console.log("-----------------------------------------------");
		}
	}
	return {data:"update all duration success.",status:true};
}

exports.delete_user =async(userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	//check userid is primary user.
	const checkuserid = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	console.log("length: " + checkuserid.length);
	if(checkuserid.length == 0){
		var objuser = {"_id":ObjectId(userid)};
		const deleteuser = await db.collection("users").deleteOne(objuser);
		console.log("data: delete user success.");
		return {data:"delete user success.",status:true};
	}else{
		console.log("projectid: "+checkuserid[0]._id);
		console.log("data: this userid it's primary user.");
		return {data:"this userid it's primary user.",status:false};
	}
}

exports.delete_plan = async(planid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objplan = {"_id":ObjectId(planid)};
	const deleteplan = await db.collection("planorders").deleteOne(objplan);
	console.log("data:delete plan sucess.");
	return {data:"delete plan sucess.",status:true};
}

exports.getdetailproject = async(projectid,userid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objdetail_project = {
		projectid:"",
		projectname:"",
		projectvalue:0,
		userprimary:"",
		documentation:"",
		plans:[],
		team:[],
		percentproject:0,
		statusproject:"",
		Fill_in:""
	};

	//get datadetail project 
	const getdetail_project = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(getdetail_project[0].status == 1){
		objdetail_project.statusproject = "กำลังดำเนินการ";
		objdetail_project.Fill_in = "กำลังกรอกข้อมูล";
	}else if(getdetail_project[0].status == 2){
		objdetail_project.statusproject = "รอดำเนินการ";
		objdetail_project.Fill_in = "ส่งเอกสารแล้ว";
	}else if(getdetail_project[0].status == 3){
		objdetail_project.statusproject = "ปรับแก้เอกสาร";
		objdetail_project.Fill_in = "ปรับแก้เอกสาร";
	}else if(getdetail_project[0].status == 4){
		objdetail_project.statusproject = "อนุมัติ Timesheet";
		objdetail_project.Fill_in = "อนุมัติ Timesheet";
	}

	//get userprimary 
	const getuserprimary = await db.collection("users").find({"_id":ObjectId(userid)}).toArray();
	var objuserprimary = {
		userid:getuserprimary[0]._id,
		fristname:getuserprimary[0].fristname,
		lastname:getuserprimary[0].lastname,
		position:getuserprimary[0].position
	}
	objdetail_project.team.push(objuserprimary);

	console.log("------------------------------");

	console.log("projectid: "+ projectid);
	objdetail_project.projectid = projectid;

	console.log("projectname: "+ getdetail_project[0].projectname);
	objdetail_project.projectname = getdetail_project[0].projectname;

	console.log("projectvalue: "+ getdetail_project[0].projectvalue);
	objdetail_project.projectvalue = getdetail_project[0].projectvalue;

	console.log("userprimary: "+ getuserprimary[0].fristname + " " + getuserprimary[0].lastname);
	objdetail_project.userprimary = getuserprimary[0].fristname + " " + getuserprimary[0].lastname;

	console.log("documentation: "+getdetail_project[0].Documentation);
	objdetail_project.documentation = getdetail_project[0].Documentation;

	//get plan in project 
	const getplaninproject = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var i = 0; i < getplaninproject.length; i++){
		const getdurationorders = await db.collection("duration").find({"_id":ObjectId(getplaninproject[i].durationid)}).toArray();
		var durationorder = getdurationorders[0].durationorder;
		var objplan = {
			planid:getplaninproject[i]._id,
			planname:getplaninproject[i].planname,
			duration:durationorder,
			jobvalue:getplaninproject[i].jobvalue,
			timelength:getplaninproject[i].timelength
		}
		objdetail_project.plans.push(objplan);
		console.log("plans: ");
		console.log(objplan);
	}

	//documentation 
	var partdocument =0;
	const getdocumetation = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if (getdocumetation[0].Documentation == ""){
		partdocument = 0;
	}else{
		partdocument = 100;
	}
	console.log("percent part 1: "+partdocument);
	var percent1 = partdocument;
	console.log("---------------------------------------");

	//plan order
	console.log("amount plan: "+ getdocumetation[0].amountplan);
	var amountplan = getdocumetation[0].amountplan;
	const getallplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	var plans = getallplanorder.length;
	console.log("plans: "+plans);
	var percentplan = 100/amountplan;
	var calculate_percent_plan = percentplan * plans;
	console.log("percent part 2: "+calculate_percent_plan);
	var percent2 = calculate_percent_plan;
	console.log("---------------------------------------");

	//team 
	const get_amount_team = await  db.collection("users").find({projectid:projectid}).toArray();
	var amountusers = get_amount_team.length;//team_added
	var userprimary = 1;
	console.log("amount team: "+ (amountusers+userprimary));
	var allusers = (amountusers+userprimary);
	const getamount_setteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var setamount = getamount_setteam[0].amountteam;
	var percentteam = 100/setamount;
	console.log("percent amount users: "+ percentteam);
	var percent3 = (percentteam * allusers);
	console.log("percent part 3:"+ percentteam * allusers);
	console.log("---------------------------------------");

	//percent1to3
	objdetail_project.percentproject = ((percent1 + percent2 + percent3) * 100) /300;

	//get team 
	const get_team = await db.collection("users").find({projectid:projectid}).toArray();
	if(get_team.length == 0){
		console.log("team: "+ objdetail_project.team[0].fristname + " " +objdetail_project.team[0].lastname);
		console.log("-----------------------------");
		return objdetail_project;
	}else{
		console.log("team: ");
		for(var i = 0; i < get_team.length; i++){
			var objuser ={
				userid:get_team[i]._id,
				fristname:get_team[i].fristname,
				lastname:get_team[i].lastname,
				position:get_team[i].position
			}
			console.log(objuser);
			objdetail_project.team.push(objuser);
		}
		console.log("-----------------------------");
		return objdetail_project;
	}
}

exports.updatepassword = async(userid,password)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	const updatepassword = await db.collection("users").updateOne(
												{"_id":ObjectId(userid)},
												{ $set:{password:password}});
	return {data:"update password success.",status:true};
}

exports.getamountteam =async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objamountteam = {
		amountset:0,
		amountadded:0
	}
	//get amount 
	const getamountteam = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	objamountteam.amountset = getamountteam[0].amountteam;
	//get amount team added
	const getteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	objamountteam.amountadded = (getteamadded.length +1);
	console.log("-----------getamountteam-------------");
	console.log(objamountteam);
	return objamountteam;
}

exports.updateamountteam = async(projectid,amount) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	//update amount team 
	const updateamountteam = await db.collection("projects").updateOne(
												{"_id":ObjectId(projectid)},
												{ $set: {amountteam:amount}});
	return {data:"update success.",status:true};
}

exports.checklastdraft = async(userid,projectid) =>{
	console.log("-----------Check last draft----------");
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);

	//check part 1 : upload document 
	const checkdocument = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkdocument[0].Documentation == ""){
		console.log("draft: last activity upload documentation.");
		var respond = {data:"last activity upload documentation.",activity:"1",activityname:"เอกสารประกอบโครงการ"};
		return respond;
	}else{
		console.log("draft: activity upload documentation success.");
	}

	//check part 2 : create project
	const checkcreateproject = await db.collection("projects").find({Primaryresponsibility:userid}).toArray();
	if(checkcreateproject[0].projectname == "" || checkcreateproject[0].projectvalue == ""){
		console.log("draft: last activity create project.");
		var respond = {data:"last activity create project.",activity:"2",activityname:"แผนการใช้งบประมาณ"};
		return respond;
	}else{
		console.log("draft: activity create project success.");
	}

	//check part 2.1 : add amount duration
	const checkamountduration = await db.collection("duration").find({projectid:projectid}).toArray();
	if(checkamountduration.length == 0){
		console.log("draft: last activity create duration.");
		var respond = {data:"last activity create duration.",activity:"2.1",activityname:"สร้างงวดงาน"};
		return respond;
	}else{
		console.log("draft: activity create duration success.");
	}

	//check part 2.2 create amount plan 
	const checkamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkamountplan[0].amountplan == 0){
		console.log("draft: last activity create amount plan.");
		var respond = {data:"last activity create amount plan.",activity:"2.2",activityname:"สร้างจำนวนแผนงาน"};
		return respond;
	}else{
		console.log("draft: activity create amount plan success.");
	}

	//check part 2.3 create plan 
	const checkplanorder = await db.collection("planorders").find({projectid:projectid}).toArray();
	const getamountplan = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var planadded = checkplanorder.length;
	var amountplan = getamountplan[0].amountplan;
	if(planadded < amountplan){
		console.log("draft: last activity create plan order.");
		var respond = {data:"last activity create plan order.",activity:"2.3",activityname:"สร้างแผนงาน"};
		return respond;
	}else{
		console.log("draft: activity create plan order success.");
	}

	//check part 3.1 create amount team
	const checkamountteam =  await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	if(checkamountteam[0].amountteam == 0){
		console.log("draft: last activity create amount team.");
		var respond = {data:"last activity create amount team.",activity:"3.1",activityname:"เพิ่มจำนวนคนทำงาน"};
		return respond;
	}else{
		console.log("draft: activity create amount team success.");
	}

	//check part 3.2 create normal users 
	var amountteam = checkamountteam[0].amountteam;
	const checkteamadded = await db.collection("users").find({projectid:projectid}).toArray();
	if((checkteamadded.length + 1) < amountteam){
		console.log("draft: last activity create normal users.");
		console.log("users added: " + (checkteamadded.length + 1));
		var respond = {data:"last activity create normal users.",activity:"3.2",activityname:"เพิ่มข้อมูลรายละเอียดสมาชิก"};
		return respond;
	}else{
		console.log("draft: activity create normal success.");
		var respond = {data:"activity create normal success.",activity:"4",activityname:"สรุปและส่งข้อมูลโครงการ"};
		return respond;
	}
}	

exports.getplandashboard = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var objproject = {
		projectid:"",
		projectname:"",
		statusproject:"",
		projectvalue:0,
		valueused:0,
		fristname:"",
		lastname:"",
		week:0,
		day:0,
		planorders:[]
	}
	const getprojectdetail = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	const getuserdetail =  await db.collection("users").find({"_id":ObjectId(getprojectdetail[0].Primaryresponsibility)}).toArray();
	objproject.projectid = projectid;
	objproject.projectname = getprojectdetail[0].projectname;
	objproject.statusproject = getprojectdetail[0].status;
	objproject.projectvalue = getprojectdetail[0].projectvalue;
	objproject.fristname = getuserdetail[0].fristname;
	objproject.lastname = getuserdetail[0].lastname;
	var datenow = moment().format("D/MMMM/YYYY");
	console.log("this day: "+datenow);
	const check_date_on_week = await db.collection("days").find({datestring:datenow}).toArray();
	if(check_date_on_week.length == 0){
		console.log("no have day on week.");
		return {data:"no have day on week.",status:false};
	}else{
		console.log("have day on week.");
		//get plan on this week.
		var weekid = check_date_on_week[0].weekid;
		const getweeknumber = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
		var weeknumber = getweeknumber[0].weeknumber;
		objproject.week = weeknumber;
		objproject.day = check_date_on_week[0].daynumber;
		const getplanonweek = await db.collection("weeksplanorder").find({weekid:weekid}).toArray();
		for(var i = 0; i < getplanonweek.length;i++){
			var planid = getplanonweek[i].planid;
			const getplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
			var durationid = getplan[0].durationid;
			const getduration = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
			var objplan = {
				planid:planid,
				planname:getplan[0].planname,
				planvalue:getplan[0].jobvalue,
				valueused:0,
				duration:getduration[0].durationorder,
				timelength:getplan[0].timelength,
				timeused:0
			}
			objproject.planorders.push(objplan);
		}
		return objproject;
	}

}

//weekly progress
exports.getallweeksofplan = async(projectid,planid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var datenow = moment().format("D/MMMM/YYYY");
	console.log("this day: "+datenow);
	var obj_weekly = {
		projectid:projectid,
		projectname:"",
		projectvalue:0,
		totalvalue:0,
		weekid:"",
		weeknumber:0,
		dayid:"",
		daynumber:0,
		weeks:[]
	}

	//get project value 
	const getprojectvalue = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var projectvalue = getprojectvalue[0].projectvalue;
	obj_weekly.projectvalue = projectvalue;
	obj_weekly.projectname = getprojectvalue[0].projectname;

	//Sum total value timsheet from timesheet => totalvalueonday
	var totalvalue = 0;
	const get_total_value_timesheet = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var project = 0; project < get_total_value_timesheet.length;project++){
		totalvalue += get_total_value_timesheet[project].totalvalueonday;
	}
	obj_weekly.totalvalue = totalvalue;


	//find days on weeks 
	const getdayonweek = await db.collection("days").find({datestring:datenow}).toArray();
	if(getdayonweek.length == 0){
		console.log("data: no have this day on week.");
		return {data:"no have this day on week.",status:false};	
	}else{
		//find week of plan 
		const getweekofplan = await db.collection("weeks").find({planid:ObjectId(planid)}).toArray();
		for(var week = 0;week < getweekofplan.length; week++){
			console.log(getweekofplan[week]);
			obj_weekly.weeks.push(getweekofplan[week]);
		}

		console.log("weekid: "+getdayonweek[0].weekid);	
		obj_weekly.weekid = getdayonweek[0].weekid;
		//find detail week.
		const getweekdetail = await db.collection("weeks").find({$and:[{"_id":getdayonweek[0].weekid},{projectid:projectid}]}).toArray();
		obj_weekly.weeknumber = getweekdetail[0].weeknumber;
		obj_weekly.dayid = getdayonweek[0]._id;
		obj_weekly.daynumber = getdayonweek[0].daynumber;
		console.log("week number: "+ obj_weekly.weeknumber);
		console.log("day id: "+obj_weekly.dayid);
		console.log("day number" + obj_weekly.daynumber);
		// var obj_thisweek ={
		// 	weekid:getdayonweek[0].weekid,
		// 	weeknumber:getweekdetail[0].weeknumber,
		// 	status:getweekdetail[0].statusweek,
		// 	statusstring:"บันทึกความก้าวหน้า"
		// }
		// obj_weekly.weeks.push(obj_thisweek);


		// //find all weeked.
		// const getallweeked_reject = await db.collection("weeks").find({$and:[{projectid:projectid},{statusweek:2}]}).toArray();
		// for(var reject = 0; reject < getallweeked_reject.length;reject++){
		// 	var obj_rejectweek ={
		// 		weekid:getallweeked_reject[0].weekid,
		// 		weeknumber:getallweeked_reject[0].weeknumber,
		// 		status:getallweeked_reject[0].statusweek,
		// 		statusstring:"ปรับแก้"
		// 	}
		// 	console.log("weekid: "+getallweeked_reject[reject].weekid);
		// 	obj_weekly.weeks.push(obj_rejectweek);
		// }
		// const getallweeked_approve = await db.collection("weeks").find({$and:[{projectid:projectid},{statusweek:3}]}).toArray();
		// for(var approve = 0; approve < getallweeked_approve.length;approve++){
		// 	var obj_approveweek ={
		// 		weekid:getallweeked_approve[0].weekid,
		// 		weeknumber:getallweeked_approve[0].weeknumber,
		// 		status:getallweeked_approve[0].statusweek,
		// 		statusstring:"ตรวจสอบแล้ว"
		// 	}
		// 	console.log("weekid: "+getallweeked_approve[reject].weekid);
		// 	obj_weekly.weeks.push(obj_approveweek);
		// }

		return obj_weekly;
	}
	
}

//Daily progress
exports.dailyprogress = async(projectid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var datenow = moment().format("D/MMMM/YYYY");
	console.log("this day: "+datenow);
	var obj_project={
		projectid:projectid,
		projectname:"",
		projectvalue:0,
		valueused:0,
		weekid:"",
		weeknumber:0,
		dayid:"",
		daynumber:0,
		plans:[]
	}

	//Sum total use value
	var sumtotal =0;
	const sumusedvalue = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var i =0; i < sumusedvalue.length; i++){
		sumtotal += sumusedvalue[i].totalvalueonday;
	}
	obj_project.valueused = sumtotal;
	
	//getdetail project
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	obj_project.projectname = getdetailproject[0].projectname;
	obj_project.projectvalue = getdetailproject[0].projectvalue;

	//get weekid , dayid
	const getday_detail = await db.collection("days").find({datestring:datenow}).toArray();
	obj_project.dayid = getday_detail[0]._id;
	obj_project.daynumber = getday_detail[0].daynumber;
	const getweekdetail = await db.collection("weeks").find({$and:[{projectid:projectid},{"_id":ObjectId(getday_detail[0].weekid)}]}).toArray();
	obj_project.weekid = getweekdetail[0]._id;
	obj_project.weeknumber = getweekdetail[0].weeknumber;

	//get all plans in project
	const getallplans = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var plan = 0;plan < getallplans.length; plan++){
		var obj_plan ={
			planid:"",
			planname:"",
			duraionid:"",
			durationorder:"",
			jobvalue:0,
			valueused:0,
			timelength:0,
			timeused:0,
			status_timesheet:false
		}
		const check_planonweek = await db.collection("weeks").find({$and:[{planid:ObjectId(getallplans[plan]._id)},{weeknumber:getweekdetail[0].weeknumber}]}).toArray();

		if(check_planonweek.length > 0){
			obj_plan.status_timesheet = true;
		}else{
			obj_plan.status_timesheet = false;
		}

		obj_plan.planid = getallplans[plan]._id;
		obj_plan.planname = getallplans[plan].planname;
		obj_plan.durationid = getallplans[plan].durationid;
		obj_plan.jobvalue = getallplans[plan].jobvalue;
		obj_plan.timelength = getallplans[plan].timelength;

		//Sum total value used on plan 
		var TotalValueUsedOnplan = 0;
		var TotalTimeUsed = 0;
		const getvalue_used_onplan = await db.collection("timesheet").find({planid:getallplans[plan]._id}).toArray();
		for(var value = 0; value < getvalue_used_onplan.length; value++){
			TotalValueUsedOnplan += getvalue_used_onplan[value].totalvalueonday;
			TotalTimeUsed +=getvalue_used_onplan[value].hours;
		}
		obj_plan.valueused = TotalValueUsedOnplan;
	
		//Sum total timeused
		obj_plan.timeused = TotalTimeUsed / 8;


		//get duration order 
		const getduration = await db.collection("duration").find({"_id":ObjectId(getallplans[plan].durationid)}).toArray();
		obj_plan.durationorder = getallplans[plan].durationorder;
		obj_project.plans.push(obj_plan);
	}
	console.log(obj_project);
	return obj_project;
}

exports.getweektimesheet = async(dayid,projectid,planid,weeknumber) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var getweekid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{planid:ObjectId(planid)}]}).toArray();
	console.log("weekid: " + getweekid[0]._id);
	var weekid = getweekid[0]._id.toString();
	
	var obj_thisweek = {
		projectid:projectid,
		planid:planid,
		planname:"",
		jobvalue:0,
		usedvalue:0,
		timelength:0,
		timeused:0,
		weekid:weekid,
		weeknumber:0,
		dayid:dayid,
		daynumber:0,
		team:[],
		day1:[],
		day1_id:"",
		statusday1:false,
		day2:[],
		day2_id:"",
		statusday2:false,
		day3:[],
		day3_id:"",
		statusday3:false,
		day4:[],
		day4_id:"",
		statusday4:false,
		day5:[],
		day5_id:"",
		statusday5:false,
		day6:[],
		day6_id:"",
		statusday6:false
	}


	//get day number 
	const getdetailday = await db.collection("days").find({"_id":ObjectId(dayid)}).toArray();
	obj_thisweek.daynumber = getdetailday[0].daynumber;

	//get detail plan
	const getdetailplan = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	obj_thisweek.planname = getdetailplan[0].planname;
	obj_thisweek.jobvalue = getdetailplan[0].jobvalue;
	obj_thisweek.timelength = getdetailplan[0].timelength;

	//get sum timeused
	var sumtotal = 0;
	var sumhours = 0;
	const gettimeused = await db.collection("timesheet").find({planid:planid}).toArray();
	for(var day =0; day < gettimeused.length; day++){
		sumtotal += gettimeused[day].totalvalueonday;
		sumhours += gettimeused[day].hours;
	}
	obj_thisweek.usedvalue = sumtotal;
	obj_thisweek.timeused = sumhours;

	//get week detail 
	const getweekdetail = await db.collection("weeks").find({"_id":ObjectId(weekid)}).toArray();
	obj_thisweek.weeknumber = getweekdetail[0].weeknumber;

	//get team (primary user)
	const getteam_primary_user = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	obj_primaryuser ={
		userid:getteam_primary_user[0].Primaryresponsibility,
		username:""
	}
	const getusername_primaryuser = await db.collection("users").find({"_id":ObjectId(getteam_primary_user[0].Primaryresponsibility)}).toArray();
	obj_primaryuser.username = getusername_primaryuser[0].fristname+" "+getusername_primaryuser[0].lastname;
	obj_thisweek.team.push(obj_primaryuser);

	//get user in team 
	const getallteam = await db.collection("users").find({projectid:projectid}).toArray();
	for(var user =0;user < getallteam.length;user++){
		obj_user = {
			userid:getallteam[user]._id,
			username:getallteam[user].fristname +" "+getallteam[user].lastname
		}
		obj_thisweek.team.push(obj_user);
	}

	//set on this day dtms get open and close day.
	var onthisday_dtms = 0;//moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]);
	const get_onthisday = await db.collection("days").find({"_id":ObjectId(dayid)}).toArray();
	onthisday_dtms = moment(get_onthisday[0].datetimestamp._d,["YYYY-MM-DD"]);
	console.log("on this day: " + onthisday_dtms);
	console.log("on this day string: "+onthisday_dtms.format("YYYY-MM-DD"));
	console.log("---------------------------");

	//get days on weeks
	var sort = {daynumber:1};
	const getdays_onweek = await db.collection("days").find({weekid:ObjectId(weekid)}).sort(sort).toArray();
	console.log("weekid: "+obj_thisweek.weekid);
	console.log("week number: "+obj_thisweek.weeknumber);
	for(var days = 0;days < getdays_onweek.length;days++){
		console.log("dayid: " + getdays_onweek[days]._id);
		console.log("daynumber: " + getdays_onweek[days].daynumber);
		console.log("datetimestamp: "+ moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]));
		console.log("on this day: "+onthisday_dtms.format("YYYY-MM-DD"));
		var convert_getdate_dtsm = moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]);
		console.log("convert_getdate_dtsm: "+convert_getdate_dtsm.format("YYYY-MM-DD"));
		
		if(getdays_onweek[days].daynumber == 1){
			obj_thisweek.day1_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday1 = true;
			}else{
				obj_thisweek.statusday1 = false;
			}
		}else if(getdays_onweek[days].daynumber == 2){
			obj_thisweek.day2_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday2 = true;
			}else{
				obj_thisweek.statusday2 = false;
			}
		}else if(getdays_onweek[days].daynumber == 3){
			obj_thisweek.day3_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday3 = true;
			}else{
				obj_thisweek.statusday3 = false;
			}
		}else if(getdays_onweek[days].daynumber == 4){
			obj_thisweek.day4_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday4 = true;
			}else{
				obj_thisweek.statusday4 = false;
			}
		}else if(getdays_onweek[days].daynumber == 5){
			obj_thisweek.day5_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday5 = true;
			}else{
				obj_thisweek.statusday5 = false;
			}
		}else if(getdays_onweek[days].daynumber == 6){
			obj_thisweek.day6_id = getdays_onweek[days]._id;
			if(convert_getdate_dtsm <= onthisday_dtms){
				obj_thisweek.statusday6 = true;
			}else{
				obj_thisweek.statusday6 = false;
			}
		}

		console.log("count team: " + obj_thisweek.team.length);
		console.log("---------------------------");
		for(var i = 0;i < obj_thisweek.team.length;i++){
			var dayid_find = getdays_onweek[days]._id.toString();
			var userid_find = obj_thisweek.team[i].userid.toString();
			console.log("dayid: " + dayid_find);
			console.log("userid: "+ userid_find);
			const gettimesheet = await db.collection("timesheet").find({$and:[{userid:userid_find},{dayid:dayid_find}]}).toArray();
			console.log("gettimesheet.length: " + gettimesheet.length);
			if(gettimesheet.length == 0){
				console.log("hours: " + 0);
				var obj_user_timesheet_onday = {
					userid:userid_find,
					dayid:dayid_find,
					hours:0
				}
				if(getdays_onweek[days].daynumber == 1){
					obj_thisweek.day1.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 2){
					obj_thisweek.day2.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 3){
					obj_thisweek.day3.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 4){
					obj_thisweek.day4.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 5){
					obj_thisweek.day5.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 6){
					obj_thisweek.day6.push(obj_user_timesheet_onday);
				}
			}else{
				console.log("hours: " + gettimesheet[0].hours);	
				var obj_user_timesheet_onday = {
					userid:userid_find,
					dayid:dayid_find,
					hours:gettimesheet[0].hours
				}
				if(getdays_onweek[days].daynumber == 1){
					obj_thisweek.day1.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 2){
					obj_thisweek.day2.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 3){
					obj_thisweek.day3.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 4){
					obj_thisweek.day4.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 5){
					obj_thisweek.day5.push(obj_user_timesheet_onday);
				}else if(getdays_onweek[days].daynumber == 6){
					obj_thisweek.day6.push(obj_user_timesheet_onday);
				}
			}
			console.log("---------------------------");
		}
		
	}// for days
	console.log(obj_thisweek);
	return obj_thisweek;
}

exports.addtimesheet = async(projectid,planid,weekid,day1,day2,day3,day4,day5,day6,day1_id,day2_id,day3_id,day4_id,day5_id,day6_id) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	console.log("day1: " + day1.length);
	console.log("day2: " + day2.length);
	console.log("day3: " + day3.length);
	console.log("day4: " + day4.length);
	console.log("day5: " + day5.length);
	console.log("day6: " + day6.length);
	console.log("------------------------------------");
	//get average value on day.
	const getaverage = await db.collection("planorders").find({"_id":ObjectId(planid)}).toArray();
	var cal_average = getaverage[0].averagewages;
	console.log("cal_average: "+cal_average);

	//day1
	console.log("-------------day1--------------");
	for(var d1 =0; d1 < day1.length;d1++){
		//console.log(day1[d1]);
		var obj_day_timesheet = {
			userid:day1[d1].userid,
			dayid:day1[d1].dayid,
			planid:planid,
			projectid:projectid,
			hours:day1[d1].hours,
			totalvalue:(parseInt(cal_average) / 8) * day1[d1].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);

		const checkday = await db.collection("timesheet").find({$and:[{dayid:day1_id},{userid:day1[d1].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day1[d1].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day2
	console.log("-------------day2--------------");
	for(var d2 =0; d2 < day2.length;d2++){
		//console.log(day2[d2]);
		var obj_day_timesheet = {
			userid:day2[d2].userid,
			dayid:day2[d2].dayid,
			planid:planid,
			projectid:projectid,
			hours:day2[d2].hours,
			totalvalue:(parseInt(cal_average) / 8) * day2[d2].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day2_id},{userid:day2[d2].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day2[d2].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}	
	}
	
	//day3
	console.log("-------------day3--------------");
	for(var d3 =0; d3 < day3.length;d3++){
		//console.log(day3[d3]);
		var obj_day_timesheet = {
			userid:day3[d3].userid,
			dayid:day3[d3].dayid,
			planid:planid,
			projectid:projectid,
			hours:day3[d3].hours,
			totalvalue:(parseInt(cal_average) / 8) * day3[d3].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day3_id},{userid:day3[d3].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day3[d3].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}	
	}
	
	//day4
	console.log("-------------day4--------------");
	for(var d4 =0; d4 < day4.length;d4++){
		//console.log(day4[d4]);
		var obj_day_timesheet = {
			userid:day4[d4].userid,
			dayid:day4[d4].dayid,
			planid:planid,
			projectid:projectid,
			hours:day4[d4].hours,
			totalvalue:(parseInt(cal_average) / 8) * day4[d4].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day4_id},{userid:day4[d4].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day4[d4].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day5
	console.log("-------------day5--------------");
	for(var d5 =0; d5 < day5.length;d5++){
		//console.log(day5[d5]);
		var obj_day_timesheet = {
			userid:day5[d5].userid,
			dayid:day5[d5].dayid,
			planid:planid,
			projectid:projectid,
			hours:day5[d5].hours,
			totalvalue:(parseInt(cal_average) / 8) * day5[d5].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day5_id},{userid:day5[d5].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day5[d5].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	
	//day6
	console.log("-------------day6--------------");
	for(var d6 =0; d6 < day6.length;d6++){
		//console.log(day6[d6]);
		var obj_day_timesheet = {
			userid:day6[d6].userid,
			dayid:day6[d6].dayid,
			planid:planid,
			projectid:projectid,
			hours:day6[d6].hours,
			totalvalue:(parseInt(cal_average) / 8) * day6[d6].hours,
			updatetime:pastdatetime._created
		}
		//console.log(obj_day_timesheet);	
		const checkday = await db.collection("timesheet").find({$and:[{dayid:day6_id},{userid:day6[d6].userid}]}).toArray();
		if(checkday.length > 0){
			//update day
			console.log("status: update");
			var dayid_update = obj_day_timesheet.dayid.toString();
			var userid_update = obj_day_timesheet.userid.toString();
			const update_timesheet = await db.collection("timesheet").updateOne(
												{dayid:dayid_update,userid:userid_update},
												{$set: 
													{
														hours:day6[d6].hours,
														totalvalue:obj_day_timesheet.totalvalue,
														updatetime:pastdatetime._created
													}
												});
		}else{
			//insert day 
			console.log("status: insert");
			const insert_timesheet = await db.collection("timesheet").insertOne(obj_day_timesheet);
			console.log(obj_day_timesheet);
		}
	}
	return {data:"add timesheet success.",status:true};
	
}

exports.getbatchname = async(batchid)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	console.log("batchid: " + batchid);
	const getbatchname = await db.collection("batch").find({"_id":ObjectId(batchid)}).toArray();
	console.log(getbatchname[0]);
	var objbatch = {batchname:getbatchname[0].modelname};
	return objbatch;
}

exports.getdetail_progress_week = async(projectid,weeknumber)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	
	//get detail project 
	const getdetailproject = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	//sum total value used
	var totalproject_valueused = 0;
	const get_total_valueused = await db.collection("timesheet").find({projectid:projectid}).toArray();
	for(var counttotal = 0;counttotal < get_total_valueused.length;counttotal++){
		totalproject_valueused+=get_total_valueused[counttotal].totalvalue;
	}

	var obj_progress_week = {
		projectid:projectid,
		projectname:getdetailproject[0].projectname,
		projectvalue:getdetailproject[0].projectvalue,
		usedvalueproject:totalproject_valueused,
		weeknumber:weeknumber,
		plans:[]
	}

	//get plan on week 
	const getplan_onweek = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	console.log("getplan_onweek: "+getplan_onweek.length);
	for(var plan = 0;plan < getplan_onweek.length;plan++){
		console.log("planid: " + getplan_onweek[plan].planid);

		var obj_plan = {
			planid:getplan_onweek[plan].planid,
			planname:"",
			durationid:"",
			durationorder:"",
			jobvalue:0,
			usevalue:0,
			timelength:0,
			timeused:0,
			team:[],
			document:"",
			barrier:""
		} 

		//get plan detail 
		const getplan_detail  = await db.collection("planorders").find({"_id":ObjectId(getplan_onweek[plan].planid)}).toArray(); 
		
		console.log("planname: " + getplan_detail[0].planname);
		obj_plan.planname = getplan_detail[0].planname;

		console.log("durationid: " + getplan_detail[0].durationid);
		obj_plan.durationid = getplan_detail[0].durationid;

		console.log("durationorder: " + getplan_detail[0].durationorder);
		obj_plan.durationorder = getplan_detail[0].durationorder;

		console.log("jobvalue: " + getplan_detail[0].jobvalue);
		obj_plan.jobvalue = getplan_detail[0].jobvalue;

		var sum_usedvalue = 0;
		var sum_timeused = 0;
		var planid_find = getplan_onweek[plan].planid.toString();
		const get_sumvaluesuse = await db.collection("timesheet").find({planid:planid_find}).toArray();
		for(var i = 0; i < get_sumvaluesuse.length; i++){
			sum_usedvalue += get_sumvaluesuse[i].totalvalue;
			sum_timeused += get_sumvaluesuse[i].hours;
		}
		console.log("usevalue: " + sum_usedvalue);
		obj_plan.usevalue = sum_usedvalue;

		console.log("timelength: " + getplan_detail[0].timelength)
		obj_plan.timelength = getplan_detail[0].timelength;

		console.log("timeused: " + (sum_timeused/8));
		obj_plan.timeused =sum_timeused / 8;

		
		//get primary user 
		var obj_userprimary = {
			userid:"",
			username:"",
			hours:0,
			comment:""
		}
		const get_userprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
		var userid_primary = get_userprimary[0].Primaryresponsibility.toString();
		const get_detail_user_primary = await db.collection("users").find({"_id":ObjectId(userid_primary)}).toArray();
		console.log("username: " + get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname);
		obj_userprimary.username = get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname;
		obj_userprimary.userid = userid_primary;
		//Sum hours
		var sum_totaluser_hour = 0;
		const get_sum_hours = await db.collection("timesheet").find({$and:[{planid:planid_find},{userid:userid_primary}]}).toArray();
		for(var a =0;a < get_sum_hours.length;a++){
			sum_totaluser_hour += get_sum_hours[a].hours;
		}
		console.log("total hours: "+ sum_totaluser_hour);
		obj_userprimary.hours = sum_totaluser_hour;
		obj_plan.team.push(obj_userprimary);
		console.log("-------------------------------------");


		//get all user 
		const getalluser_onproject = await db.collection("users").find({projectid:projectid}).toArray();
		console.log("getalluser_onproject.length: " +getalluser_onproject.length);
		for(var user =0; user < getalluser_onproject.length;user++){
			var obj_user= {
				userid:"",
				username:"",
				hours:0,
				comment:""
			}
			console.log("userid: "+ getalluser_onproject[user]._id);
			var userid = getalluser_onproject[user]._id.toString();
			console.log("username: " + getalluser_onproject[user].fristname +" "+ getalluser_onproject[user].lastname);
			obj_user.userid = getalluser_onproject[user]._id;
			obj_user.username = getalluser_onproject[user].fristname +" "+ getalluser_onproject[user].lastname;
			//Sum hours
			var sum_totaluser = 0;
			const get_sum_hours = await db.collection("timesheet").find({$and:[{planid:planid_find},{userid:userid}]}).toArray();
			for(var j =0;j < get_sum_hours.length;j++){
				sum_totaluser += get_sum_hours[j].hours;
			}
			console.log("total hours: "+ sum_totaluser);
			obj_user.hours = sum_totaluser;
			obj_plan.team.push(obj_user);
			console.log("-------------------------------------");
		}
		console.log("-----------------------------------");	
		obj_progress_week.plans.push(obj_plan);
	}
	console.log(obj_progress_week);
	return obj_progress_week;
}


exports.get_timesheet_allusers = async(projectid,weeknumber) =>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	var obj_progress_timesheet_user = {
		projectid:projectid,
		weeknumber:weeknumber,
		team:[]
	}
	var weekid = "";

	//get weekid 
	const getweekid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	weekid = getweekid[0]._id;
	console.log("getweekid[0]._id"+getweekid[0]._id);
	console.log("getweekid[1]._id"+getweekid[1]._id);
	//get userprimary 
	var obj_userprimary = {
		userid:"",
		username:"",
		totalhours:0,
		plans:[]
	}
	const getuserprimary = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();
	var userprimary_id = getuserprimary[0].Primaryresponsibility.toString();

	const get_detail_user_primary = await db.collection("users").find({"_id":ObjectId(userprimary_id)}).toArray();
	console.log("userid: " + userprimary_id);
	console.log("username: " + get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname);
	obj_userprimary.userid = userprimary_id;
	obj_userprimary.username = get_detail_user_primary[0].fristname +" "+ get_detail_user_primary[0].lastname;


	//get plan on week 
	var totalhours_on_week = 0;
	const get_plan = await db.collection("planorders").find({projectid:projectid}).toArray();
	for(var plan = 0; plan < get_plan.length; plan++){
		var obj_plan={
			planid: get_plan[plan]._id,
			planname:get_plan[plan].planname,
			durationid:"",
			durationorder:0,
			totalhours:0,
			valueused:0
		}
		console.log("planid: " + get_plan[plan]._id);
		var planid_find = get_plan[plan]._id.toString();
		const getplan_onweek = await db.collection("weeks").find({$and:[{planid:ObjectId(planid_find)},{weeknumber:weeknumber}]}).toArray();
		if(getplan_onweek.length == 0){
			console.log("getplan_onweek.length: "+getplan_onweek.length);
			console.log("----------------------------------------------");
		}else{

			console.log("getplan_onweek.length: "+getplan_onweek.length);
			console.log("durationid: "+getplan_onweek[0].durationid);
			obj_plan.durationid = getplan_onweek[0].durationid;
			const get_durationorder = await db.collection("duration").find({"_id":ObjectId(getplan_onweek[0].durationid)}).toArray();
			console.log("durationorder: " + get_durationorder[0].durationorder);
			obj_plan.durationorder = get_durationorder[0].durationorder;

			var sumtotal_hours = 0;
			var sumtotal_value = 0;
			const get_day_onplan = await db.collection("days").find({weekid:ObjectId(getplan_onweek[0]._id)}).toArray();
			for(var days = 0; days < get_day_onplan.length;days++){
				const get_hours = await db.collection("timesheet").find({dayid:get_day_onplan[days]._id.toString()}).toArray();
				if(get_hours.length == 0){
					console.log("dayid "+ days+": "+get_day_onplan[days]._id+" Hours: "+0);
				}else{
					console.log("dayid "+ days+": "+get_day_onplan[days]._id+" Hours: " + get_hours[0].hours);
					sumtotal_hours += get_hours[0].hours;
					sumtotal_value += get_hours[0].totalvalue;
					totalhours_on_week += get_hours[0].hours;
				}
			}


			console.log("totalhours: " + sumtotal_hours);
			obj_plan.totalhours = sumtotal_hours;

			//get average plan manday
			const get_average = await db.collection("planorders").find({"_id":ObjectId(get_plan[plan]._id)}).toArray();
			var average = get_average[0].averagewages;
			obj_plan.valueused = sumtotal_value;
			console.log("----------------------------------------------");
			obj_userprimary.plans.push(obj_plan);
		}
	}//for plan	
	obj_userprimary.totalhours = totalhours_on_week;
	obj_progress_timesheet_user.team.push(obj_userprimary);
	
	//get all user 
	console.log("----------------get users---------------------");
	const getallusers = await db.collection("users").find({projectid:projectid}).toArray();
	for(var users = 0; users < getallusers.length;users++){
		var total_hours_on_weekusers = 0;
		var obj_user = {
			userid:"",
			username:"",
			totalhours:0,
			plans:[]
		}
		console.log("userid: " + getallusers[users]._id);
		console.log("username: " + getallusers[users].fristname +" "+ getallusers[users].lastname);
		obj_user.userid = getallusers[users]._id;
		obj_user.username = getallusers[users].fristname +" "+ getallusers[users].lastname;
		const getplan_onweek = await db.collection("weeks").find({$and:[{projectid:projectid},{weeknumber:weeknumber}]}).toArray();
		console.log("getplan_onweek.length: " + getplan_onweek.length);
		for(var countplan=0;countplan < getplan_onweek.length;countplan++){
			
			//get plan detail 
			const get_detail_plan = await db.collection("planorders").find({"_id":ObjectId(getplan_onweek[countplan].planid)}).toArray();
			var obj_plan={
				planid: getplan_onweek[countplan].planid,
				planname:get_detail_plan[0].planname,
				durationid:get_detail_plan[0].durationid,
				durationorder:get_detail_plan[0].durationorder,
				totalhours:0,
				valueused:0
			}

			console.log("planid: " + getplan_onweek[countplan].planid);
			console.log("weekid: " + getplan_onweek[countplan]._id);
			const get_day_onweek = await db.collection("days").find({weekid:ObjectId(getplan_onweek[countplan]._id)}).toArray();
			var SumTotalHours = 0;
			var SumTotalvalue = 0;
			for(var days = 0;days < get_day_onweek.length;days++){
				//console.log("dayid: " + days + " "+ get_day_onweek[days]._id);
				var dayid_find = get_day_onweek[days]._id.toString();
				var userid_find = getallusers[users]._id.toString();
				const get_hours_value = await db.collection("timesheet").find({$and:[{dayid:dayid_find},{userid:userid_find}]}).toArray();
				if(get_hours_value.length == 0){
					SumTotalHours += 0;
					SumTotalvalue += 0;
				}else{
					SumTotalHours += get_hours_value[0].hours;
					SumTotalvalue += get_hours_value[0].totalvalue;
					total_hours_on_weekusers +=get_hours_value[0].hours;
				}
			}
			obj_plan.totalhours = SumTotalHours;
			obj_plan.valueused = SumTotalvalue;
			console.log("SumTotalHours: "+ SumTotalHours);
			console.log("SumTotalvalue: "+ SumTotalvalue);
			obj_user.plans.push(obj_plan);
		}
		obj_user.totalhours = total_hours_on_weekusers;
		obj_progress_timesheet_user.team.push(obj_user);
		console.log("----------------------------------------------");
	}
	//console.log(obj_progress_timesheet_user);
	return obj_progress_timesheet_user;
}

exports.send_progress_week = async(projectid,weeknumber,plans)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);

	//get detail week 
	const get_detail_week = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{projectid:projectid}]}).toArray();
	console.log("get_detail_week.length: " +get_detail_week.length);
	console.log("projectid: "+ projectid);
	console.log("count plan: " + plans.length);

	//get batchid 
	const get_batchid = await db.collection("projects").find({"_id":ObjectId(projectid)}).toArray();

	//insert progress_groupplan
	var obj_progress_groupplan={
		projectid:projectid,
		batchid:get_batchid[0].batchid,
		durationid:get_detail_week[0].durationid,
		status:1,
		statusstring:"ส่งความก้าวหน้าของงาน",
		updatetime:pastdatetime._created
	}
	const insert_progress_groupplan = await db.collection("progress_groupplan").insertOne(obj_progress_groupplan);
	const get_last_progress_groupplan_id = await db.collection("progress_groupplan").find({updatetime:pastdatetime._created}).toArray();
	console.log("last rogress_groupplan_id: " + get_last_progress_groupplan_id[0]._id);
	var progress_groupplan_id =  get_last_progress_groupplan_id[0]._id;
	
	for(var countplan = 0; countplan < plans.length;countplan++){

		//get weekid 
		const get_weekid_atplanid = await db.collection("weeks").find({$and:[{weeknumber:weeknumber},{planid:ObjectId(plans[countplan].planid)}]}).toArray();

		//update status week = 1 sending , 0 = missing , 3 = reject 
		const update_statusweek = await db.collection("weeks").updateOne(
												{"_id":ObjectId(get_weekid_atplanid[0]._id)},
												{ $set:{status:1}});
		var obj_progress_week={
			progress_groupplan_id:progress_groupplan_id,
			weekid:get_weekid_atplanid[0]._id,
			weeknumber:weeknumber,
			batchid:get_batchid[0].batchid,
			projectid:projectid,
			planid:plans[countplan].planid,
			planname:plans[countplan].planname,
			durationid:plans[countplan].durationid,
			durationorder:plans[countplan].durationorder,
			planvalue:plans[countplan].jobvalue,
			valueused:plans[countplan].usevalue,
			timelength:plans[countplan].timelength,
			timeused:plans[countplan].timeused,
			documentation:plans[countplan].document,
			barries:plans[countplan].barrier,
			updatetime:pastdatetime._created,
			status:1,
			statusstring:"ส่งความก้าวหน้าของงาน"
		}
		console.log(obj_progress_week);
		const insert_obj_progress_week = await db.collection("progress_weeks").insertOne(obj_progress_week);

		const getlast_progressid = await db.collection("progress_weeks").find({$and:[{weekid:ObjectId(get_weekid_atplanid[0]._id)},{planid:plans[countplan].planid}]}).toArray();
		console.log("count team: "+ plans[countplan].team.length);
		var progress_weekid = getlast_progressid[0]._id;
		for(var countteam = 0;countteam < plans[countplan].team.length; countteam++){
			var obj_user = {
				progress_weekid:progress_weekid,
				userid:plans[countplan].team[countteam].userid,
				username:plans[countplan].team[countteam].username,
				totalhours:plans[countplan].team[countteam].hours,
				comment:plans[countplan].team[countteam].comment
			}
			console.log(obj_user);
			const insert_obj_detailprogress = await db.collection("detailprogress_weeks").insertOne(obj_user);
		}
		console.log("------------------------------");
	}
	return {data:"send progress week success.",status:true};
}

exports.update_progress_week = async(projectid,weeknumber,plans)=>{
	const client = await MongoClient.connect(url,{ useNewUrlParser: true });
	const db = client.db(DbTimesheet);
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	var status = 1;
	var statusstring = "ส่งความก้าวหน้า";

	console.log("projectid: "+projectid);
	console.log("weeknumber: "+weeknumber);
	console.log("plans.length: "+plans.length);

	//get progress_groupplan_id
	const get_progress_groupplan_id = await db.collection("progress_weeks").find({$and:[{projectid:projectid},{weeknumber:weeknumber}]}).toArray();
	console.log("progress_groupplan_id: "+ get_progress_groupplan_id[0].progress_groupplan_id);
	var progress_groupplan_id = get_progress_groupplan_id[0].progress_groupplan_id;

	//update progress groupplan 
	const update_progress_groupplan = await db.collection("progress_groupplan").update({"_id":ObjectId(progress_groupplan_id)},
																					  {$set:{status:status,statusstring:statusstring}});

	//update progress week
	for(var countplan=0;countplan < plans.length;countplan++){
		var planid = plans[countplan].planid;
		var usevalue = plans[countplan].usevalue;
		var timeused = plans[countplan].timeused;
		var documentation = plans[countplan].document;
		var barrier = plans[countplan].barrier;
		var weeknumber = weeknumber;
		const update_status_progressweek = await db.collection("progress_weeks").updateOne({planid:planid,weeknumber:weeknumber},
																					  {$set:{status:status,
																					  		 statusstring:statusstring,
																					  		 valueused:usevalue,
																					  		 timeused:timeused,
																					  		 documentation:documentation,
																					  		 barries:barrier
																					  		}});
		const get_progress_weekid = await db.collection("progress_weeks").find({$and:[{planid:planid},{weeknumber:weeknumber}]}).toArray();
		var progress_weekid_find = get_progress_weekid[0]._id;
		console.log("progress weekid: " +progress_weekid_find);
		console.log("team.length: "+plans[countplan].team.length);
		for(var countteam = 0;countteam < plans[countplan].team.length;countteam++){
			var userid = plans[countplan].team[countteam].userid;
			var hours =  plans[countplan].team[countteam].hours;
			var comment = plans[countplan].team[countteam].comment;
			console.log("progress_weekid: "+progress_weekid_find);
			console.log("userid: "+userid);
			console.log("hours: "+hours);
			console.log("comment: "+comment);
			const update_data_useronprogressweek = await db.collection("detailprogress_weeks").update({progress_weekid:ObjectId(progress_weekid_find),userid:userid},
																					  {$set:{totalhours:hours,
																					  		 comment:comment
																					  		}});
		}
		console.log("--------------------------------");
	}		
	console.log("data: update status progress weeks success.");
	return {data:"update status progress weeks success.",status:true};
}


