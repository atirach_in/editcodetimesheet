const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
var Dbname = "DbTimesheet";
let date = require('date-and-time');
var moment = require('moment');
var nodedatetime = require('node-datetime');
const dbservice = require('./dbservice_bsm.js');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(3000, () => {
  console.log('Start server at port 3000.');
  console.log("----------------");
});


//create account
app.post('/createaccount',(req,res)=>{
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var phone = req.body.phone;
	var email = req.body.email;
	var password = req.body.password;
	var duedate = req.body.duedate;
	var batchid = req.body.batchid;
	var companyname = req.body.companyname;
	if (fristname == "" || lastname == "" || phone == "" || email == "" || password =="" || companyname == ""){
		var result ={
			data:"data invalid",
			status:false
		}
		res.json(result);
	}else{
		dbservice.createaccount(fristname,lastname,phone,email,password,duedate,batchid,companyname)
		.then(function(result){
			res.json(result);
		});
	}
});

//login 
app.post('/login',(req,res)=>{
	var email = req.body.email;
	var password = req.body.password;
	if(email == "" || password == ""){
		var respond ={data:"invalid email or password."};
		res.json(respond);
	}else{
		dbservice.loginuser(email,password)
		.then(function(result){
			res.json(result);
		});
	}
});

//dashboard
app.post('/dashboard',(req,res)=>{
	var userid = req.body.userid;
	if (userid == ""){
		var result = {
			data:"invalid userid"
		}
		console.log("invalid userid");
		res.json(result);
	}else{
		dbservice.dashboard(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

//create project
app.post('/createproject',(req,res)=>{
	var userid = req.body.userid;
	var projectname = req.body.projectname;
	var projectvalue = req.body.projectvalue;
	var batchid = req.body.batchid;
	if(userid == "" || projectname == "" || projectvalue == "" || batchid == ""){
		console.log(userid);
		var result = {
			data:"data invalid",
			status:false
		}
		res.json(result);
	}else{
		dbservice.createproject(userid,projectname,projectvalue,batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

//updateuserprimary
app.post('/updateuserprimary',(req,res)=>{
	var userid = req.body.userid;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var phone = req.body.phone;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	if(fristname == "" || lastname == "" || phone == "" || position == ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.updateuserprimary(userid,fristname,lastname,phone,position,profileimage)
		.then(function(result){
			res.json(result);
		});
	}
});

//get userprimary 
app.post('/getdatauserprimary',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid."};
		res.json(respond);
	}else{
		dbservice.getuserprimary(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

//add and update document project 
app.post('/updatedocumentproject',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var pathdocument = req.body.documentname;
	if (userid == "" || pathdocument == "" || projectid == ""){
		console.log("data: path document invalid");
		var result={data:"path document invalid",status:false}
		res.json(result);
	}else{
		dbservice.adddocument(userid,projectid,pathdocument)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/addplanorder',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid
	var planname = req.body.planname
	var jobvalue = req.body.jobvalue
	var durationid = req.body.durationid
	var timelength = req.body.timelength //days
	var averagewages = req.body.averagewages
	var moneydifference = req.body.moneydifference
	var enforceemployee = req.body.enforceemployee
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(userid == "" || planname == "" || jobvalue == "" || durationid == "" || timelength == "" || averagewages == "" || enforceemployee == "" || projectid == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.addplanorder(userid,projectid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getpallplanorder',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.getpallplanorder(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getplanorder',(req,res)=>{
	var planid = req.body.planid;
	if(planid == ""){
		var result={data:"invalid planid",status:false}
		res.json(result);
	}else{
		dbservice.getplanorder(planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/setamountorder',(req,res)=>{
	var userid = req.body.userid;
	var amountorder = req.body.amount;
	var projectid = req.body.projectid;
	if(amountorder == "" || userid == "" || projectid == ""){
		var respond = {data:"projectid or amount order invalid.",status:false};
		res.json(respond);
	}else{
		dbservice.setamountorder(amountorder,projectid,userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getsetamountorder',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respone ={data:"invalid projectid.",status:false};
		res.json(respone);
	}else{
		dbservice.getsetamountorder(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/updateplanorder',(req,res)=>{
	var planid = req.body.planid;
	var planname = req.body.planname;
	var jobvalue = req.body.jobvalue;
	var durationid = req.body.durationid;
	var timelength = req.body.timelength;
	var averagewages = req.body.averagewages;
	var moneydifference = req.body.moneydifference;
	var enforceemployee = req.body.enforceemployee;
	if (planname == "" || jobvalue == "" || durationid == "" || timelength == "" || averagewages == "" || planid == "" || moneydifference == "" || enforceemployee == ""){
		var result = {data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.updateplanorder(planid,planname,jobvalue,durationid,timelength,averagewages,moneydifference,enforceemployee)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/add/amount/team',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var amount = req.body.amount;
	if(userid == "" || projectid == "" || amount == ""){
		var respond = {data:"invalid userid or projectid , amount.",status:false};
		res.json(respond);
	}else{
		dbservice.addamounteam(userid,projectid,amount)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/addteamwork',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	var email = req.body.email;
	var phone = req.body.phone;
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(userid == "" || fristname == "" || lastname == "" || position == "" || email == "" || phone == "" ||profileimage == ""){
		var result ={data:"data invalid",status:false};
		res.json(result);
	}else{
		dbservice.addteamwork(userid,projectid,fristname,lastname,position,profileimage,email,phone)
		.then(function(result){
			res.json(result);
		});
	}
});

//get all teamwork 
app.post('/getteamwork',(req,res)=>{
	var projectid = req.body.projectid;
	var userprimarykeyid = req.body.userid;
	if(projectid == ""){
		var result={data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.getteamwork(projectid,userprimarykeyid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getteamwhereid',(req,res)=>{
	var userid = req.body.userid
	if(userid == ""){
		var respond = {data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.getteamwhereid(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/updatedatauser',(req,res)=>{
	var userid = req.body.userid;
	var fristname = req.body.fristname;
	var lastname = req.body.lastname;
	var position = req.body.position;
	var profileimage = req.body.profileimage;
	var email = req.body.email;
	var phone = req.body.phone;
	var password = req.body.password;
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if (userid == "" || fristname == "" || lastname == "" || position == ""){
		var result={data: "data invalid",status:false};
		res.json(result);
	}else{
		if(password == "not password"){
				dbservice.updatedatauser(userid,fristname,lastname,position,profileimage,email,phone)
				.then(function(result){
				res.json(result);
			});
		}else{
			dbservice.updatedatauser(userid,fristname,lastname,position,profileimage,email,phone)
				.then(function(result){
				res.json(result);
			});
		}
		
	}
});

//update document
app.post('/updatedocumentproject',(req,res)=>{
	var projectid = req.body.projectid;
	var documentname = req.body.documentname;
	if (projectid == "" || documentname == ""){
		var result={data:"data invalid",status:false}
		res.json(result);
	}else{
		dbservice.updatedocumentproject(projectid,documentname)
		.then(function(result){
			res.json(result);
		});
	}
});

//sending project 
app.post('/sendproject',(req,res)=>{
	var projectid = req.body.projectid
	if (projectid == ""){
		var result={data:"invalid projectid",status:false}
		res.json(result);
	}else{
		dbservice.sendproject(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

//add duration (งวดงาน)
app.post('/add/duration',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	var durationorder = req.body.duration;
	console.log(durationorder);
	if (durationorder == ""){
		var respond = {data:"duration or month invalid.",status:false};
		res.json(respond);
	}else{
		dbservice.addduration(projectid,durationorder,userid)
		.then(function(result){
			res.json(result);
		});
	}

});

app.post('/get/amount/duration/order',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	if(userid == "" || projectid == ""){
		var respond = {data:"invalid userid and projectid."};
		res.json(respond);
	}else{
		dbservice.getamount_duration_order(userid,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/duration',(req,res)=>{
	var projectid = req.body.projectid;
	//var userid = req.body.userid;
	if(projectid == ""){
		var respond = {data:"invalid projectid or userid."};
		res.json(respond);
	}else{
		dbservice.getallduration(projectid)
		.then(function(result){
			res.json(result);
		});
	}	
});


app.post('/update/duration',(req,res)=>{
	var duration = req.body.duration;
	var projectid = req.body.projectid;
	if(duration == "" || projectid == ""){
		var respond = {data:"invalid duration.",status:false};
		res.json(respond);
	}else{
		dbservice.updateduration(duration,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});
app.post('/count/day',(req,res)=>{
	dbservice.testcountdate()
	.then(function(result){
		//res.json();
	});
});

app.post('/dashboard/percent',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respone = {data:"invalid project."};
		res.json(respone);
	}else{
		dbservice.dashboard_percent(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/delete/user/:userid',(req,res)=>{
	var userid = req.params.userid;
	if(userid == ""){
		var respond ={data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.delete_user(userid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/delete/plan/:planid',(req,res)=>{
	var planid = req.params.planid;
	if(planid == ""){
		var respond = {data:"invalid planid."};
		res.json(respond);
	}else{
		dbservice.delete_plan(planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/getdetailproject',(req,res)=>{
	var projectid = req.body.projectid;
	var userid = req.body.userid;
	if(projectid == "" || userid == ""){
		var respond = {data:"",status:false};
		res.json(respond);
	}else{
		dbservice.getdetailproject(projectid,userid)
		.then(function(result){
			res.json(result);
		});	
	}
});	

app.post('/update/password',(req,res)=>{
	var userid = req.body.userid
	var password = req.body.password;
	if(userid == "" || password == ""){
		var respond = {data:"invalid userid or password."};
		res.json(respond);
	}else{
		dbservice.updatepassword(userid,password)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/amount/team',(req,res)=>{
	var amount = req.body.amount;
	var projectid = req.body.projectid;
	if(amount == "" || projectid == ""){
		var respond = {data:"invalid amount or projectid."};
		res.json(respond);
	}else{
		dbservice.updateamountteam(projectid,amount)
		.then(function(result){
			res.json(result);
		});
	}
});
//
app.post('/get/amount/team',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid amount or projectid."};
		res.json(respond);
	}else{
		dbservice.getamountteam(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/last/activity/:userid',(req,res)=>{
	var userid = req.body.userid;
	if(userid == ""){
		var respond ={data:"invalid userid.",status:false};
		res.json(respond);
	}else{
		dbservice.getlastactivity()
		.then(function(result){
			//res.json();
		});
	}
});

app.post('/check/last/activity',(req,res)=>{
	var userid = req.body.userid;
	var projectid = req.body.projectid;
	if(userid == "" || projectid == ""){
		var respond = {data:"invalid userid or projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.checklastdraft(userid,projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/planorder/dashboard',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getplandashboard(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/all/weeks/ofplan',(req,res)=>{
	var planid = req.body.planid;
	var projectid = req.body.projectid;
	if(planid == "" || projectid == ""){
		var respond = {data:"invalid planid or projectid.",status:false};
		res.json(respond);
	}else{
		dbservice.getallweeksofplan(projectid,planid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/daily/progress',(req,res)=>{
	var projectid = req.body.projectid;
	if(projectid == ""){
		var respond = {data:"invalid projectid"};
		res.json(respond);
	}else{
		dbservice.dailyprogress(projectid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/days/onweek/timesheet',(req,res)=>{
	var weekid = req.body.weekid;
	var dayid = req.body.dayid;
	var projectid = req.body.projectid;
	var planid = req.body.planid;
	var weeknumber = req.body.weeknumber;
	if(weekid =="" || dayid == "" || projectid == "" || planid == ""){
		var respond = {data:"invalid data.",status:false};
		res.json(respond);
	}else{
		dbservice.getweektimesheet(dayid,projectid,planid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/add/timesheet/days',(req,res)=>{
	var projectid = req.body.projectid;
	var planid = req.body.planid;
	var weekid = req.body.weekid;
	var day1_id = req.body.day1_id;
	var day2_id = req.body.day2_id;
	var day3_id = req.body.day3_id;
	var day4_id = req.body.day4_id;
	var day5_id = req.body.day5_id;
	var day6_id = req.body.day6_id;
	var day1 = req.body.day1;
	var day2 = req.body.day2;
	var day3 = req.body.day3;
	var day4 = req.body.day4;
	var day5 = req.body.day5;
	var day6 = req.body.day6;
	if(projectid == "" || planid == "" || weekid == ""){
		var respond = {data:"invalid data."};
		res.json(respond);
	}else{
		dbservice.addtimesheet(projectid,planid,weekid,day1,day2,day3,day4,day5,day6,day1_id,day2_id,day3_id,day4_id,day5_id,day6_id)
		.then(function(result){
			res.json(result);
		});
	}
});

app.get('/get/batchname/:batchid',(req,res)=>{
	var batchid = req.params.batchid;
	if(batchid == ""){
		var respond = {data:"invalid batchid",status:false};
		res.json(respond);
	}else{
		dbservice.getbatchname(batchid)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/detail/progress/week',(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	if(projectid == "" || weeknumber == ""){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.getdetail_progress_week(projectid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/get/timesheet/all/users',(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	if(projectid == "" || weeknumber == ""){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.get_timesheet_allusers(projectid,weeknumber)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/send/progress/week',(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	var plans = req.body.plans;
	if(projectid == "" || weeknumber == "" || plans.length == 0){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.send_progress_week(projectid,weeknumber,plans)
		.then(function(result){
			res.json(result);
		});
	}
});

app.post('/update/progress/week/after/reject',(req,res)=>{
	var projectid = req.body.projectid;
	var weeknumber = req.body.weeknumber;
	var plans = req.body.plans;
	if(projectid == "" || weeknumber == "" || plans.length == 0){
		var respond = {data:"invalid data.",status:false};
		console.log(respond);
		res.json(respond);
	}else{
		dbservice.update_progress_week(projectid,weeknumber,plans)
		.then(function(result){
			res.json(result);
		});
	}
});
	