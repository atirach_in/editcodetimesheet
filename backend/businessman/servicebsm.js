const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');

var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/DbTimesheet";
var Dbname = "DbTimesheet";
let date = require('date-and-time');
var nodedatetime = require('node-datetime');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(3000, () => {
  console.log('Start server at port 3000.');
  console.log("----------------");
});

//create account
app.post('/createaccount',(req,res)=>{
	var fristname = req.body.fristname
	var lastname = req.body.lastname
	var phone = req.body.phone
	var email = req.body.email
	var password = req.body.password
	var duedate = req.body.duedate
	var batchid = req.body.batchid
	if (fristname == "" || lastname == "" || phone == "" || email == "" || password ==""){
		var result ={
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
				//check email
				 date.locale('th');
				 var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
				 var pastdatetime = nodedatetime.create(datenow);
				 var dbo = db.db(Dbname);
				 dbo.collection("users").find({email:email}).toArray(function(err,result){
				 	if(result.length > 0){
					    		db.close();
					    		console.log(result);
								console.log("data: email duplicate");
								console.log("----------------------");
								var resultcheckemail = {
										data:"email has been used",
										status:false
								}
								console.log(resultcheckemail);
								//res.end()
								res.status(201).json(resultcheckemail);
								
					}else if(result.length == 0){
								db.close();
								console.log(result);
								console.log("data: This email is valid.");
								console.log("----------------------");
						//create account
						MongoClient.connect(url, function(err, db) {
						 	 var dbo = db.db(Dbname);
							 var userobj={
							 		batchid:batchid,
									fristname:fristname,
									lastname:lastname,
									phone:phone,
									email:email,
									password:password,
									Insdate:pastdatetime._created
								};
							
								 //create account
								  dbo.collection("users").insertOne(userobj, function(err, res) {
									if (err) throw err;
										console.log("1 document inserted");
										console.log("-------------------");
										db.close();
								});
								var resultreturn = {
								data:"create account sucesss",
								status:true
								}
								res.status(201).json(resultreturn);
						});
			
					}
			     });
		});
	}
});

//login
app.post('/login',(req,res)=>{
	console.log("path login");
	console.log("----------------");
	var email = req.body.email
	var password = req.body.password
	var userid =""
	var projectid = "";
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastdatetime = nodedatetime.create(datenow);
	if (email != "" && password !="") {
		var getuser
		//update lastlogin
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("users").updateOne(
					{"email":email},
					{ $set: {lastlogin:pastdatetime._created}}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data: update lastlogin success");
					    console.log("-------------------");
					    db.close();
				});
		});
		
		//check username and password
		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  	 var dbo = db.db(Dbname);
			 dbo.collection("users").find({$and:[{email:email},{password:password}]}).toArray(function(err,result){
			 	if (err){
			 		console.log("Error:"+err);
			 		db.close();
			 	}else{
			 		db.close();
			 		const getprojectid = dbo.collection("projects").find({Primaryresponsibility:result[0]._id}).toArray();
			 		console.log(getprojectid);
			 		//projectid = getprojectid[0]._id;
			 		if (result.length > 0){
			 			console.log("login pass");
			 			console.log("---------------");
			 			console.log(result);
			 			console.log("---------------");

			 			var resultdata={
			 				data:"login pass",
			 				userid:result[0]._id,
			 				fristname:result[0].fristname,
			 				lastname:result[0].lastname,
			 				phone:result[0].phone,
			 				batchid:result[0].batchid,
			 				projectid:projectid,
			 				status:true
			 			}
			 			res.status(201).json(resultdata);
			 		}else if(result.length == 0){
			 			console.log("login not pass");
			 			var resultdata={
			 				data:"login not pass",
			 				status:true
			 			}
			 			res.status(201).json(resultdata);
			 		}
			 	}
			 });
		});

	}else{
		var result = {
			data:"invalid username or password"
		}
		res.status(201).json(result);
	}
});


//if check project more than one -> load dashboard
app.post('/dashboard',(req,res)=>{
	var userid = req.body.userid
	if (userid == ""){
		var result = {
			data:"invalid userid"
		}
		console.log("invalid userid");
		res.status(201).json(result);
	}else{
		//loadproject 
		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  	 var dbo = db.db(Dbname);
			 dbo.collection("projects").find({Primaryresponsibility:userid}).toArray(function(err,result){
			 	if (err){
			 		console.log("Error:"+err);
			 		db.close();
			 	}else{
			 		if(result.length > 0){
			 			db.close();
			 			console.log("data: have projects");
			 			var result={
							data:"have project",
							status:true
						}
						res.status(201).json(result);
			 		}else{
			 			db.close();
			 			console.log("data: not have project");
			 			var result={
								data:"no project",
								status:false
						}
						res.status(201).json(result);
			 		}
			 	}
			 });
		});
	}
});

//create project
app.post('/createproject',(req,res)=>{
	var userid = req.body.userid
	var projectname = req.body.projectname
	var projectvalue = req.body.projectvalue
	var fristname = req.body.fristname
	var lastname = req.body.lastname
	var phone = req.body.phone 
	var position = req.body.position
	var batchid = req.body.batchid
	var profileimage = req.body.profileimage
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(projectname == "" || projectvalue == "" || fristname == "" || lastname == "" || phone == "" || position == "" || profileimage == "" || batchid == ""){
		var result = {
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		//check user in users table
		if(userid ==""){
			var result ={
				data:" invalid userid",
				status:false
			}
			res.status(201).json(result);
		}else{
			//add project 
			MongoClient.connect(url, function(err, db) {
						 	 var dbo = db.db(Dbname);
							 var porjectobj={
							 		batchid:batchid,
									projectname:projectname,
									projectvalue:projectvalue,
									Primaryresponsibility:userid,
									Documentation:"",
									Insdate:pastdatetime._created,
									updateproject:"",
									senddatetime:"",
									status:1,
									active:1
								};
							dbo.collection("projects").insertOne(porjectobj, function(err, res) {
									if (err) throw err;
										console.log("data: add frist project success");
										console.log("-------------------");
										db.close();
								});
			});
			
			//update Primaryresponsibility
			MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("users").updateOne(
					{"_id":ObjectId(userid)},
					{ $set: { fristname: fristname,
							  lastname: lastname,
							  phone:phone,
							  position:position,
							  profileimage:profileimage,
							  updatetime:pastdatetime._created
							  }}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data: update user success");
					    console.log("-------------------");
					    db.close();
				});
			});
			
			
			var projectobjres = {
					 //data:"new project sucess",
					 //userid:userid,
					 //projectid:result[i]._id,
					 //projectname:result[i].projectname,
					 //status:true
			}
			var batchbsm = {
				//batchid:
				//projectid:
				//status:
				//active:
			}
			//find _id project 
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("projects").find({Primaryresponsibility:userid}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		//add batchbusinessman
				 		console.log(result);
				 		console.log("---------------------------");
				 		for (var i in result) {
				 		projectobjres = {
					 			data:"new project sucess",
					 			userid:userid,
					 			projectid:result[i]._id,
					 			projectname:result[i].projectname,
								status:true
					 		}
						}
					 batchbsm = {
								batchid:batchid,
								projectid:projectobjres.projectid,
								Insdate:pastdatetime._created,
								updatetime:"",
								status:"1",
								active:"1"
							}
					console.log(batchbsm);
					 dbo.collection("batchbusinessman").insertOne(batchbsm, function(err, res) {
						if (err) throw err;
							console.log("data: add to batchbusinessman success");
							console.log("-------------------");
							db.close();
						});
					res.status(201).json(projectobjres);
					}
				 });
				
			});
		//update aleady businessman 
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("projects").find({batchid:batchid}).toArray(function(err,result){
				 	dbo.collection("projects").updateOne(
						{"_id":ObjectId(batchid)},
						{ $set: { alreadybusinessman: result.length} }, 
						function(err, res) {
						    if (err) throw err;
						    console.log("data: update already businessman "+result.length);
						    console.log("-------------------");
						    db.close();
						});
				 });
			});
		}//else
	}
});

//add document project 
app.post('/adddocument',(req,res)=>{
	var projectid = req.body.projectid
	var pathdocument = req.body.pathdocument
	if (pathdocument == ""){
		console.log("data: path document invalid");
		var result={
			data:"path document invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		//update path document project
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("projects").updateOne(
					{"_id":ObjectId(projectid)},
					{ $set: { Documentation: pathdocument} }, 
					function(err, res) {
					    if (err) throw err;
					    console.log("1 document updated");
					    console.log("-------------------");
					    db.close();
				});
		});
		//console.log("data: add document success");
		var result = {
			data:"add document success",
			status:true
		}
		res.status(201).json(result);
	}
});

//add plan order 
app.post('/addplanorder',(req,res)=>{
	var projectid = req.body.projectid
	var planname = req.body.planname
	var jobvalue = req.body.jobvalue
	var installment = req.body.installment
	var timelength = req.body.timelength //days
	var averagewages = req.body.averagewages
	var moneydifference = req.body.moneydifference
	var enforceemployee = req.body.enforceemployee
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if(planname == "" || jobvalue == "" || installment == "" || timelength == "" || averagewages == ""){
		var result = {
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
	//add new plan
		MongoClient.connect(url, function(err, db) {
						 	 var dbo = db.db(Dbname);
						 	 var totalhours = timelength * 8
						 	 let countplanorder
							 var planobj={
									projectid:projectid,
									planorder:"",
									planname:planname,
									jobvalue:jobvalue,
									moneydifference:moneydifference,
									enforceemployee:enforceemployee,
									installment:installment,
									averagewages:averagewages,
									timelength:timelength,
									totalhours:totalhours,
									Insdate:pastdatetime._created,
									active:1
								};
							dbo.collection("planorders").insertOne(planobj, function(err, res) {
									if (err) throw err;
										console.log("data: create plan order success");
										console.log("-------------------");
										db.close();
							});
							var resultrespone={
								data:"data: create plan order success",
								status:true
							}
							res.status(201).json(resultrespone);
		});

	}
});

// get all plan order
app.post('/getpallplanorder',(req,res)=>{
	var projectid = req.body.projectid
	var datapaln={
				project:{
				plan:[
					//planid:"1",
					// planname:"plan1",
					// installment:"1",
					// jobvalue:"1",
					// timelength:"20"
				]
			}
		}
	if(projectid == ""){
		var result = {
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("planorders").find({projectid:projectid}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		console.log(result);
				 		console.log("---------------------");
				 		for (var i in result) {
				 			var dataresult = result[i];
				 			var dataorder = {
									planid:dataresult._id,
								    planname:dataresult.planname,
									installment:dataresult.installment,
									jobvalue:dataresult.jobvalue,
									timelength:dataresult.timelength
							}
							datapaln.project.plan.push(dataorder);
				 		}
				 		db.close();
				 		res.status(201).json(datapaln);
				 	}
				 });
			});
	}
});

//get plan data where planid
app.post('/getplanorder',(req,res)=>{
	var planid = req.body.planid
	var dataplan={
		plandetial:[
			//planname
			//jobvalue
			//installment
			//timelength
			//averagewage
		]
	}
	if (planid == ""){
		var result={
			data:"invalid planid",
			status:false
		}
		console.log("invalid planid");
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("planorders").find({"_id":ObjectId(planid)}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		db.close();
				 		console.log(result);
				 		var dataplandetail = {
							planname:result[0].planname,
							jobvalue:result[0].jobvalue,
							installment:result[0].installment,
							timelength:result[0].timelength,
							averagewage:result[0].averagewages
						}
						dataplan.plandetial.push(dataplandetail);
				 		res.status(201).json(dataplandetail);
				 	}
				 });
			});
	}
});

//update plan order
app.post('/updateplanorder',(req,res)=>{
	var planid = req.body.planid
	var planname = req.body.planname
	var jobvalue = req.body.jobvalue
	var installment = req.body.installment
	var timelength = req.body.timelength
	var averagewages = req.body.averagewages
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if (planname == "" || jobvalue == "" || installment == "" || timelength == "" || averagewages == "" || planid == ""){
		var result = {
			data:"data invalid",
			status:false
		}
		console.log("data: data invalid");
		res.status(201).json(result);
	}else{
		//update plan order
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("planorders").updateOne(
					{"_id":ObjectId(planid)},
					{ $set: { planname: planname,
							  jobvalue: jobvalue,
							  installment: installment,
							  timelength: timelength,
							  averagewages: averagewages,
							  updatetime:pastdatetime._created}}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("1 document updated");
					    console.log("-------------------");
					    db.close();
				});
				var result={
					data:"update plan detail sucess",
					planid:planid,
					status:true
				}
				res.status(201).json(result);
		});
	}
});

//add teamwork
app.post('/addteamwork',(req,res)=>{
	var projectid = req.body.projectid
	var fristname = req.body.fristname
	var lastname = req.body.lastname
	var position = req.body.position
	var profileimage = req.body.profileimage
	var email = req.body.email
	var phone = req.body.phone
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	//var dbo = db.db(Dbname);
	if(fristname == "" || lastname == "" || position == "" || email == "" || phone == "" ||profileimage == ""){
		console.log("data: data invalid");
		var result ={
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		/// check email 
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("users").find({email:email}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		console.log(result);
				 		console.log("---------------------");
				 		if(result.length == 0){
				 			console.log("data: email is acitve.");
				 			console.log("---------------------");
				 			var dbo = db.db(Dbname);
							var userobj={
								projectid:projectid,
								fristname:fristname,
								lastname:lastname,
								position:position,
								email:email,
								phone:phone,
								profileimage:profileimage,
								Insdate:pastdatetime._created,
								active:1
							};
							//add teamwork tp project 
							dbo.collection("users").insertOne(userobj, function(err, res) {
								if (err) throw err;
									console.log("data: add frist project success");
									console.log("-------------------");
									db.close();
							});
							var result ={
									data:"add teamwork success",
									status:true
								}
							res.status(201).json(result);
				 		}else{
				 			console.log("data: email has been used.");
				 			console.log("---------------------");
				 			var result={
				 				data:"email has been used.",
				 				status:false
				 			}
				 			res.status(201).json(result);
				 		}
				 	}
				 });
			});
		}
});


//get all teamwork 
app.post('/getteamwork',(req,res)=>{
	var projectid = req.body.projectid
	var userprimarykeyid = req.body.userid
	var teamwork={
		team:[
			//fristname
			//lastname
			//position
			]
	}
	if(projectid == ""){
		var result={
			data:"data invalid",
			status:false
		}
		console.log("data: invalid projectid");
		res.status(201).json(result);
	}else{
		//get primary user in collection project
		MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			   var dbo = db.db(Dbname);
			   dbo.collection("users").find({"_id":ObjectId(userprimarykeyid)}).toArray(function(err,resultusers){
				  	
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		console.log("-----------------PK user------------------");
				 		console.log("fristname: "+resultusers[0].fristname);
				 		console.log("lastname: "+resultusers[0].lastname);
				 		console.log("position: "+resultusers[0].position);
				 		console.log("-----------------------------------");
				 		var detailusers={
				 			fristname:resultusers[0].fristname,
				 			lastname:resultusers[0].lastname,
				 			position:resultusers[0].position
				 		}
				 		teamwork.team.push(detailusers);
				 	}
				 });
			   //get all team in project
			    dbo.collection("users").find({"projectid":projectid}).toArray(function(err,result){
				  	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		for (var i in result) {
				 			//var dataresult = result[i];
				 			var detailusers={
					 			fristname:result[i].fristname,
					 			lastname:result[i].lastname,
					 			position:result[i].position
				 			}
				 		teamwork.team.push(detailusers);
				 		}
				 		res.status(201).json(teamwork);
				 	}
				 });
		});
	}
});

//get team where userid 
app.post('/getteamwhereid',(req,res)=>{
	var userid = req.body.userid
	var datauser={
		data:[
			//fristname
			//lastname
			//position
			//profileiamge
			//email
			//phone
		]
	}
	if(userid == ""){
		console.log("data: invalid userid");
		var result={
			data:"invalid userid",
			status:false
		}
		res.status(201).json(result);
	}else{
		//get user in project 
		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db(Dbname);
			 dbo.collection("users").find({"_id":ObjectId(userid)}).toArray(function(err,resultusers){
			 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		if(resultusers.length == 0){
				 			var result={
				 				data:"invalid userid.",
				 				status:false
				 			}
				 			res.status(201).json(result);
				 		}else{
				 			var detailusers={
				 			fristname:resultusers[0].fristname,
				 			lastname:resultusers[0].lastname,
				 			position:resultusers[0].position,
				 			profileiamge:resultusers[0].profileiamge,
				 			email:resultusers[0].email,
				 			phone:resultusers[0].phone
				 		}
				 		datauser.data.push(detailusers);
				 		res.status(201).json(datauser);
				 		}
				 	}
			 });
		});
	}
});

//update team
app.post('/updatedatauser',(req,res)=>{
	var userid = req.body.userid
	var fristname = req.body.fristname
	var lastname = req.body.lastname
	var position = req.body.position
	var profileimage = req.body.profileimage
	var email = req.body.email
	var phone = req.body.phone
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if (userid == "" || fristname == "" || lastname == "" || position == "" || profileimage == ""){
		var result={
			data: "data invalid",
			status:false
		}
		console.log("data: data invalid");
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("users").updateOne(
					{"_id":ObjectId(userid)},
					{ $set: { fristname: fristname,
							  lastname:lastname,
							  position:position,
							  profileimage:profileimage,
							  email:email,
							  phone:phone,
							  updatetime:pastdatetime._created
								} }, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data: update data at userid="+userid);
					    console.log("-------------------");
					    db.close();
				});
				var result={
					data:"update success at userid="+userid,
					status:true
				}
				res.status(201).json(result);
		});
	}
});

//update document
app.post('/updatedocumentproject',(req,res)=>{
	var projectid = req.body.projectid
	var documentname = req.body.documentname
	if (projectid == "" || documentname == ""){
		console.log("data: data invalid");
		var result={
			data:"data invalid",
			status:false
		}
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("projects").updateOne(
					{"_id":ObjectId(projectid)},
					{ $set: {Documentation:documentname}}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data: update document at projectid.");
					    console.log("-------------------");
					    db.close();
				});
				var result={
					data:"update document at projectid",
					projectid:projectid,
					status:true
				}
				res.status(201).json(result);
		});
	}
});

// send project to Inspection Officer
app.post('/sendproject',(req,res)=>{
	var projectid = req.body.projectid
	var status = 2 //send peoject is pending not update or edit project (bsm)
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
	var pastdatetime = nodedatetime.create(datenow);
	if (projectid == ""){
		var result={
			data:"invalid projectid",
			status:false
		}
		res.status(201).json(result);
	}else{
		//Send a project successfully.
		MongoClient.connect(url, function(err, db) {
				var dbo = db.db(Dbname);
				dbo.collection("projects").updateOne(
					{"_id":ObjectId(projectid)},
					{ $set: {
							 status:status,
							 senddatetime:pastdatetime._created
							}
					}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data:Send a project successfully.");
					    console.log("-------------------");
					    db.close();
				});

				//update status project in batchbusiinessman = 2 
				dbo.collection("batchbusinessman").updateOne(
					{"projectid":ObjectId(projectid)},
					{ $set: {
							 status:"2"//send and pending.
							}
					}, 
					function(err, res) {
					    if (err) throw err;
					    console.log("data:update status success.");
					    console.log("-------------------");
					    db.close();
				});
				var result={
					data:"Send a project successfully.",
					status:true
				}
				res.status(201).json(result);
		});
	}
});

//get batch name from localhost:3000/createaccount/batchid
app.post('/getbatch/:batchid',(req,res)=>{
	var batchid = req.params.batchid
	if(batchid == ""){
		var result={
			data:"batch invalid.",
			status:false
		}
		res.status(201).json(result);
	}else{
		MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  	 var dbo = db.db(Dbname);
				 dbo.collection("batch").find({"_id":ObjectId(batchid)}).toArray(function(err,result){
				 	if (err){
				 		console.log("Error:"+err);
				 		db.close();
				 	}else{
				 		db.close();
				 		if(result.length == 0){
				 			console.log("data: invalid batch.");
				 			console.log("-------------------");
				 			var resultjson={
				 				data:"invalid batch.",
				 				status:false
				 			}
				 			res.status(201).json(resultjson);
				 		}else{
				 			console.log(result);
				 			console.log("-------------------");
				 			var resultjson={
				 					//modelname:result[i].modelname	
				 				}
				 			for(var i in result){
				 				resultjson={
				 					bathid:result[i]._id,
				 					modelname:result[i].modelname	
				 				}
				 			}
				 			res.status(201).json(resultjson);
				 		}
				 	}
				});
		});
	}
});

app.post('/testtime',(req,res)=>{
	date.locale('th');
	var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); // => 'lundi 11 janvier'
	var pastdatetime = nodedatetime.create(datenow);
	var time = {
		dateimte:datenow,
		timestamp:pastdatetime._created
	}
	res.status(201).json(time);
});

//calculate week and days in plan 
app.post('/testcalweek',(req,res)=>{
	var days = req.body.days
	if(days == "" || days == 0){
		var result = {
			data:"invalid days.",
			status:false
		}
		res.status(201).json(result);
	}else{
		var weeks = days / 6
		var weekscal = Math.ceil(weeks)
		console.log("project weeks: "+weekscal);
		for (var i = 1; i <= weekscal; i++) {
			if (days >= 6){
				var caldays = days - 6
				console.log("week"+i+": have 6 days.")
				days = caldays
				
			}else if(days < 6) {
				console.log("week"+i+": have "+days+" days.")
			}
				
		}
	}
});
// Math.round(x) //ใช้ปัดเศษไปหาจำนวนเต็มที่ใกล้ที่สุด
// Math.floor(x) //ใช้ปัดเศษทิ้ง (ปัดลง)
// Math.ceil(x) //ปัดขึ้น