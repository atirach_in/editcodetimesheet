var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addAmountTeam = addAmountTeam;

async function addAmountTeam(userId,projectId,amount){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        //add amount user in projects
        var addAmountTeam = await db.collection("projects").updateOne(
                                {"_id":ObjectId(projectId)},
                                { $set: {
                                    amountteam:amount
                                }});
        console.log("add amount team in project: "+ amount);
        //add activity 
        var activityOrder = "3.1"; // เพิ่มจำนวนคนทำงาน
        var getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
        console.log("activity _id: " + getActivityId[0]._id);
        console.log("update amount plan success.");
        console.log("userId: "+ userId);
        console.log("projectid: "+ projectId);
        console.log("---------------------------");
        //insert activityusers
        var insertActivityUser = await db.collection("activityusers").insert({
                                        userid:userId,
                                        activityid:getActivityId[0]._id,
                                        datetime:pastDatetime._created
                                  });
        client.close();
        return {data:"add amount success.",status:true};
    }catch(e){
        console.log('\n',e);
        return{data:"add amount failed.",status:false};
    }
}