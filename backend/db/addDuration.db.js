var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = {
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addDuration = addDuration;

async function addDuration(projectId,durationOrder,userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        console.log("duration index:"+durationOrder.length);
        console.log(durationOrder);
        var countDurationOrder = 1;
        //insert duration
        for(var i = 0; i < durationOrder.length; i++){
            const durationMonth = await db.collection("duration").insert({
                                        durationorder:countDurationOrder, 
                                        month: durationOrder[i].month,
                                        projectid:projectId
                                  });
            console.log("durationOrder: "+durationOrder[i].durationorder);
            console.log("month: "+durationOrder[i].month);
            console.log("-----------------------------");
            countDurationOrder += 1;
        }
        
        //get activity_id  
        var activityOrder = "2.1";
        const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
        console.log("activity _id: " + getActivityId[0]._id);
        //insert activityusers
        const insertActivityUser = await db.collection("activityusers").insert({
                                        userid:userId,
                                        activityid:getActivityId[0]._id,
                                        datetime:pastDatetime._created
                                  });  
        client.close();
        return {data:"insert duration success.",draft:"2.2",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"insert duration failed.",draft:"",status:false};
    }
}
