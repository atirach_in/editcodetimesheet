var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addPlanOrder = addPlanOrder;

async function addPlanOrder(userId,projectId,planname,jobValue,durationId,timeLength,averageWages,moneyDifference,enforceEmployee){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var totalHours = timeLength * 8
        let countPlanOrder
        const getCountPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("getCountPlan.length: "+ getCountPlan.length);
        let amountPlan = getCountPlan.length+1;
        const getDurationNumber = await db.collection("duration").find({"_id":ObjectId(durationId)}).toArray();
        var planObj={
                projectid:projectId,
                planorder:amountPlan,
                planname:planname,
                jobvalue:jobValue,
                moneydifference:moneyDifference,
                enforceemployee:enforceEmployee,
                durationid:durationId,
                durationorder:getDurationNumber[0].durationorder,
                averagewages:averageWages,
                timelength:timeLength,
                totalhours:totalHours,
                averagehours:0,
                Insdate:pastDatetime._created,
                status:1,
                active:1
        };
        //calculate average hour/man
        var averageHr = ((jobValue/timeLength) / enforceEmployee)/8;
        planObj.averagehours = averageHr;

        const insertPlan = await db.collection("planorders").insertOne(planObj);
        //get activity_id  
        var activityOrder = "2.3"; // สร้างแผนงาน
        const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
        console.log("activity _id: " + getActivityId[0]._id);
        console.log("update amount plan success.");
        console.log("projectId: "+ projectId);
        console.log("---------------------------");
        //insert activityusers
        const insertActivityUser = await db.collection("activityusers").insert({
                                        userid:userId,
                                        activityid:getActivityId[0]._id,
                                        datetime:pastDatetime._created
                                });

        // get amount plan 
        const getAmountPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        console.log("amountplan: "+getAmountPlan[0].amountplan);
        var amount = getAmountPlan[0].amountplan;
        // get plan where projectId 
        const getPlanFromProject = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("get plan: "+getPlanFromProject.length);
        var getPlan = getPlanFromProject.length;
        var calPercent = 100/amount;
        console.log("percent unit:"+ calPercent + "%");
        var calPlanPercent = calPercent * getPlan;
        console.log("percent plan: "+calPlanPercent +"%");
        console.log("---------------------------");
        if(calPlanPercent < 100){
            var draftAt = "2.3";
            console.log("draft at: "+ draftAt);
            return {data:"insert plan success.",draft:draftAt,status:true};
        }else{
            var draftAt = "3.1";
            console.log("draft at: "+ draftAt);
            //get activity_id  
            var activityOrder = draftAt; //เพิ่มจำนวนคนทำงาน
            const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
            console.log("activity _id: " + getActivityId[0]._id);
            console.log("update amount plan success.");
            console.log("projectId: "+ projectId);
            console.log("---------------------------");
            //insert activityusers
            const insertActivityUser = await db.collection("activityusers").insert({
                                            userid:userId,
                                            activityid:getActivityId[0]._id,
                                            datetime:pastDatetime._created
                                    });
                                    
            client.close();
            return {data:"insert plan success.",draft:draftAt,status:true};
        }
    }catch(e){
        console.log('\n',e);
        return {data:"insert plan failed.",draft:null,status:false};
    }
}