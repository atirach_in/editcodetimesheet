var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addTeamWork = addTeamWork;

async function addTeamWork(userId,projectId,fristName,lastName,position,profileImage,email,phone){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        const checkEmail = await db.collection("users").find({email:email}).toArray();
        if(checkEmail.length > 0){
            var respond = {data:"data: email has been used.",status:false};
            return respond;
        }else{
            //var path_profileimage = "../../fontend/src/assets/upload/profile"+profileImage;
            var profileImg = ""
            if(profileImage == "data: invalid file."){
    
            }else{
    
            }
            var userObj={
                projectid:projectId,
                fristname:fristName,
                lastname:lastName,
                position:position,
                email:email,
                phone:phone,
                profileimage:profileImage,
                Insdate:pastDatetime._created,
                active:1
            };
        //add teamwork tp project 
        const insertTeam = await db.collection("users").insertOne(userObj);
        //get activity_id  
        var activityOrder = "3.2";
        const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
        console.log("activity _id: " + getActivityId[0]._id);
        console.log("update amount plan success.");
        console.log("projectid: "+ projectId);
        console.log("---------------------------");
        //insert activityusers
        const insertActivityUser = await db.collection("activityusers").insert({
                                        userid:userId,
                                        activityid:getActivityId[0]._id,
                                        datetime:pastDatetime._created
                                  });
        //get amount team 
        // const getamountteam = await db.collection("users").find({projectid:activityorder}).toArray();
        // console.log("get team in project: "+ getamountteam.length);

        client.close();
        return {data:"insert team success.",status:true};
        }
    }catch(e){
        console.log('\n',e);
        return {data:"insert team failed.",status:false};
    }
}