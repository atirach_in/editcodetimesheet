var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addTimesheet = addTimesheet;

async function addTimesheet(projectId,planId,weekId,day1,day2,day3,day4,day5,day6,day1Id,day2Id,day3Id,day4Id,day5Id,day6Id){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        console.log("day1: " + day1.length);
        console.log("day2: " + day2.length);
        console.log("day3: " + day3.length);
        console.log("day4: " + day4.length);
        console.log("day5: " + day5.length);
        console.log("day6: " + day6.length);
        console.log("------------------------------------");
        //get average value on day.
        const getAverage = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
        var calAverage = getAverage[0].averagewages;
        console.log("calAverage: "+calAverage);
    
        //day1
        console.log("-------------day1--------------");
        for(var d1 =0; d1 < day1.length;d1++){
            //console.log(day1[d1]);
            var objDayTimesheet = {
                userid:day1[d1].userid,
                dayid:day1[d1].dayid,
                planid:planId,
                projectid:projectId,
                hours:day1[d1].hours,
                totalvalue:(parseInt(calAverage) / 8) * day1[d1].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);
    
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day1Id},{userid:day1[d1].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day1[d1].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }
        }
        
        //day2
        console.log("-------------day2--------------");
        for(var d2 =0; d2 < day2.length;d2++){
            //console.log(day2[d2]);
            var objDayTimesheet = {
                userid:day2[d2].userid,
                dayid:day2[d2].dayid,
                planid:planId,
                projectid:projectId,
                hours:day2[d2].hours,
                totalvalue:(parseInt(calAverage) / 8) * day2[d2].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);	
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day2Id},{userid:day2[d2].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day2[d2].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }	
        }
        
        //day3
        console.log("-------------day3--------------");
        for(var d3 =0; d3 < day3.length;d3++){
            //console.log(day3[d3]);
            var objDayTimesheet = {
                userid:day3[d3].userid,
                dayid:day3[d3].dayid,
                planid:planId,
                projectid:projectId,
                hours:day3[d3].hours,
                totalvalue:(parseInt(calAverage) / 8) * day3[d3].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);	
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day3Id},{userid:day3[d3].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day3[d3].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }	
        }
        
        //day4
        console.log("-------------day4--------------");
        for(var d4 =0; d4 < day4.length;d4++){
            //console.log(day4[d4]);
            var objDayTimesheet = {
                userid:day4[d4].userid,
                dayid:day4[d4].dayid,
                planid:planId,
                projectid:projectId,
                hours:day4[d4].hours,
                totalvalue:(parseInt(calAverage) / 8) * day4[d4].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);	
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day4Id},{userid:day4[d4].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day4[d4].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }
        }
        
        //day5
        console.log("-------------day5--------------");
        for(var d5 =0; d5 < day5.length;d5++){
            //console.log(day5[d5]);
            var objDayTimesheet = {
                userid:day5[d5].userid,
                dayid:day5[d5].dayid,
                planid:planId,
                projectid:projectId,
                hours:day5[d5].hours,
                totalvalue:(parseInt(calAverage) / 8) * day5[d5].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);	
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day5Id},{userid:day5[d5].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day5[d5].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }
        }
        
        //day6
        console.log("-------------day6--------------");
        for(var d6 =0; d6 < day6.length;d6++){
            //console.log(day6[d6]);
            var objDayTimesheet = {
                userid:day6[d6].userid,
                dayid:day6[d6].dayid,
                planid:planId,
                projectid:projectId,
                hours:day6[d6].hours,
                totalvalue:(parseInt(calAverage) / 8) * day6[d6].hours,
                updatetime:pastDatetime._created
            }
            //console.log(objDayTimesheet);	
            const checkDay = await db.collection("timesheet").find({$and:[{dayid:day6Id},{userid:day6[d6].userid}]}).toArray();
            if(checkDay.length > 0){
                //update day
                console.log("status: update");
                var dayIdUpdate = objDayTimesheet.dayid.toString();
                var userIdUpdate = objDayTimesheet.userid.toString();
                const updateTimesheet = await db.collection("timesheet").updateOne(
                                                    {dayid:dayIdUpdate,userid:userIdUpdate},
                                                    {$set: 
                                                        {
                                                            hours:day6[d6].hours,
                                                            totalvalue:objDayTimesheet.totalvalue,
                                                            updatetime:pastDatetime._created
                                                        }
                                                    });
            }else{
                //insert day 
                console.log("status: insert");
                const insertTimesheet = await db.collection("timesheet").insertOne(objDayTimesheet);
                console.log(objDayTimesheet);
            }
        }

        client.close();
        return {data:"add timesheet success.",status:true};
    }catch(e){
        console.log('\n',e);
        return;
    }
}