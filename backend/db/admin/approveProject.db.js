var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = {
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.approveProject = approveProject;

async function approveProject(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var respond = {stausUpdate:[]};
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        //approve status project collection BatchBusinessman
        const updateStatusBatchbsm = await db.collection("batchbusinessman").updateOne(
                                            {projectid:ObjectId(projectId)},
                                            {$set:{status:"4",
                                                   updatetime:pastDatetime._created}})
                                            .then(function(result){
                                                respond.stausUpdate.push({data:"approve batchbusinessman.",status:true});
                                            });
    
        //approve status project collection project 
        const updateStatusProject = await db.collection("projects").updateOne(
                                            {"_id":ObjectId(projectId)},
                                            {$set:{status:"4"}}).then(function(result){
                                                respond.stausUpdate.push({data:"approve projects.",status:true});
                                            });
    
        //get detail project 
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        console.log("getDetailProject.length: "+getDetailProject.length);
        var projectName = getDetailProject[0].projectname;
        
        //get batch name 
        const getBatchName = await db.collection("batch").find({"_id":ObjectId(getDetailProject[0].batchid)}).toArray();
        console.log("getBatchName.length: "+getBatchName.length);
        var batchName = getBatchName[0].modelname;
    
        //get user detail 
        const getUsers = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
        console.log("getUsers.length: "+getUsers.length);
        var username = getUsers[0].fristname+"  "+getUsers[0].lastname;
        var email = getUsers[0].email;
        var phone = getUsers[0].phone;
    
        //insert collection notification_users
        var dtsmp = pastDatetime._created;
        var dtsmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:projectId,
            planid:"",
            comment_rejectid:"",
            statusaction:4,
            statusstring:"อนุมัติ Timsheet",
            read:"",
            progress_groupplan_id:"",
            datetimestamp:dtsmp,
            datetimestring:dtsmpString
        }
        const addNotificationUsers = await db.collection("notification_users").insertOne(objNotification);
        
        client.close();
        return respond;
    }catch(e){
        console.log('\n',e);
        return;
    }
}