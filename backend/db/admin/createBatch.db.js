var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addBatch = addBatch;
async function addBatch(adminInsert,adminUpdate,modelName,piscalYear,numberOfOperators,startingDate,dtsStartingDate,dueDate,dtsDueDate,alreadyBusinessman,dateNow,pastDatetime,updateTime,status,active){
                     
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
            //Create batch
            var userObj={
                modelname:modelName,
                piscalyear:piscalYear,
                numberofoperators:numberOfOperators,
                alreadybusinessman:alreadyBusinessman,
                startingdate:dtsStartingDate,
                duedate:dtsDueDate,
                Insdate:pastDatetime._created,
                updatetime:updateTime,
                admininsert:adminInsert,
                adminupdate:adminUpdate,
                status:status,
                active:active
            }; 
            const createBatch = await db.collection("batch").insertOne(userObj, function(err, res) {
                if (err) throw err;
                    console.log("data: create batch success.");
                    console.log("-------------------");
            });

            client.close();
            var resultreturn = {data:"create batch success.",status:true}
            return resultreturn;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}