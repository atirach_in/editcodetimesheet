var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.createLink = createLink;

async function createLink(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getLink = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        var resultJson={
            data:"create link success.",
            link:"http://ted.integreat.in.th/register/"+getLink[0]._id,
            status:true
        }
        
        client.close();
        return resultJson;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}