var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllBatch = getAllBatch;

async function getAllBatch(){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getBatchs = await db.collection("batch").find().toArray();

        client.close();
        return getBatchs;
    }catch(e){
        console.log('\n',e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}