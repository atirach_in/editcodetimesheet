var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllBsm = getAllBsm;

async function getAllBsm(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objAllProject = {
            projects:[]
        }

        //get project on batch 
        const getAllProject = await db.collection("projects").find({batchid:batchId}).toArray();
        for(var countProject = 0 ;countProject < getAllProject.length; countProject++){
            var objProject={
                projectId:"",
                projectName:"",
                companyName:"",
                userPrimary:""
            }
            const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getAllProject[countProject].Primaryresponsibility)}).toArray();

            console.log("projectid: "+getAllProject[countProject]._id);
            console.log("projectname: "+getAllProject[countProject].projectname);
            console.log("companyname: "+getUserPrimary[0].companyname);
            console.log("userprimary: "+getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname);

            objProject.projectId = getAllProject[countProject]._id;
            objProject.projectName = getAllProject[countProject].projectname;
            objProject.companyName = getUserPrimary[0].companyname;
            objProject.userPrimary = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
            console.log("-----------------------------------------------------");
            objAllProject.projects.push(objProject);
        }

        console.log(objAllProject.projects);
        
        client.close();
        return objAllProject;
    }catch(e){
        console.log('\n',e);
        return;
    }
}