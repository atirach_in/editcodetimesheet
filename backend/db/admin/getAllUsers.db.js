var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllUsers = getAllUsers;

async function getAllUsers(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objUsers = {
            users:[]
        }
        const getAllProject = await db.collection("projects").find({batchid:batchId}).toArray();
        for(var count = 0;count < getAllProject.length;count++){
            const getUser = await db.collection("users").find({"_id":ObjectId(getAllProject[count].Primaryresponsibility)}).toArray();
            var obj={
                projectId:getAllProject[count]._id,
                projectName:getAllProject[count].projectname,
                username:getUser[0].fristname+" "+getUser[0].lastname,
                companyName:getUser[0].companyname
            }
            objUsers.users.push(obj);
        }
        console.log(objUsers);
        console.log("-------------------------------");
        
        client.close();
        return objUsers;
    }catch(e){
        console.log('\n',e);
        return;
    }
}