var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getCommentedReject = getCommentedReject;

async function getCommentedReject(projectId,planId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var objComment = {
            allComment:[]
        }
        const getComment = await db.collection("commentreject").find({$and:[{projectid:projectId},{planid:planId}]}).toArray();
        for(var count=0;count < getComment.length;count++){
            var time = moment(getComment[count].insertdate).format("D/MM/YYYY HH:mm:ss");
            var objReject = {
                comment:getComment[count].comment,
                fileName:getComment[count].pahtfile,
                insertDate:time
            }
            objComment.allComment.push(objReject);
        }
        console.log("data: get all comment reject.");
        console.log(objComment);
        console.log("-----------------------------");

        client.close();
        return objComment;
    }catch(e){
        console.log('\n',e);
        return {
            allComment:[]
        };
    }
}