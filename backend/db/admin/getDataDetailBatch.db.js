var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDataDetailBatch = getDataDetailBatch;

async function getDataDetailBatch(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        const getDetail = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        var objBatch = {
            batchName:"",
            piscalYear:"",
            amountBsm:"",
            dueDate:"",
            dueDateAmount:0
        }
    
        if(getDetail.length == 0){
            var respond = {data:"invalid batchId.",status:false};

            client.close();
            return respond;
        }else{
            var startDateDtsm = pastDatetime._created;//getDetail[0].startingdate;
            var dueDateDtsm = getDetail[0].duedate;
            if(startDateDtsm < dueDateDtsm){
                //get days from date now count down
                    var startDay = moment(startDateDtsm).format("DD");
                    var startMonth = moment(startDateDtsm).format("MM");
                    var startYear = moment(startDateDtsm).format("YYYY");
                    var start = moment([startYear, startMonth, startDay]);
                    
                    var dueDay = moment(dueDateDtsm).format("DD");
                    var dueMonth =  moment(dueDateDtsm).format("MM");
                    var dueYear =  moment(dueDateDtsm).format("YYYY");
                    var due = moment([dueYear, dueMonth, dueDay]);
                    var diffDay = due.diff(start, 'days');
    
                objBatch.batchName = getDetail[0].modelname;
                objBatch.piscalYear = getDetail[0].piscalyear;
                objBatch.amountBsm = getDetail[0].numberofoperators;
    
                var dayString = moment(dueDateDtsm).format("DD");
                var monthString = moment(dueDateDtsm).format("MM");
                var yearString = moment(dueDateDtsm).format("YYYY");
                var yearTh =  parseInt(yearString) + 543;
                var dueDateString = dayString+"/"+monthString+"/"+yearTh;
                objBatch.dueDate = dueDateString;
                objBatch.dueDateAmount = diffDay;
                console.log(dueDateString);

                client.close();
                return objBatch;
            }else{
                objBatch.batchName = getDetail[0].modelname;
                objBatch.piscalYear = getDetail[0].piscalyear;
                objBatch.amountBsm = getDetail[0].numberofoperators;
    
                var dayString = moment(dueDateDtsm).format("DD");
                var monthString = moment(dueDateDtsm).format("MM");
                var yearString = moment(dueDateDtsm).format("YYYY");
                var yearTh =  parseInt(yearString) + 543;
                var dueDateString = dayString+"/"+monthString+"/"+yearTh;
                objBatch.dueDate = dueDateString;
                objBatch.dueDateAmount = 0;
                console.log(dueDateString);

                client.close();
                return objBatch;
            }
            
        }
    }catch(e){
        console.log('\n',e);
        return null;
    }
}