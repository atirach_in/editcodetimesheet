var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailBatch = getDetailBatch;

async function getDetailBatch(batchId){
    try{
        var countBsm;
        var projectPending;
        var projectReject;
        var projectSuccess;
        var respond ={
            countBsm:0,
            projectPending:"",
            percentPending:0,
            projectReject:"",
            percentReject:0,
            projectSuccess:"",
            percentSucess:0,
            dueDate:"",
            differanceTime:0
        }
        
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getDetailBatch = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        const getCountBsm = await db.collection("projects").find({batchid:batchId}).toArray();
        countBsm = getDetailBatch[0].numberofoperators;
        respond.countBsm = getCountBsm.length;
        console.log("count bsm: "+countBsm);
    
        //get todays date
        var today = moment();
        var dueDate = getDetailBatch[0].duedate;//moment(getDetailBatch[0].duedate).format("D/MM/YYYY");
        respond.dueDate = dueDate;
        console.log("dueDate: "+dueDate);
        console.log("Day: "+ moment(getDetailBatch[0].duedate).format("D"));
        var day =  moment(getDetailBatch[0].duedate).format("D");
        console.log("Month: "+ moment(getDetailBatch[0].duedate).format("MM"));
        var month = moment(getDetailBatch[0].duedate).format("MM")
        console.log("Year: " + moment(getDetailBatch[0].duedate).format("YYYY"));
        var year = moment(getDetailBatch[0].duedate).format("YYYY");
        var calYear = year-543;
        var setDueDate = moment(calYear+"/"+month+"/"+day, 'YYYY-MM-DD');
        var diffdate = setDueDate.diff(today, 'day');
        console.log("diff date: "+diffdate);
        respond.differanceTime = diffdate;
    
        //project status sending
        const getProjectPending = await db.collection("batchbusinessman").find({$and:[{batchid:batchId},{status:"2"}]}).toArray();
        projectPending = getProjectPending.length;
        respond.projectPending = projectPending;
        respond.percentPending = (projectPending * 100) / countBsm;
        console.log("project pending: "+projectPending);
    
        //project status reject 
        const getProjectReject = await db.collection("batchbusinessman").find({$and:[{batchid:batchId},{status:"3"}]}).toArray();
        projectReject = getProjectReject.length;
        respond.projectReject = projectReject;
        respond.percentReject = (projectReject * 100)/countBsm;
        console.log("project reject: "+projectReject);
    
        //project status success 
        const getProjectSuccess = await db.collection("batchbusinessman").find({$and:[{batchid:batchId},{status:"4"}]}).toArray();
        projectSuccess = getProjectSuccess.length;
        respond.projectSuccess = projectSuccess;
        respond.percentSucess = (projectSuccess * 100) / countBsm;
        console.log("project sucess: "+projectSuccess);

        client.close();
        return respond;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}