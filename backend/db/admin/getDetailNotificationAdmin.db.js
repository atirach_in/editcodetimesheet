var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateReadNotiAdmin = updateReadNotiAdmin;

async function updateReadNotiAdmin(notiId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
    
        //update noti read = true 
        const updateNotiFication = await db.collection("notification_admin").updateOne(
                                                    {"_id":ObjectId(notiId)},
                                                    { $set: {read:true}});
        console.log("update notiId: "+notiId +" success.");

        client.close();
        return {data:"update notiId success.",status:true}; 
    }catch(e){
        console.log('\n',e);
        return{data:"update notiId failed.",status:false};
    }
}