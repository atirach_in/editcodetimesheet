var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailProgressWeekApprove = getDetailProgressWeekApprove;

async function getDetailProgressWeekApprove(progressGroupPlanId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objDetailProgress = {
            projectId:"",
            projectName:"",
            projectValue:0,
            valueUsed:0,
            userPrimary:"",
            weekNumber:0,
            plans:[]
        }
        const getDetailProgressGroupPlan = await db.collection("progress_groupplan").find({"_id":ObjectId(progressGroupPlanId)}).toArray();
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getDetailProgressGroupPlan[0].projectid)}).toArray();
        const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
        objDetailProgress.projectId = getDetailProgressGroupPlan[0].projectid;
        objDetailProgress.projectName = getDetailProject[0].projectname;
        objDetailProgress.userPrimary = getUserPrimary[0].fristname+" "+ getUserPrimary[0].lastname;
        objDetailProgress.projectValue = getDetailProject[0].projectvalue;
    
        //sum total value used 
        var projectFindSumValueUsed = getDetailProgressGroupPlan[0].projectid.toString();
        const getSumValue = await db.collection("timesheet").find({projectid:projectFindSumValueUsed}).toArray();
        for(var sumvalueused = 0; sumvalueused < getSumValue.length;sumvalueused++){
            objDetailProgress.valueUsed += getSumValue[sumvalueused].totalvalue;
        }
    
        console.log("projectId: "+getDetailProgressGroupPlan[0].projectid);
        console.log("projectName: "+getDetailProject[0].projectname);
        console.log("username: " + getUserPrimary[0].fristname+" "+ getUserPrimary[0].lastname);
    
        //get weekNumber 
        const getWeekNumber = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progressGroupPlanId)}).toArray();
        objDetailProgress.weekNumber = getWeekNumber[0].weeknumber;
    
        const getProgressWeek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progressGroupPlanId)}).toArray();
        console.log("getProgressWeek.length: " + getProgressWeek.length);
        for(var countProgress = 0; countProgress < getProgressWeek.length; countProgress++){
            var progressWeekid = getProgressWeek[countProgress]._id;
            var objPlan = {
                planName:"",
                durationOrder:0,
                planValue:0,
                valueUsed:0,
                timeLength:0,
                timeUsed:0,
                team:[],
                document:"",
                barrier:"",
                totalHours:0
            }
            var totalHoursPlan = 0;
            console.log("planName: "+getProgressWeek[countProgress].planname);
            console.log("durationOrder: "+getProgressWeek[countProgress].durationorder);
            console.log("planValue: "+getProgressWeek[countProgress].planvalue);
            console.log("valueUsed: "+getProgressWeek[countProgress].valueused);
            console.log("timeLength: "+getProgressWeek[countProgress].timelength);
            console.log("timeUsed: "+getProgressWeek[countProgress].timeused);
            console.log("document: "+getProgressWeek[countProgress].documentation);
            console.log("barrier: "+getProgressWeek[countProgress].barries);
    
            objPlan.planName = getProgressWeek[countProgress].planname;
            objPlan.durationOrder = getProgressWeek[countProgress].durationorder;
            objPlan.planValue = getProgressWeek[countProgress].planvalue;
            objPlan.valueUsed = getProgressWeek[countProgress].valueused;
            objPlan.timeLength = getProgressWeek[countProgress].timelength;
            objPlan.timeUsed = getProgressWeek[countProgress].timeused;
            objPlan.document = getProgressWeek[countProgress].documentation;
            objPlan.barrier = getProgressWeek[countProgress].barries;
    
            const getUserTimeSheet = await db.collection("detailprogress_weeks").find({progress_weekid:ObjectId(progressWeekid)}).toArray(); 
            console.log("getUserTimeSheet.length: "+getUserTimeSheet.length);
            for(var countuser =0;countuser < getUserTimeSheet.length;countuser++){
                var objUser={
                    username:"",
                    hours:0,
                    comment:""
                }
                console.log("username: "+getUserTimeSheet[countuser].username);
                console.log("hours: "+getUserTimeSheet[countuser].totalhours);
                console.log("comment: "+getUserTimeSheet[countuser].comment);
                objUser.username = getUserTimeSheet[countuser].username;
                objUser.hours = getUserTimeSheet[countuser].totalhours;
                objUser.comment = getUserTimeSheet[countuser].comment;
                totalHoursPlan += getUserTimeSheet[countuser].totalhours;
                objPlan.team.push(obj_user);
            }
            objPlan.totalHours = totalHoursPlan;
            objDetailProgress.plans.push(objPlan);
            console.log("------------------------------------------");
        }

        client.close();
        return objDetailProgress;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}