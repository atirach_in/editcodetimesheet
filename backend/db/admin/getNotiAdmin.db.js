var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
    authSource:'admin'
    

};

exports.getNotiAdmin = getNotiAdmin;

async function getNotiAdmin(adminId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        //check adminid 
        const getAdmin = await db.collection("accountadmin").find({"_id":ObjectId(adminId)}).toArray();
        if(getAdmin.length > 0){
            var objNotifications={
                notifications:[],
                countRead:0
            }
            //count read 
            const getCountRead = await db.collection("notification_admin").find({read:""}).toArray();
            objNotifications.countRead = getCountRead.length;
            //get collection notification_users
            const getNotiAdmin = await db.collection("notification_admin").find({}).toArray();
            console.log("getNotiAdmin.length : "+ getNotiAdmin.length);
            for(var countNoti = (getNotiAdmin.length - 1);countNoti >= 0;countNoti--){
                var objNoti ={
                    notiId:getNotiAdmin[countNoti]._id,
                    companyName:"",
                    projectId:getNotiAdmin[countNoti].projectid,
                    planId:getNotiAdmin[countNoti].planid,
                    statusAction:getNotiAdmin[countNoti].statusaction,
                    statusString:getNotiAdmin[countNoti].statusstring,
                    progressGroupPlanId:getNotiAdmin[countNoti].progress_groupplan_id,
                    read:getNotiAdmin[countNoti].read,
                    lastTime:""
                }
    
                //get company 
                const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getNotiAdmin[countNoti].projectid)}).toArray();
                const getCompanyName = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
                objNoti.companyName = getCompanyName[0].companyname;
    
                var lastProjectTime = moment(Math.ceil(getNotiAdmin[countNoti].datetimestamp));
                var thisTime = moment(pastDatetime._created);
                console.log("lastProjectTime: "+lastProjectTime);
                console.log("this time: "+pastDatetime._created);
    
                //date project
                var dateProjectMoment = moment(lastProjectTime).format('DD/MM/YYYY');
                var dayPro = moment(lastProjectTime).format('DD');
                var monthPro = moment(lastProjectTime).format('MM');
                var yearPro = moment(lastProjectTime).format('YYYY');
                var hrPro = moment(lastProjectTime).format('HH');
                var minutePro = moment(lastProjectTime).format('mm');
                var ssPro = moment(lastProjectTime).format('ss');
                var lastUpdateProject = moment([yearPro, monthPro, dayPro,hrPro,minutePro]);
                console.log("lastUpdateProject: "+lastUpdateProject);
                var yearTh = parseInt(yearPro) + 543;
                objNoti.lastTime =  dayPro+"/"+monthPro+"/"+yearPro+"  "+hrPro+":"+minutePro+":"+ssPro
    
                //date now 
                // var datenow_moment = moment(thisTime).format('DD/MM/YYYY');
                // var daynow = moment(thisTime).format('DD');
                // var monthnow = moment(thisTime).format('MM');
                // var yearnow = moment(thisTime).format('YYYY');
                // var hrnow = moment(thisTime).format('HH');
                // var minutenow = moment(thisTime).format('mm');
                // var ssnow = moment(thisTime).format('ss');
                // var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
                // console.log("datenow_action: "+datenow_action);
    
                // var datepro = new Date(yearPro, monthPro, dayPro);
                // var dateNow = new Date(yearnow, monthnow, daynow);
    
                // var diff_minute = datenow_action.diff(lastUpdateProject, 'minute');
                // var diff_Hr = datenow_action.diff(lastUpdateProject, 'hour');
                // var diff_day = datenow_action.diff(lastUpdateProject, 'days');
    
                // console.log("diff_minute: " + diff_minute);
                // console.log("diff_Hr: " + diff_Hr);
                // console.log("diff_day: " + diff_day);
                // if(diff_day > 0){
                // 	objNoti.lastTime = diff_day +" "+"วัน";
                // }else if(diff_Hr > 0){
                // 	objNoti.lastTime = diff_Hr +" "+"ชั่วโมง";
                // }else if(diff_minute > 0){
                // 	objNoti.lastTime = diff_minute +" "+"นาที";
                // }
                objNotifications.notifications.push(objNoti);
    
                // var diff = new DateDiff(dateNow,datepro);
                // console.log("diff days: "+diff.days());
                // console.log("diff minute: "+diff.minutes());
                // console.log("diff hours: "+diff.hours());
                // console.log("--------------------------");
                // if(diff.minutes() >= 1440){
                // 	var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
                // 	console.log("calculate days:" + days);
                // }else if (diff.minutes() >= 60){
                // 	var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
                // 	console.log("calculate hours:" + hours);
                // }else if(diff.minutes() <= 59){
                // 	var minutes = (diff.minutes()) +" "+ "นาที";
                // 	console.log("calculate minutes:" + minutes);
                // }
                console.log("--------------------------");
            }
            client.close();
            return objNotifications;
        }else{
            client.close();
            return {data:"invalid adminid.",status:false};
        }  
    }catch(e){
        console.log('\n',e);
        return;
    }
}