var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getOverAllProgressProject = getOverAllProgressProject;

async function getOverAllProgressProject(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objOverallProject = {
            projectId:projectId,
            projectName:"",
            userPrimary:"",
            projectValue:0,
            plans:[],
            team:[],
            document:""
        }
    
        //get detail project 
        var getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        //get user primary 
        var getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
        var objUserprimary = {
            username:getUserPrimary[0].fristname +" "+getUserPrimary[0].lastname,
            position:getUserPrimary[0].position,
            profileImage:getUserPrimary[0].profileimage
        }
        objOverallProject.team.push(objUserprimary);
        objOverallProject.projectName = getDetailProject[0].projectname;
        objOverallProject.userPrimary = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
        objOverallProject.projectValue = getDetailProject[0].projectvalue;
        objOverallProject.document = getDetailProject[0].Documentation;
    
    
        //get all plan on project 
        var getAllPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("getAllPlan.length: " + getAllPlan.length);
        for(var countPlan =0;countPlan < getAllPlan.length;countPlan++){
            var objPlan={
                planName:getAllPlan[countPlan].planname,
                durationOrder:0,
                jobValue:0,
                usedValue:0,
                timeLength:getAllPlan[countPlan].timelength,
                usedTime:0,
                moneyDifference:0,
                enforceEmployee:0,
                averageWages:0,
                statusPlan:""
            }
            //get durationid 
            const getDurationOrder = await db.collection("duration").find({"_id":ObjectId(getAllPlan[countPlan].durationid)}).toArray();
            //Sum total 
            var sumHours = 0;
            var sumTotalValue = 0;
            var planIdFind = getAllPlan[countPlan]._id.toString();
            const getSumTotal = await db.collection("timesheet").find({planid:planIdFind}).toArray();
            for(var countTimeSheet = 0;countTimeSheet < getSumTotal.length;countTimeSheet++){
                sumHours += getSumTotal[countTimeSheet].hours;
                sumTotalValue += getSumTotal[countTimeSheet].totalvalue;
            }
    
            console.log("planName: " + getAllPlan[countPlan].planname);
            console.log("valueproject: " + getAllPlan[countPlan].jobvalue);
            console.log("timeLength: " + getAllPlan[countPlan].timelength);
            console.log("duration: " + getDurationOrder[0].durationorder);
            console.log("usetime: "+sumHours);
            console.log("usedValue: "+sumtotalvalue);
    
            objPlan.planName =  getAllPlan[countPlan].planname;
            objPlan.jobValue = getAllPlan[countPlan].jobvalue;
            objPlan.durationOrder = getDurationOrder[0].durationorder;
            objPlan.timeLength =  getAllPlan[countPlan].timelength;
            objPlan.usedValue = sumTotalValue;
            objPlan.usedTime = sumHours;
            objPlan.moneyDifference = getAllPlan[countPlan].moneydifference;
            objPlan.averageWages = getAllPlan[countPlan].averagewages;
            objPlan.enforceEmployee = getAllPlan[countPlan].enforceemployee;
            objPlan.statusPlan = getAllPlan[countPlan].statusplan;
            objOverallProject.plans.push(objPlan);
            console.log("------------------------------------------------");
        }
    
        //get team 
        const getTeam = await db.collection("users").find({projectid:projectId}).toArray();
        for(var countTeam=0;countTeam < getTeam.length;countTeam++){
            var objUser = {
                username:getTeam[countTeam].firstname +" "+getTeam[countTeam].lastname,
                position:getTeam[countTeam].position,
                profileImage:getTeam[countTeam].profileimage
            }
            objOverallProject.team.push(objUser);
        }
        console.log(objOverallProject);
        
        client.close();
        return objOverallProject;
    }catch(e){
        console.log('\n',e);
        return;
    }
}