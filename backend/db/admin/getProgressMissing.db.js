var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getProgressMissing = getProgressMissing;

async function getProgressMissing(batchId){
    try{    
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = dateAndTime.format(new Date(), 'D/MMMM/YYYY');
        var pastDatetime = nodeDatetime.create(dateNow);
        var objProgressMissing = {
            progressWeeks:[]
        }
        //get bsm missing
        console.log("----------------missing---------------------");
        var countMissing = 0;
        const getProjectOnBatch = await db.collection("projects").find({batchid:batchId}).toArray();
    
        //all project 
        for(var countProject =0; countProject < getProjectOnBatch.length; countProject++){
            //check week on project 
            var projectIdGet = getProjectOnBatch[countProject]._id.toString();
            //var sort = {weeknumber:1};
            console.log("projectid: "+projectIdGet);
            const checkWeek = await db.collection("weeks").find({projectid:projectIdGet}).toArray();
            console.log("checkWeek.length: " +checkWeek.length);
            var objProgressWeek = {
                projectId:"",
                projectName:"",
                comapnyName:"",
                userPrimary:"",
                weekId:"",
                weekNumber:0
            }
                //get day on this dateNow 
                const getDayOnDateNow = await db.collection("days").find({$and:[{datestring:dateNow},{projectid:projectIdGet}]}).toArray();
                if(getDayOnDateNow.length == 0){
                }
                else{
                    console.log("datestring now: "+ getDayOnDateNow[0].datestring);
                    console.log("weekId: "+getDayOnDateNow[0].weekid);

                    //get week on project 
                    const getWeekOnDateNow = await db.collection("weeks").find({"_id":ObjectId(getDayOnDateNow[0].weekid)}).toArray();
                    console.log("weekNumber: " + getWeekOnDateNow[0].weeknumber);
                    if(getWeekOnDateNow[0].weeknumber <= 1){
                        countMissing +=0;
                    }else{
                        //get less than week now on collection progress weeks
                        //const getLessthanWeek = await db.collection("progressWeeks").find({$and:[{weeknumber:{$lt:getWeekOnDateNow[0].weeknumber}},{projectid:projectIdGet}]}).toArray();
    
                        for(var count = 1;count < getWeekOnDateNow[0].weeknumber;count++){
                            //get less than week now on collection progress weeks
                            const getLessthanWeek = await db.collection("progress_weeks").find({$and:[
                                                                                                    {weeknumber:count},
                                                                                                    {projectid:projectIdGet}
                                                                                                    ]}).toArray();
                            if(getLessthanWeek.length > 0){
                                countMissing += 0;
                            }else{
                                //console.log("planid: "+getWeekOnDateNow[count].planid);
                                console.log("at week: "+ count);
                                objProgressWeek.weekNumber = count;
                                console.log("getLessthanWeek.length: "+getLessthanWeek.length);
                                console.log("---------------------------------------------------");
                                const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectIdGet)}).toArray();
                                const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
                                objProgressWeek.projectId = projectIdGet;
                                objProgressWeek.projectName = getDetailProject[0].projectname;
                                objProgressWeek.comapnyName = getUserPrimary[0].companyname;
                                objProgressWeek.userPrimary = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
                                objProgressWeek.weekId = "";
                                objProgressMissing.progressWeeks.push(objProgressWeek);
                                //countMissing += 1;
                            }
                        }
    
                        //console.log("getLessthanWeek.length: "+getLessthanWeek.length);
                        //countMissing += 1;
                        // for(var countprogress =0; countprogress < getLessthanWeek.length;countprogress++){
                        // 	const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectIdGet)}).toArray();
                        // 	objProgressWeek.projectId = projectIdGet;
                        // 	objProgressWeek.projectName = getDetailProject[0].projectname;
                        // 	objProgressWeek.comapnyName = getDetailProject[0].companyname;
                        // 	objProgressWeek.weekId = "";
                        // 	objProgressWeek.weekNumber = getWeekOnDateNow[0].weeknumber;
                        // 	objProgressMissing.progress_weeks.push(objProgressWeek);
                        // }
                    }
                }
                console.log("--------------------");
            
        console.log("-------------------------------------");
        }
        console.log(objProgressMissing);
        
        client.close();
        return objProgressMissing;
    }catch(e){
        console.log('\n',e);
        return;
    }
}