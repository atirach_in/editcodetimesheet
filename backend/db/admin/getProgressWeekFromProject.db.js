var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getProgressWeekFromProject = getProgressWeekFromProject;

async function getProgressWeekFromProject(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        var objWeeks={
            weekNumberNow:0,
            weeks:[]
        }
        var dateNowFind = moment().format('DD/MMMM/YYYY');
        const getDayNow = await db.collection("days").find({$and:[{datestring:dateNowFind},{projectid:projectId}]}).toArray();
        if(getDayNow.length == 0){
            console.log("data: no have days on timesheet.");
            console.log("--------------------------------");
        }else{
            const getWeekNow = await db.collection("weeks").find({"_id":ObjectId(getDayNow[0].weekid)}).toArray();
            objWeeks.weekNumberNow = getWeekNow[0].weeknumber;
        }
        const getAllWeek = await db.collection("weeks").find({projectid:projectId}).toArray();
        for(var count=0;count < getAllWeek.length;count++){
            var objWeek={
                weekNumber:0,
                statusWeek:0,
                progressGroupPlanId:""
            }
            const getProgressGroupPlanId = await db.collection("progress_weeks").find({weekid:ObjectId(getAllWeek[count]._id)}).toArray();
            if(getProgressGroupPlanId.length == 0){
                objWeek.progressGroupPlanId = "";
            }else{
                objWeek.progressGroupPlanId = getProgressGroupPlanId[0].progress_groupplan_id;
                console.log("progress week group plan id:"+ getProgressGroupPlanId[0].progress_groupplan_id);
            }
            console.log("week id:"+ getAllWeek[count]._id);
            console.log("at week:"+getAllWeek[count].weeknumber);
            console.log("-------------------------------------");
            objWeek.weekNumber = getAllWeek[count].weeknumber;
            objWeek.statusWeek = getAllWeek[count].statusweek;
    
            objWeeks.weeks.push(objWeek);
        }
        
        client.close();
        return objWeeks;  
    }catch(e){
        console.log('\n',e);
        return null;
    }
}