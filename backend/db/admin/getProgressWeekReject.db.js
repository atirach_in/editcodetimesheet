var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getProgressWeekReject = getProgressWeekReject;

async function getProgressWeekReject(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objProgressReject = {
            progress:[]
        }
        //get progress week.
        const getProgressWeek = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:3}]}).toArray();
        console.log("getProgressWeek.length: "+getProgressWeek.length);
        console.log("--------------------------------------------");
        for(var countweek =0;countweek < getProgressWeek.length;countweek++){
            var objProgress = {
                progressGroupPlanId:getProgressWeek[countweek]._id,
                projectName:"",
                userName:"",
                weekNumber:0,
                updateTime:""
            }
            //get detail project 
            const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getProgressWeek[countweek].projectid)}).toArray();
            //get users primary 
            const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
            //get weeknumber 
            const getWeekNumber = await db.collection("progress_weeks").find({progressGroupPlanId:ObjectId(getProgressWeek[countweek]._id)}).toArray();
            console.log("getWeekNumber.length: "+ getWeekNumber.length);
            console.log("weekNumber: "+getWeekNumber[0].weeknumber);
            objProgress.weekNumber = getWeekNumber[0].weeknumber;
            
            console.log("progress_groupid: "+getProgressWeek[countweek]._id);
            console.log("userName: "+getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname);
            
            objProgress.projectName = getDetailProject[0].projectname;
            objProgress.userName = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
            var updateTime = getProgressWeek[countweek].updatetime;
            objProgress.updateTime = moment(updateTime).format("DD/MM/YYYY");
            console.log("updateTime: "+moment(updateTime).format("DD/MM/YYYY"));
            console.log("--------------------------------------------");
            objProgressReject.progress.push(objProgress);
        }
        
        client.close();
        return objProgressReject;
    }catch(e){
        console.log('\n',e);
        return;
    }
}