var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getProgressSending = getProgressSending;

async function getProgressSending(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objSending={
            sending:[]
        }
        //get bsm sending progress week.
        const getSending = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:1}]}).toArray();
        console.log("bsm sending: "+getSending.length);
        for(var countSending = 0;countSending < getSending.length;countSending++){
            var objProgressWeek = {
                progressGroupPlanId:"",
                projectId:"",
                projectName:"",
                userName:"",
                updateTime:0,
                weekId:"",
                weekNumber:0,
                statusWeek:0
            }
            const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getSending[countSending].projectid)}).toArray();
            const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
            objProgressWeek.progressGroupPlanId = getSending[countSending]._id;
            objProgressWeek.projectId = getSending[countSending].projectid;
            objProgressWeek.projectName =  getDetailProject[0].projectname;
            objProgressWeek.userName = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
            objProgressWeek.statusWeek = getSending[countSending].status;
    
            var day = moment(getSending[countSending].updatetime).format("DD");
            var month = moment(getSending[countSending].updatetime).format("MM");
            var year = moment(getSending[countSending].updatetime).format("YYYY");
            var yearTh = parseInt(year) + 543;
            var time = moment(getSending[countSending].updatetime).format("HH:mm:ss");
            objProgressWeek.updateTime = day+"/"+month+"/"+yearTh+" "+time; 
    
            //get weekNumber
            const getProgressWeek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(getSending[countSending]._id)}).toArray();
            objProgressWeek.weekNumber = getProgressWeek[0].weeknumber;
            objSending.sending.push(objProgressWeek);
        }
    
        //get bsm sending progress week late .
        const getSendingLate = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:5}]}).toArray();
        console.log("bsm sending: "+getSendingLate.length);
        for(var countSending = 0;countSending < getSendingLate.length;countSending++){
            var objProgressWeek = {
                progressGroupPlanId:"",
                projectId:"",
                projectName:"",
                userName:"",
                updateTime:0,
                weekId:"",
                weekNumber:0,
                statusWeek:0
            }
            const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getSendingLate[countSending].projectid)}).toArray();
            const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getDetailProject[0].Primaryresponsibility)}).toArray();
            objProgressWeek.progressGroupPlanId = getSendingLate[countSending]._id;
            objProgressWeek.projectId = getSendingLate[countSending].projectid;
            objProgressWeek.projectName =  getDetailProject[0].projectname;
            objProgressWeek.userName = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
            objProgressWeek.statusWeek = getSendingLate[countSending].status;
    
            var day = moment(getSendingLate[countSending].updatetime).format("DD");
            var month = moment(getSendingLate[countSending].updatetime).format("MM");
            var year = moment(getSendingLate[countSending].updatetime).format("YYYY");
            var yearTh = parseInt(year) + 543;
            var time = moment(getSendingLate[countSending].updatetime).format("HH:mm:ss");
            objProgressWeek.updateTime = day+"/"+month+"/"+yearTh+" "+time; 
    
            //get weekNumber
            const getProgressWeek = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(getSendingLate[countSending]._id)}).toArray();
            objProgressWeek.weekNumber = getProgressWeek[0].weeknumber;
    
            objSending.sending.push(objProgressWeek);
        }
    
        console.log(objSending);

        client.close();
        return objSending;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}