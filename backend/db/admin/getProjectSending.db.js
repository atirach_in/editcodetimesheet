var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getProjectSending = getProjectSending;

async function getProjectSending(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var status = "2";//reject 
        var objProjects={
            batchId:batchId,
            project:[]
        }
        //get project 
        const getProject = await db.collection("batchbusinessman").find({$and:[{batchid:batchId},{status:status}]}).toArray();
        for(var i = 0; i < getProject.length; i++){
            var objProject = {
                batchId:batchId,
                projectId:getProject[i].projectid,
                projectName:"",
                userPrimary:"",
                status:2,
                statusString:"ส่งข้อมูล",
                updateTime:""
            };
            var projectId = getProject[i].projectid;
            const getProjectDetail = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
            var userId = getProjectDetail[0].Primaryresponsibility;
            const getUsername = await db.collection("users").find({"_id":ObjectId(userId)}).toArray();
            var username = getUsername[0].fristname +" "+getUsername[0].lastname;
            objProject.projectName = getProjectDetail[0].projectname;
            objProject.userPrimary = username;
            var day = moment(getProject[i].updatetime).format("DD");
            var month = moment(getProject[i].updatetime).format("MM");
            var year = moment(getProject[i].updatetime).format("YYYY");
            var yearTh = parseInt(year) + 543;
            objProject.updateTime = day+"/"+month+"/"+yearTh+" "+moment(getProject[i].updatetime).format("HH:mm:ss");
            objProjects.project.push(objProject);
        }
        console.log(objProjects);
        console.log("-------------------------");

        client.close();
        return objProjects;
    }catch(e){
        console.log('\n',e);
        return;
    }
}