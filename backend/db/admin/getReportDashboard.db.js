var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getReportDashboard = getReportDashboard;

async function getReportDashboard(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = dateAndTime.format(new Date(), 'D/MMMM/YYYY');
        var pastDatetime = nodeDatetime.create(dateNow);
    
        var objDashboard = {
            batchId:batchId,
            modelName:"",
            piscalYear:"",
            allValueUsedBatch:0,
            allTimeused:0,
            numberOfOperators:0,
            sendingProgress:0,
            missingProgress:0,
            rejectProgress:0,
            nowOperators:0
        }
    
        var sumTotalAllValueused = 0;
        var sumTotalAllTimeused = 0;
        var setFirstDayOnproject = 0;
        var dateNowStringDay = "";
        //get detail batch 
        const getDetailBatch = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        objDashboard.modelName = getDetailBatch[0].modelname;
        objDashboard.piscalYear = getDetailBatch[0].piscalyear;
        objDashboard.numberOfOperators = getDetailBatch[0].numberofoperators;
    
        //get all project on batch
        const getAllProject = await db.collection("projects").find({batchid:batchId}).toArray();
        console.log("project on batch: "+ getAllProject.length);
    
        for(var countProject = 0; countProject < getAllProject.length; countProject++){
            console.log("projectid: "+getAllProject[countProject]._id);
            var projectIdFind = getAllProject[countProject]._id.toString();
            //get sum total used value on batch.
            const getTotalValue = await db.collection("progress_weeks").find({projectid:projectIdFind}).toArray();
            if(getTotalValue.length == 0){
                sumTotalAllValueused += 0;
                sumTotalAllTimeused += 0;
            }else{
                sumTotalAllValueused += getTotalValue[0].valueused;	
                sumTotalAllTimeused += getTotalValue[0].timeused;
            }
        }
        console.log("sumTotalAllValueused: " + sumTotalAllValueused +" Bath.");
        console.log("sumTotalAllTimeused: " + sumTotalAllTimeused +" days.");
        objDashboard.allValueUsedBatch = sumTotalAllValueused;
        objDashboard.allTimeused = sumTotalAllTimeused;
        objDashboard.nowOperators = getAllProject .length
    
        //get bsm sending progress week.
        const getSending = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:1}]}).toArray();
        console.log("bsm sending: "+getSending.length);
        //objDashboard.sendingProgress = getSending.length;
    
        //get bsm sending progress week late.
        const getSendingLate = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:5}]}).toArray();
        console.log("bsm sending: "+getSendingLate.length);
        objDashboard.sendingProgress = getSendingLate.length+getSending.length;
    
    
        //get bsm reject progress week.
        const getReject = await db.collection("progress_groupplan").find({$and:[{batchid:batchId},{status:3}]}).toArray();
        console.log("bsm reject: " + getReject.length);
        objDashboard.rejectProgress = getReject.length;
    
        //get bsm missing
        console.log("----------------missing---------------------");
        var countMissing = 0;
        const getProjectOnBatch = await db.collection("projects").find({batchid:batchId}).toArray();
    
        //all project 
        for(var countProjectMissing =0;countProjectMissing < getProjectOnBatch.length;countProjectMissing++){
            //check week on project 
            var projectIdGet = getProjectOnBatch[countProjectMissing]._id.toString();
            //var sort = {weeknumber:1};
            console.log("projectid: "+projectIdGet);
            const checkWeek = await db.collection("weeks").find({projectid:projectIdGet}).toArray();
            console.log("checkWeek.length: " +checkWeek.length);
        
                //get day on this dateNow 
                const getDayOnDateNow = await db.collection("days").find({$and:[{datestring:dateNow},{projectid:projectIdGet}]}).toArray();
                if(getDayOnDateNow.length == 0){
                }
                else{
                    console.log("datestring now: "+ getDayOnDateNow[0].datestring);
                    console.log("weekid: "+getDayOnDateNow[0].weekid);
                    //get week on project 
                    const getWeekOnDateNow = await db.collection("weeks").find({"_id":ObjectId(getDayOnDateNow[0].weekid)}).toArray();
                    console.log("weeknumber: " + getWeekOnDateNow[0].weeknumber);
                    if(getWeekOnDateNow[0].weeknumber <= 1){
                        countMissing +=0;
                    }else{
                        for(var count = 1;count < getWeekOnDateNow[0].weeknumber;count++){
                            //get less than week now on collection progress weeks
                            const getLessthanWeek = await db.collection("progress_weeks").find({$and:[
                                                                                                    {weeknumber:count},
                                                                                                    {projectid:projectIdGet}
                                                                                                    ]}).toArray();
                            if(getLessthanWeek.length > 0){
                                countMissing += 0;
                            }else{
                                //console.log("planid: "+getWeekOnDateNow[count].planid);
                                console.log("at week: "+ count);
                                console.log("getLessthanWeek.length: "+getLessthanWeek.length);
                                console.log("---------------------------------------------------");
                                countMissing += 1;
                            }
                        }
                    }
                }
                console.log("--------------------");
            
        console.log("-------------------------------------");
        objDashboard.missingProgress = countMissing;
        }
        console.log(objDashboard);
        
        client.close();
        return objDashboard;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}