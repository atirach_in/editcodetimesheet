var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailPoject = getDetailPoject;

async function getDetailPoject(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        var projectObj ={
                batchName:"",
                piscalYear:"",
                numberOfOperators:"",
                startingDate:0,
                dueDate:0,
                projectName:"",
                primaryUser:"",
                dateTime:0,
                documentation:[],
                projectValue:0,
                planOrder:[],
                team:[],
                lastUpdateTime:"",
                statusString:""
        }
        //Primaryresponsibility
        const project = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var statusProject = project[0].status.toString();
        //status project 
        if(statusProject == "0"){
            projectObj.statusString = "กำลังกรอกข้อมูล";
        }else if(statusProject == "1"){
            projectObj.statusString = "ส่งข้อมูลแล้ว";
        }else if(statusProject == "3"){
            projectObj.statusString = "ปรับแก้เอกสาร";
        }else if(statusProject == "4"){
            projectObj.statusString = "อนุมัติการใช้ Timsheet";
        }
        //get bacth name 
        const getDetailBatch = await db.collection("batch").find({"_id":ObjectId(project[0].batchid)}).toArray();
        projectObj.batchName = getDetailBatch[0].modelname;
        projectObj.piscalYear = getDetailBatch[0].piscalyear;
        projectObj.numberOfOperators = getDetailBatch[0].numberofoperators;
        projectObj.startingDate = getDetailBatch[0].startingdate;
        projectObj.dueDate = getDetailBatch[0].duedate;
    
        console.log("batchName: "+ projectObj.batchName);
        console.log("piscalyear: "+ projectObj.piscalYear);
        console.log("numberOfOperators: " + projectObj.numberOfOperators);
        console.log("startingDate: "+ projectObj.startingDate);
        console.log("dueDate: "+projectObj.dueDate);
    
        //cal between time 
        const getTimeProject = await db.collection("batchbusinessman").find({"projectid":ObjectId(projectId)}).toArray();
        var lastProjectTime = moment(Math.ceil(getTimeProject[0].updatetime));
        var thisTime = moment(pastDatetime._created);
        console.log("lastProjectTime: "+lastProjectTime);
        console.log("this time: "+pastDatetime._created);
    
        //date project
        var dateProjectMoment = moment(lastProjectTime).format('DD/MM/YYYY');
        var dayPro = moment(lastProjectTime).format('DD');
        var monthPro = moment(lastProjectTime).format('MM');
        var yearPro = moment(lastProjectTime).format('YYYY');
        var hrPro = moment(lastProjectTime).format('HH');
        var minutePro = moment(lastProjectTime).format('mm');
        var ssPro = moment(lastProjectTime).format('ss');
    
        //date now 
        var dateNowMoment = moment(thisTime).format('DD/MM/YYYY');
        var dayNow = moment(thisTime).format('DD');
        var monthNow = moment(thisTime).format('MM');
        var yearNow = moment(thisTime).format('YYYY');
        var hrNow = moment(thisTime).format('HH');
        var minuteNow = moment(thisTime).format('mm');
        var ssNow = moment(thisTime).format('ss');
    
        var datePro = new Date(yearPro, monthPro, dayPro);
        var dateNow = new Date(yearNow, monthNow, dayNow);
        var diff = new DateDiff(dateNow,datePro);
        console.log("diff days: "+diff.days());
        console.log("diff minute: "+diff.minutes());
        console.log("diff hours: "+diff.hours());
        console.log("--------------------------");
        if(diff.minutes() >= 1440){
            var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
            projectObj.lastUpdateTime = days;
            console.log("calculate days:" + days);
        }else if (diff.minutes() >= 60){
            var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
            projectObj.lastUpdateTime = days;
            console.log("calculate hours:" + hours);
        }else if(diff.minutes() <= 59){
            var minutes = (diff.minutes()) +" "+ "นาที";
            projectObj.lastUpdateTime = minutes;
            console.log("calculate minutes:" + minutes);
        }
        console.log("--------------------------");
        // diff.years(); // ===> 1.9
        // diff.months(); // ===> 23
        // diff.days(); // ===> 699
    
    
        // diff.weeks(); // ===> 99.9
        // diff.hours(); // ===> 16776
        // diff.minutes(); // ===> 1006560
        // diff.seconds(); // ===> 60393600
        
    
        const userPrimary = await db.collection("users").find({"_id":ObjectId(project[0].Primaryresponsibility)}).toArray();
        //add user primary to frist person in team 
        for(var i = 0; i < userPrimary.length; i++){
            var userObj = userPrimary[i];
            projectObj.team.push(userObj);
        }
        console.log("project name: "+ project[0].projectname);
        projectObj.projectName = project[0].projectname;
        console.log("user: "+ userPrimary[0].fristname +" "+userPrimary[0].lastname);
        projectObj.primaryUser = userPrimary[0].fristname +" "+userPrimary[0].lastname;
        console.log("dateTime: "+ project[0].senddatetime);
        projectObj.dateTime = project[0].senddatetime;
    
        // console.log("documentation: "+project[0].Documentation);
        // projectObj.documentation = project[0].Documentation;
        const getAllFile = await db.collection("fileproject").find({projectid:projectId}).toArray();
        for(var count = 0;count < getAllFile.length;count++){
            var obj={
                fileName:getAllFile[count].filename
            }
            projectObj.documentation.push(obj);
        }
    
        console.log("project value: "+project[0].projectvalue);
        projectObj.projectValue = project[0].projectvalue;
        //plan order
        const planOrders = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("-----planOrders: "+ planOrders.length +"-----");
        for(var i = 0; i < planOrders.length; i++){
            var planObj = {
                planId:planOrders[i]._id,
                planName:planOrders[i].planname,
                durationId:planOrders[i].durationid,
                duration:0,
                planValue:planOrders[i].jobvalue,
                timeLength:planOrders[i].timelength,
                jobValue:planOrders[i].jobvalue,
                moneyDifference:planOrders[i].moneydifference,
                enforceEmployee:planOrders[i].enforceemployee,
                averageWages:planOrders[i].averagewages
            };
            const getDurationNumber = await db.collection("duration").find({"_id":ObjectId(planOrders[i].durationid)}).toArray();
            planObj.duration = getDurationNumber[0].durationorder;
            console.log(planObj);
            projectObj.planOrder.push(planObj);
        }
        console.log("---------------------");
        //team work 
        const team = await db.collection("users").find({projectid:projectId}).toArray();
        console.log("----------team----------");
        for(var i = 0;i < team.length; i++){
            var teamObj = team[i];
            console.log(teamObj);
            projectObj.team.push(teamObj);
        }
        console.log("--------------------");
        
        client.close();
        return projectObj;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}