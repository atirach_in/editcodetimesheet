var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../../init.config.json');
var moment = require('moment');
var jwt = require('jsonwebtoken');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
const SECRET = "00f5vdd5r12fg1g[][dsdd122"; 
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getUser = getUser;

async function getUser(username,password){
    try{
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        console.log("data: login admin");
        console.log("username: " + username);
        console.log("password: " + password);
    
        //check username and password
         const res = await db.collection("accountadmin").find({username:username}).toArray();
         var decodePassword = jwt.verify(res[0].password,SECRET);
         var decode = jwt.verify(password,SECRET);
         var password = decode.password;
         console.log("decodePassword: "+ decodePassword.pass);
         console.log("password: "+ password);
         if(decodePassword.password == password){
             try {
             //update login time 
               const updateTimeLogin = await db.collection("accountadmin").updateOne(
                                    {username:username},
                                    { $set: {lastlogin:pastDatetime._created}}, 
                                    function(err, res) {
                                        if (err) throw err;
                                        console.log("data: update lastlogin success");
                                        console.log("-------------------");
                                     });
             
              if(res.length == 0){
                  response = {data:"invalid username or password",status:false};
                  client.close();
                  return response;
              }else{
                  response = res;
                  client.close();
                  return response;
              }
            }
            catch(err) {
                console.log(err);
                client.close();
            }
         }else{
            client.close();
            response = {data:"invalid username or password",status:false};
            return response;
         }
    }catch(e){
        console.log('\n',e);
        return{data:e,status:false};
    }
}