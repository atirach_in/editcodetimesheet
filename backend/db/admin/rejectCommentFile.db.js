var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.rejectFile = rejectFile;

async function rejectFile(projectId,comment,filename,filenameReject){
    try{    
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(datenow);
        ///add Table:commentreject_file
        var objRejectFile = {
            projectid:projectId,
            filename:filename,
            filename_reject:filenameReject,
            comment:comment,
            insertdate:pastDatetime._created,
            read:"",
            status:3,
            statusstring:"ปรับแก้เอกสารโครงการ"
        }
        const insertRejectFile = await db.collection("commentreject_file").insertOne(objRejectFile);
        console.log(objRejectFile);
        console.log("----------------------------");
        //add notification_users
        const getCommentRejectFileId = await db.collection("commentreject_file").find({$and:[{projectid:projectId},{filename:filename},{insertdate:pastdatetime._created}]}).toArray();
        var objNoti = {
            projectid:projectId,
            planid:"",
            comment_rejectid:getCommentRejectFileId[0]._id,
            statusaction:getCommentRejectFileId[0].status,
            statusstring:getCommentRejectFileId[0].statusstring,
            read:"",
            progress_groupplan_id:"",
            datetimestamp:pastDatetime._created,
            datetimestring:moment(pastDatetime._created).format('DD/MM/YYYY HH:mm:ss')
        }
        const insertNotification = await db.collection("notification_users").insertOne(objNoti);
        console.log(objNoti);
        console.log("----------------------------");
    
    
        //get plan_id
        const getPlanId = await db.collection("planorders").find({projectid:projectId}).toArray();
        var planId = getPlanId[0]._id;
        //update comment and status , updatetime 
        const updateRejectPlan = await db.collection("planorders").updateOne(
                                    {"_id":ObjectId(planId)},
                                    {$set: {
                                        comment:"",
                                        status:3,
                                        updatetime:pastDatetime._created
                                    }
                                    });
        const getProjectId = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
        //update comment and status , updatetime 
        const updateRejectProject = await db.collection("projects").updateOne(
                                        {"_id":ObjectId(projectId)},
                                        {$set:{status:3}});
        //update comment and status , updatetime 
        const updateStatusBatchBsm = await db.collection("batchbusinessman").updateOne(
                                            {projectid:ObjectId(projectId)},
                                            {$set:{status:"3",
                                                   updatetime:pastDatetime._created}});
        client.close();
        return {data:"reject file success.",status:true}
    }catch(e){
        console.log('\n',e);
        return;
    }
}