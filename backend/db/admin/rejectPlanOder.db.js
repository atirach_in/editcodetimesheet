var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.rejectProject = rejectProject;

async function rejectProject(planId,comment,pahtFile){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        dateAndTime.locale('th');
        var dateNow = dateAndTime.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        //update comment and status , updatetime 
        const updateRejectPlan = await db.collection("planorders").updateOne(
                                    {"_id":ObjectId(planId)},
                                    {$set: {
                                        comment:comment,
                                        status:3,
                                        updatetime:pastDatetime._created
                                    }
                                    });
        const getProjectId = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
        var projectId = getProjectId[0].projectid;
        //update comment and status , updatetime 
        const updateRejectProject = await db.collection("projects").updateOne(
                                        {"_id":ObjectId(projectId)},
                                        {$set:{status:3}});
        //update comment and status , updatetime 
        const updateStatusBatchBsm = await db.collection("batchbusinessman").updateOne(
                                            {projectid:ObjectId(projectId)},
                                            {$set:{status:"3",
                                                   updatetime:pastDatetime._created}});
        //insert to collection comment 
        var objComment = {
            projectid:projectId,
            planid:planId,
            comment:comment,
            pahtfile:pahtFile,
            insertdate:pastDatetime._created,
            read:"",
            status:3,
            statusstring:"ปรับแก้เอกสาร"
        };
        const insertComment = await db.collection("commentreject").insertOne(objComment);
        const getCommentRejectId = await db.collection("commentreject").find({insertdate:pastDatetime._created}).toArray();	
    
        //insert collection notification_users
        var dtSmp = pastDatetime._created;
        var dtSmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:projectId,
            planid:planId,
            comment_rejectid:getCommentRejectId[0]._id,
            statusaction:3,
            statusstring:"ปรับแก้เอกสาร",
            read:"",
            progress_groupplan_id:"",
            datetimestamp:dtSmp,
            datetimestring:dtSmpString
        }
        const addNotificationUsers = await db.collection("notification_users").insertOne(objNotification);

        client.close();
        return {data:"reject plan order success.",statusProject:"ปรับแก้เอกสาร",status:true};
    }catch(e){
        console.log('\n',e);
        return;
    }
}