var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.rejectProgressPlan = rejectProgressPlan;

async function rejectProgressPlan(progressGroupPlanId,commentReject,documentReject){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = dateAndTime.format(new Date(), 'D/MMMM/YYYY');
        var pastDatetime = nodeDatetime.create(dateNow);
        //update status progressgroup_plan 
        const updateProgressGroupPlan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progressGroupPlanId)},
                                                            {$set: {status:3,
                                                                    statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
                                                                    comment_reject:commentReject,
                                                                    document_reject:documentReject
                                                                    }
                                                        });
    
        //update status progressweeks
        const updateProgressGroupWeeks = await db.collection("progress_weeks").updateOne({progress_groupplan_id:ObjectId(progressGroupPlanId)},
                                                            {$set:{status:3,statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์"}});
        //get projectid from progress_weeks
        const getProjectId = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progressGroupPlanId)}).toArray();
        let projectIdFind = getProjectId[0].projectid;
    
        //update status weeks
        const getProgressWeeks = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progressGroupPlanId)}).toArray();
        for(var countProgress = 0;countProgress < getProgressWeeks.length;countProgress++){
            console.log("update weekid: "+ getProgressWeeks[countProgress].weekid);
            const updateStatusWeeks = await db.collection("weeks").updateOne({"_id":ObjectId(getProgressWeeks[countProgress].weekid)},
                                                            {$set:{status:3,
                                                                   statusweek:3,
                                                                   statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
                                                                }});
        }
        var dtSmp = pastDatetime._created;
        var dtSmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        //insert comment_progressweek
        var objRejectCommentProgressWeek = {
                progress_groupplan_id:progressGroupPlanId,
                coment:commentReject,
                documentation:"",
                datetimestamp:dtSmp,
                datetimestring:dtSmpString
        }
        const insertCommentProgressWeek = await db.collection("comment_progressweek").insertOne(objRejectCommentProgressWeek);
        //get comment_id 
        const getCommentId = await db.collection("comment_progressweek").find({$and:[{progress_groupplan_id:progressGroupPlanId},{datetimestamp:dtsmp}]}).toArray();
        console.log("getCommentId: "+ getCommentId[0]._id);
        //insert collection notification_users
        var objNotification={
                projectid:projectIdFind,
                planid:"",
                comment_rejectid:getCommentId[0]._id,
                statusaction:3,
                statusstring:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
                read:"",
                progress_groupplan_id:progressGroupPlanId,
                datetimestamp:dtSmp,
                datetimestring:dtSmpString
        }
        const addNotificationAdmin = await db.collection("notification_users").insertOne(objNotification);
    
        console.log("data:reject progress week sucess.");
        
        client.close();
        return {data:"reject progress week sucess.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"reject progress week failed.",status:false};
    }
}