var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.approveProgressWeek = approveProgressWeek;

async function approveProgressWeek(progressGroupPlanId,late){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var status = 4;
        var statusString = "ตรวจสอบแล้ว";
        var dateNow = dateAndTime.format(new Date(), 'D/MMMM/YYYY');
        var pastDatetime = nodeDatetime.create(dateNow);
    
        if(late == "ส่งล่าช้า"){
            status = 6;
            statusString = "ตรวจแล้วล่าช้า";
        }else{
             status = 4;
             statusString = "ตรวจสอบแล้ว";
        }
    
        //update status progress_groupplan
        const updateStatusGroupPlan = await db.collection("progress_groupplan").updateOne({"_id":ObjectId(progressGroupPlanId)},
                                                                                          {$set:{status:status,
                                                                                                   statusstring:statusString}});
    
        //upate status progress week
        const updateStstusProgressWeek = await db.collection("progress_weeks").update({progress_groupplan_id:ObjectId(progressGroupPlanId)},
                                                                                          {$set:{status:status,
                                                                                                   statusstring:statusString}},
                                                                                           {upsert:true,
                                                                                               multi:true});
    
        //get weeks 
        const getWeeksId = await db.collection("progress_weeks").find({progress_groupplan_id:ObjectId(progressGroupPlanId)}).toArray();
        for(var countWeek = 0;countWeek < getWeeksId.length;countWeek++){
            //update status week
            const updateStatusWeek = await db.collection("weeks").updateOne({"_id":ObjectId(getWeeksId[countWeek].weekid)},
                                                                               {$set:{status:status,
                                                                                         statusstring:statusString,
                                                                                         statusweek:status}});
        }
    
        //insert collection notification_users
        var dtSmp = pastDatetime._created;
        var dtSmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:getWeeksId[0].projectid,
            planid:"",
            comment_rejectid:"",
            statusaction:4,
            statusstring:"ตรวจสอบแล้ว",
            read:"",
            progress_groupplan_id:progressGroupPlanId,
            datetimestamp:dtSmp,
            datetimestring:dtSmpString
        }
        const addNotificationUsers = await db.collection("notification_users").insertOne(objNotification);
    
        console.log("data:update status approve success.,progressGroupPlanId:"+progressGroupPlanId+",status:true");
        client.close();
        return {data:"update status approve success.",progressGroupPlanId:progressGroupPlanId,status:true};
    }catch(e){
        console.log('\n',e);
        return;
    }
}