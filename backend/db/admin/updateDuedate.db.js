var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateDueDateTime = updateDueDateTime;

async function updateDueDateTime(batchId,dtsDueDate){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        var objUpdateDueDate = {
                data:"update dtsDueDate success.",
                batchId:batchId,
                dtsDueDate:"",
                duedateAmount:0,
                status:true
        }
        const updateDueDate =  db.collection("batch").updateOne(
                                            {"_id":ObjectId(batchId)},
                                            {$set:{duedate:dtsDueDate}});
        console.log("duedate update: "+ dtsDueDate);
        console.log("batchId :" + batchId);
    
        //get detail batch 
        const getDetail = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        var startDateDtsm = pastDatetime._created;
        var dueDateDtsm = getDetail[0].duedate;
    
            //get days from date now count down
                var startDay = moment(startDateDtsm).format("DD");
                var startMonth = moment(startDateDtsm).format("MM");
                var startYear = moment(startDateDtsm).format("YYYY");
                var start = moment([startYear, startMonth, startDay]);
                
                var dueDay = moment(dueDateDtsm).format("DD");
                var dueMonth =  moment(dueDateDtsm).format("MM");
                var dueYear =  moment(dueDateDtsm).format("YYYY");
                var due = moment([dueYear, dueMonth, dueDay]);
                var diffDay = due.diff(start, 'days');
    
        var dayString = moment(dueDateDtsm).format("DD");
        var monthString = moment(dueDateDtsm).format("MM");
        var yearString = moment(dueDateDtsm).format("YYYY");
        var yearTh = parseInt(yearString) + 543;
        var dueDateString = dayString+"/"+monthString+"/"+ yearTh;
        objUpdateDueDate.duedateAmount = diffDay;
        objUpdateDueDate.dtsDueDate = dueDateString;
        console.log(objUpdateDueDate);
        
        client.close();
        return objUpdateDueDate;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}