var initConfig = require('../../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateStatusProjectInvest = updateStatusProjectInvest;

async function updateStatusProjectInvest(projectId,status){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var respond = {stausUpdate:[]};
        console.log("status: "+status);
        console.log("projectId: "+projectId);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        //update status project collection BatchBusinessman
        const updateStatusBatchBsm = await db.collection("batchbusinessman").updateOne(
                                            {projectid:ObjectId(projectId)},
                                            {$set:{status:status,
                                                   updatetime:pastDatetime._created}})
                                            .then(function(result){
                                                respond.stausUpdate.push({data:"update batchbusinessman.",status:true});
                                            });
    
        //update status project collection project 
        const updateStatusProject = await db.collection("projects").updateOne(
                                            {"_id":ObjectId(projectId)},
                                            {$set:{status:status}}).then(function(result){
                                                respond.stausUpdate.push({data:"update projects.",status:true});
                                            });
         client.close();
         return respond; 
    }catch(e){
        console.log('\n',e);
        return;
    }
}