var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.checkLastDraft = checkLastDraft;

async function checkLastDraft(userId,projectId){
    try{
        console.log("-----------Check last draft----------");
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
    
        //check part 1 : upload document 
        const checkDocument = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(checkDocument[0].Documentation == ""){
            console.log("draft: last activity upload documentation.");
            var respond = {data:"last activity upload documentation.",activity:"1",activityName:"เอกสารประกอบโครงการ"};
            
            client.close();
            return respond;
        }else{
            console.log("draft: activity upload documentation success.");
        }
    
        //check part 2 : create project
        const checkCreateProject = await db.collection("projects").find({Primaryresponsibility:userId}).toArray();
        if(checkCreateProject[0].projectname == "" || checkCreateProject[0].projectvalue == ""){
            console.log("draft: last activity create project.");
            var respond = {data:"last activity create project.",activity:"2",activityName:"แผนการใช้งบประมาณ"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create project success.");
        }
    
        //check part 2.1 : add amount duration
        const checkAmountDuration = await db.collection("duration").find({projectid:projectId}).toArray();
        if(checkAmountDuration.length == 0){
            console.log("draft: last activity create duration.");
            var respond = {data:"last activity create duration.",activity:"2.1",activityName:"สร้างงวดงาน"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create duration success.");
        }
    
        //check part 2.2 create amount plan 
        const checkAmountPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(checkAmountPlan[0].amountplan == 0){
            console.log("draft: last activity create amount plan.");
            var respond = {data:"last activity create amount plan.",activity:"2.2",activityName:"สร้างจำนวนแผนงาน"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create amount plan success.");
        }
    
        //check part 2.3 create plan 
        const checkPlanOrder = await db.collection("planorders").find({projectid:projectId}).toArray();
        const getAmountPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var plannedDed = checkPlanOrder.length;
        var amountPlan = getAmountPlan[0].amountplan;
        if(plannedDed < amountPlan){
            console.log("draft: last activity create plan order.");
            var respond = {data:"last activity create plan order.",activity:"2.3",activityName:"สร้างแผนงาน"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create plan order success.");
        }
    
        //check part 3.1 create amount team
        const checkAmountTeam =  await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(checkAmountTeam[0].amountteam == 0){
            console.log("draft: last activity create amount team.");
            var respond = {data:"last activity create amount team.",activity:"3.1",activityName:"เพิ่มจำนวนคนทำงาน"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create amount team success.");
        }
    
        //check part 3.2 create normal users 
        var amountTeam = checkAmountTeam[0].amountteam;
        const checkTeamAdded = await db.collection("users").find({projectid:projectId}).toArray();
        if((checkTeamAdded.length + 1) < amountTeam){
            console.log("draft: last activity create normal users.");
            console.log("users added: " + (checkTeamAdded.length + 1));
            var respond = {data:"last activity create normal users.",activity:"3.2",activityName:"เพิ่มข้อมูลรายละเอียดสมาชิก"};

            client.close();
            return respond;
        }else{
            console.log("draft: activity create normal success.");
            var respond = {data:"activity create normal success.",activity:"4",activityName:"สรุปและส่งข้อมูลโครงการ"};

            client.close();
            return respond;
        }
    }catch(e){
        console.log('\n',e);
        return null;
    }
}