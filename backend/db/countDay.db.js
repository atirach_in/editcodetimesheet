var moment = require('moment');


exports.testCountDate = testCountDate;

async function testCountDate(){
    try{
        // 7 days in week 
        // 7 = sunday at this week 
        // 6 = saturday at this week
        //------------------------
        // -7 = sunday at last weeked.
        // -6 = monday at last weeked.
        // -1 = saturday at last weeked.

        // set default start Timesheet at maonday next week.
        var days = moment().day(7+1);
        console.log("days: "+ days);
        console.log("day: "+ days.format("dddd"));
        console.log("at date: "+ days.format("D"));
        console.log("month: "+days.format("MMMM"));
        console.log("year: "+ days.format("YYYY"));
        console.log("------------------------------");

        //get todays date
        var today = moment()
            //get birth date
        var birthDay = moment(1993+"/"+6+"/"+8, 'YYYY-MM-DD');
        var result = moment.duration(8).months();
        console.log(today.diff(birthDay, 'day'));
        console.log("-----------------------------");

        //add days 
        var date = moment().add(7, 'days');
        var dateLocate = date.locale('th');
        console.log("add days: " + dateLocate.format('DD/MM/YY'));

        client.close();
        return;
    }catch(e){
        console.log('\n',e);
        return;
    }
}