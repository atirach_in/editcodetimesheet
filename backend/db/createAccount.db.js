var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.createAccount = createAccount;

async function createAccount(fristName,lastName,phone,email,token,dueDate,batchId,companyName){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        //var decode = jwt.verify(token,SECRET);
        var password = token;//decode.password;
    //res.json({token:token,decode:decode.password});

        const getDueDate = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        var dueDate = getDueDate[0].duedate;//.format("D/MMMM/YYYY");
        var day = moment().format("D");
        var month = moment().format("MMMM");
        var year = moment().format("YYYY");
        var yearTh = parseInt(year) + 543;
        console.log("dueDate: " + dueDate);
        console.log("thisday: "+ day+"/"+month+"/"+yearTh);
        if(dueDate < pastDatetime._created){
            console.log("data: not create account becuase is dueDate.");
            var respond={data:"not create account becuase is dueDate.",status:false};
            client.close();
            return {
                success:true,
                data:'',
                obj:respond
            };
        }else{
            var userObj={
                    batchid:batchId,
                    fristname:fristName,
                    lastname:lastName,
                    phone:phone,
                    email:email,
                    password:password,
                    position:"",
                    companyname:companyName,
                    profileimage:"",
                    Insdate:pastDatetime._created
                };
            var respond ={data:[]}
            //check email 
            const checkEmail = await db.collection("users").find({email:email}).toArray();
            if(checkEmail.length > 0){
                var resultCheckEmail = {data:"email has been used",status:false}
                console.log(resultCheckEmail);
                client.close();
                return {
                    success:false,
                    data:'',
                    obj:resultCheckEmail
                };
            }else{
                //create account
                const insertAccount = await db.collection("users").insertOne(userObj)
                                            .then(function(result){
                                                respond.data.push({data:"create account sucess.",status:true});
                                            });
                //get batch detail 
                const getBatchDetail = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
                var batchName = getBatchDetail[0].modelname;

                //send email 									
                var transporter = await nodemailer.createTransport(smtpTransport({
                    service: 'gmail',
                    host: 'smtp.gmail.com',
                    auth:{
                        xoauth2:xoauth2.createXOAuth2Generator({
                            user:'tedfund.app@gmail.com',
                            clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
                            clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
                            refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
                        })
                    },
                }));

                const output = `
                    <h3 style="color:#128553">Create Account Success</h3>
                    <h4 style="color:#128553;margin-left:20px">${batchName}</h4>
                    <ul>  
                        <li style="margin-bottom:5px">Name:  ${fristName}  ${lastName}</li>
                        <li style="margin-bottom:5px">Email: ${email}</li>
                        <li style="margin-bottom:5px">Phone: ${phone}</li>
                    </ul>
                    <hr color='128553'>
                `;
                //set mail options
                var mailOptions = {
                    from:'tedfund.app <tedfund.app@gmail.com>',
                    to:email,
                    subject:'Create account succcess.',
                    text:'Register TEDfund.',
                    html:output
                }
                // sent email
                var sentEmail = await transporter.sendMail(mailOptions,function(err,res){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('Email sent.');
                        console.log(mailOptions);
                    }
                });
                client.close();
                return {
                    success:false,
                    data:'',
                    obj:respond
                };
            }		
        }
    }catch(e){
        console.log(e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}