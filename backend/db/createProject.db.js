var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.createProject = createProject;

async function createProject(userId,projectName,projectValue,batchId){
    try{
        console.log("userId: "+userId);
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var projectObj={
            batchid:batchId,
            projectname:projectName,
            projectvalue:projectValue,
            Primaryresponsibility:userId,
            Documentation:"",
            Insdate:pastDatetime._created,
            updateproject:"",
            senddatetime:"",
            amountteam:0,
            status:1,
            active:1
        };
        var projectObjRes = {}
        var batchBsm = {}
        //insert project 
        const insertProject = await db.collection("projects").insertOne(projectObj);
        //find _id project 
        const getProject = await db.collection("projects").find({Primaryresponsibility:userId}).toArray();
        projectObjRes = {
                            data:"new project sucess",
                            userId:userId,
                            projectId:getProject[0]._id,
                            projectName:getProject[0].projectname,
                            status:true
                        }
        batchBsm = {
                        batchid:batchId,
                        projectid:getProject[0]._id,
                        Insdate:pastDatetime._created,
                        updatetime:"",
                        status:"1",
                        active:"1"
                    }
        //insert project in bsm 
        const insertBsm = await db.collection("batchbusinessman").insertOne(batchBsm);
        //update aleady businessman 
        const findBsmInBatch =  await db.collection("projects").find({batchid:batchId}).toArray();
        const updateAlready = await db.collection("projects").updateOne(
                                                    {"_id":ObjectId(batchId)},
                                                    { $set: {alreadybusinessman:findBsmInBatch.length}});
        //safe draft 2 สร้างโครงการ
        const getIdActivity = await db.collection("activity").find({activityorder:"2"}).toArray();
        console.log("get activity _id: "+ getIdActivity[0]._id);
        var objActivity = {
            userid:userId,
            activityid:getIdActivity[0]._id,
            datetime:pastDatetime._created
        }
        console.log("---------insert activity---------");
        console.log(objActivity);
        console.log("---------------------------------");
        const insertActivity = await db.collection("activityusers").insertOne(objActivity); 
        
        client.close();
        return {
            success:true,
            data:'',
            obj:projectObjRes
        };
    }catch(e){
        console.log(e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}