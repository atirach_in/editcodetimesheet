var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var dateAndTime = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.createWeeks = createWeeks;

async function createWeeks(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        dateAndTime.locale('th');
        var dateNow = dateAndTime.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
    
        //check double 
        const checkWeekOfProjectId = await db.collection("weeks").find({projectid:projectId}).toArray();
        if(checkWeekOfProjectId.length > 0){
            client.close();
            return {data:"this project approved.",status:false};
        }
    
        var startDateDuration = "";
        //get sort array  
        var sort = {durationid:1};
        const getSort = await db.collection("planorders").find({projectid:projectId}).sort(sort).toArray();
        for(var z =0; z < getSort.length;z++){
            console.log("sort plan of durationid: "+ getSort[z]._id);
        }
        console.log("--------------------");
        //get planorders from projectId 
        const getPlanOrders = await db.collection("planorders").find({projectid:projectId}).sort(sort).toArray();
        //await db.collection("planorders").find({projectid:projectId}).toArray();
        var countWeeks =0;
        var durationOrder = 0;
        var lastStartDateDuration = 0;
        var startDateInCodition ="";
        for(var plan =0;plan < getPlanOrders.length;plan++){
            console.log("planid: "+ getPlanOrders[plan]._id);
            console.log("time length: " + getPlanOrders[plan].timelength);
            const getDayOfDuration = await db.collection("duration").find({"_id":ObjectId(getPlanOrders[plan].durationid)}).toArray();
            var timeLength = getDayOfDuration[0].month * 30;//getPlanOrders[plan].timelength;
            var calWeeks = timeLength / 6;
            console.log("durationorders: " + getPlanOrders[plan].durationorder);
            console.log("calWeeks float :"+calWeeks);
            console.log("calWeeks: " + Math.floor(calWeeks));
            console.log("calWeeks Math ceil: " + Math.ceil(calWeeks));
            var calWeeksCeil = Math.ceil(calWeeks);//ปัดขึ้นหมด
            var daysNotDecimal = Math.floor(calWeeks) * 6;
            var daysDecimal = timeLength - daysNotDecimal;
            console.log("daysNotDecimal: " + daysNotDecimal);
            console.log("daysDecimal: "+ daysDecimal);
            var startDate = "";
            if(startDateDuration == ""){
                startDate = moment().day(7+1);
                startDateInCodition = startDate;
            }else if( durationOrder == getPlanOrders[plan].durationorder){
                startDate = lastStartDateDuration;
                startDateInCodition = startDate;
                countWeeks =0;
            }
            else if(startDateDuration != ""){
                startDate = startDateDuration;
                startDateInCodition = startDate;
            }
            console.log("start frist date on duration: "+ startDate.format("D dddd/MM/YYYY"));
            console.log("---------------------------");
            var partDate = "";
            for(var i =1; i <= calWeeksCeil;i++){
                countWeeks+=1;
                var objWeek = {
                    durationid:getPlanOrders[plan].durationid,
                    projectid:getPlanOrders[plan].projectid,
                    planid:getPlanOrders[plan]._id,
                    weeknumber:countWeeks,
                    monthnumber:0,
                    daysinweek:6,
                    weekdatetimestamp:0,
                    weekvalue:0,
                    updatetime:pastDatetime._created,
                    statusweek:0
                }
                //insert weeks 
                const insertWeek = await db.collection("weeks").insertOne(objWeek);
                //get new weekid 
                const getWeekId = await db.collection("weeks").find({$and:[
                                                                {durationid:getPlanOrders[plan].durationid},
                                                                {projectid:getPlanOrders[plan].projectid},
                                                                {planid:getPlanOrders[plan]._id},
                                                                {weeknumber:countWeeks}
                                                                ]}).toArray();
                if(timeLength < 6){
                    console.log("weekid: "+getWeekId[0]._id);
                    console.log("week: " + countWeeks + " at days: " + timeLength);
                    var dayNumberArray = 0;
                    for(var j=0;j < timeLength;j++){// add days
                        var date = moment(startDate).add(j, 'days');
                        dayNumberArray = j;
                        var dayNumber =0;
                        console.log(date.format("D dddd/MMMM/YYYY")+ " day number: "+ j);
                        if(dayNumberArray == 0){
                            dayNumber=1;
                        }else if(dayNumberArray == 1){
                            dayNumber=2;
                        }else if(dayNumberArray == 2){
                            dayNumber=3;
                        }else if(dayNumberArray == 3){
                            dayNumber=4;
                        }else if(dayNumberArray == 4){
                            dayNumber=5;
                        }else if(dayNumberArray == 5){
                            dayNumber=6;
                        }
                        //insert day 
                        var objDay = {
                            projectid:projectId,
                            weekid:getWeekId[0]._id,
                            daynumber:dayNumber,
                            datetimestamp:date,
                            datestring:date.format("D/MMMM/YYYY"),
                            daystring:date.format("dddd"),
                            statusopen:0
                        }
                        const insertDay = await db.collection("days").insertOne(objDay);
                    }
                }else{
                    timeLength = timeLength - 6;
                    console.log("weekid: "+getWeekId[0]._id);
                    console.log("week: " + countWeeks + " at days: 6");
                    var dayNumberArray = 0;
                    //create days 
                    for(var days = 0;days < 6;days++){//add days
                        dayNumberArray = days;
                        var dayNumber =0;
                        var date = moment(startDate).add(days, 'days');
                        console.log(date.format("D dddd/MM/YYYY")+" day number: "+ days);
                        if(dayNumberArray == 0){
                            dayNumber=1;
                        }else if(dayNumberArray == 1){
                            dayNumber=2;
                        }else if(dayNumberArray == 2){
                            dayNumber=3;
                        }else if(dayNumberArray == 3){
                            dayNumber=4;
                        }else if(dayNumberArray == 4){
                            dayNumber=5;
                        }else if(dayNumberArray == 5){
                            dayNumber=6;
                        }
                        //insert day 
                        var objDay = {
                            projectid:projectId,
                            weekid:getWeekId[0]._id,
                            daynumber:dayNumber,
                            datetimestamp:date,
                            datestring:date.format("D/MMMM/YYYY"),
                            daystring:date.format("dddd"),
                            statusopen:0
                        }
                        const insertDay = await db.collection("days").insertOne(objDay);
                        if(days == 5){
                            console.log("set new start date:"+ moment(date).day(7+1).format("D dddd/MMMM/YYYY"));
                            startDate = moment(date).day(7+1);
                        }
    
                    }//days
                    if(i == calWeeksCeil){
                            //start date next dutaion
                            lastStartDateDuration = startDateInCodition;
                            startDateDuration = moment(date).day(7+1);
                            durationOrder = getPlanOrders[plan].durationorder;
                            console.log("next date duration: " + startDateDuration.format("D dddd/MMMM/YYYY"));
                            console.log("last startDate: " + lastStartDateDuration.format("D dddd/MMMM/YYYY"));
                            console.log("durationOrder: "+durationOrder);
                        }
                    console.log("---------------------------");
                }
            }
        }//plan
    
        //get start project 
        var weekIdStart = "";
        const getStartWeek = await db.collection("weeks").find({projectid:projectId}).toArray();
        for(var countWeek = 0;countWeek < getStartWeek.length;countWeek++){
            if(getStartWeek[countWeek].weeknumber == 1){
                //get frist week 
                weekIdStart = getStartWeek[countWeek]._id;
            }
        }
    
        //get frist day 
        var fristDayString = "";
        var dayString = "";
        var dateString = "";
        const getFristDay = await db.collection("days").find({weekid:ObjectId(weekIdStart)}).toArray();
        //day string TH
        if(getFristDay[0].daystring == "Monday"){
            dayString = "วันจันทร์";
        }else if(getFristDay[0].daystring == "Tuesday"){
            dayString = "วันอังคาร";
        }else if(getFristDay[0].daystring == "Wednesday"){
            dayString = "วันพุธ";
        }else if(getFristDay[0].daystring == "Thursday"){
            dayString = "วันพฤหัสบดี";
        }else if(getFristDay[0].daystring == "Friday"){
            dayString = "วันศุกร์";
        }else if(getFristDay[0].daystring == "Saturday"){
            dayString = "วันเสาร์";
        }
    
        var dd = moment(getFristDay[0].datetimestamp._d).locale('th').format("DD");
        var mm = moment(getFristDay[0].datetimestamp._d).locale('th').format("MMMM");
        var yy = moment(getFristDay[0].datetimestamp._d).locale('th').format("YYYY");
        var yyTh = parseInt(yy) + 543;
    
        dateString = dd+"/"+mm+"/"+yyTh;
        console.log("dayString "+ dayString);
        console.log("dateString: "+ dateString);
        fristDayString = dayString +"  "+ dateString;
    
        //get batchname 
        const getProjectDetail = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        const getUserPrimary = await db.collection("users").find({"_id":ObjectId(getProjectDetail[0].Primaryresponsibility)}).toArray();
        const getBatchName = await db.collection("batch").find({"_id":ObjectId(getProjectDetail[0].batchid)}).toArray();
        var batchName = getBatchName[0].modelname;
        var projectName = getprojectdetail[0].projectname;
        var username = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
        var email = getUserPrimary[0].email;
        var phone = getUserPrimary[0].phone;
    
        //send email 									
                var transporter = nodemailer.createTransport(smtpTransport({
                    service: 'gmail',
                      host: 'smtp.gmail.com',
                    auth:{
                        xoauth2:xoauth2.createXOAuth2Generator({
                            user:'tedfund.app@gmail.com',
                            clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
                            clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
                            refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
                        })
                    },
                }));
    
                 const output = `
                     <h3 style="color:#128553">Approve project.</h3>
                    <h4 style="color:#128553;margin-left:20px"><b>${batchName}</b></h4>
                    <ul> 
                        <li style="margin-bottom:5px"><b>ชื่อโครงการ:</b>  ${projectName}</li>
                        <li style="margin-bottom:5px"><b>เริ่มลงเวลา:</b>  ${fristDayString}</li>
                        <li style="margin-bottom:5px"><b>ชื่อผู้ประสานงานหลัก:</b>  ${username}</li>
                        <li style="margin-bottom:5px"><b>Email:</b> ${email}</li>
                        <li style="margin-bottom:5px"><b>เบอร์โทรศัพท์:</b> ${phone}</li>
                    </ul>
                    <hr color='128553'>
                  `;
                //set mail options
                var mailOptions = {
                    from:'tedfund.app <tedfund.app@gmail.com>',
                    to:email,
                    subject:'Approve project.',
                    text:'Approve project.',
                    html:output
                }
                // sent email
                transporter.sendMail(mailOptions,function(err,res){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('Email sent.');
                        console.log(mailOptions);
                    }
                });
    
        client.close();
        return {data:"create weeks success.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"create weeks failed.",status:false};
    }
}