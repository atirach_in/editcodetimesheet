var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.dashboard = dashboard;

async function dashboard(userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        const getProject = await db.collection("projects").find({Primaryresponsibility:userId}).toArray();
        if(getProject.length === 0){
            console.log("data: no have projects");
            //var result = {data:" no have project",status:true}
            client.close();
            return {
                success:true,
                data:'no have project',
                obj:null
            };
        }else{
            console.log("data: have projects");
            //var result = {data:"have project",status:true}
            client.close();
            return {
                success:true,
                data:'have project',
                obj:getProject
            };
        }
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}