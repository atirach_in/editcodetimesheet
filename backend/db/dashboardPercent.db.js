var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.dashboardPercent = dashboardPercent;

async function dashboardPercent(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objProject = {
            projectName:"",
            projectValue:0,
            statusProject:"",
            fillIn:"",
            partDocument:0,
            partPlan:0,
            partTeam:0,
            partSending:0,
            percent1To3:0,
            percentCircle:0
        }
        //documentation 
        const getDocumetation = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        const getAllDocumetation = await db.collection("fileproject").find({projectid:projectId}).toArray();
        if (getAllDocumetation.length == 0){
            objProject.partDocument = 0;
        }else{
            objProject.partDocument = 100;
        }
        console.log("percent part 1: "+objProject.partDocument);
        var percent1 = objProject.partDocument;
        objProject.partDocument = percent1;
        console.log("---------------------------------------");
    
        //plan order
        console.log("amount plan: "+ getDocumetation[0].amountplan);
        var amountPlan = getDocumetation[0].amountplan;
        const getAllPlanOrder = await db.collection("planorders").find({projectid:projectId}).toArray();
        var plans = getAllPlanOrder.length;
        console.log("plans: "+plans);
        var percentPlan = 100 / amountPlan;
        var calculatePercentPlan = percentPlan * plans;
        console.log("percent part 2: "+calculatePercentPlan);
        var percent2 = calculatePercentPlan;
        objProject.partPlan = percent2;
        console.log("---------------------------------------");
    
        //team 
        const getAmountTeam = await  db.collection("users").find({projectid:projectId}).toArray();
        var amountUsers = getAmountTeam.length;//team_added
        var userPrimary = 1;
        console.log("amount team: "+ (amountUsers+userPrimary));
        var allUsers = (amountUsers+userPrimary);
        const getAmountSetTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var setAmount = getAmountSetTeam[0].amountteam;
        var percentTeam = 100 / setAmount;
        console.log("percent amount users: "+ percentTeam);
        var percent3 = (percentTeam * allUsers);
        objProject.partTeam = percent3;
        console.log("percent part 3:"+ percentTeam * allUsers);
        console.log("---------------------------------------");
    
        //percent1To3
        objProject.percent1To3 = ((percent1 + percent2 + percent3) * 50) / 300;
        
        //part sending
        const getStatusProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var status = getStatusProject[0].status;
        if(status == 1){
            console.log("part 4 sending: 0%");
            console.log("---------------------------------------");
            objProject.partSending = objProject.percent1To3;
        }else{
            console.log("part 4 sending: 100%");
            console.log("---------------------------------------");
            objProject.partSending = 100;
        }
        //percent all 
        var percentAll = (((percent1 + percent2 + percent3) + objProject.partSending) * 100)/400 ;
        //var totalpercent = (percentAll*100) / 300;
        objProject.percentCircle = percentAll;
        console.log("percentCircle: "+ percentAll);
        console.log("---------------------------------------");
        console.log(objProject);
    
        //get detail project 
        // projectName:"",
        // projectValue:0,
        // statusProject:"",
        // fillIn:"",
        objProject.projectName = getStatusProject[0].projectname;
        objProject.projectValue = getStatusProject[0].projectvalue;
        if(getStatusProject[0].status == 1){
            objProject.statusProject = "กำลังดำเนินการ";
            objProject.fillIn = "กำลังกรอกข้อมูล";
        }else if(getStatusProject[0].status == 2){
            objProject.statusProject = "รอดำเนินการ";
            objProject.fillIn = "ส่งเอกสารแล้ว";
        }else if(getStatusProject[0].status == 3){
            objProject.statusProject = "ปรับแก้เอกสาร";
            objProject.fillIn = "ปรับแก้เอกสาร";
        }else if(getStatusProject[0].status == 4){
            objProject.statusProject = "อนุมัติ Timesheet";
            objProject.fillIn = "อนุมัติ Timesheet";
        }
        
        client.close();
        return objProject;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}