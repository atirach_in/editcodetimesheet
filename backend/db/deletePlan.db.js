var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.deletePlan = deletePlan;

async function deletePlan(planId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objPlan = {"_id":ObjectId(planId)};
        const deletePlan = await db.collection("planorders").deleteOne(objPlan);
        console.log("data:delete plan sucess.");

        client.close();
        return {data:"delete plan sucess.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"delete plan failed.",status:false};
    }
}