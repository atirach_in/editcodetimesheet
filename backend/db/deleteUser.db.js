var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.deleteUser = deleteUser;

async function deleteUser(delUserId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        //check delUserId is primary user.
        const checkUserId = await db.collection("projects").find({Primaryresponsibility:delUserId}).toArray();
        console.log("length: " + checkUserId.length);
        if(checkUserId.length == 0){
            var objUser = {"_id":ObjectId(delUserId)};
            const deleteUser = await db.collection("users").deleteOne(objUser);
            console.log("data: delete user success.");

            client.close();
            return {data:"delete user success.",status:true};
        }else{
            console.log("projectid: "+checkUserId[0]._id);
            console.log("data: this delUserId it's primary user.");

            client.close();
            return {data:"this delUserId it's primary user.",status:false};
        }
    }catch(e){
        console.log('\n',e);
        return {data:"this delUserId it's primary user.",status:false};
    }
}