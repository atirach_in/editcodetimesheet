var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.fillInUserPrimary = fillInUserPrimary;

async function fillInUserPrimary(userId,batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objProject = {
            firstName:"",
            lastName:"",
            phone:"",
            batchId:"",
            batchName:""
        }
    
        //get user detail 
        const getUser = await db.collection("users").find({"_id":ObjectId(userId)}).toArray();
        objProject.firstName = getUser[0].fristname;
        objProject.lastName = getUser[0].lastname;
        objProject.phone = getUser[0].phone;
        objProject.batchId = batchId;
    
        //get batch name 
        const getBatchName = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        objProject.batchName = getBatchName[0].modelname;
        console.log(objProject);
        
        client.close();
        return {
            success:true,
            data:'',
            obj:objProject
        };  
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}