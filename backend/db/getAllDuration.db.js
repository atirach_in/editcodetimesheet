var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllDuration = getAllDuration;

async function getAllDuration(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        //get duration 
        const getAllDuration = await db.collection("duration").find({projectid:projectId}).toArray();
        console.log("-------getAllDuration------");
        console.log(getAllDuration);
        console.log("-------------------");
        
        client.close();
        return getAllDuration;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}