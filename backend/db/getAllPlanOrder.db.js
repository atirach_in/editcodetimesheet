var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getPallPlanOrder = getPallPlanOrder;

async function getPallPlanOrder(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var newDateString = moment().format("dddd");
        console.log("newDateString: "+ newDateString);
        var newDateNow = moment().format('D/MMMM/YYYY');
        console.log("newDateNow: "+ newDateNow);
        var dataPlan={
                    project:{
                        lateWeek:"",
                        projectName:"",
                        projectValue:0,
                        valueUsed:0,
                        amountPlan:0,
                        planAdded:0,
                        plan:[],
                        amountSetTeam:0,
                        teamAdded:0,
                        activity:"",
                        duration:0,
                        durationNumber:0,
                        weekNumber:0,
                        dayNumber:0,
                        statusProject:0,
                        statusProjectString:"",
                        
                }
            }

        if(newDateString == "Sunday"){
            var day6 = moment().add(-1,'days').format("D/MMMM/YYYY");
            console.log("day6: "+day6);
            //get week id 
            const getWeekId = await db.collection("days").find({$and:[{datestring:day6},{projectid:projectId}]}).toArray();
            //get week number 
            //const getWeekNumber = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{projectid:projectId}]}).toArray();
            const getWeekNumber = await db.collection("weeks").find({"_id":ObjectId(getWeekId[0].weekid)}).toArray();
            dataPlan.project.weekNumber = weekNumber;
            dataPlan.project.dayNumber = 7;
            var statusLate = getWeekNumber[0].status_late;
            if(statusLate == true){
                dataPlan.project.lateWeek = "true";
                console.log("************************************************");
                console.log("getWeekNumber[0].status_late:" + getWeekNumber[0].status_late);
                console.log("dataPlan.project.lateWeek=true");
                console.log("************************************************");
            }else{
                dataPlan.project.lateWeek = "false";
                console.log("************************************************");
                console.log("dataPlan.project.lateWeek=false");
                console.log("************************************************");
            }
            

            //get duration number 
            const getDurationNumber = await db.collection("duration").find({"_id":ObjectId(getWeekNumber[0].durationid)}).toArray();
            dataPlan.project.durationNumber = getDurationNumber[0].durationorder;
        }else{
            //get dayNumber 
            const getDayNumber = await db.collection("days").find({$and:[{datestring:newDateNow},{projectid:projectId}]}).toArray();
            if(getDayNumber.length == 0){
                console.log("data: no have day on timesheet this week.");
            }else{
                dataPlan.project.dayNumber = getDayNumber[0].daynumber;
                var weekId = getDayNumber[0].weekid;

                ///get weekNumber and dayNumber 
                //const getWeekNumber = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{projectid:projectId}]}).toArray();
                const getWeekNumber = await db.collection("weeks").find({"_id":ObjectId(weekId)}).toArray();
                console.log("getWeekNumber:"+ getWeekNumber);
                dataPlan.project.weekNumber = getWeekNumber[0].weeknumber;
                var statusLate = getWeekNumber[0].status_late;
                if(statusLate == true){
                    dataPlan.project.lateWeek = "true";
                    console.log("************************************************");
                    console.log("getWeekNumber[0].status_late:" + statusLate);
                    console.log("dataPlan.project.lateWeek=true");
                    console.log("************************************************");
                }else{
                    dataPlan.project.lateWeek = "false";
                    console.log("************************************************");
                    console.log("getWeekNumber[0].status_late:" + statusLate);
                    console.log("dataPlan.project.lateWeek=false");
                    console.log(getWeekNumber);
                    console.log("************************************************");
                }
                
                //get duration number 
                const getDurationNumber = await db.collection("duration").find({"_id":ObjectId(getWeekNumber[0].durationid)}).toArray();
                dataPlan.project.durationNumber = getDurationNumber[0].durationorder;
            }
        }	
            

        //check duration on project 
        const checkDuration = await db.collection("duration").find({projectid:projectId}).toArray();
        dataPlan.project.duration = checkDuration.length;
        console.log("check duration: "+checkDuration.length);
        //find total jobvalue = sum(jobvalud in plan)
        var totalProjectValue = 0;

        // //find detail project 
        var getAllPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        dataPlan.project.amountPlan = getAllPlan[0].amountplan;
        dataPlan.project.projectName = getAllPlan[0].projectname;
        dataPlan.project.statusProject = getAllPlan[0].status;
        if(getallgetAllPlanplan[0].status == 4){
            dataPlan.project.statusProjectString = "อนุมัติ Timesheet";
        }else{
            dataPlan.project.statusProjectString = "รอการอนุมัติ";
        }

        //sum total project value used 
        var sumTotalValueUsed = 0;
        const getProjectValueUsed = await db.collection("timesheet").find({projectid:projectId}).toArray();
        for(var countProject =0;countProject < getProjectValueUsed.length;countProject++){
            sumTotalValueUsed += getProjectValueUsed[countProject].totalvalue;
        }
        dataPlan.project.valueUsed = sumTotalValueUsed;

        //find planorder
        var getAllPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("get all plan: "+getAllPlan.length);
        var totalPlanAdded = 0;
        for(var i = 0; i < getAllPlan.length;i++){
            totalProjectValue += getAllPlan[i].jobvalue;
            console.log(getAllPlan[i].durationid);
            
            //sum job value used 
            var totalValueUsed = 0;
            var planIdFind = getAllPlan[i]._id.toString();
            const getSumValueUsed = await db.collection("timesheet").find({planid:planIdFind}).toArray();
            for(var count = 0;count < getSumValueUsed.length;count++){
                totalValueUsed += getSumValueUsed[count].totalvalue;
            }

            //status plan 
            var statusPlanString = "";
            if(getAllPlan[i].statusplan == ""){
                statusPlanString = "";
            }else if(getAllPlan[i].statusplan == true){
                statusPlanString = true;
            }else if(getAllPlan[i].statusplan == false){
                statusPlanString = false;
            }

            //find duration 
            var getDuration = await db.collection("duration").find({"_id":ObjectId(getAllPlan[i].durationid)}).toArray();
            var durationNumber = getDuration[0].durationorder;
            var dataOrder = {
                planId:getAllPlan[i]._id,
                planName:getAllPlan[i].planname,
                duration:durationNumber,
                jobValue:getAllPlan[i].jobvalue,
                moneyDifference:getAllPlan[i].moneydifference,
                timeLength:getAllPlan[i].timelength,
                valueUsed:totalValueUsed,
                statusPlan:statusPlanString
            }
            dataPlan.project.plan.push(dataOrder);
            totalPlanAdded += 1;
        }
        dataPlan.project.planAdded = totalPlanAdded;
        // dataPlan.project.planAdded = getAllPlan.length;
        dataPlan.project.projectValue = totalProjectValue;


        //get amount set team 
        const getAmountSetTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(getAmountSetTeam.length == 0){
            dataPlan.project.amountSetTeam = 0;
        }else{
            dataPlan.project.amountSetTeam = getAmountSetTeam[0].amountteam;
        }

        //get team added
        const getTeamAdded = await db.collection("users").find({projectid:projectId}).toArray();
        if(getTeamAdded.length == 0){
            dataPlan.project.teamAdded = 0;
        }else{	
            dataPlan.project.teamAdded = (getTeamAdded.length +1);
        }

        //set next activity 
        if(dataPlan.project.amountSetTeam == 0){
            dataPlan.project.activity = "3.1";
        }else{
            dataPlan.project.activity = "3.2";
        }
        
        console.log(dataPlan.project);

        client.close();
        return dataPlan.project;
    }catch(e){
        console.log('\n',e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}