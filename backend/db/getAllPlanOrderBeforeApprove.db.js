var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllPlanOrderBeforeApprove = getAllPlanOrderBeforeApprove;

async function getAllPlanOrderBeforeApprove(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var newDateString = moment().format("dddd");
        console.log("newDateString: "+ newDateString);
        var newDateNow = moment().format('D/MMMM/YYYY');
        console.log("newDateNow: "+ newDateNow);
        var dataPlan={
                project:{
                    projectName:"",
                    projectValue:0,
                    valueUsed:0,
                    amountPlan:0,
                    planAdded:0,
                    plan:[],
                    amountSetTeam:0,
                    teamAdded:0,
                    activity:"",
                    duration:0,
                    weekNumber:0,
                    dayNumber:0,
                    statusProject:0,
                    statusProjectString:""
                }
            }
    
        //check duration on project 
        const checkDuration = await db.collection("duration").find({projectid:projectId}).toArray();
        dataPlan.project.duration = checkDuration.length;
        console.log("check duration: "+checkDuration.length);
        //find total jobvalue = sum(jobvalud in plan)
        var totalProjectValue = 0;
    
        // //find detail project 
        var getAllPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(getAllPlan[0].amountPlan == ""){
            dataPlan.project.amountPlan = 0;
        }else if(getAllPlan[0].amountPlan > 0){
            dataPlan.project.amountPlan = getAllPlan[0].amountplan;
        }else{
            dataPlan.project.amountPlan = 0;
        }
        
        dataPlan.project.projectName = getAllPlan[0].projectname;
        dataPlan.project.statusProject = getAllPlan[0].status;
        if(getAllPlan[0].status == 4){
            dataPlan.project.statusProjectString = "อนุมัติ Timesheet";
        }else{
            dataPlan.project.statusProjectString = "รอการอนุมัติ";
        }
    
        //sum total project value used 
        var sumTotalValueUsed = 0;
        const getProjectValueUsed = await db.collection("timesheet").find({projectid:projectId}).toArray();
        for(var countproject =0;countproject < getProjectValueUsed.length;countproject++){
            sumTotalValueUsed += getProjectValueUsed[countproject].totalvalue;
        }
        dataPlan.project.valueUsed = sumTotalValueUsed;
    
        //find planorder
        var getAllPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("get all plan: "+getAllPlan.length);
        var totalPlanAdded = 0;
        for(var i = 0; i < getAllPlan.length;i++){
            totalProjectValue += getAllPlan[i].jobvalue;
            console.log(getAllPlan[i].durationid);
            //find duration 
            var getDuration = await db.collection("duration").find({"_id":ObjectId(getAllPlan[i].durationid)}).toArray();
            var durationNumber = getDuration[0].durationorder;
            var dataOrder = {
                planId:getAllPlan[i]._id,
                planName:getAllPlan[i].planname,
                duration:durationNumber,
                jobValue:getAllPlan[i].jobvalue,
                timeLength:getAllPlan[i].timelength
            }
            dataPlan.project.plan.push(dataOrder);
            totalPlanAdded += 1;
        }
        dataPlan.project.planAdded = totalPlanAdded;
        // dataPlan.project.planAdded = getAllPlan.length;
        dataPlan.project.projectValue = totalProjectValue;
    
    
        //get amount set team 
        const getAmountSetTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(getAmountSetTeam.length == 0){
            dataPlan.project.amountSetTeam = 0;
        }else{
            dataPlan.project.amountSetTeam = getAmountSetTeam[0].amountteam;
        }
    
        //get team added
        const getTeamAdded = await db.collection("users").find({projectid:projectId}).toArray();
        if(getTeamAdded.length == 0){
            dataPlan.project.teamAdded = 0;
        }else{	
            dataPlan.project.teamAdded = (getTeamAdded.length +1);
        }
    
        //set next activity 
        if(dataPlan.project.amountSetTeam == 0){
            dataPlan.project.activity = "3.1";
        }else{
            dataPlan.project.activity = "3.2";
        }
        console.log(dataPlan.project);

        client.close();
        return dataPlan.project;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}