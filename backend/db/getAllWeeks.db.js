var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllWeeks = getAllWeeks;

async function getAllWeeks(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getWeeks = await db.collection("weeks").find({projectid:projectId}).toArray();
        var objWeeks = {
            weeks:[]
        }
        for(var week=0; week < getWeeks.length; week++){
            var statusWeek = "";
            var statusEng = "";
            if(getWeeks[week].statusweek == 0){
                statusWeek="ยังไม่ดำเนินการ";
                statusEng = "not used.";
            }else if(getWeeks[week].statusweek == 1){
                statusWeek="บันทึกความก้าวหน้า";
                statusEng = "Record progress.";
            }else if(getWeeks[week].statusweek == 2){
                statusWeek="ปรับแก้";
                statusEng = "Reject.";
            }else if(getWeeks[week].statusweek == 3){
                statusWeek="ตรวจสอบแล้ว";
                statusEng = "Checked success.";
            }
            var objWeek = {
                week:getWeeks[week].weeknumber,
                status:statusEng
            }
            objWeeks.weeks.push(objWeek);
            console.log("weeknumber: " + getWeeks[week].weeknumber +" "+statusEng);
        }

        client.close();
        return objWeeks;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}