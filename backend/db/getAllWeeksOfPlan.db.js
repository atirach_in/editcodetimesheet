var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAllWeeksOfPlan = getAllWeeksOfPlan;

async function getAllWeeksOfPlan(projectId,planId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = moment().format("D/MMMM/YYYY");
        var dateStringNow =  moment().format("dddd");
        console.log("this day: "+dateNow);
        console.log("projectId: "+projectId);
        console.log("planId: " + planId);
        var objWeekly = {
            projectId:projectId,
            projectName:"",
            projectValue:0,
            totalValue:0,
            weekId:"",
            weekNumber:0,
            dayId:"",
            dayNumber:0,
            weeks:[],
            status:true
        }
    
        //get project value 
        const getProjectValue = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var projectValue = getProjectValue[0].projectvalue;
        objWeekly.projectValue = projectValue;
        objWeekly.projectName = getProjectValue[0].projectname;
    
        //Sum total value timsheet from timesheet => totalvalueonday
        var totalValue = 0;
        const getTotalValueTimesheet = await db.collection("timesheet").find({projectid:projectId}).toArray();
        for(var project = 0; project < getTotalValueTimesheet.length;project++){
            totalValue += getTotalValueTimesheet[project].totalvalueonday;
        }
        objWeekly.totalValue = totalValue;
    
        // codition sunday 
        if(dateStringNow == "Sunday"){
            var newDateNow = moment().add(-1,'days').format("D/MMMM/YYYY");
            //find days on weeks 
            const getDayOnWeek = await db.collection("days").find({datestring:newDateNow},{projectid:projectId}).toArray();
            console.log("dateNow: " + dateNow);
            console.log("get day on week: " +getDayOnWeek[0].weekid);
            if(getDayOnWeek.length == 0){
                console.log("data: no have this day on week.");
                client.close();
                return {data:"no have this day on week.",status:false};	
            }else{
                //find week of plan 
                const getWeekOfPlan = await db.collection("weeks").find({planid:ObjectId(planId)}).toArray();
                if(getWeekOfPlan.length == 0){
                    client.close();
                    return objWeekly;
                }else{
                    for(var week = 0;week < getWeekOfPlan.length; week++){
                        console.log(getWeekOfPlan[week]);
                        objWeekly.weeks.push(getWeekOfPlan[week]);
                    }
    
                    console.log("weekId: "+getDayOnWeek[0].weekid);	
                    objWeekly.weekId = getDayOnWeek[0].weekid;
                    //find detail week.
                    const getWeekDetail = await db.collection("weeks").find({"_id":ObjectId(getDayOnWeek[0].weekid)}).toArray();
                    //$and:[{"_id":ObjectId(getDayOnWeek[0].weekid)},{projectid:projectId}]
                    objWeekly.weekNumber = getWeekDetail[0].weeknumber;
                    objWeekly.dayId = getDayOnWeek[0]._id;
                    objWeekly.dayNumber = 7;//getDayOnWeek[0].daynumber;
                    console.log("week number: "+ objWeekly.weekNumber);
                    //console.log("day id: "+objWeekly.dayId);
                    console.log("day number" + objWeekly.dayNumber);

                    client.close();
                    return objWeekly;
                }
                
            }
        }else{
            //find days on weeks 
            const getDayOnWeek = await db.collection("days").find({$and:[{datestring:dateNow},{projectid:projectId}]}).toArray();
            console.log("dateNow: " + dateNow);
            if(getDayOnWeek.length == 0){
                console.log("data: no have this day on week.");
                client.close();
                return {data:"no have this day on week.",status:false};	
            }else{
                //find week of plan 
                console.log("get day on week: " +getDayOnWeek[0].weekid);
                const getWeekOfPlan = await db.collection("weeks").find({planid:ObjectId(planId)}).toArray();
                for(var week = 0;week < getWeekOfPlan.length; week++){
                    console.log(getWeekOfPlan[week]);
                    objWeekly.weeks.push(getWeekOfPlan[week]);
                }
    
                console.log("weekId: "+getDayOnWeek[0].weekid);	
                objWeekly.weekId = getDayOnWeek[0].weekid;
                //find detail week.
                const getWeekDetail = await db.collection("weeks").find({"_id":ObjectId(getDayOnWeek[0].weekid)}).toArray();
                //$and:[{"_id":ObjectId(getDayOnWeek[0].weekid)},{projectid:projectId}]
                objWeekly.weekNumber = getWeekDetail[0].weeknumber;
                objWeekly.dayId = getDayOnWeek[0]._id;
                objWeekly.dayNumber = getDayOnWeek[0].daynumber;
                console.log("week number: "+ objWeekly.weekNumber);
                console.log("day id: "+objWeekly.dayId);
                console.log("day number" + objWeekly.dayNumber);
                
                client.close();
                return objWeekly;
            }
        }
    }catch(e){
        console.log('\n',e);
        return;
    }
}