var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAmountDurationOrder = getAmountDurationOrder;

async function getAmountDurationOrder(userId,projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objDurationOrder = {
            projectId:projectId,
            userId:userId,
            duration:[],
            planAdded:0,
            amountPlan:0
        }
        //get duration 
        const getDuration = await db.collection("duration").find({projectid:projectId}).toArray();
        console.log("projectId: "+ projectId);
        console.log("userId: "+ userId);
        console.log("---------------------");
        console.log("duratoin");
        console.log(getDuration);
        for(var i =0; i < getDuration.length; i++){
            var durationId = getDuration[i]._id;
            var durationNumber = getDuration[i].durationorder;
            var month = getDuration[i].month;
            objDurationOrder.duration.push({durationId:durationId,durationNumber:durationNumber,month:month});
        }
    
        console.log("---------------------");
        //get amount plan  : amountPlan
        const getAmountPlan = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        objDurationOrder.amountPlan = getAmountPlan[0].amountplan
        console.log("get amount plan in project: "+ getAmountPlan[0].amountplan)
        //get plan 
        const getPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        console.log("-------------getPlan---------------");
        console.log("plan: "+ getPlan.length);
        console.log("-----------------------------------");
        var planAdded = getPlan.length;
        objDurationOrder.planAdded = planAdded;

        client.close();
        return objDurationOrder;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}