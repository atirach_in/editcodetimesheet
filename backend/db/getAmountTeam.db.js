var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getAmountTeam = getAmountTeam;

async function getAmountTeam(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objAmountTeam = {
            amountSet:0,
            amountAdded:0
        }
        //get amount 
        const getAmountTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        objAmountTeam.amountSet = getAmountTeam[0].amountteam;
        //get amount team added
        const getTeamAdded = await db.collection("users").find({projectid:projectId}).toArray();
        objAmountTeam.amountAdded = (getTeamAdded.length +1);
        console.log("-----------getAmountTeam-------------");
        console.log(objAmountTeam);

        client.close();
        return objAmountTeam;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}