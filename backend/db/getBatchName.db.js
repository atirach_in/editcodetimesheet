var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getBatchName = getBatchName;

async function getBatchName(batchId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = moment();
        console.log("batchId: " + batchId);
        const getBatchName = await db.collection("batch").find({"_id":ObjectId(batchId)}).toArray();
        console.log(getBatchName[0]);
        var dueDate = getBatchName[0].duedate;//moment().format('DD/MMMM/YYYY');
        console.log("dueDate: "+ dueDate);
        console.log("dateNow: "+ dateNow);
        var statusDueDate = false;
        if(dueDate >= dateNow){
            statusDueDate = true;
        }else{
            statusDueDate = false;
        }
        var objBatch = {
            batchName:getBatchName[0].modelname,
            dueDate:statusDueDate
        };

        client.close();
        return objBatch;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}