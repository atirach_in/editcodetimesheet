var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getCommentRejectData = getCommentRejectData;

async function getCommentRejectData(commentRejectId){
    try{
    const client = await MongoClient.connect(url,authSources);
	const db = client.db(DATABASENAME);
	var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
	var pastDatetime = nodeDatetime.create(dateNow);
	var objComment = {
		projectName:"",
		planOrder:0,
		planName:"",
		durationOrder:0,
		jobValue:0,
		timeLength:0,
		statusString:"ปรับแก้เอกสาร",
		lastTime:0,
		comment:"",
		pathDocument:""
	}
	//get comment reject data 
	const getComment = await db.collection("commentreject").find({"_id":ObjectId(commentRejectId)}).toArray();
	objComment.comment = getComment[0].comment;
	objComment.pathDocument = getComment[0].pahtfile;
	//get plan name 
	const getPlanName = await db.collection("planorders").find({"_id":ObjectId(getComment[0].planid)}).toArray();
	objComment.planName = getPlanName[0].planname;
	objComment.planOrder = getPlanName[0].planorder;
	objComment.durationOrder = getPlanName[0].durationorder;
	objComment.jobValue = getPlanName[0].jobvalue
	objComment.timeLength = getPlanName[0].timelength

	//get detail project 
	const getDetailProject = await db.collection("projects").find({"_id":ObjectId(getComment[0].projectid)}).toArray();
	objComment.projectName = getDetailProject[0].projectname;
	

		var lastProjectTime = moment(Math.ceil(getComment[0].insertdate));
		var thistime = moment(pastDatetime._created);
		console.log("lastProjectTime: "+lastProjectTime);
		console.log("this time: "+pastDatetime._created);

		//date project
		var dateProjectMoment = moment(lastProjectTime).format('DD/MM/YYYY');
		var dayPro = moment(lastProjectTime).format('DD');
		var monthPro = moment(lastProjectTime).format('MM');
		var yearPro = moment(lastProjectTime).format('YYYY');
		var hrPro = moment(lastProjectTime).format('HH');
		var minutePro = moment(lastProjectTime).format('mm');
		var ssPro = moment(lastProjectTime).format('ss');
		var lastUpdateProject = moment([yearPro, monthPro, dayPro,hrPro,minutePro]);
		console.log("lastUpdateProject: "+lastUpdateProject);

		//date now 
		var dateNowMoment = moment(thistime).format('DD/MM/YYYY');
		var dayNow = moment(thistime).format('DD');
		var monthNow = moment(thistime).format('MM');
		var yearNow = moment(thistime).format('YYYY');
		var hrNow = moment(thistime).format('HH');
		var minuteNow = moment(thistime).format('mm');
		var ssNow = moment(thistime).format('ss');
		var dateNowAction = moment([yearNow, monthNow, dayNow,hrNow,minuteNow]);
		console.log("dateNowAction: "+dateNowAction);

		var datePro = new Date(yearPro, monthPro, dayPro);
		var dateNow = new Date(yearNow, monthNow, dayNow);

		var diffMinute = dateNowAction.diff(lastUpdateProject, 'minute');
		var diffHr = dateNowAction.diff(lastUpdateProject, 'hour');
		var diffDay = dateNowAction.diff(lastUpdateProject, 'days');

		console.log("diffMinute: " + diffMinute);
		console.log("diffHr: " + diffHr);
		console.log("diffDay: " + diffDay);
		if(diffDay > 0){
			objComment.lastTime = diffDay +" "+"วัน";
		}else if(diffHr > 0){
			objComment.lastTime = diffHr +" "+"ชั่วโมง";
		}else if(diffMinute > 0){
			objComment.lastTime = diffMinute +" "+"นาที";
		}
		client.close();
		return {
            success:false,
            data:'',
            obj:objComment
        };
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}