var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getCommentRejectProgressWeek = getCommentRejectProgressWeek;

async function getCommentRejectProgressWeek(commentRejectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var objComment = {
            statusString:"ปรับแก้ความก้าวหน้ารายสัปดาห์",
            lastTime:0,
            comment:"",
            pathDocument:"",
            planName:""
        }
        //get all comment where progress_groupplan_id
        const getAllComment = await db.collection("comment_progressweek").find({"_id":ObjectId(commentRejectId)}).toArray();
        console.log("getAllComment.length: " + getAllComment.length);
        let lengthComment = (getAllComment.length - 1);
        if(lengthComment < 0){
            client.close();
            return {data:"not have comment reject progress week.",status:false};
        }else{
            objComment.comment = getAllComment[lengthComment].coment;
            objComment.pathDocument = getAllComment[lengthComment].documentation;

            var lastProjectTime = moment(Math.ceil(getAllComment[lengthComment].datetimestamp));
            var thisTime = moment(pastDatetime._created);
            console.log("lastProjectTime: "+lastProjectTime);
            console.log("this time: "+pastDatetime._created);

            //date project
            var dateProjectMoment = moment(lastProjectTime).format('DD/MM/YYYY');
            var dayPro = moment(lastProjectTime).format('DD');
            var monthPro = moment(lastProjectTime).format('MM');
            var yearPro = moment(lastProjectTime).format('YYYY');
            var hrPro = moment(lastProjectTime).format('HH');
            var minutePro = moment(lastProjectTime).format('mm');
            var ssPro = moment(lastProjectTime).format('ss');
            var lastUpdateProject = moment([yearPro, monthPro, dayPro,hrPro,minutePro]);
            console.log("lastUpdateProject: "+lastUpdateProject);

            //date now 
            var dateNowMoment = moment(thisTime).format('DD/MM/YYYY');
            var dayNow = moment(thisTime).format('DD');
            var monthNow = moment(thisTime).format('MM');
            var yearNow = moment(thisTime).format('YYYY');
            var hrNow = moment(thisTime).format('HH');
            var minuteNow = moment(thisTime).format('mm');
            var ssNow = moment(thisTime).format('ss');
            var dateNowAction = moment([yearNow, monthNow, dayNow,hrNow,minuteNow]);
            console.log("dateNowAction: "+dateNowAction);

            var datePro = new Date(yearPro, monthPro, dayPro);
            var dateNow = new Date(yearNow, monthNow, dayNow);

            var diffMinute = dateNowAction.diff(lastUpdateProject, 'minute');
            var diffHr = dateNowAction.diff(lastUpdateProject, 'hour');
            var diffDay = dateNowAction.diff(lastUpdateProject, 'days');

            console.log("diffMinute: " + diffMinute);
            console.log("diffHr: " + diffHr);
            console.log("diffDay: " + diffDay);
            if(diffDay > 0){
                objComment.lastTime = diffDay +" "+"วัน";
            }else if(diffHr > 0){
                objComment.lastTime = diffHr +" "+"ชั่วโมง";
            }else if(diffMinute > 0){
                objComment.lastTime = diffMinute +" "+"นาที";
            }
            client.close();
            return {
                success:true,
                data:'',
                obj:objComment
            };
        }
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}