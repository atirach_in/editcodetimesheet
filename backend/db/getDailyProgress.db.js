var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.dailyProgress = dailyProgress;

async function dailyProgress(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = moment().format("D/MMMM/YYYY");
        console.log("this day: "+dateNow);
        var objProject={
            projectId:projectId,
            projectName:"",
            projectValue:0,
            valueUsed:0,
            weekId:"",
            weekNumber:0,
            dayId:"",
            dayNumber:0,
            plans:[]
        }
    
        //Sum total use value
        var sumTotal =0;
        const sumUsedValue = await db.collection("timesheet").find({projectid:projectId}).toArray();
        for(var i =0; i < sumUsedValue.length; i++){
            sumTotal += sumUsedValue[i].totalvalueonday;
        }
        objProject.valueUsed = sumTotal;
        
        //getdetail project
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        objProject.projectName = getDetailProject[0].projectname;
        objProject.projectValue = getDetailProject[0].projectvalue;
    
        //get weekId , dayId
        const getDayDetail = await db.collection("days").find({datestring:dateNow}).toArray();
        objProject.dayId = getDayDetail[0]._id;
        objProject.dayNumber = getDayDetail[0].daynumber;
        const getWeekDetail = await db.collection("weeks").find({$and:[{projectid:projectId},{"_id":ObjectId(getDayDetail[0].weekid)}]}).toArray();
        objProject.weekId = getWeekDetail[0]._id;
        objProject.weekNumber = getWeekDetail[0].weeknumber;
    
        //get all plans in project
        const getAllPlans = await db.collection("planorders").find({projectid:projectId}).toArray();
        for(var plan = 0;plan < getAllPlans.length; plan++){
            var objPlan ={
                planId:"",
                planName:"",
                duraionId:"",
                durationOrder:"",
                jobValue:0,
                valueUsed:0,
                timeLength:0,
                timeUsed:0,
                statusTimesheet:false
            }
            const checkPlanOnWeek = await db.collection("weeks").find({$and:[{planid:ObjectId(getAllPlans[plan]._id)},{weeknumber:getWeekDetail[0].weeknumber}]}).toArray();
    
            if(checkPlanOnWeek.length > 0){
                objPlan.statusTimesheet = true;
            }else{
                objPlan.statusTimesheet = false;
            }
    
            objPlan.planId = getAllPlans[plan]._id;
            objPlan.planName = getAllPlans[plan].planname;
            objPlan.durationid = getAllPlans[plan].durationid;
            objPlan.jobValue = getAllPlans[plan].jobvalue;
            objPlan.timeLength = getAllPlans[plan].timelength;
    
            //Sum total value used on plan 
            var TotalValueUsedOnPlan = 0;
            var TotalTimeUsed = 0;
            const getValueUsedOnPlan = await db.collection("timesheet").find({planid:getAllPlans[plan]._id}).toArray();
            for(var value = 0; value < getValueUsedOnPlan.length; value++){
                TotalValueUsedOnPlan += getValueUsedOnPlan[value].totalvalueonday;
                TotalTimeUsed +=getValueUsedOnPlan[value].hours;
            }
            objPlan.valueUsed = TotalValueUsedOnPlan;
        
            //Sum total timeused
            objPlan.timeUsed = TotalTimeUsed / 8;
    
    
            //get duration order 
            const getDuration = await db.collection("duration").find({"_id":ObjectId(getAllPlans[plan].durationid)}).toArray();
            objPlan.durationOrder = getAllPlans[plan].durationorder;
            objProject.plans.push(objPlan);
        }
        console.log(objProject);
        client.close();
        return objProject;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}