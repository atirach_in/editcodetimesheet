var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getUserPrimary = getUserPrimary;

async function getUserPrimary(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getIdUserPrimary = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        console.log("userprimary: "+ getIdUserPrimary[0].Primaryresponsibility);
        const getDataUserPrimary = await db.collection("users").find({"_id":ObjectId(getIdUserPrimary[0].Primaryresponsibility)}).toArray();
        console.log(getDataUserPrimary);
        client.close();
        return getDataUserPrimary;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}