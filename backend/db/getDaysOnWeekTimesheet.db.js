var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getWeekTimesheet = getWeekTimesheet;

async function getWeekTimesheet(dayId,projectId,planId,weekNumber){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var intWeekNumber = parseInt(weekNumber);
        var getWeekId = await db.collection("weeks").find({$and:[{weeknumber:intWeekNumber},{planid:ObjectId(planId)}]}).toArray();
        console.log("weekid: " + getWeekId[0]._id);
        var weekId = getWeekId[0]._id.toString();
        
        var objThisWeek = {
            projectId:projectId,
            planId:planId,
            planName:"",
            jobValue:0,
            usedValue:0,
            timeLength:0,
            timeUsed:0,
            weekId:weekId,
            weekNumber:0,
            dayId:dayId,
            dayNumber:0,
            team:[],
            day1:[],
            day1Id:"",
            statusDay1:false,
            day2:[],
            day2Id:"",
            statusDay2:false,
            day3:[],
            day3Id:"",
            statusDay3:false,
            day4:[],
            day4Id:"",
            statusDay4:false,
            day5:[],
            day5Id:"",
            statusDay5:false,
            day6:[],
            day6Id:"",
            statusDay6:false
        }
    
    
        //get day number 
        const getDetailDay = await db.collection("days").find({"_id":ObjectId(dayId)}).toArray();
        objThisWeek.dayNumber = getDetailDay[0].daynumber;
    
        //get detail plan
        const getDetailPlan = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
        objThisWeek.planName = getDetailPlan[0].planname;
        objThisWeek.jobValue = getDetailPlan[0].jobvalue;
        objThisWeek.timeLength = getDetailPlan[0].timelength;
    
        //get sum timeUsed
        var sumTotal = 0;
        var sumHours = 0;
        const getTimeUsed = await db.collection("timesheet").find({planid:planId}).toArray();
        for(var day =0; day < getTimeUsed.length; day++){
            sumTotal += getTimeUsed[day].totalvalue;
            sumHours += getTimeUsed[day].hours;
        }
        objThisWeek.usedValue = sumTotal;
        objThisWeek.timeUsed = sumHours;
    
        //get week detail 
        const getWeekDetail = await db.collection("weeks").find({"_id":ObjectId(weekId)}).toArray();
        objThisWeek.weekNumber = getWeekDetail[0].weeknumber;
    
        //get team (primary user)
        const getTeamPrimaryUser = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        objPrimaryUser ={
            userId:getTeamPrimaryUser[0].Primaryresponsibility,
            username:"",
            profileImage:""
        }
        const getUsernamePrimaryUser = await db.collection("users").find({"_id":ObjectId(getTeamPrimaryUser[0].Primaryresponsibility)}).toArray();
        objPrimaryUser.username = getUsernamePrimaryUser[0].fristname+" "+getUsernamePrimaryUser[0].lastname;
        objPrimaryUser.profileImage = getUsernamePrimaryUser[0].profileimage;
        objThisWeek.team.push(objPrimaryUser);
    
        //get user in team 
        const getAllTeam = await db.collection("users").find({projectid:projectId}).toArray();
        for(var user =0;user < getAllTeam.length;user++){
            objUser = {
                userId:getAllTeam[user]._id,
                username:getAllTeam[user].fristname +" "+getAllTeam[user].lastname,
                profileImage:getAllTeam[user].profileimage
            }
            objThisWeek.team.push(objUser);
        }
    
        //set on this day dtms get open and close day.
        var onThisDayDtms = 0;//moment(getdays_onweek[days].datetimestamp._d,["YYYY-MM-DD"]);
        const getOnThisDay = await db.collection("days").find({"_id":ObjectId(dayId)}).toArray();
        onThisDayDtms = moment(getOnThisDay[0].datetimestamp._d,["YYYY-MM-DD"]).add(1, 'days');
        //moment(startdate).add(days, 'days');
        console.log("on this day: " + onThisDayDtms);
        console.log("on this day string: "+onThisDayDtms.format("YYYY-MM-DD"));
        console.log("---------------------------");
    
        //get days on weeks
        var sort = {daynumber:1};
        const getDaysOnWeek = await db.collection("days").find({weekid:ObjectId(weekId)}).sort(sort).toArray();
        console.log("weekId: "+objThisWeek.weekId);
        console.log("week number: "+objThisWeek.weekNumber);
        for(var days = 0;days < getDaysOnWeek.length;days++){
            console.log("dayId: " + getDaysOnWeek[days]._id);
            console.log("dayNumber: " + getDaysOnWeek[days].daynumber);
            console.log("datetimestamp: "+ moment(getDaysOnWeek[days].datetimestamp._d,["YYYY-MM-DD"]));
            console.log("on this day: "+onThisDayDtms.format("YYYY-MM-DD"));
            var convertGetDateDtsm = moment(getDaysOnWeek[days].datetimestamp._d,["YYYY-MM-DD"]);
            console.log("convertGetDateDtsm: "+convertGetDateDtsm.format("YYYY-MM-DD"));
            
            if(getDaysOnWeek[days].daynumber == 1){
                objThisWeek.day1Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay1 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay1 = false;
                    console.log("open day: false");
                }
            }else if(getDaysOnWeek[days].daynumber == 2){
                objThisWeek.day2Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay2 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay2 = false;
                    console.log("open day: false");
                }
            }else if(getDaysOnWeek[days].daynumber == 3){
                objThisWeek.day3Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay3 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay3 = false;
                    console.log("open day: false");
                }
            }else if(getDaysOnWeek[days].daynumber == 4){
                objThisWeek.day4Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay4 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay4 = false;
                    console.log("open day: fasle");
                }
            }else if(getDaysOnWeek[days].daynumber == 5){
                objThisWeek.day5Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay5 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay5 = false;
                    console.log("open day: false");
                }
            }else if(getDaysOnWeek[days].daynumber == 6){
                objThisWeek.day6Id = getDaysOnWeek[days]._id;
                if(convertGetDateDtsm < onThisDayDtms){
                    objThisWeek.statusDay6 = true;
                    console.log("open day: true");
                }else{
                    objThisWeek.statusDay6 = false;
                    console.log("open day: fasle");
                }
            }else{
    
            }
    
            console.log("count team: " + objThisWeek.team.length);
            console.log("---------------------------");
            for(var i = 0;i < objThisWeek.team.length;i++){
                var dayIdFind = getDaysOnWeek[days]._id.toString();
                var userIdFind = objThisWeek.team[i].userid.toString();
                console.log("dayId: " + dayIdFind);
                console.log("userId: "+ userIdFind);
                const getTimesheet = await db.collection("timesheet").find({$and:[{userid:userIdFind},{dayid:dayIdFind}]}).toArray();
                console.log("getTimesheet.length: " + getTimesheet.length);
                if(getTimesheet.length == 0){
                    console.log("hours: " + 0);
                    var objUserTimesheetOnDay = {
                        userId:userIdFind,
                        dayId:dayIdFind,
                        hours:0
                    }
                    if(getDaysOnWeek[days].daynumber == 1){
                        objThisWeek.day1.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 2){
                        objThisWeek.day2.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 3){
                        objThisWeek.day3.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 4){
                        objThisWeek.day4.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 5){
                        objThisWeek.day5.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 6){
                        objThisWeek.day6.push(objUserTimesheetOnDay);
                    }
                }else{
                    console.log("hours: " + getTimesheet[0].hours);	
                    var objUserTimesheetOnDay = {
                        userId:userIdFind,
                        dayId:dayIdFind,
                        hours:getTimesheet[0].hours
                    }
                    if(getDaysOnWeek[days].daynumber == 1){
                        objThisWeek.day1.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 2){
                        objThisWeek.day2.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 3){
                        objThisWeek.day3.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 4){
                        objThisWeek.day4.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 5){
                        objThisWeek.day5.push(objUserTimesheetOnDay);
                    }else if(getDaysOnWeek[days].daynumber == 6){
                        objThisWeek.day6.push(objUserTimesheetOnDay);
                    }
                }
                console.log("---------------------------");
            }
            
        }// for days
        console.log(objThisWeek);
        client.close();
        return objThisWeek;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}