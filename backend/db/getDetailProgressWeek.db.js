var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailProgressWeek = getDetailProgressWeek;

async function getDetailProgressWeek(projectId,weekNumber){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = moment().format('D/MMMM/YYYY');
        //var pastdatetime = nodedatetime.create(dateNow);
        //get detail project 
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        //sum total value used
        var totalProjectValueUsed = 0;
        const getTotalValueUsed = await db.collection("timesheet").find({projectid:projectId}).toArray();
        for(var countTotal = 0;countTotal < getTotalValueUsed.length;countTotal++){
            totalProjectValueUsed += getTotalValueUsed[countTotal].totalvalue;
        }
    
        var objProgressWeek = {
            projectId:projectId,
            projectName:getDetailProject[0].projectname,
            projectValue:getDetailProject[0].projectvalue,
            usedValueProject:totalProjectValueUsed,
            weekNumber:weekNumber,
            weekNow:0,
            plans:[]
        }
    
        //get week now 
        console.log("dateNow: "+dateNow);
        console.log("-----------------------------");
        const getDayNow = await db.collection("days").find({$and:[{projectid:projectId},{datestring:dateNow}]}).toArray();
        const getWeekNumber = await db.collection("weeks").find({"_id":ObjectId(getDayNow[0].weekid)}).toArray();
        objProgressWeek.weekNow = getWeekNumber[0].weeknumber;
        //get plan on week 
        const getPlanOnWeek = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{projectid:projectId}]}).toArray();
        console.log("getPlanOnWeek: "+getPlanOnWeek.length);
        for(var plan = 0;plan < getPlanOnWeek.length;plan++){
            console.log("planId: " + getPlanOnWeek[plan].planid);
    
            var objPlan = {
                planId:getPlanOnWeek[plan].planid,
                planName:"",
                durationId:"",
                durationOrder:"",
                jobValue:0,
                useValue:0,
                timeLength:0,
                timeUsed:0,
                team:[],
                document:"",
                barrier:"",
                statusPlan:""
            } 
    
            //get plan detail 
            const getPlanDetail  = await db.collection("planorders").find({"_id":ObjectId(getPlanOnWeek[plan].planid)}).toArray(); 
            
            console.log("planName: " + getPlanDetail[0].planname);
            objPlan.planName = getPlanDetail[0].planname;
    
            console.log("durationId: " + getPlanDetail[0].durationid);
            objPlan.durationId = getPlanDetail[0].durationid;
    
            console.log("durationOrder: " + getPlanDetail[0].durationorder);
            objPlan.durationOrder = getPlanDetail[0].durationorder;
    
            console.log("jobValue: " + getPlanDetail[0].moneydifference);
            objPlan.jobValue = getPlanDetail[0].moneydifference;
    
            var sumUsedValue = 0;
            var sumTimeUsed = 0;
            var planIdFind = getPlanOnWeek[plan].planid.toString();
            const getSumValuesUsed = await db.collection("timesheet").find({planid:planIdFind}).toArray();
            for(var i = 0; i < getSumValuesUsed.length; i++){
                sumUsedValue += getSumValuesUsed[i].totalvalue;
                sumTimeUsed += getSumValuesUsed[i].hours;
            }
            console.log("useValue: " + sumUsedValue);
            objPlan.useValue = sumUsedValue;
    
            console.log("timeLength: " + getPlanDetail[0].timelength)
            objPlan.timeLength = getPlanDetail[0].timelength;
    
            console.log("timeUsed: " + (sumTimeUsed/8));
            objPlan.timeUsed = sumTimeUsed / 8;
    
            
            //get primary user 
            var objUserPrimary = {
                userId:"",
                userName:"",
                hours:0,
                comment:""
            }
            const getUserPrimary = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
            var userIdPrimary = getUserPrimary[0].Primaryresponsibility.toString();
            const getDetailUserPrimary = await db.collection("users").find({"_id":ObjectId(userIdPrimary)}).toArray();
            console.log("userName: " + getDetailUserPrimary[0].fristname +" "+ getDetailUserPrimary[0].lastname);
            objUserPrimary.userName = getDetailUserPrimary[0].fristname +" "+ getDetailUserPrimary[0].lastname;
            objUserPrimary.userId = userIdPrimary;
            //Sum hours
            var sumTotalUserHour = 0;
            const getSumHours = await db.collection("timesheet").find({$and:[{planid:planIdFind},{userid:userIdPrimary}]}).toArray();
            for(var a =0;a < getSumHours.length;a++){
                sumTotalUserHour += getSumHours[a].hours;
            }
            console.log("total hours: "+ sumTotalUserHour);
            objUserPrimary.hours = sumTotalUserHour;
            objPlan.team.push(objUserPrimary);
            console.log("-------------------------------------");
    
    
            //get all user 
            const getAllUserOnProject = await db.collection("users").find({projectid:projectId}).toArray();
            console.log("getAllUserOnProject.length: " +getAllUserOnProject.length);
            for(var user =0; user < getAllUserOnProject.length;user++){
                var objUser= {
                    userId:"",
                    userName:"",
                    hours:0,
                    comment:""
                }
                console.log("userId: "+ getAllUserOnProject[user]._id);
                var userId = getAllUserOnProject[user]._id.toString();
                console.log("userName: " + getAllUserOnProject[user].fristname +" "+ getAllUserOnProject[user].lastname);
                objUser.userid = getAllUserOnProject[user]._id;
                objUser.userName = getAllUserOnProject[user].fristname +" "+ getAllUserOnProject[user].lastname;
                //Sum hours
                var sumTotalUser = 0;
                const getSumHours = await db.collection("timesheet").find({$and:[{planid:planIdFind},{userid:userId}]}).toArray();
                for(var j =0;j < getSumHours.length;j++){
                    sumTotalUser += getSumHours[j].hours;
                }
                console.log("total hours: "+ sumTotalUser);
                objUser.hours = sumTotalUser;
                objPlan.team.push(objUser);
                console.log("-------------------------------------");
            }
            console.log("-----------------------------------");	
            objProgressWeek.plans.push(objPlan);
        }
        console.log(objProgressWeek);

        client.close();
        return objProgressWeek;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}