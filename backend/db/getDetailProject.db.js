var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailProject = getDetailProject;

async function getDetailProject(projectId,userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objDetailProject = {
            projectId:"",
            projectName:"",
            projectValue:0,
            userPrimary:"",
            documentation:[],
            plans:[],
            team:[],
            percentProject:0,
            statusProject:"",
            fillIn:""
        };
    
        //get datadetail project 
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        if(getDetailProject[0].status == 1){
            objDetailProject.statusProject = "กำลังดำเนินการ";
            objDetailProject.fillIn = "กำลังกรอกข้อมูล";
        }else if(getDetailProject[0].status == 2){
            objDetailProject.statusProject = "รอดำเนินการ";
            objDetailProject.fillIn = "ส่งเอกสารแล้ว";
        }else if(getDetailProject[0].status == 3){
            objDetailProject.statusProject = "ปรับแก้เอกสาร";
            objDetailProject.fillIn = "ปรับแก้เอกสาร";
        }else if(getDetailProject[0].status == 4){
            objDetailProject.statusProject = "อนุมัติ Timesheet";
            objDetailProject.fillIn = "อนุมัติ Timesheet";
        }
    
        //get userPrimary 
        const getUserPrimary = await db.collection("users").find({"_id":ObjectId(userId)}).toArray();
        var objUserPrimary = {
            userId:getUserPrimary[0]._id,
            fristName:getUserPrimary[0].fristname,
            lastName:getUserPrimary[0].lastname,
            position:getUserPrimary[0].position,
            profileImage:getUserPrimary[0].profileimage
        }
        objDetailProject.team.push(objUserPrimary);
    
        console.log("------------------------------");
    
        console.log("projectId: "+ projectId);
        objDetailProject.projectId = projectId;
    
        console.log("projectName: "+ getDetailProject[0].projectname);
        objDetailProject.projectName = getDetailProject[0].projectname;
    
        console.log("projectValue: "+ getDetailProject[0].projectvalue);
        objDetailProject.projectValue = getDetailProject[0].projectvalue;
    
        console.log("userPrimary: "+ getUserPrimary[0].fristname + " " + getUserPrimary[0].lastname);
        objDetailProject.userPrimary = getUserPrimary[0].fristname + " " + getUserPrimary[0].lastname;
    
        // console.log("documentation: "+getDetailProject[0].Documentation);
        // objDetailProject.documentation = getDetailProject[0].Documentation;
        const getAllFile = await db.collection("fileproject").find({projectid:projectId}).toArray();
         for(var count = 0;count < getAllFile.length;count++){
              var obj={
                   fileName:getAllFile[count].filename
              }
              objDetailProject.documentation.push(obj);
         }
    
    
        //get plan in project 
        const getPlanInProject = await db.collection("planorders").find({projectid:projectId}).toArray();
        for(var i = 0; i < getPlanInProject.length; i++){
            const getDurationOrders = await db.collection("duration").find({"_id":ObjectId(getPlanInProject[i].durationid)}).toArray();
            var durationOrder = getDurationOrders[0].durationorder;
            var objPlan = {
                planid:getPlanInProject[i]._id,
                planname:getPlanInProject[i].planname,
                duration:durationOrder,
                jobvalue:getPlanInProject[i].jobvalue,
                timelength:getPlanInProject[i].timelength
            }
            objDetailProject.plans.push(objPlan);
            console.log("plans: ");
            console.log(objPlan);
        }
    
        //documentation 
        var partDocument =0;
        const getDocumetation = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        const getAllDocumetation = await db.collection("fileproject").find({projectid:projectId}).toArray();
        if (getAllDocumetation.length == 0){
            partDocument = 0;
        }else{
            partDocument = 100;
        }
        console.log("percent part 1: "+partDocument);
        var percent1 = partDocument;
        console.log("---------------------------------------");
    
        //plan order
        console.log("amount plan: "+ getDocumetation[0].amountplan);
        var amountPlan = getDocumetation[0].amountplan;
        const getAllPlanOrder = await db.collection("planorders").find({projectid:projectId}).toArray();
        var plans = getAllPlanOrder.length;
        console.log("plans: "+plans);
        var percentPlan = 100/amountPlan;
        var calculatePercentPlan = percentPlan * plans;
        console.log("percent part 2: "+calculatePercentPlan);
        var percent2 = calculatePercentPlan;
        console.log("---------------------------------------");
    
        //team 
        const getAmountTeam = await  db.collection("users").find({projectid:projectId}).toArray();
        var amountUsers = getAmountTeam.length;//team_added
        var userPrimary = 1;
        console.log("amount team: "+ (amountUsers+userPrimary));
        var allUsers = (amountUsers+userPrimary);
        const getAmountSetTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var setAmount = getAmountSetTeam[0].amountteam;
        var percentTeam = 100/setAmount;
        console.log("percent amount users: "+ percentTeam);
        var percent3 = (percentTeam * allUsers);
        console.log("percent part 3:"+ percentTeam * allUsers);
        console.log("---------------------------------------");
    
        //percent1to3
        objDetailProject.percentProject = ((percent1 + percent2 + percent3) * 100) / 300;
    
        //get team 
        const getTeam = await db.collection("users").find({projectid:projectId}).toArray();
        if(getTeam.length == 0){
            console.log("team: "+ objDetailProject.team[0].fristname + " " +objDetailProject.team[0].lastname);
            console.log("-----------------------------");
            client.close();
            return objDetailProject;
        }else{
            console.log("team: ");
            for(var i = 0; i < getTeam.length; i++){
                var objUser ={
                    userId:getTeam[i]._id,
                    fristName:getTeam[i].fristname,
                    lastName:getTeam[i].lastname,
                    position:getTeam[i].position,
                    profileImage:getTeam[i].profileimage
                }
                console.log(objUser);
                objDetailProject.team.push(objUser);
            }
            console.log("-----------------------------");
            client.close();
            return objDetailProject;
        }
    }catch(e){
        console.log('\n',e);
        return null;
    }
}