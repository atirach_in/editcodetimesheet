var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getDetailUserPrimary = getDetailUserPrimary;

async function getDetailUserPrimary(userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objUser = {
            userId:userId,
            userName:"",
            profileImage:""
        }
        //get detailuser primary 
        const getUserPrimary = await db.collection("users").find({"_id":ObjectId(userId)}).toArray();
        objUser.userName = getUserPrimary[0].fristname+" "+getUserPrimary[0].lastname;
        objUser.profileImage = getUserPrimary[0].profileimage;
        
        client.close();
        return {
            success:false,
            data:'',
            obj:objUser
        };
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}