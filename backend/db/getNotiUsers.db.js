var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var nodeDatetime = require('node-datetime');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};



exports.getNotificationUser = getNotificationUser;

async function getNotificationUser(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDateTime = nodeDatetime.create(dateNow);
        var objNotifications={
            notiFications:[],
            countRead:0
        }
        //count read 
        const getCountRead = await db.collection("notificationUsers").find({$and:[{projectId:projectId},{read:""}]}).toArray();
        objNotifications.countRead = getCountRead.length;
        //get collection notificationUsers
        const getNotiUsers = await db.collection("notificationUsers").find({projectId:projectId}).toArray();
        for(var countNoti = (getNotiUsers.length-1);countNoti >= 0;countNoti--){
            var objNoti ={
                notiId:getNotiUsers[countNoti]._id,
                commentRejectId:getNotiUsers[countNoti].commentRejectId,
                projectId:projectId,
                planId:getNotiUsers[countNoti].planId,
                statusAction:getNotiUsers[countNoti].statusAction,
                statusString:getNotiUsers[countNoti].statusString,
                progressGroupPlanId:getNotiUsers[countNoti].progressGroupPlanId,
                lastTime:"",
                read:getNotiUsers[countNoti].read
        }
            var lastProjectTime = moment(Math.ceil(getNotiUsers[countNoti].dateTimeStamp));
            var thisTime = moment(pastDateTime._created);
            console.log("lastProjectTime: "+lastProjectTime);
            console.log("this time: "+pastDateTime._created);
            

            //date project
            var dateProjectMoment = moment(lastProjectTime).format('DD/MM/YYYY');
            var dayPro = moment(lastProjectTime).format('DD');
            var monthPro = moment(lastProjectTime).format('MM');
            var yearPro = moment(lastProjectTime).format('YYYY');
            var hrPro = moment(lastProjectTime).format('HH');
            var minutePro = moment(lastProjectTime).format('mm');
            var ssPro = moment(lastProjectTime).format('ss');
            var lastUpdateProject = moment([yearPro, monthPro, dayPro,hrPro,minutePro]);
            console.log("lastUpdateProject: "+lastUpdateProject);
            var yearTh = parseInt(yearPro) + 543;
            objNoti.lastTime = dayPro+"/"+monthPro+"/"+yearTh+"  "+hrPro+":"+minutePro+":"+ssPro;

            //date now 
            // var datenow_moment = moment(thisTime).format('DD/MM/YYYY');
            // var daynow = moment(thisTime).format('DD');
            // var monthnow = moment(thisTime).format('MM');
            // var yearnow = moment(thisTime).format('YYYY');
            // var hrnow = moment(thisTime).format('HH');
            // var minutenow = moment(thisTime).format('mm');
            // var ssnow = moment(thisTime).format('ss');
            // var datenow_action = moment([yearnow, monthnow, daynow,hrnow,minutenow]);
            // console.log("datenow_action: "+datenow_action);

            // var datepro = new Date(yearPro, monthPro, dayPro);
            // var dateNow = new Date(yearnow, monthnow, daynow);

            // var diff_minute = datenow_action.diff(lastUpdateProject, 'minute');
            // var diff_Hr = datenow_action.diff(lastUpdateProject, 'hour');
            // var diff_day = datenow_action.diff(lastUpdateProject, 'days');

            // console.log("diff_minute: " + diff_minute);
            // console.log("diff_Hr: " + diff_Hr);
            // console.log("diff_day: " + diff_day);
            // if(diff_day > 0){
            // 	objNoti.lastTime = diff_day +" "+"วัน";
            // }else if(diff_Hr > 0){
            // 	objNoti.lastTime = diff_Hr +" "+"ชั่วโมง";
            // }else if(diff_minute > 0){
            // 	objNoti.lastTime = diff_minute +" "+"นาที";
            // }
            objNotifications.notiFications.push(objNoti);

            // var diff = new DateDiff(datenow,datepro);
            // console.log("diff days: "+diff.days());
            // console.log("diff minute: "+diff.minutes());
            // console.log("diff hours: "+diff.hours());
            // console.log("--------------------------");
            // if(diff.minutes() >= 1440){
            // 	var days = (diff.minutes() / 1440) +" "+"วัน";//1440 = 1 day
            // 	console.log("calculate days:" + days);
            // }else if (diff.minutes() >= 60){
            // 	var hours = (diff.minutes() / 60) +" "+ "ชั่วโมง";// 1 hours
            // 	console.log("calculate hours:" + hours);
            // }else if(diff.minutes() <= 59){
            // 	var minutes = (diff.minutes()) +" "+ "นาที";
            // 	console.log("calculate minutes:" + minutes);
            // }
            console.log("--------------------------");
        }
        
        client.close();
        return {
            success:true,
            data:'',
            obj:objNotifications
        };
    }catch(e){
        console.log('\n\n\n',e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}