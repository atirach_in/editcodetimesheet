var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getPlanOnNowWeek = getPlanOnNowWeek;

async function getPlanOnNowWeek(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/MMMM/YYYY');
        var pastDatetime = nodeDatetime.create(dateNow);
        console.log("dateNow: "+ dateNow);
        
        var objData = {
            dayNumber:0,
            projectId:projectId,
            projectName:"",
            projectValue:0,
            totalValue:0,
            weekNumber:0,
            plans:[]
    
        }
        //get detail project
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        objData.projectName = getDetailProject[0].projectname;
        objData.projectValue = getDetailProject[0].projectvalue;
    
        //get detail day 
        const getDetailDay = await db.collection("days").find({$and:[{datestring:dateNow},{projectid:projectId}]}).toArray();
        console.log("getDetailDay: "+ getDetailDay.length);
    
        console.log("-------------------------------------------------------");
        //get week number 
        for(var countDay = 0;countDay < getDetailDay.length;countDay++){
            
            var weekId = getDetailDay[countDay].weekid;
            const getWeekNumber = await db.collection("weeks").find({$and:[{"_id":ObjectId(weekId)},{projectid:projectId}]}).toArray();	
            console.log("weekId: "+ getWeekNumber[0]._id);
            console.log("weekNumber: "+ getWeekNumber[0].weeknumber);
            console.log("planId: " + getWeekNumber[0].planid);
            
    
            //get detail plan 
            const getDetailPlan = await db.collection("planorders").find({"_id":ObjectId(getWeekNumber[0].planid)}).toArray();
            console.log("planName: " + getDetailPlan[0].planname);
            console.log("planOrder: "+ getDetailPlan[0].planorder);
            console.log("-------------------------------------------------------");
            var objPlan ={
                dayId:getDetailDay[countDay]._id,
                dayNumber:getDetailDay[countDay].daynumber,
                weekId:weekId,
                weekNumber:getWeekNumber[0].weeknumber,
                planId:getDetailPlan[0]._id,
                planOrder:getDetailPlan[0].planorder,
                planName:getDetailPlan[0].planname,
                jobValue:getDetailPlan[0].jobvalue,
                durationOrder:getDetailPlan[0].durationorder,
                timeLength:getDetailPlan[0].timelength
            }
            objData.plans.push(objPlan);
        }
        console.log("-----------------------------------");
        console.log(objData);

        client.close();
        return objData;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}