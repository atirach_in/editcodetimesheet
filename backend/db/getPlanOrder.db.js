var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getPlanOrder = getPlanOrder;

async function getPlanOrder(planId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = await nodeDatetime.create(dateNow);
        var dataPlan={
            planAdded:0,
            amountPlan:0,
            planDetial:[
                // planName:getPlan[0].planname,
                // jobValue:getPlan[0].jobvalue,
                // duration:getDuration[0].durationorder,
                // timeLength:getPlan[0].timelength,
                // averageWage:getPlan[0].averagewages,
                // enforceEmployee:getPlan[0].enforceemployee
                // duration:
                 
            ],
            duration:[]
        }
        const getPlan = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
        const getDuration = await  db.collection("duration").find({"_id":ObjectId(getPlan[0].durationid)}).toArray();
        const getAllDuration = await  db.collection("duration").find({projectid:getPlan[0].projectid}).toArray();
        //count plan in project 
        const getAmountPlan = await db.collection("projects").find({"_id":ObjectId(getPlan[0].projectid)}).toArray();
        dataPlan.amountPlan = parseInt(getAmountPlan[0].amountplan);
        //find plan added 
        const findPlanAdded = await db.collection("planorders").find({projectid:getPlan[0].projectid}).toArray();
        dataPlan.planAdded = findPlanAdded.length;
        var dataPlanDetail = {
            planName:getPlan[0].planname,
            jobValue:getPlan[0].jobvalue,
            duration:getDuration[0].durationorder,
            durationId:getDuration[0]._id,
            timeLength:getPlan[0].timelength,
            averageWage:getPlan[0].averagewages,
            moneyDifference:getPlan[0].moneydifference,
            enforceEmployee:getPlan[0].enforceemployee
        }
        for(var i = 0; i < getAllDuration.length; i++){
            dataPlan.duration.push(getAllDuration[i]);
        }
        dataPlan.planDetial.push(dataPlanDetail);
        console.log(dataPlan);
        console.log("---------------------------------");

        client.close();
        return dataPlan;
    }catch(e){
        console.log('\n',e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}