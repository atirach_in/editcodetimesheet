var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getPlanDashboard = getPlanDashboard;

async function getPlanDashboard(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objProject = {
            projectId:"",
            projectName:"",
            statusProject:"",
            projectValue:0,
            valueUsed:0,
            fristName:"",
            lastName:"",
            week:0,
            day:0,
            planOrders:[]
        }
        const getProjectDetail = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        const getUserDetail =  await db.collection("users").find({"_id":ObjectId(getProjectDetail[0].Primaryresponsibility)}).toArray();
        objProject.projectId = projectId;
        objProject.projectName = getProjectDetail[0].projectname;
        objProject.statusProject = getProjectDetail[0].status;
        objProject.projectValue = getProjectDetail[0].projectvalue;
        objProject.fristName = getUserDetail[0].fristname;
        objProject.lastName = getUserDetail[0].lastname;
        var dateNow = moment().format("D/MMMM/YYYY");
        console.log("this day: "+dateNow);
        const checkDateOnWeek = await db.collection("days").find({datestring:dateNow}).toArray();
        if(checkDateOnWeek.length == 0){
            console.log("no have day on week.");
            client.close();
            return {data:"no have day on week.",status:false};
        }else{
            console.log("have day on week.");
            //get plan on this week.
            var weekId = checkDateOnWeek[0].weekid;
            const getWeekNumber = await db.collection("weeks").find({"_id":ObjectId(weekId)}).toArray();
            var weekNumber = getWeekNumber[0].weeknumber;
            objProject.week = weekNumber;
            objProject.day = checkDateOnWeek[0].daynumber;
            const getPlanOnWeek = await db.collection("weeksplanorder").find({weekid:weekId}).toArray();
            for(var i = 0; i < getPlanOnWeek.length;i++){
                var planId = getPlanOnWeek[i].planid;
                const getPlan = await db.collection("planorders").find({"_id":ObjectId(planId)}).toArray();
                var durationId = getPlan[0].durationid;
                const getDuration = await db.collection("duration").find({"_id":ObjectId(durationId)}).toArray();
                var objPlan = {
                    planId:planId,
                    planName:getPlan[0].planname,
                    planValue:getPlan[0].jobvalue,
                    valueUsed:0,
                    duration:getDuration[0].durationorder,
                    timeLength:getPlan[0].timelength,
                    timeUsed:0
                }
                objProject.planOrders.push(objPlan);
            }
            client.close();
            return objProject;
        }
    }catch(e){
        console.log('\n',e);
        return null;
    }
}