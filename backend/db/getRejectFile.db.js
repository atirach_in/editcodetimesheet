var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getRejectFile = getRejectFile;

async function getRejectFile(projectId,commentRejectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodedatetime.create(dateNow);
        var objReject = {
            comment:"",
            fileName:"",
            fileNameReject:""
        }

        const getRejectFile = await db.collection("commentreject_file").find({$and:[{"_id":ObjectId(commentRejectId)},{projectid:projectId}]}).toArray();
        objReject.comment = getRejectFile[0].comment;
        objReject.fileName = getRejectFile[0].filename;
        objReject.fileNameReject = getRejectFile[0].filename_reject;
        console.log(objReject);
        console.log("-----------------------");
        
        client.close();
        return {
            succes:true,
            data:'',
            obj:objReject
        };
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:'',
            obj:null
        };
    }
}