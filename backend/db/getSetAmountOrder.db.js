var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getSetAmountOrder = getSetAmountOrder;

async function getSetAmountOrder(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        const getAmountOrder = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray(); 
        client.close();
        return {projectId:projectId,amountPlan:getAmountOrder[0].amountplan};
    }catch(e){
        console.log(e);
        return{
            projectId:'',
            amountPlan:0
        };
    }
}
