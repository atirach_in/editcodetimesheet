var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getTeamWhereId = getTeamWhereId;

async function getTeamWhereId(userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var typeUser = "";
        var password = "not password";
        //get type user 
        const getTypeUser = await db.collection("projects").find({Primaryresponsibility:userId}).toArray();
        //get users
        const getDetailUser = await db.collection("users").find({"_id":ObjectId(userId)}).toArray();
        if(getTypeUser.length == 0){
            typeUser = "normal user";
        }else{
            typeUser = "primary user";
            password = getDetailUser[0].password;
        }
        var dataUser={
            userId:userId,
            detail:{
                fristName:getDetailUser[0].fristname,
                lastName:getDetailUser[0].lastname,
                position:getDetailUser[0].position,
                profileImage:getDetailUser[0].profileimage,
                email:getDetailUser[0].email,
                password:password,
                phone:getDetailUser[0].phone,
                typeUser:typeUser
            }
        }
        client.close();
        return dataUser;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}