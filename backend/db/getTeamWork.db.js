var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getTeamWork = getTeamWork;

async function getTeamWork(projectId,userPrimaryKeyId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var teamWork={
            projectId:projectId,
            amountSet:0,
            addedTeam:0,
            team:[
                //fristname
                //lastname
                //position
                ]
        }
    
        //get set amount team 
        const getSetAmountTeam = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var getSetAmountTeam2 = getSetAmountTeam[0].amountteam;
        if(getSetAmountTeam2 == ""){
            console.log("not set amount team.");
        }else{
            teamWork.amountSet = getSetAmountTeam[0].amountteam;
        }
        
    
        //find user primary
        const userPrimary = await db.collection("users").find({"_id":ObjectId(userPrimaryKeyId)}).toArray();
        var objUserPrimary = {
            userId:userPrimary[0]._id,
            fristName:userPrimary[0].fristname,
            lastName:userPrimary[0].lastname,
            position:userPrimary[0].position,
            profileImage:userPrimary[0].profileimage
        }
        teamWork.team.push(objUserPrimary);
        const getTeamWork = await db.collection("users").find({"projectid":projectId}).toArray();
        teamWork.addedTeam = (getTeamWork.length +1);//+1 is userpirmary
        for(var i = 0; i < getTeamWork.length; i++){
            var objuser = {
                userId:getTeamWork[i]._id,
                fristName:getTeamWork[i].fristname,
                lastName:getTeamWork[i].lastname,
                position:getTeamWork[i].position,
                profileImage:getTeamWork[i].profileimage
            }
            teamWork.team.push(objuser);
        }
        console.log("-------------getTeamWork------------------");
        console.log(teamWork);
        console.log("--------------------------------");
        client.close();
        return teamWork;
    }catch(e){
        console.log('\n',e);
        return null;
    }
}