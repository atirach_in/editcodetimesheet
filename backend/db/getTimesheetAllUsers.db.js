var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.getTimesheetAllUsers = getTimesheetAllUsers;

async function getTimesheetAllUsers(projectId,weekNumber){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        var objProgressTimesheetUser = {
            projectId:projectId,
            weekNumber:weekNumber,
            team:[]
        }
        var weekId = "";
    
        //get weekId 
        const getWeekId = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{projectid:projectId}]}).toArray();
        weekId = getWeekId[0]._id;
        console.log("getWeekId[0]._id"+getWeekId[0]._id);
        //console.log("getWeekId[1]._id"+getWeekId[1]._id);
        //get userprimary 
        var objUserPrimary = {
            userId:"",
            userName:"",
            totalHours:0,
            plans:[]
        }
        const getUserPrimary = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var userPrimaryId = getUserPrimary[0].Primaryresponsibility.toString();
    
        const getDetailUserPrimary = await db.collection("users").find({"_id":ObjectId(userPrimaryId)}).toArray();
        console.log("userId: " + userPrimaryId);
        console.log("userName: " + getDetailUserPrimary[0].fristname +" "+ getDetailUserPrimary[0].lastname);
        objUserPrimary.userId = userPrimaryId;
        objUserPrimary.userName = getDetailUserPrimary[0].fristname +" "+ getDetailUserPrimary[0].lastname;
    
    
        //get plan on week 
        var totalHoursOnWeek = 0;
        const getPlan = await db.collection("planorders").find({projectid:projectId}).toArray();
        for(var plan = 0; plan < getPlan.length; plan++){
            var objPlan={
                planId: getPlan[plan]._id,
                planName:getPlan[plan].planname,
                durationId:"",
                durationOrder:0,
                totalHours:0,
                valueUsed:0
            }
            console.log("planId: " + getPlan[plan]._id);
            var planIdFind = getPlan[plan]._id.toString();
            const getPlanOnWeek = await db.collection("weeks").find({$and:[{planid:ObjectId(planIdFind)},{weeknumber:weekNumber}]}).toArray();
            if(getPlanOnWeek.length == 0){
                console.log("getPlanOnWeek.length: "+getPlanOnWeek.length);
                console.log("----------------------------------------------");
            }else{
    
                console.log("getPlanOnWeek.length: "+getPlanOnWeek.length);
                console.log("durationId: "+getPlanOnWeek[0].durationid);
                objPlan.durationId = getPlanOnWeek[0].durationid;
                const getDurationOrder = await db.collection("duration").find({"_id":ObjectId(getPlanOnWeek[0].durationid)}).toArray();
                console.log("durationOrder: " + getDurationOrder[0].durationorder);
                objPlan.durationOrder = getDurationOrder[0].durationorder;
    
                var sumTotalHours = 0;
                var sumTotalValue = 0;
                const getDayOnPlan = await db.collection("days").find({weekid:ObjectId(getPlanOnWeek[0]._id)}).toArray();
                for(var days = 0; days < getDayOnPlan.length;days++){
                    const getHours = await db.collection("timesheet").find({dayid:getDayOnPlan[days]._id.toString()}).toArray();
                    if(getHours.length == 0){
                        console.log("dayid "+ days+": "+getDayOnPlan[days]._id+" Hours: "+0);
                    }else{
                        console.log("dayid "+ days+": "+getDayOnPlan[days]._id+" Hours: " + getHours[0].hours);
                        sumTotalHours += getHours[0].hours;
                        sumTotalValue += getHours[0].totalvalue;
                        totalHoursOnWeek += getHours[0].hours;
                    }
                }
    
    
                console.log("totalHours: " + sumTotalHours);
                objPlan.totalHours = sumTotalHours;
    
                //get average plan manday
                const getAverage = await db.collection("planorders").find({"_id":ObjectId(getPlan[plan]._id)}).toArray();
                var average = getAverage[0].averagewages;
                objPlan.valueUsed = sumTotalValue;
                console.log("----------------------------------------------");
                objUserPrimary.plans.push(objPlan);
            }
        }//for plan	
        objUserPrimary.totalHours = totalHoursOnWeek;
        objProgressTimesheetUser.team.push(objUserPrimary);
        
        //get all user 
        console.log("----------------get users---------------------");
        const getAllUsers = await db.collection("users").find({projectid:projectId}).toArray();
        for(var users = 0; users < getAllUsers.length;users++){
            var totalHoursOnWeekUsers = 0;
            var objUser = {
                userId:"",
                userName:"",
                totalHours:0,
                plans:[]
            }
            console.log("userId: " + getAllUsers[users]._id);
            console.log("userName: " + getAllUsers[users].fristname +" "+ getAllUsers[users].lastname);
            objUser.userId = getAllUsers[users]._id;
            objUser.userName = getAllUsers[users].fristname +" "+ getAllUsers[users].lastname;
            const getPlanOnWeek = await db.collection("weeks").find({$and:[{projectid:projectId},{weeknumber:weekNumber}]}).toArray();
            console.log("getPlanOnWeek.length: " + getPlanOnWeek.length);
            for(var countPlan=0;countPlan < getPlanOnWeek.length;countPlan++){
                
                //get plan detail 
                const getDetailPlan = await db.collection("planorders").find({"_id":ObjectId(getPlanOnWeek[countPlan].planid)}).toArray();
                var objPlan={
                    planId: getPlanOnWeek[countPlan].planid,
                    planName:getDetailPlan[0].planname,
                    durationId:getDetailPlan[0].durationid,
                    durationOrder:getDetailPlan[0].durationorder,
                    totalHours:0,
                    valueUsed:0
                }
    
                console.log("planId: " + getPlanOnWeek[countPlan].planid);
                console.log("weekId: " + getPlanOnWeek[countPlan]._id);
                const getDayOnWeek = await db.collection("days").find({weekid:ObjectId(getPlanOnWeek[countPlan]._id)}).toArray();
                var SumTotalHours = 0;
                var SumTotalvalue = 0;
                for(var days = 0;days < getDayOnWeek.length;days++){
                    //console.log("dayid: " + days + " "+ getDayOnWeek[days]._id);
                    var dayIdFind = getDayOnWeek[days]._id.toString();
                    var userIdFind = getAllUsers[users]._id.toString();
                    const getHoursValue = await db.collection("timesheet").find({$and:[{dayid:dayIdFind},{userid:userIdFind}]}).toArray();
                    if(getHoursValue.length == 0){
                        SumTotalHours += 0;
                        SumTotalvalue += 0;
                    }else{
                        SumTotalHours += getHoursValue[0].hours;
                        SumTotalvalue += getHoursValue[0].totalvalue;
                        totalHoursOnWeekUsers +=getHoursValue[0].hours;
                    }
                }
                objPlan.totalHours = SumTotalHours;
                objPlan.valueUsed = SumTotalvalue;
                console.log("SumTotalHours: "+ SumTotalHours);
                console.log("SumTotalvalue: "+ SumTotalvalue);
                objUser.plans.push(objPlan);
            }
            objUser.totalHours = totalHoursOnWeekUsers;
            objProgressTimesheetUser.team.push(objUser);
            console.log("----------------------------------------------");
        }
        //console.log(objProgressTimesheetUser);
        client.close();
        return objProgressTimesheetUser;
    }catch(e){
        console.log('\n',e);
        return;
    }
}