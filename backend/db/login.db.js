var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.login = login;

async function login(email,token){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var decodeFront = jwt.verify(token,SECRET);
        var passwordFront = decodeFront.password;
        console.log("passwordFront: "+ passwordFront);

        //check json token 
        const getPassJwt = await db.collection("users").find({email:email}).toArray();
        //check email 
        if(getPassJwt.length == 0){
            //var respond = {data:"invalid username or password.",status:false};
            client.close();
            return {
                success:false,
                data:'invalid username or password.',
                obj:null
            };
        }else{
            var decodeBack = jwt.verify(getPassJwt[0].password,SECRET);
            var passwordBack = decodeBack.password;
            console.log("passwordBack:"+passwordBack);
            if(passwordFront == passwordBack){
                var password = passwordFront;
                console.log("userid: "+ getPassJwt[0]._id);
                    var userId = getPassJwt[0]._id;
                    const getProjectId = await db.collection("projects").find({Primaryresponsibility:userId.toString()}).toArray();
                    if (getProjectId.length == 0){
                        var resultData={
                            data:"login pass",
                            userId:getPassJwt[0]._id,
                            fristName:getPassJwt[0].fristname,
                            lastName:getPassJwt[0].lastname,
                            phone:getPassJwt[0].phone,
                            batchId:getPassJwt[0].batchid,
                            projectId:"",
                            position:getPassJwt[0].position,
                            status:true
                        }
                        console.log(resultData);
                        client.close();
                        return {
                            success:true,
                            data:'',
                            obj:resultData
                        };
                    }else{
                        var resultData={
                            data:"login pass",
                            userid:getPassJwt[0]._id,
                            fristName:getPassJwt[0].fristname,
                            lastName:getPassJwt[0].lastname,
                            phone:getPassJwt[0].phone,
                            batchId:getProjectId[0].batchid,
                            projectId:getProjectId[0]._id,
                            projectName:getProjectId[0].projectname,
                            position:getPassJwt[0].position,
                            status:true,
                            statusProject:"",
                            fillIn:""
                        }
                        //check status project
                        if(getProjectId[0].status == 1){
                            resultData.statusProject = "กำลังดำเนินการ";
                            resultData.fillIn = "กำลังกรอกข้อมูล";
                        }else if(getProjectId[0].status == 2){
                            resultData.statusProject = "รอดำเนินการ";
                            resultData.fillIn = "ส่งเอกสารแล้ว";
                        }else if(getProjectId[0].status == 3){
                            resultData.statusProject = "ปรับแก้เอกสาร";
                            resultData.fillIn = "ปรับแก้เอกสาร";
                        }else if(getProjectId[0].status == 4){
                            resultData.statusProject = "อนุมัติ Timesheet";
                            resultData.fillIn = "อนุมัติ Timesheet";
                        }

                        console.log(resultData);
                        client.close();
                        return {
                            success:true,
                            data:'',
                            obj:resultData
                        };
                    }
                
            }else{
                //var respond = {data:"invalid username or password.",status:false};
                client.close();
                return {
                    success:true,
                    data:'',
                    obj:resultData
                };
            }
        }
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:'',
            obj:null
        };
    }
}