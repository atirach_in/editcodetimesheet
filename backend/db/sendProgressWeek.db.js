var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.sendProgressWeek = sendProgressWeek;

async function sendProgressWeek(projectId,weekNumber,plans,late){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
    
        //get detail week 
        const getDetailWeek = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{projectid:projectId}]}).toArray();
        console.log("getDetailWeek.length: " +getDetailWeek.length);
        console.log("projectId: "+ projectId);
        console.log("count plan: " + plans.length);
    
        //get batchid 
        const getBatchId = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
    
        //insert progress_groupplan
        var objProgressGroupPlan={
            projectid:projectId,
            batchid:getBatchId[0].batchid,
            durationid:getDetailWeek[0].durationid,
            status:1,
            statusstring:"ส่งความก้าวหน้า",
            updatetime:pastDatetime._created
        }
        if(late == "ส่งล่าช้า"){
            objProgressGroupPlan.status = 5;
            objProgressGroupPlan.statusstring = "ส่งล่าช้า";
        }else{
            objProgressGroupPlan.status = 1;
            objProgressGroupPlan.statusstring = "ส่งความก้าวหน้า";
        }
    
        const insertProgressGroupPlan = await db.collection("progress_groupplan").insertOne(objProgressGroupPlan);
        const getLastProgressGroupPlanId = await db.collection("progress_groupplan").find({updatetime:pastDatetime._created}).toArray();
        console.log("last rogress_groupplan_id: " + getLastProgressGroupPlanId[0]._id);
        var progressGroupPlanId =  getLastProgressGroupPlanId[0]._id;
        
        for(var countPlan = 0; countPlan < plans.length;countPlan++){
    
            console.log("***************************************************************");
            console.log("***************************************************************");
            console.log("plans[count].statusplan: "+ plans[countPlan].statusplan);
            console.log("***************************************************************");
            console.log("***************************************************************");
            if(plans[countPlan].statusplan == true){
                console.log("data: this plan is success.");
                const updateStatusPlan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countPlan].planid)},
                                                                        {$set:{
                                                                            statusplan:true
                                                                        }});
            }else if(plans[countPlan].statusplan == false || plans[countPlan].statusplan == ""){
                console.log("data: this plan not success.");
                const updateStatusPlan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countPlan].planid)},
                                                                        {$set:{
                                                                            statusplan:false
                                                                        }});
            }else{
                console.log("data: this plan status undefind.");
            }
    
    
            //get weekid 
            const getWeekIdAtPlanId = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{planid:ObjectId(plans[countPlan].planid)}]}).toArray();
    
            var updateWeekStatus = 1;
            var updateWeekStatusWeek = 1;
            var statusLate = false;
            if(late == "ส่งล่าช้า"){
                updateWeekStatus = 5;
                updateWeekStatusWeek = "ส่งล่าช้า";
                statusLate = true;
            }else{
                updateWeekStatus = 1;
                updateWeekStatusWeek = "ส่งความก้าวหน้า";
                statusLate = false;
            }
            //update status week = 1 sending , 2 = missing , 3 = reject ,4 = approve "_id":ObjectId(getWeekIdAtPlanId[0]._id)
            const updateStatusWeek = await db.collection("weeks").updateOne(
                                                    {$and:[{weeknumber:weekNumber},{projectid:projectId}]},
                                                    {$set:{
                                                        statusweek:updateWeekStatus,
                                                        status:updateWeekStatus,
                                                        statusstring:updateWeekStatusWeek,
                                                        status_late:statusLate
                                                    }});
            var objProgressWeek={
                progress_groupplan_id:progressGroupPlanId,
                weekid:getWeekIdAtPlanId[0]._id,
                weeknumber:weekNumber,
                batchid:getBatchId[0].batchid,
                projectid:projectId,
                planid:plans[countPlan].planid,
                planname:plans[countPlan].planname,
                durationid:plans[countPlan].durationid,
                durationorder:plans[countPlan].durationorder,
                planvalue:plans[countPlan].jobvalue,
                valueused:plans[countPlan].usevalue,
                timelength:plans[countPlan].timelength,
                timeused:plans[countPlan].timeused,
                documentation:plans[countPlan].document,
                barries:plans[countPlan].barrier,
                updatetime:pastDatetime._created,
                status:1,
                statusstring:"ส่งความก้าวหน้าของงาน"
            }
    
            if(late == "ส่งล่าช้า"){
                objProgressWeek.status = 5;
                objProgressWeek.statusstring = "ส่งล่าช้า";
            }else{
                objProgressWeek.status = 1;
                objProgressWeek.statusstring = "ส่งความก้าวหน้าของงาน";
            }
    
            console.log(objProgressWeek);
            const insertObjProgressWeek = await db.collection("progress_weeks").insertOne(objProgressWeek);
    
            const getLastProgressId = await db.collection("progress_weeks").find({$and:[{weekid:ObjectId(getWeekIdAtPlanId[0]._id)},{planid:plans[countPlan].planid}]}).toArray();
            console.log("count team: "+ plans[countPlan].team.length);
            var progressWeekId = getLastProgressId[0]._id;
            for(var countteam = 0;countteam < plans[countPlan].team.length; countteam++){
                var objUser = {
                    progress_weekid:progressWeekId,
                    userid:plans[countPlan].team[countteam].userid,
                    username:plans[countPlan].team[countteam].username,
                    totalhours:plans[countPlan].team[countteam].hours,
                    comment:plans[countPlan].team[countteam].comment
                }
                console.log(objUser);
                const insertObjDetailProgress = await db.collection("detailprogress_weeks").insertOne(objUser);
            }
            console.log("------------------------------");
        }
    
        //insert collection notification_admin 
        var dtsmp = pastDatetime._created;
        var dtsmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:projectId,
            planid:"",
            comment_rejectid:"",
            statusaction:1,
            statusstring:"ส่งความก้าวหน้า",
            read:"",
            progress_groupplan_id:progressGroupPlanId,
            datetimestamp:dtsmp,
            datetimestring:dtsmpString
        }
        const addNotificationAdmin = await db.collection("notification_admin").insertOne(objNotification);

        client.close();
        return {data:"send progress week success.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"send progress week failed.",status:false};
    }
}