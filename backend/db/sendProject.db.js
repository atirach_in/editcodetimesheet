var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.sendProject = sendProject;

async function sendProject(projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        var status = 2 //send peoject is pending not update or edit project (bsm)
        const updateStstusProject = await db.collection("projects").updateOne(
                                            {"_id":ObjectId(projectId)},
                                            { $set: {
                                                     status:status,
                                                     senddatetime:pastDatetime._created
                                            }});
        //update status project in batchbusiinessman = 2 
        const updateBsm = await db.collection("batchbusinessman").updateOne(
                                    {"projectid":ObjectId(projectId)},
                                    {$set: {
                                             updatetime:pastDatetime._created,
                                             status:"2"//send and pending.
                                            }
                                    });
    
        //insert collection notification_admin 
        var dtsmp = pastDatetime._created;
        var dtsmpString = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:projectId,
            planid:"",
            comment_rejectid:"",
            statusaction:2,
            statusstring:"ส่งเอกสารแล้ว",
            read:"",
            progress_groupplan_id:"",
            datetimestamp:dtsmp,
            datetimestring:dtsmpString
        }
        const addNotificationAdmin = await db.collection("notification_admin").insertOne(objNotification);
    
        //insert activity status
        const getDetailProject = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        var objLog={
            projectid:projectId,
            projectname:getDetailProject[0].projectname,
            userid:getDetailProject[0].Primaryresponsibility,
            status:3,
            statusstring:"ส่งเอกสารแล้ว",
            datetimestamp:pastDatetime._created
        }
        const insertLog =  await db.collection("logactivityproject").insertOne(objLog); 
        
        client.close();
        return {data:"sending project success.",projectId:projectId,statusproject:"sending",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"sending project failed.",projectId:projectId,statusproject:"not sending",status:false};
    }
}