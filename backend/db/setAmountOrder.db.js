var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.setAmountOrder = setAmountOrder;

async function setAmountOrder(amountOrder,projectId,userId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        const setAmountPlan = await db.collection("projects").updateOne(
                                        {"_id":ObjectId(projectId)},
                                        {$set: {amountplan: amountOrder}});
        //get activity_id  
        var activityOrder = "2.2";
        const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
        console.log("activity _id: " + getActivityId[0]._id);
        console.log("update amount plan success.");
        console.log("projectId: "+ projectId);
        console.log("---------------------------");
        //insert activityusers
        const insertActivityUser = await db.collection("activityusers").insert({
                                        userid:userId,
                                        activityid:getActivityId[0]._id,
                                        datetime:pastDatetime._created
                                  });  
        client.close();
        return {data:"set amount plan success.",draft:"2.3",status:true};
   }catch(e){
       console.log('\n',e);
       return {data:"set amount plan failed.",draft:"",status:false};
   }
}