var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateAmountTeam = updateAmountTeam;

async function updateAmountTeam(projectId,amount){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        //update amount team 
        const updateAmountTeam = await db.collection("projects").updateOne(
                                                    {"_id":ObjectId(projectId)},
                                                    { $set: {amountteam:amount}});
        client.close();
        return {data:"update success.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"update failed.",status:false};
    }
}