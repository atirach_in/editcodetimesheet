var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateDataUser = updateDataUser;

async function updateDataUser(userId,fristName,lastName,position,profileImage,email,phone){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
        var pastDatetime = nodeDatetime.create(dateNow);
        if(profileImage == ""){
            const updateUser = await db.collection("users").updateOne(
                                {"_id":ObjectId(userId)},
                                { $set: { fristname:fristName,
                                          lastname:lastName,
                                          position:position,
                                          email:email,
                                          phone:phone,
                                          updatetime:pastDatetime._created
                                }});
            client.close();
            return {data:"update user success.",userId:userId,status:true};
        }else{
            const updateUser = await db.collection("users").updateOne(
                                {"_id":ObjectId(userId)},
                                { $set: { fristname:fristName,
                                          lastname:lastName,
                                          position:position,
                                          profileimage:profileImage,
                                          email:email,
                                          phone:phone,
                                          updatetime:pastDatetime._created
                                }});
            client.close();
            return {data:"update user success.",userId:userId,status:true};
        }
    }catch(e){
        console.log('\n',e);
        return {data:"update user failed.",userId:userId,status:false};
    }
}