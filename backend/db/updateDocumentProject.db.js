var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.addDocument = addDocument;

async function addDocument(userId,projectId,pathDocument){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        const updateDocument = await db.collection("projects").updateOne(
                                        {"_id":ObjectId(projectId)},
                                        {$set: {Documentation: pathDocument}});
        //get documentation 
        const getDocumetation = await db.collection("projects").find({"_id":ObjectId(projectId)}).toArray();
        console.log("documenttaion: "+ getDocumetation[0].Documentation);
        //get activity_id  
            var activityOrder = "1"; // สร้างแผนงาน
            const getActivityId = await db.collection("activity").find({activityorder:activityOrder}).toArray();
            console.log("activity _id: " + getActivityId[0]._id);
            console.log("update document success.");
            console.log("projectId: "+ projectId);
            console.log("---------------------------");
            //insert activityusers
            const insertActivityUser = await db.collection("activityusers").insert({
                                            userid:userId,
                                            activityid:getActivityId[0]._id,
                                            datetime:pastDatetime._created
                                    });
        //check document for draft 
        if(getDocumetation[0].Documentation == ""){
            var draftAt = "1";
            console.log("draft at: "+ draftAt);
            client.close();
            return {data:"no have document.",draft:draftAt,status:true};
        }else{
            var draftAt = "2";
            console.log("draft at: "+ draftAt);
            client.close();
            return {data:"update document sucess.",status:true};
        }
    }catch(e){
        console.log('\n',e);
        return null;
    }
}