var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateDuration = updateDuration;

async function updateDuration(duration,projectId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        for(var i = 0; i < duration.length;i++){
            if(duration[i].durationid == ""){
                //insert duration 
                var objDuration = {
                    durationorder:duration[i].durationorder,
                    month:duration[i].month,
                    projectid:projectId
                };
                const insertDuration =  await db.collection("duration").insertOne(objDuration);
                console.log("insert duration: "+ i);
    
                console.log("----------------------------------------------");
            }else{
                //check duraion in db 
                const getDuration = await db.collection("duration").find({"_id":ObjectId(duration[i].durationid)}).toArray();
                console.log(duration[i].durationid);
                var durationOrder = duration[i].durationorder;
                var month = duration[i].month;
                // update duration
                const updateDuration = await db.collection("duration").updateOne(
                                                    {"_id":ObjectId(duration[i].durationid)},
                                                    { $set: { 
                                                        durationorder:duration[i].durationorder,
                                                        month:duration[i].month
                                                    }});
                console.log("update duration: "+i);
                console.log("-----------------------------------------------");
            }
        }
        client.close();
        return {data:"update all duration success.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"update all duration failed.",status:false};
    }
}