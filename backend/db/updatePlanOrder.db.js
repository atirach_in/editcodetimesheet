var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updatePlanOrder = updatePlanOrder;

async function updatePlanOrder(planId,planName,jobValue,durationId,timeLength,averageWages,moneyDifference,enforceEmployee){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(datenow);
        const getDurationNumber = await db.collection("duration").find({"_id":ObjectId(durationid)}).toArray();
        const updatePlanOrder = await db.collection("planorders").updateOne(
                                        {"_id":ObjectId(planId)},
                                        { $set: { planname: planName,
                                                  jobvalue: jobValue,
                                                  durationid: durationId,
                                                  durationorder:getDurationNumber[0].durationorder,
                                                  timelength: timeLength,
                                                  averagewages: averageWages,
                                                  moneydifference:moneyDifference,
                                                  enforceemployee:enforceEmployee,
                                                  updatetime:pastDatetime._created}});
        client.close();
        return {data:"update plan sucess.",planId:planId,status:true};
    }catch(e){
        console.log(e);
        return {data:"update plan failed.",planId:planId,status:false};;
    }
}