var initConfig = require('../init.config.json');
var moment = require('moment');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateProgressWeekAfterReject = updateProgressWeekAfterReject;

async function updateProgressWeekAfterReject(projectId,weekNumber,plans,late){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        date.locale('th');
        var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(dateNow);
        var status = 1;
        var statusString = "ส่งความก้าวหน้า";
    
        console.log("projectId: "+projectId);
        console.log("weekNumber: "+weekNumber);
        console.log("plans.length: "+plans.length);
    
        //get progress_groupplan_id
        const getProgressGroupPlanId = await db.collection("progress_weeks").find({$and:[{projectid:projectId},{weeknumber:weekNumber}]}).toArray();
        console.log("progressGroupPlanId: "+ getProgressGroupPlanId[0].progress_groupplan_id);
        var progressGroupPlanId = getProgressGroupPlanId[0].progress_groupplan_id;
    
        //update progress groupplan 
        var updateWeekProgressGroupStatus = 1;
        var updateWeekProgressgroupStatusSeek = 1;
        var statusProgressGroupLate = false;
            if(late == "ส่งล่าช้า"){
                updateWeekProgressGroupStatus = 5;
                updateWeekProgressgroupStatusSeek = "ส่งล่าช้า";
                statusProgressGroupLate = true;
            }else{
                updateWeekProgressGroupStatus = 1;
                updateWeekProgressgroupStatusSeek = "ส่งความก้าวหน้า";
                statusProgressGroupLate = false;
            }
        const updateProgressGroupPlan = await db.collection("progress_groupplan").update({"_id":ObjectId(progressGroupPlanId)},
                                                                                          {$set:{
    
                                                                                              status:updateWeekProgressGroupStatus,
                                                                                              statusstring:updateWeekProgressgroupStatusSeek}});
        console.log("***************************************************************");
        console.log("count plans:"+plans.length);
        //update progress week
        for(var countPlan=0;countPlan < plans.length;countPlan++){
            var planId = plans[countPlan].planid;
            var useValue = plans[countPlan].usevalue;
            var timeUsed = plans[countPlan].timeused;
            var documentation = plans[countPlan].document;
            var barrier = plans[countPlan].barrier;
            var weekNumber = weekNumber;
            console.log("***************************************************************");
            console.log("***************************************************************");
            console.log("plans[count].statusplan: "+ plans[countPlan].statusplan);
            console.log("***************************************************************");
            console.log("***************************************************************");
            if(plans[countPlan].statusplan == true){
                console.log("data: this plan is success.");
                const updateStatusPlan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countPlan].planid)},
                                                                        {$set:{
                                                                            statusplan:true
                                                                        }});
            }else if(plans[countPlan].statusplan == false || plans[countPlan].statusplan == ""){
                console.log("data: this plan not success.");
                const updateStatusPlan = await db.collection("planorders").updateOne({"_id":ObjectId(plans[countPlan].planid)},
                                                                        {$set:{
                                                                            statusplan:false
                                                                        }});
            }else{
                console.log("data: this plan status undefind.");
            }
    
            //get weekid 
            const getWeekIdAtPlanId = await db.collection("weeks").find({$and:[{weeknumber:weekNumber},{planid:ObjectId(planid)}]}).toArray();
    
            var updateWeekStatus = 1;
            var updateWeekStatusWeek = 1;
            var statusLate = false;
            if(late == "ส่งล่าช้า"){
                updateWeekStatus = 5;
                updateWeekStatusWeek = "ส่งล่าช้า";
                statusLate = true;
            }else{
                updateWeekStatus = 1;
                updateWeekStatusWeek = "ส่งความก้าวหน้า";
                statusLate = false;
            }
            //update status week = 1 sending , 2 = missing , 3 = reject ,4 = approve {"_id":ObjectId(getWeekIdAtPlanId[0]._id)
            const updateStatusWeek = await db.collection("weeks").updateOne(
                                                    {$and:[{weeknumber:weekNumber},{projectid:projectId}]},
                                                    {$set:{
                                                        statusweek:updateWeekStatus,
                                                        status:updateWeekStatus,
                                                        statusstring:updateWeekStatusWeek,
                                                        status_late:statusLate
                                                    }});
    
            const updateStatusProgressweek = await db.collection("progress_weeks").updateOne({planid:planId,weeknumber:weekNumber},
                                                                                          {$set:{status:updateWeekStatus,
                                                                                                   statusstring:updateWeekStatusWeek,
                                                                                                   valueused:useValue,
                                                                                                   timeused:timeUsed,
                                                                                                   documentation:documentation,
                                                                                                   barries:barrier
                                                                                                  }});
            const getProgressWeekId = await db.collection("progress_weeks").find({$and:[{planid:planId},{weeknumber:weekNumber}]}).toArray();
            var progressWeekIdFind = getProgressWeekId[0]._id;
            console.log("progress weekid: " +progressWeekIdFind);
            console.log("team.length: "+plans[countPlan].team.length);
            for(var countTeam = 0;countTeam < plans[countPlan].team.length;countTeam++){
                var userId = plans[countPlan].team[countTeam].userid;
                var hours =  plans[countPlan].team[countTeam].hours;
                var comment = plans[countPlan].team[countTeam].comment;
                console.log("progress_weekid: "+progressWeekIdFind);
                console.log("userId: "+userId);
                console.log("hours: "+hours);
                console.log("comment: "+comment);
                const updateDataUserOnProgressWeek = await db.collection("detailprogress_weeks").update({progress_weekid:ObjectId(progressWeekIdFind),userid:userId},
                                                                                          {$set:{totalhours:hours,
                                                                                                   comment:comment
                                                                                                  }});
            }
            console.log("--------------------------------");
        }	
        //insert collection notification_admin 
        var dtsmp = pastDatetime._created;
        var dtsmp_string = moment().format("DD/MM/YYYY HH:mm:ss");
        var objNotification={
            projectid:projectId,
            planid:"",
            comment_rejectid:"",
            statusaction:1,
            statusstring:"ส่งความก้าวหน้า",
            read:"",
            progress_groupplan_id:progressGroupPlanId,
            datetimestamp:dtsmp,
            datetimestring:dtsmp_string
        }
        const addNotificationAdmin = await db.collection("notification_admin").insertOne(objNotification);
    
        console.log("data: update status progress weeks success.");
        client.close();
        return {data:"update status progress weeks success.",status:true};
    }catch(e){
        console.log('\n',e);
        return {data:"update status progress weeks failed.",status:false};
    }
}