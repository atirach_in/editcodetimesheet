var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateReadNoti = updateReadNoti;

async function updateReadNoti(notiId){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        //update noti read = true 
        const updateNotiFication = await db.collection("notification_users").updateOne(
                                                    {"_id":ObjectId(notiId)},
                                                    { $set: {read:true}});
        console.log("update notiId: "+notiId +" success.");
        client.close();
        return {
            success:true,
            data:"update notiId success.",
            obj:null
        };
    }catch(e){
        console.log(e);
        return {
            success:false,
            data:e,
            obj:null
        };
    }
}