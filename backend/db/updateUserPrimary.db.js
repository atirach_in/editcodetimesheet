var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var initConfig = require('../init.config.json');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');
var DATABASENAME = initConfig.DATABASENAME;
var url = 'mongodb://'+initConfig.DATABASEUSERNAME+':'+initConfig.DATABASEPASSWORD+'@'+initConfig.DATABASEHOST+':'+initConfig.DATABASEPORT+'/'+initConfig.DATABASENAME;
const authSources = { 
	//useNewUrlParser: true,
	native_parser:true, 
	authSource:'admin'
};

exports.updateUserPrimary = updateUserPrimary;

async function updateUserPrimary(userId,fristName,lastName,phone,position,profileImage){
    try{
        const client = await MongoClient.connect(url,authSources);
        const db = client.db(DATABASENAME);
        
        date.locale('th');
        var datenow = date.format(new Date(), 'D/M/YYYY HH:mm:ss');
        var pastDatetime = nodeDatetime.create(datenow);
        var objUser = {data:"update user success.",user:[],status:true};
        //var path_profileimage = "../../fontend/src/assets/upload/profil"+profileimage;
        //update user primary 
        const updateUserPrimary = await db.collection("users").updateOne(
                        {"_id":ObjectId(userId)},
                        { $set: { fristname: fristName,
                                lastname: lastName,
                                phone:phone,
                                position:position,
                                profileimage:profileImage,
                                updatetime:pastDatetime._created
                        }});
        var user = {
            fristName: fristName,
            lastName: lastName,
            phone:phone,
            position:position,
            profileImage:profileImage
        }
        objUser.user.push(user);
        client.close();
        return objUser;
    }catch(e){
        console.log(e);
        return{
            success:false,
            data:e,
            obj:null
        };
    }
}