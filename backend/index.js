var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
const fs = require('fs');
const http = require('http');
const https = require('https');
const path = require('path');
const mime = require('mime');
const multer = require('multer');


const fileFilter = (req, file, cb) => {
	// reject a file
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype ==='application/pdf') {
	  cb(null, true);
	}else{
	  cb(null, false);
	}
  };
  const fileFilter_doc = (req, file, cb) => {
	// reject a file
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype ==='application/pdf' || file.mimetype ==='application/msword' ||  file.mimetype==='application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.mimetype ==='application/vnd.openxmlformats-officedocument.wordprocessingml.template' || file.mimetype === 'application/vnd.ms-word.document.macroEnabled.12' || file.mimetype === 'application/vnd.ms-word.template.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.template' || file.mimetype === 'application/vnd.ms-excel.sheet.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel.template.macroEnabled.12' || file.mimetype ==='application/vnd.ms-excel.addin.macroEnabled.12' || file.mimetype === 'application/vnd.ms-excel.sheet.binary.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.presentation' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.template' || file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.slideshow' || file.mimetype === 'application/vnd.ms-powerpoint.addin.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint.presentation.macroEnabled.12' || file.mimetype === 'application/vnd.ms-powerpoint.template.macroEnabled.12' || file.mimetype ==='application/vnd.ms-powerpoint.slideshow.macroEnabled.12'){
	  cb(null, true);
	}else{
		console.log(file);
	  cb(null, false);
	}
  };
  
  ////upload image 
  const storage_image = multer.diskStorage({
	  destination: function (req, file, cb) {
	  cb(null, path.resolve(__dirname, '../upload/profile'))
	},
	  filename: function (req, file, cb) {
	  var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	  //var pastdatetime = nodedatetime.create(datenow);
	  cb(null,datenow+"_"+file.originalname)
	  }
  });
  const upload_image = multer({
   storage:storage_image,
	limits: {
	  fileSize: 1024 * 1024 * 5
	},
	fileFilter: fileFilter
  });
  
  ////upload file 
  const storage_file = multer.diskStorage({
	  destination: function (req, file, cb) {
	  cb(null, path.resolve(__dirname, '../upload/project'))//src/assets/upload/project
	},
	  filename: function (req, file, cb) {
	  var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	  cb(null,"document"+datenow+"_"+file.originalname)
	}
  });
  
  const upload_file = multer({
   storage:storage_file,
   fileFilter: fileFilter_doc
  });

  app.post('/send', (req, res) => {
	//send email 	
		var email = 'atirach.in.dev@gmail.com';								
		var transporter = nodemailer.createTransport(smtpTransport({
			service: 'gmail',
			  host: 'smtp.gmail.com',
			auth:{
				xoauth2:xoauth2.createXOAuth2Generator({
					user:'tedfund.app@gmail.com',
					clientId:'207283746551-28rcdbelrpi8263mo972mkp4rh2rje02.apps.googleusercontent.com',
					clientSecret:'xjqJGv8GKO7nHCbgY1xwgzsD',
					refreshToken:'1/yFEkO3eDSEMywR_eHgPw5QoLU_fRCiO6NttK2HGT4b0'
				})
			},
		}));

		 const output = `
			 <h3 style="color:#128553">Create Account Success</h3>
			<h4 style="color:#128553;margin-left:20px">bacthname</h4>
			<ul>  
				<li style="margin-bottom:5px">Name:fristname  lastname</li>
				<li style="margin-bottom:5px">Email: email</li>
				<li style="margin-bottom:5px">Phone: phone</li>
			</ul>
			<hr color='128553'>
		  `;
		//set mail options
		var mailOptions = {
			from:'tedfund.app <tedfund.app@gmail.com>',
			to:email,
			subject:'Create account succcess.',
			text:'Register TEDfund.',
			html:output
		}
		// sent email
		transporter.sendMail(mailOptions,function(err,res){
			if(err){
				console.log(err);
			}else{
				console.log('Email sent.');
				console.log(mailOptions);
			}
		});
var result = {from:'tedfund.app <tedfund.app@gmail.com>',to:email,status:'success'}
res.json(result);
});


app.get('/download/:filename', function(req, res){
var filename = req.params.filename;
var path = '../../frontend/src/assets/upload/project/';
//var filename = path.basename(file);
//var mimetype = mime.lookup(file);
res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

// var filestream = fs.createReadStream(file);
// filestream.pipe(res);
console.log("filename: "+path+filename);
res.download(path+filename);
});

//update profile user 
app.post('/update/profile/user',upload_image.single('profileimage'),(req,res)=>{
var userid = req.body.userid;
if(req.file && userid){ 
	console.log(req.file);
	var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	console.dir("path : " + datenow+"_"+req.file.originalname);
	var filename = datenow+"_"+req.file.originalname; 
	dbservice.updateprofile(userid,filename)
	.then(function(result){
		return res.end(datenow+"_"+req.file.originalname); 	
	});
}else{
	console.log(req.file);
	console.log("data no file: " + req.file);
	return res.end('data: invalid file.');
} 
});

//delete file 
app.post('/delete/file',(req,res)=>{
var filename = req.body.filename;
if(filename == ""){
	console.log("data: invalid filename.");
	res.json({
		data:"invalid filename.",
		status:false
	});
}else{
	//\\work\\tedtimesheet\\backend\\Allservice\\image\\
	fs.unlink('./upload/project/'+filename, function(error) {
	if (error) {
		throw error;
		}
		console.log('data: delete '+filename);
		dbservice.deletefile(filename)
		.then(function(result){
			res.json(result);
		});
	});
}
});

//path upload file 
app.post("/upload/user/image",upload_image.single('profileimage'),(req,res)=>{
var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
if(req.file){ 
	console.log(req.file);
	console.dir("path : " + datenow+"_"+req.file.originalname); 
	return res.end(datenow+"_"+req.file.originalname); 
}else{
	console.log(req.file);
	console.log("data no file: " + req.file);
	return res.end('data: invalid file.');
} 
});

app.use('/images', express.static(`${__dirname}/../upload`));

//path upload file 
app.post("/upload/user/project",upload_file.single('projectdoc'),(req,res)=>{
if(req.file){ 
	var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
	console.dir("path : " + "document"+datenow+"_"+req.file.originalname); 
	return res.end("document"+datenow+"_"+req.file.originalname); 	
}else{
	console.log("data no file: " + req.file);
	return res.end('');
} 
});

//path upload multi file
app.post("/upload/multi/file/project",upload_file.single('projectdoc'),(req,res)=>{
var projectid = req.body.projectid;
if(projectid == ""){
	console.log("data: invalid projectid.");
	res.end('data: invalid projectid.');
}else{
	if(req.file){ 
		console.log("projectid: "+ projectid);
		var datenow = date.format(new Date(), 'DMYYYYHHmm').toString();
		console.dir("projectid: "+ projectid);
		console.dir("path : " + "document"+datenow+"_"+req.file.originalname); 
		var filename = "document"+datenow+"_"+req.file.originalname;
		dbservice.addfilename_project(projectid,filename)
		.then(function(result){
			res.end(result);
		});
		//return res.end("document"+datenow+"_"+req.file.originalname); 	
	}else{
		console.log("data no file: " + req.file);
		return res.end('data: invalid file.');
	}
}
});



//user
var getNotiUser = require('./module/user/getNotiUsers.module');
var updateReadNoti = require('./module/user/updateReadNoti.module');
var getCommentRejectData = require('./module/user/getCommentRejectData.module');
var getCommentRejectProgressweek = require('./module/user/getCommentRejectProgressWeek.module');
var getDetailNotiUser = require('./module/user/getDetailNotiUser.module');
var getrejectFile = require('./module/user/getRejectFile.module');
var getDetailUserPrimary = require('./module/user/getDetailUserPrimary.module');
var createAccount = require('./module/user/createAccount.module');
var login = require('./module/user/login.module');
var fillInUserPrimary = require('./module/user/fillInUserPrimary.module');
var dashboard = require('./module/user/dashboard.module');
var createProject = require('./module/user/createProject.module');
var updateUserPrimary = require('./module/user/updateUserPrimary.module');
var getDataUserPrimary = require('./module/user/getDataUserPrimary.module');
var updateDocumentProject = require('./module/user/updateDocumentProject.module');
var addPlanOrder = require('./module/user/addPlanOrder.module');
var getAllPlanOrder = require('./module/user/getAllPlanOrder.module');
var getAllPlanReport = require('./module/user/getAllPlanReport.module');
var getPlanOrder = require('./module/user/getPlanOrder.module');
var setAmountOrder = require('./module/user/setAmountOrder.module');
var getSetAmountOrder = require('./module/user/getSetAmountOrder.module');
var updatePlanOrder = require('./module/user/updatePlanOrder.module');
var addAmountTeam =	require('./module/user/addAmountTeam.module');
var addTeamWork = require('./module/user/addTeamWork.module');
var getTeamWork = require('./module/user/getTeamWork.module');
var getTeamWhereId = require('./module/user/getTeamWhereId.module');
var updateDataUser = require('./module/user/updateDataUser.module');
var sendProject = require('./module/user/sendProject.module');
var addDuration = require('./module/user/addDuration.module');
var getAmountDurationOrder = require('./module/user/getAmountDurationOrder.module');
var getAllDuration = require('./module/user/getAllDuration.module');
var updateDuration = require('./module/user/updateDuration.module');
var countDay = require('./module/user/countDay.module');
var dashboardPercent = require('./module/user/dashboardPercent.module');
var deleteUser = require('./module/user/deleteUser.module');
var deletePlan = require('./module/user/deletePlan.module');
var getDetailProject = require('./module/user/deletePlan.module');
var updatePassword = require('./module/user/updatePassword.module');
var updateAmountTeam = require('./module/user/updateAmountTeam.module');
var getAmountTeam = require('./module/user/getAmountTeam.module');
var getLastActivity = require('./module/user/getLastActivity.module');
var checkLastActivity = require('./module/user/checkLastActivity.module');
var getPlanOrderDashboard = require('./module/user/getPlanOrderDashboard.module');
var getAllWeekOfPlan = require('./module/user/getAllWeeksOfPlan.module');
var getDailyProgress = require('./module/user/getDailyProgress.module');
var getDaysOnWeekTimesheet = require('./module/user/getDaysOnWeekTimesheet.module');
var getBatchName = require('./module/user/getBatchName.module');
var getTimesheetAllUsers = require('./module/user/getTimesheetAllUsers.module');
var sendProgressWeek = require('./module/user/sendProgressWeek.module');
var updateProgressWeekAfterReject = require('./module/user/updateProgressWeekAfterReject.module');
var getAllPlanOrderBeforeApprove = require('./module/user/getAllPlanOrderBeforeApprove.module');
var getPlanOnNowWeek = require('./module/user/getPlanOnNowWeek.module');
var createWeeks = require('./module/user/createWeek.module');
var getAllWeeks = require('./module/user/getAllWeeks.module');

//admin
var updatePasswordAdmin = require('./module/admin/updatePasswordAdmin.module');
var rejectCommentFile = require('./module/admin/rejectCommentFile.module');
var getAllUsers = require('./module/admin/getAllUsers.module');
var getNotiAdmin = require('./module/admin/getNotiAdmin.module');
var getCommentedReject = require('./module/admin/getCommentedReject.module');
var loginAdmin = require('./module/admin/loginAdmin.module');
var getDetailNotificationAdmin = require('./module/admin/getDetailNotificationAdmin.module');
var createBatch = require('./module/admin/createBatch.module');
var getAllBatch = require('./module/admin/getAllBatch.module');
var createLink = require('./module/admin/createLink.module');
var getDetailbatch = require('./module/admin/getDetailBatch.module');
var updateDuedate = require('./module/admin/updateDuedate.module');
var getProjectSending = require('./module/admin/getProjectSending.module');
var invertProjectSending = require('./module/admin/investProjectSending.module');
var updateStatusProjectInvest = require('./module/admin/updateStatusProjectInvest.module');
var rejectProject = require('./module/admin/rejectProject.module');
var approveProject = require('./module/admin/approveProject.module');
var getProjectStatusReject = require('./module/admin/getProjectStatusReject.module');
var rejectPlanOrder = require('./module/admin/rejectPlanOder.module');
var getDataDetailBatch = require('./module/admin/getDataDetailBatch.module');
var getProjectStatusApprove = require('./module/admin/getProjectStatusApprove.module');
var getReportDashboard = require('./module/admin/getReportDashboard.module');
var getProgressWeekSending = require('./module/admin/getProgressWeekSending.module');
var getDetailProgressWeekSending = require('./module/admin/getDetailProgressWeekSending.module');
var rejectProgressPlan = require('./module/admin/rejectProgressPlan.module');
var getProgressMissing = require('./module/admin/getProgressMissing.module');
var getProgressWeekReject = require('./module/admin/getProgressWeekReject.module');
var getDetailProgressWeekReject = require('./module/admin/getDetailProgressWeekReject.module');
var updateApproveProgressWeek = require('./module/admin/updateApproveProgressWeek.module');
var getAllBsm = require('./module/admin/getAllBsm.module');
var getProgressWeekFromProject = require('./module/admin/getProgressWeekFromProject.module');
var getDetailProgressWeekApprove = require('./module/admin/getDetailProgressWeekApprove.module');
var getOverAllProgressProject = require('./module/admin/getOverAllProgressProject.module');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
// allow cors
app.use(cors());

app.listen(3101,()=>{
    console.log('-----------------REST API Timesheet is run 3101.-----------------');
});

app.get('/', function (req, res, next) {
	res.status(200).send('IT WORK !');
});

getNotiUser.initialize(app);
updateReadNoti.initialize(app);//
getCommentRejectData.initialize(app);
getCommentRejectProgressweek.initialize(app);
getDetailNotiUser.initialize(app);
getrejectFile.initialize(app);
getDetailUserPrimary.initialize(app);
createAccount.initialize(app);
login.initialize(app);
fillInUserPrimary.initialize(app);
dashboard.initialize(app);
createProject.initialize(app);
updateUserPrimary.initialize(app);
getDataUserPrimary.initialize(app);
updateDocumentProject.initialize(app);
addPlanOrder.initialize(app);
getAllPlanOrder.initialize(app);
getAllPlanReport.initialize(app);
getPlanOrder.initialize(app);
setAmountOrder.initialize(app);
getSetAmountOrder.initialize(app);
updatePlanOrder.initialize(app);
addAmountTeam.initialize(app);
addTeamWork.initialize(app);
getTeamWork.initialize(app);
getTeamWhereId.initialize(app);
updateDataUser.initialize(app);
sendProject.initialize(app);
addDuration.initialize(app);
getAmountDurationOrder.initialize(app);
getAllDuration.initialize(app);
updateDuration.initialize(app);
countDay.initialize(app);
dashboardPercent.initialize(app);
deleteUser.initialize(app);
deletePlan.initialize(app);
getDetailProject.initialize(app);
updatePassword.initialize(app);
updateAmountTeam.initialize(app);
getAmountTeam.initialize(app);
getLastActivity.initialize(app);
checkLastActivity.initialize(app);
getPlanOrderDashboard.initialize(app);
getAllWeekOfPlan.initialize(app);
getDailyProgress.initialize(app);
getDaysOnWeekTimesheet.initialize(app);
getBatchName.initialize(app);
getTimesheetAllUsers.initialize(app);
sendProgressWeek.initialize(app);
updateProgressWeekAfterReject.initialize(app);
getAllPlanOrderBeforeApprove.initialize(app);
getPlanOnNowWeek.initialize(app);
createWeeks.initialize(app);
getAllWeeks.initialize(app);

//admin
updatePasswordAdmin.initialize(app);
rejectCommentFile.initialize(app);
getAllUsers.initialize(app);
getNotiAdmin.initialize(app);
getCommentedReject.initialize(app);
loginAdmin.initialize(app);
getDetailNotificationAdmin.initialize(app);
createBatch.initialize(app);
getAllBatch.initialize(app);
createLink.initialize(app);
getDetailbatch.initialize(app);
updateDuedate.initialize(app);
getProjectSending.initialize(app);
invertProjectSending.initialize(app);
updateStatusProjectInvest.initialize(app);
rejectProject.initialize(app);
approveProject.initialize(app);
getProjectStatusReject.initialize(app);
rejectPlanOrder.initialize(app);
getDataDetailBatch.initialize(app);
getProjectStatusApprove.initialize(app);
getReportDashboard.initialize(app);
getProgressWeekSending.initialize(app);
getDetailProgressWeekSending.initialize(app);
rejectProgressPlan.initialize(app);
getProgressMissing.initialize(app);
getProgressWeekReject.initialize(app);
getDetailProgressWeekReject.initialize(app);
updateApproveProgressWeek.initialize(app);
getAllBsm.initialize(app);
getProgressWeekFromProject.initialize(app);
getDetailProgressWeekApprove.initialize(app);
getOverAllProgressProject.initialize(app);