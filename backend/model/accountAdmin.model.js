const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const accountAdminSchema = mongoose.Schema({
    username:String,
    password:String,
    lastLogin:Date,
    type:String,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('accountAdmin',accountAdminSchema,'accountAdmin');