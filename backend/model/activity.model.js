const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const activitySchema = mongoose.Schema({
    activityOrder:String,
    activityName:String
},{
	timestamps: true
});

module.exports = mongoose.model('activity',activitySchema,'activity');