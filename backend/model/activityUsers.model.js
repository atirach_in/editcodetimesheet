const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const activityUsersSchema = mongoose.Schema({
    userId:String,
    activityId:String ,
    datetime:Number
},{
	timestamps: true
});

module.exports = mongoose.model('activityUsers', activityUsersSchema, 'activityUsers');