const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const batchSchema = mongoose.Schema({
    adminInsert:String,
    adminUpdate:String,
    modelName:String,
    fiscalYear:String,
    numberOfBusinessman:Number,
    startIngDate:Date,
    DueDate:Date,
    alreadyBusinessman:Number,
    InsDate:Date,
    updateTime :Date,
    status:String,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('batch',batchSchema,'batch');
