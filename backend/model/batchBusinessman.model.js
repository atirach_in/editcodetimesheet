const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const batchBusinessmanSchema = mongoose.Schema({
    batchId:String,
    projectId:String,
    commentReject:String,
    documentReject:String,
    insDate:Date,
    updateTime:Date,
    status:String,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('batchBusinessman',batchBusinessmanSchema,'batchBusinessman');