const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const comentProgressWeekSchema = mongoose.Schema({
    progressGroupPlanId:String,
    coment:String,
    documentation:String,
    dateTimestamp:Number,
    datetimeString:String 
},{
	timestamps: true
});

module.exports = mongoose.model('comentProgressWeek',comentProgressWeekSchema,'comentProgressWeek');