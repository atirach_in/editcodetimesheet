const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const commentRejectSchema = mongoose.Schema({
    projectId:String,
    planId:String,
    comment:String,
    insertDate:Date,
    read:Boolean,
    status:String,
    statusString:String
},{
	timestamps: true
});

module.exports = mongoose.model('commentReject',commentRejectSchema,'commentReject');