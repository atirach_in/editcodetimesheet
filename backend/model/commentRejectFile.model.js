const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const commentRejectFileSchema = mongoose.Schema({
    projectId:String,
    fileName:String,
    fileNameReject :String,
    comment:String,
    insertDate:Date,
    read:Boolean,
    status:String,
    statusString:String 
},{
	timestamps: true
});

module.exports = mongoose.model('commentRejectFile',commentRejectFileSchema,'commentRejectFile');