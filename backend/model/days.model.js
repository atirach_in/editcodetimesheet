const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const daysSchema = mongoose.Schema({
    weekId:String,
    dayNumber:Number,
    dateTimestamp:Number,
    dateString:String,
    dayString:String,
    statusOpen:Number,
},{
	timestamps: true
});

module.exports = mongoose.model('days',daysSchema,'days');