const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const detailProgressWeeksSchema = mongoose.Schema({
    progressWeekId:String,
    userId:String,
    username:String,
    totalHours:Number,
    comment:String

},{
	timestamps: true
});

module.exports = mongoose.model('detailProgressWeeks',detailProgressWeeksSchema,'detailProgressWeeks');