const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const durationSchema = mongoose.Schema({
    durationOrder:String,
    month:Number,
    projectId:String
},{
	timestamps: true
});

module.exports = mongoose.model('duration',durationSchema,'duration');