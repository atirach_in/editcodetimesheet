const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const fileProjectSchema = mongoose.Schema({
    projectId:String,
    fileName:String ,
    insDate:Number
},{
	timestamps: true
});

module.exports = mongoose.model('fileProject',fileProjectSchema,'fileProject');