const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const logActivityProjectSchema = mongoose.Schema({
    projectId:String,
    projectName:String, 
    userId:String,
    status:String,
    statuString:String,
    dateTimestamp:Number
},{
	timestamps: true
});

module.exports = mongoose.model('logActivityProject',logActivityProjectSchema,'logActivityProject');