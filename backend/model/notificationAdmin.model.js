const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const notificationAdminSchema = mongoose.Schema({
    projectId:String,
    statusAction:Number,
    statusString:String ,
    read:String ,
    progressWeekId:String ,
    dateTimestamp:Number,
    dateTimestring:String

},{
	timestamps: true
});

module.exports = mongoose.model('notificationAdmin',notificationAdminSchema,'notificationAdmin');
