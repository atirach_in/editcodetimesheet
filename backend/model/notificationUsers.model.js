const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const notificationUsersSchema = mongoose.Schema({
    projectId:String,
    planId:String,
    commentRejectId:String,
    statusAction:Number,
    statusString:String,
    read:String,
    progressWeekId:String,
    dateTimestamp:Number,
    dateTimestring:String

},{
	timestamps: true
});

module.exports = mongoose.model('notificationUsers',notificationUsersSchema,'notificationUsers');