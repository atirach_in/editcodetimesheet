const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const planOrderSchema = mongoose.Schema({
    projectId:String,
    planOrder:Number,
    planName:String,
    jobValue:Number,
    moneyDifference:Number,
    enforceEmployee:Number,
    durationId:Number,
    durationOrder:Number,
    averageWages:Number,
    timeLength:Number,
    totalHours:Number,
    averageHours:Number,
    InsDate:Date,
    updateTime:Date,
    comentReject:String,
    status:Number,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('planOrder',planOrderSchema,'planOrder');