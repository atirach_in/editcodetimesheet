const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const positionSchema = mongoose.Schema({
    positionName:String,
    active:String

},{
	timestamps: true
});

module.exports = mongoose.model('position',positionSchema,'position');