const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const progressGroupPlanSchema = mongoose.Schema({
    projectId:String,
    batchId:String,
    durationId:String,
    status:String,
    statusString:String,
    updateTime:Number

},{
	timestamps: true
});

module.exports = mongoose.model('progressGroupPlan',progressGroupPlanSchema,'progressGroupPlan');