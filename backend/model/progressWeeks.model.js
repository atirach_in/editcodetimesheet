const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const progressWeeksSchema = mongoose.Schema({
    progressGroupPlanId:String,
    weekId:String,
    weekNumber:Number,
    planId:String,
    planName:String,
    durationId:String,
    durationOrder :String,
    planValue:Number,
    valueUsed:Number,
    timeLength:Number,
    timeUsed:Number,
    documentation :String,
    barries:String,
    updateTime:Number,
    status:Number,
    statusString:String,
    batchId:String,
    projectId:String
},{
	timestamps: true
});

module.exports = mongoose.model('progressWeeks',progressWeeksSchema,'progressWeeks');