const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const projectSchema = mongoose.Schema({
    projectname:String,
    projectvalue:Number,
    Documentation:String,
    Primaryresponsibility :String,
    setamountorder :Number,
    Insdate:Date,
    updateproject:Date,
    senddatetime:Date,
    approvedatetime:Date,
    amountteam:Number,
    status:String,
    amountplan:Number,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('project',projectSchema,'project');