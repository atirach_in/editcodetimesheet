const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const teamworkSchema = mongoose.Schema({
    memberFristname:String,
    memberLastname:String,
    positionName:String,
    memberImage:String,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('teamwork',teamworkSchema,'teamwork');