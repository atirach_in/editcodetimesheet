const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const timesheetSchema = mongoose.Schema({
    userId:String,
    dayId:String,
    planId:String,
    projectId:String,
    hours:Number,
    totalValueOnDay:Number,
    updateTime:Date,
},{
	timestamps: true
});

module.exports = mongoose.model('timesheet', timesheetSchema, 'timesheet');