const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const usersSchema = mongoose.Schema({
    email:String,
    password:String,
    fristName:String,
    lastName:String,
    positionName:String,
    phone:String,
    profileImage:String,
    projectId:String,
    insdate:Date,
    lastlogin:Date,
    updatetTme:Date,
    active:String
},{
	timestamps: true
});

module.exports = mongoose.model('users', usersSchema, 'users');