const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const weeksSchema = mongoose.Schema({
    durationId:String,
    projectId:String,
    planId:String,
    weekNumber:Number,
    monthNumber:Number,
    daysInWeek:Number,
    weekStartDate:Number,
    weekEndDateTimestamp:Number,
    weekValue:Number,
    updateTime:Date,
    statusWeek:Number
},{
	timestamps: true
});

module.exports = mongoose.model('weeks',weeksSchema,'weeks');