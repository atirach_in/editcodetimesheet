var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/createBatch.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/createbatch',createBatch);
}

async function createBatch(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var adminInsert = req.body.userId
            var adminUpdate = ""
            var modelName = req.body.modelName
            var piscalYear = req.body.piscalYear
            var numberOfOperators = req.body.numberOfOperators
        
            var startingDate = req.body.startingDate
            var dtsStartingDate = Date.parse(startingDate);
        
            var dueDate = req.body.dueDate
            var dtsDueDate = Date.parse(dueDate);
        
            var alreadyBusinessman = 0
            var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
            var pastDatetime = nodeDatetime.create(dateNow);
            var updateTime = 0
            var status = "1"
            var active = "1" 
            if (modelName == "" || piscalYear == "" || numberOfOperators == "" || dueDate == "" || startingDate == ""){
                var result={data:"data invalid.",status:false}
                res.json(result);
                return;
            }else{
                dbService.addBatch(adminInsert,adminUpdate,modelName,piscalYear,numberOfOperators,startingDate,dtsStartingDate,dueDate,dtsDueDate,alreadyBusinessman,dateNow,pastDatetime,updateTime,status,active)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}