var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/createLink.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/createlink',createLink);
}


async function createLink(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var batchId = req.body.batchId
            dbService.createLink(batchId)
            .then(function(result){
                res.json(result);
                return;
            });
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}
