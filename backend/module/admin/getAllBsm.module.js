var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getAllBsm.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/get/all/bsm',getAllBsm);
}

async function getAllBsm(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var batchId = req.body.batchId;
            if(batchId == ""){
                var respond = {data:"invalid batchId.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.getAllBsm(batchId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:null,
            obj:null
        });
        return;
    }
}