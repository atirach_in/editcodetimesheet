var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getAllUsers.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.get('/get/all/users',getAllUsers);
}

async function getAllUsers(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let batchId = req.body.batchId;
            if(batchId == ""){
                console.log("data: invalid batchId.");
                res.json({
                    data:"invalid batchId.",
                    status:false
                });
                return;
            }else{
                dbService.getAllUsers(batchId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(200).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}