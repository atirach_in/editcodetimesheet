var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getCommentedReject.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.get('/get/commented/reject',getCommentedReject);
}

async function getCommentedReject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let projectId = req.body.projectId;
            let planId = req.body.planId;
            if(projectId == "" || planId ==""){
                console.log("data: invalid data.");
                res.json({
                    data:"invalid data.",
                    status:false
                });
                return;
            }else{
                dbService.getCommentedReject(projectId,planId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}