var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getDetailNotificationAdmin.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/get/detail/notification/admin',getDetailNotiAdmin);
}

async function getDetailNotiAdmin(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let projectId = req.body.projectId;
            let progressGroupPlanId = req.body.progressGroupPlanId;
            let statusString = req.body.statusString;
            let notiId = req.body.notiId;
            if(statusString == "ส่งความก้าวหน้า"){
                dbService.updateReadNotiAdmin(notiId).
                then(function(result){
                    res.json({
                        path:"/admintimesheet/sme-detail/success/",
                        id:progressGroupPlanId
                    });
                });
            }else if(statusString == "ส่งเอกสารแล้ว"){
                dbService.updateReadNotiAdmin(notiId).
                then(function(result){
                    res.json({
                        path:"/admin/project-detail/pending/",
                        id:projectId
                    });
                    return;
                });
            }else{
                res.json({
                    data:"invalid statusString.",
                    status:false
                });
                return;
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}