var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getDetailProgressWeekSending.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/get/detail/progress/week/sending',getDetailProgressWeekSending);
}

async function getDetailProgressWeekSending(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var progressGroupPlanId = req.body.progressGroupPlanId;
            if(progressGroupPlanId == ""){
                var respond = {data:"invalid progressGroupPlanId.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.getDetailProgressWeekSending(progressGroupPlanId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}