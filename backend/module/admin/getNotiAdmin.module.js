var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getNotiAdmin.db');
var date = require('date-and-time');
var nodedatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.get('/get/noti/admin',getNotiAdmin);
}

async function getNotiAdmin(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let adminId = req.body.adminId;
            if(adminId == ""){
                console.log("data: invalid adminId.");
                res.json({data: "invalid adminId."});
                return;
            }else{
                dbService.getNotiAdmin(adminId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}