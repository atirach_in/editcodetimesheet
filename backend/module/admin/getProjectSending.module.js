var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getProjectSending.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/getprojectsending',getProjectSending);
}

async function getProjectSending(req,res){
    try{    
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var batchId = req.body.batchId;
            if(batchId == ""){
                var response = {data:"invalid batchid",status:false};
                res.json(response);
                return;
            }else{
                dbService.getProjectSending(batchId)
                .then(function(result){
                    res.json(result);	
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}