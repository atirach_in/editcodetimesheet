var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/getProjectStatusApprove.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/get/project/status/approve',getProjectStatusApprove);
}

async function getProjectStatusApprove(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var batchId = req.body.batchId;
            if(batchId == ""){
                var respond = {data:"invalid batchId",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getProjectApprove(batchId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}