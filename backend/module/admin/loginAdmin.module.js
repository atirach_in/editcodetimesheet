var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/loginAdmin.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/loginadmin',loginAdmin);
}

async function loginAdmin(req,res){
    try{
        var username = req.body.username;
        var password = req.body.password;
        if(username == "" || password == ""){
            console.log("data: invalid username or password.");
            var response = {data:"invalid username or password"};
            res.json(response);
            return;
        }else{
            dbService.getUser(username,password)
            .then(function(result){
                res.json(result);
                return;
            });
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}