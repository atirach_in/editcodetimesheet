var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/rejectCommentFile.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.get('/reject/comment/file',rejectCommentFile);
}

async function rejectCommentFile(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let projectId = req.body.projectId;
            let comment = req.body.comment;
            let filename = req.body.filename;
            let filenameReject = req.body.filenameReject;
            if(projectId == ""){
                console.log("data: invalid projectId.");
                res.json({
                    data:"invalid projectId.",
                    status:false
                });
                return;
            }else{
                dbService.rejectFile(projectId,comment,filename,filenameReject)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}