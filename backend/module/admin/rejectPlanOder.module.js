var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/rejectPlanOder.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/reject/planorder',rejectPlanOder);
}

async function rejectPlanOder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var planId = req.body.planId;
            var comment = req.body.comment;
            var pahtFile = req.body.pahtFile;
            if(planId == ""){
                var respond = {data:"invalid planId."};
                res.json(respond);
                return;
            }else{
                dbService.rejectProject(planId,comment,pahtFile)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}