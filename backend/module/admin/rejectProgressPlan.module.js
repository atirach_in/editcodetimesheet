var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/rejectProgressPlan.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/reject/progressplan/',rejectProgressPlan);
}

async function rejectProgressPlan(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var progressGroupPlanId = req.body.progressGroupPlanId;
            var commentReject = req.body.commentReject;
            var documentReject = req.body.documentReject;
            if(progressGroupPlanId == ""){
                var respond = {data:"invalid progressGroupPlanId.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.rejectProgressPlan(progressGroupPlanId,commentReject,documentReject)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}