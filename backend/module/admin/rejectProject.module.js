var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/rejectProject.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/rejectproject',rejectProject);
}

async function rejectProject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var comment = req.body.comment;
            var documentComment = req.body.document;
            if(projectId === "" || comment === "" || documentComment === ""){
                var respond = {data:"invalid data.",status:false};
                res.json(respond);
            }else{
                dbService.updateCommentProject(projectId,comment,documentComment)
                .then(function(result){
                    res.json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}