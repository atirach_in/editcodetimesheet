var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/updateApproveProgressWeek.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/update/approve/progress/week',updateApproveProgressWeek);
}

async function updateApproveProgressWeek(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var progressGroupPlanId = req.body.progressGroupPlanId;
            var late = req.body.late;
            if(progressGroupPlanId == ""){
                var respond = {data:"invalid progressGroupPlanId.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.approveProgressWeek(progressGroupPlanId,late)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}