var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/updateDuedate.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/update/duedate',updateDuedate);
}

async function updateDuedate(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var batchId = req.body.batchId;
            var dueDate = req.body.dueDate;
            if(dueDate == ""){
                var respond = {data:"invalid duedate",status:false};
                res.json(respond);
                return;
            }else{
                var dtsDueDate = Date.parse(dueDate);
                dbService.updateDueDateTime(batchId,dtsDueDate)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}