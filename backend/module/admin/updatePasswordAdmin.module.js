var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/updatePasswordAdmin.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
const jwt = require('jsonwebtoken'); 
const SECRET = "00f5vdd5r12fg1g[][dsdd122"; 
exports.initialize = (app) =>{
    app.get('/update/password/admin',updatePasswordAdmin);
}

async function updatePasswordAdmin(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            let userId = req.body.userId;
            let pass = req.body.password;
            if(pass == ""){
                console.log("data: invalid userId.");
                return {data:"invalid userId.",status:false}
            }else{
                var objPass = {
                    password:pass
                }
                var token = jwt.sign(objPass,SECRET);
                dbService.updatePassword(userId,token)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}