var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/admin/updateStatusProjectInvest.db');
var date = require('date-and-time');
var nodedatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/update/status/projectinvest',updateStatusProjectInvest);
}

async function updateStatusProjectInvest(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var status = req.body.status;
            if(projectId == "" || status == ""){
                var respond = {data:"invalid projectId or status.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.updateStatusProjectInvest(projectId,status)
                .then(function(result){
                    res.json(result);
                    return;
                });	
            }
        }
    }catch(e){
        console.log(e);
        req.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}