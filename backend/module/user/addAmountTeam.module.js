var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/addAmountTeam.db');

exports.initialize = (app) =>{
    app.post('/add/amount/team',addAmountTeam);
}

async function addAmountTeam(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            var amount = req.body.amount;
            if(userId == "" || projectId == "" || amount == ""){
                var respond = {data:"invalid userId or projectId , amount.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.addAmountTeam(userId,projectId,amount)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}