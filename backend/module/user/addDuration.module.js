var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/addDuration.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/add/duration',addDuration);
}

async function addDuration(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            var durationOrder = req.body.durationOrder;
            console.log(durationorder);
            if (durationorder == ""){
                var respond = {data:"duration or month invalid.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.addDuration(projectId,durationOrder,userId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            } 
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}