var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/addPlanOrder.db');
var nodeDatetime = require('node-datetime');
var date = require('date-and-time');

exports.initialize = (app) =>{
    app.post('/addplanorder',addPlanOrder);
}

async function addPlanOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userid);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId
            var planname = req.body.planname
            var jobValue = req.body.jobValue
            var durationId = req.body.durationId
            var timeLength = req.body.timeLength //days
            var averageWages = req.body.averageWages
            var moneyDifference = req.body.moneyDifference
            var enforceEmployee = req.body.enforceEmployee
            date.locale('th');
            var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
            var pastDatetime = nodeDatetime.create(dateNow);
            if(userId == "" || planname == "" || jobValue == "" || durationId == "" || timeLength == "" || averageWages == "" || enforceEmployee == "" || projectId == ""){
                var result = {data:"data invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.addPlanOrder(userId,projectId,planname,jobValue,durationId,timeLength,averageWages,moneyDifference,enforceEmployee)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(200).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}