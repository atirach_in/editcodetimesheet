var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/addTeamWork.db');
var date = ('date-and-time');
exports.initialize = (app) =>{
    app.post('/addteamwork',addTeamWork);
}

async function addTeamWork(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            var fristName = req.body.fristName;
            var lastName = req.body.lastName;
            var position = req.body.position;
            var profileImage = req.body.profileImage;
            var email = req.body.email;
            var phone = req.body.phone;
            date.locale('th');
            var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
            var pastDatetime = nodedatetime.create(dateNow);
            if(userId == "" || fristName == "" || lastName == "" || position == "" || email == "" || phone == "" ||profileImage == ""){
                var result ={data:"data invalid",status:false};
                res.json(result);
                return;
            }else{
                dbService.addTeamWork(userId,projectId,fristName,lastName,position,profileImage,email,phone)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}