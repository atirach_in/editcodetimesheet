var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/addTimesheetDays.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/add/timesheet/days',addTimesheetDays);
}

async function addTimesheetDays(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var planId = req.body.planId;
            var weekId = req.body.weekId;
            
            var day1Id = req.body.day1Id;
            var day2Id = req.body.day2Id;
            var day3Id = req.body.day3Id;
            var day4Id = req.body.day4Id;
            var day5Id = req.body.day5Id;
            var day6Id = req.body.day6Id;
            var day1 = req.body.day1;
            var day2 = req.body.day2;
            var day3 = req.body.day3;
            var day4 = req.body.day4;
            var day5 = req.body.day5;
            var day6 = req.body.day6;
            if(projectId == "" || planId == "" || weekId == ""){
                var respond = {data:"invalid data."};
                res.json(respond);
            }else{
                dbService.addTimesheet(projectId,planId,weekId,day1,day2,day3,day4,day5,day6,day1Id,day2Id,day3Id,day4Id,day5Id,day6Id)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}