var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/checkLastActivity.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/check/last/activity',checkLastActivity);
}

async function checkLastActivity(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            if(userId == "" || projectId == ""){
                var respond = {data:"invalid userId or projectId.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.checkLastDraft(userId,projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        
    }
}