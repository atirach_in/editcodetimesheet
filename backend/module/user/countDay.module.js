var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/countDay.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/count/day',countDay);
}

async function countDay(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            dbService.testCountDate()
            .then(function(result){
                res.json(result);
            });
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}