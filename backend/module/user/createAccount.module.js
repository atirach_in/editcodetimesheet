var checkAuth = require('../../db/checkAuth');
var createAccountDb  = require('../../db/createAccount.db');
exports.initialize = (app) =>{
    app.post('/createaccount',createAccount);
}

async function createAccount(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var fristName = req.body.fristName;
            var lastName = req.body.lastName;
            var phone = req.body.phone;
            var email = req.body.email;
            var password = req.body.password;
            var dueDate = req.body.dueDate;
            var batchId = req.body.batchId;
            var companyName = req.body.companyName;
            if (fristName == "" || lastName == "" || phone == "" || email == "" || password =="" || companyName == ""){
                var result ={
                    data:"data invalid",
                    status:false
                }
                res.json(result);
            }else{
                // var obj_pass ={
                // 	password:password
                // }
                var token = password;//jwt.sign(obj_pass, SECRET, { expiresIn: 300 });
                //var decode = jwt.verify(token,SECRET);
                //res.json({token:token,decode:decode.password});
                createAccountDb.createAccount(fristName,lastName,phone,email,token,dueDate,batchId,companyName)
                .then(function(result){
                    res.json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}