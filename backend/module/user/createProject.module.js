var checkAuth = require('../../db/checkAuth');
var createProjectDb = require('../../db/createProject.db');
exports.initialize = (app) =>{
    app.post('/createproject',createProject);
}

async function createProject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var userId = req.body.userId;
            var projectName = req.body.projectName;
            var projectValue = req.body.projectValue;
            var batchId = req.body.batchId;
            if(userId == "" || projectName == "" || projectValue == "" || batchid == ""){
                console.log(userId);
                // var result = {
                //     data:"data invalid",
                //     status:false
                // }
                res.status(200).json({
                    success:false,
                    data:'invalid data.',
                    obj:null
                });
            }else{
                createProjectDb.createProject(userId,projectName,projectValue,batchId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}