var checkAuth = require('../../db/checkAuth');
var dashboardDb = require('../../db/dashboard.db');
exports.initialize = (app) =>{
    app.post('/dashboard',dashboard);
}

async function dashboard(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var userId = req.body.userId;
            if (userId === ""){
                // var result = {
                //     data:"invalid userId"
                // };
                console.log("invalid userId");
                res.status(200).json({
                    success:false,
                    data:'invalid userId',
                    obj:null
                });
            }else{
                dashboardDb.dashboard(userId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}