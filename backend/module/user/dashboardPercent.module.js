var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/dashboardPercent.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/dashboard/percent',dashboardPercent);
}

async function dashboardPercent(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respone = {data:"invalid project."};
                res.json(respone);
                return;
            }else{
                dbService.dashboardPercent(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:null,
            obj:null
        });
    }
}