var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/deletePlan.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/delete/plan/',deletePlan);
}

async function deletePlan(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var planId = req.body.planId;
            if(planId == ""){
                var respond = {data:"invalid planId."};
                res.json(respond);
                return;
            }else{
                dbService.deletePlan(planId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}