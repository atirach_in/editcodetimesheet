var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/deleteUser.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/delete/user',deleteUser);
}

async function deleteUser(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var delUserId = req.body.delUserId;
            if(delUserId == ""){
                var respond ={data:"invalid userid.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.deleteUser(delUserId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}