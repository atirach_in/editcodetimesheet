var checkAuth = require('../../db/checkAuth');
var fillInUserPrimaryDb  = require('../../db/fillInUserPrimary.db');
exports.initialize = (app) =>{
    app.post('/fill/in/userprimary',fillInUserPrimary);
}

async function fillInUserPrimary(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var batchId = req.body.batchId;
            var userId = req.body.userId;
            if(batchId == "" || userId == ""){
                //var respond ={data:"invalid batchId or userId."};
                res.status(200).json({
                    success:false,
                    data:'invalid bacthId or userId',
                    obj:null
                });
            }else{
                fillInUserPrimaryDb.fillInUserPrimary(userId,batchId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}