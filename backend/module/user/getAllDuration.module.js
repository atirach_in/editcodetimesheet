var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllDuration.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/get/all/duration',getAllDuration);
}

async function getAllDuration(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respond = {data:"invalid projectId or userId."};
                res.json(respond);
                return;
            }else{
                dbService.getAllDuration(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }	
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}