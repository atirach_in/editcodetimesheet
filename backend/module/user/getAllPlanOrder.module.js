var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllPlanOrder.db');

exports.initialize = (app) =>{
    app.post('/getpallplanorder',getAllPlanOrder);
}

async function getAllPlanOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var result = {data:"data invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.getPallPlanOrder(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(200).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}