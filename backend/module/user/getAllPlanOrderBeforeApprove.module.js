var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllPlanOrderBeforeApprove.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.get('/get/allplan/order/before/approve',getAllPlanOrderBeforeApprove);
}

async function getAllPlanOrderBeforeApprove(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respond = {data:"invalid projectId.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.getAllPlanOrderBeforeApprove(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}