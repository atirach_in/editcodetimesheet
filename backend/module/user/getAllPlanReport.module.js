var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllPlanReport.db');

exports.initialize = (app) =>{
    app.post('/get/all/plan/report',getAllPlanReport);
}

async function getAllPlanReport(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var weekNumber = req.body.weekNumber;
            if(projectId == "" || weekNumber == ""){
                var result = {data:"data invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.getAllPlanReport(projectId,weekNumber)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e); 
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}
