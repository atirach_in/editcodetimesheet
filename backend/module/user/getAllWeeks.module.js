var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllWeeks.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');

exports.initialize = (app) =>{
    app.post('/get/all/weeks',getAllWeeks);
}

async function getAllWeeks(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respond = {data:"invalid projectId.",status:false}
                res.json(respond);
                return;
            }else{	
                dbService.getAllWeeks(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
    }
}