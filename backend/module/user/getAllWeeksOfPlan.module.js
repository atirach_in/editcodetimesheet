var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAllWeeksOfPlan.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/get/all/weeks/ofplan',getAllWeeksOfPlan);
}

async function getAllWeeksOfPlan(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var planId = req.body.planId;
            var projectId = req.body.projectId;
            if(planId == "" || projectId == ""){
                var respond = {data:"invalid planId or projectId.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getAllWeeksOfPlan(projectId,planId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}