var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getAmountDurationOrder.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/get/amount/duration/order',getAmountDurationOrder);
}

async function getAmountDurationOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            if(userId == "" || projectId == ""){
                var respond = {data:"invalid userId and projectId."};
                res.json(respond);
                return;
            }else{
                dbService.getAmountDurationOrder(userId,projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}