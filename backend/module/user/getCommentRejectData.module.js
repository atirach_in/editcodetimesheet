var checkAuth = require('../../db/checkAuth');
var getCommentRejectDb = require('../../db/getCommentRejectData.db');

exports.initialize = (app) =>{
    app.post('/get/comment/reject/data',getCommentRejectData);
}

async function getCommentRejectData(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            let commentRejectId = req.body.commentRejectId;
            if(commentRejectId == ""){
                res.json({
                    data:"invalid commentRejectId.",
                    status:false
                });
            }else{
                getCommentRejectDb.getCommentRejectData(commentRejectId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}