var checkAuth = require('../../db/checkAuth');
var getCommentrejectProgressWeekDb = require('../../db/getCommentRejectProgressWeek.db');
exports.initialize = (app)=>{
    app.post('/get/comment/reject/progress/week',getCommentRejectProgressWeek);
}

async function getCommentRejectProgressWeek(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            let commentRejectId = req.body.commentRejectId;
            if(commentRejectId == ""){
                res.json({
                    data:"invalid progress_groupplan_id.",
                    status:false
                });
            }else{
                getCommentrejectProgressWeekDb.getCommentRejectProgressWeek(commentRejectId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400),json({
            success:false,
            data:e,
            obj:null
        });
    }
}