var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getDataUserPrimary.db');
exports.initialize = (app) =>{
    app.post('/get/data/user/primary',getDatauserPrimary);
}

async function getDatauserPrimary(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respond = {data:"invalid projectId."};
                res.json(respond);
                return;
            }else{
                dbService.getUserPrimary(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(200).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}