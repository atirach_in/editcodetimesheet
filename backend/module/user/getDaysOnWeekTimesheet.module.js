var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getDaysOnWeekTimesheet.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/get/days/onweek/timesheet',getDaysOnWeekTimesheet);
}

async function getDaysOnWeekTimesheet(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var weekId = req.body.weekId;
            var dayId = req.body.dayId;
            var projectId = req.body.projectId;
            var planId = req.body.planId;
            var weekNumber = req.body.weekNumber;
            if(weekId =="" || dayId == "" || projectId == "" || planId == ""){
                var respond = {data:"invalid data.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getWeekTimesheet(dayId,projectId,planId,weekNumber)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            sucess:false,
            data:e,
            obj:null
        });
        return;
    }
}