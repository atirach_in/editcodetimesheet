var checkAuth = require('../../db/checkAuth');
var updateReadNoti = require('../../db/updateReadNoti.db');
exports.initialize = (app) =>{
    app.post('/get/detail/notification/user',getDetailNotiUser);
}

async function getDetailNotiUser(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            let projectId = req.body.projectId;
            let planId = req.body.planId;
            let commentRejectId = req.body.commentRejectId;
            let statusAction = req.body.statusAction;
            let progressGroupPlanId = req.body.progressGroupPlanId;
            let statusString = req.body.statusString;
            let notiId = req.body.notiId;
            console.log("req.body.statusString2: "+ req.body.statusString2);
            if(statusString == ""){
                console.log("data: invalid status string.");
                res.json({data:"invalid status string.",status:false});
            }else{
                if(statusString == "อนุมัติ Timsheet"){ 
                    updateReadNoti.updateReadnoti(notiId)
                    .then(function(result){
                        res.json({
                            path:"/sme/dashboard",
                            status:true
                        });
                    });
                }else if(statusString == "ปรับแก้เอกสาร"){
                    updateReadNoti.updateReadnoti(notiId)
                    .then(function(result){
                        res.json({
                            path:"/sme/view-notification/",
                            id:commentRejectId,
                            status:true
                        });
                    });
                    
                }else if(statusString == "ปรับแก้ความก้าวหน้ารายสัปดาห์"){
                    updateReadNoti.updateReadnoti(notiId)
                    .then(function(result){
                        res.json({
                        path:"/timesheet/view-notification/",
                            id:commentRejectId,
                            status:true
                        });
                    });
                }else if(statusString == "ตรวจสอบแล้ว"){
                    updateReadNoti.updateReadnoti(notiId)
                    .then(function(result){
                        res.json({
                            path:"/timesheet/dashboard",
                            //id:progressGroupPlanId,
                            status:true
                        });
                    });
                }else if(statusString == "ปรับแก้เอกสารโครงการ"){
                    updateReadNoti.updateReadnoti(notiId)
                    .then(function(result){
                        res.json({
                            path:"/sme/view-notification/reject/file/",
                            id:commentRejectId,
                            status:true
                        });
                    });
                }else{
                    console.log("data: invalid status string.");
                    res.json({data:"invalid status string.",status:false});
                }
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}