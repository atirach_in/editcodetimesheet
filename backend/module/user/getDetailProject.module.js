var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getDetailProject.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/getdetailproject',getDetailProject);
}

async function getDetailProject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var userId = req.body.userId;
            if(projectId == "" || userId == ""){
                var respond = {data:"",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getDetailProject(projectId,userId)
                .then(function(result){
                    res.json(result);
                    return;
                });	
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}