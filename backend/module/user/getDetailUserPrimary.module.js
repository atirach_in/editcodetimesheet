var checkAuth = require('../../db/checkAuth');
var getDetailUserPrimaryDb  = require('../../db/getDetailUserPrimary.db');
exports.initialize = (app) =>{
    app.post('/get/detail/user/primary',getDetailUserPrimary);
}

async function getDetailUserPrimary(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            let userId = req.body.userId;
            if(userid == ""){
                console.log("data: invalid userId.");
                res.json({data:"invalid userid."});
            }else{
                getDetailUserPrimaryDb.getDetailUserPrimary(userId)
                .then(function(result){
                    res.json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}