var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getLastActivity.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/get/last/activity/:userid',getLastActivity);
}

async function getLastActivity(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            if(userId == ""){
                var respond ={data:"invalid userId.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getLastActivity()
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}