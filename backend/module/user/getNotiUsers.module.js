var checkAuth = require('../../db/checkAuth');
var getNotiUserDb = require('../../db/getNotiUsers.db');

exports.initialize = (app)=>{
    app.post("/get/noti/users",getNotiUsers);
}

async function getNotiUsers(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                console.log("data : invalid projectId.");
                res.status(200).json({
                    success:false,
                    data: "invalid projectId.",
                    obj:null
                });
                return;
            }else{
                getNotiUserDb.getNotificationUser(projectId)
                .then(function(result){
                    res.status(200).json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}