var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getPlanOnNowWeek.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.get('/get/plan/on/now/week',getPlanOnNowWeek);
}

async function getPlanOnNowWeek(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                res.json({
                    data:"invalid projectId.",
                    status:false
                });
                return;
            }else{
                dbService.getPlanOnNowWeek(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}