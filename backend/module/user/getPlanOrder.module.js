var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getPlanOrder.db');

exports.initialize = (app) =>{
    app.post('/getplanorder',getPlanOrder);
}

async function getPlanOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var planId = req.body.planId;
            if(planId == ""){
                var result={data:"invalid planId",status:false}
                res.json(result);
                return;
            }else{
                dbService.getPlanOrder(planId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);    
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}