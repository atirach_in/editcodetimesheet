var checkAuth = require('../../db/checkAuth');
var updateReadNoti = require('../../db/getRejectFile.db');
exports.initialize = (app) =>{
    app.post('/get/reject/file',getRejectFile);
}

async function getRejectFile(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            let projectId = req.body.projectId;
            let commentRejectId = req.body.commentRejectId;
            if(projectId == "" || commentRejectId == ""){
                console.log("data: invalid projectId or commentRejectId.");
                res.json({
                    data:"invalid projectId or commentRejectId.",
                    status:false
                });
            }else{
                updateReadNoti.getRejectFile(projectId,commentRejectId)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}