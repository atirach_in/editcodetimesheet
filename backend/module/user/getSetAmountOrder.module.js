var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getSetAmountOrder.db');

exports.initialize = (app) =>{
    app.post('/getsetamountorder',getSetAmountOrder);
}

async function getSetAmountOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            if(projectId == ""){
                var respone ={data:"invalid projectId.",status:false};
                res.json(respone);
                return;
            }else{
                dbService.getSetAmountOrder(projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}