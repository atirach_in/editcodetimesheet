var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getTeamWhereId.db');

exports.initialize = (app) =>{
    app.post('/getteamwhereid',getTeamWhereId);
}

async function getTeamWhereId(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userIdUpdate;
            if(userId == ""){
                var respond = {data:"invalid userId.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.getTeamWhereId(userId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}