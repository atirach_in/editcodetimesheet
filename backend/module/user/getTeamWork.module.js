var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getTeamWork.db');

exports.initialize = (app) =>{
    app.post('/getteamwork',getTeamWork);
}

async function getTeamWork(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var userPrimaryKeyId = req.body.userId;
            if(projectId == ""){
                var result={data:"data invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.getTeamWork(projectId,userPrimaryKeyId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}