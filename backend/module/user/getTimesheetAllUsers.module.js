var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/getTimesheetAllUsers.db');
var date = require('date-and-time');
var nodedatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.get('/get/timesheet/all/users',getTimesheetAllUsers);
}

async function getTimesheetAllUsers(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var weekNumber = req.body.weekNumber;
            if(projectId == "" || weekNumber == ""){
                var respond = {data:"invalid data.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.getTimesheetAllUsers(projectId,weekNumber)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}