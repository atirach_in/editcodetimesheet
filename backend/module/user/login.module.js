var checkAuth = require('../../db/checkAuth');
var loginDb  = require('../../db/login.db');
exports.initialize = (app) =>{
    app.post('/login',login);
}

async function login(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var email = req.body.email;
            var password = req.body.password;
            if(email == "" || password == ""){
                var respond ={data:"invalid email or password."};
                res.json(respond);
            }else{
                console.log("password:" + password);
                var token = password;
                loginDb.login(email,token)
                .then(function(result){
                    res.status(200).json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}