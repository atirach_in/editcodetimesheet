var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateAmountTeam.db');
var date = require('date-and-time');
var nodedatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/update/amount/team',updateAmountTeam);
}

async function updateAmountTeam(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var amount = req.body.amount;
            var projectId = req.body.projectId;
            if(amount == "" || projectId == ""){
                var respond = {data:"invalid amount or projectId."};
                res.json(respond);
                return;
            }else{
                dbService.updateAmountTeam(projectId,amount)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}