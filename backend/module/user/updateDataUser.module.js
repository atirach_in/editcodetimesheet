var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateDataUser.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/updatedatauser',updateDataUser);
}

async function updateDataUser(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userIdUpdate;
            var fristName = req.body.fristName;
            var lastName = req.body.lastName;
            var position = req.body.position;
            var profileImage = req.body.profileImage;
            var email = req.body.email;
            var phone = req.body.phone;
            var password = req.body.password;
            date.locale('th');
            var dateNow = date.format(new Date(), 'D/M/YYYY HH:mm:ss'); 
            var pastDatetime = nodeDatetime.create(dateNow);
            if (userId == "" || fristName == "" || lastName == "" || position == ""){
                var result={data: "data invalid",status:false};
                res.json(result);
                return;
            }else{
                if(password == "not password"){
                    dbService.updateDataUser(userId,fristName,lastName,position,profileImage,email,phone)
                        .then(function(result){
                        res.json(result);
                        return;
                    });
                }else{
                    dbService.updateDataUser(userId,fristName,lastName,position,profileImage,email,phone)
                        .then(function(result){
                        res.json(result);
                        return;
                    });
                }
                
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}