var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateDocumentProject.db');
exports.initialize = (app) =>{
    app.post('/updatedocumentproject',updateDocumentProject);
}

async function updateDocumentProject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId;
            var projectId = req.body.projectId;
            var pathDocument = req.body.pathDocument;
            if (userId == "" || pathDocument == "" || projectId == ""){
                console.log("data: path document invalid");
                var result={data:"path document invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.addDocument(userId,projectId,pathDocument)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(200).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}