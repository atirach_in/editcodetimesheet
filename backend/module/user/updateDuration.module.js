var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateDuration.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/update/duration',updateDuration);
}

async function updateDuration(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var duration = req.body.duration;
            var projectId = req.body.projectId;
            if(duration == "" || projectId == ""){
                var respond = {data:"invalid duration.",status:false};
                res.json(respond);
                return;
            }else{
                dbService.updateDuration(duration,projectId)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}