var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updatePassword.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.post('/update/password',updatePassword);
}

async function updatePassword(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var userId = req.body.userId
            var password = req.body.password;
            if(userId == "" || password == ""){
                var respond = {data:"invalid userId or password."};
                res.json(respond);
                return;
            }else{
                dbService.updatePassword(userId,password)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            sucess:false,
            data:e,
            obj:null
        });
    }
}