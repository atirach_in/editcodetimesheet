var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updatePlanOrder.db');

exports.initialize = (app) =>{
    app.post('/updateplanorder',updatePlanOrder);
}

async function updatePlanOrder(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var planId = req.body.planId;
            var planName = req.body.planName;
            var jobValue = req.body.jobValue;
            var durationId = req.body.durationId;
            var timeLength = req.body.timeLength;
            var averageWages = req.body.averageWages;
            var moneyDifference = req.body.moneyDifference;
            var enforceEmployee = req.body.enforceEmployee;
            if (planName == "" || jobValue == "" || durationId == "" || timeLength == "" || averageWages == "" || planId == "" || moneyDifference == "" || enforceEmployee == ""){
                var result = {data:"data invalid",status:false}
                res.json(result);
                return;
            }else{
                dbService.updatePlanOrder(planId,planName,jobValue,durationId,timeLength,averageWages,moneyDifference,enforceEmployee)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}