var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateProgressWeekAfterReject.db');
var date = require('date-and-time');
var nodeDatetime = require('node-datetime');
exports.initialize = (app) =>{
    app.get('/update/progress/week/after/reject',updateProgressWeekAfterReject);
}

async function updateProgressWeekAfterReject(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200).json({
                success:false,
                data:'invalid authorize.',
                obj:null
            });
            return;
        }else{
            var projectId = req.body.projectId;
            var weekNumber = req.body.weekNumber;
            var plans = req.body.plans;
            var late = req.body.late;
            if(projectId == "" || weekNumber == "" || plans.length == 0){
                var respond = {data:"invalid data.",status:false};
                console.log(respond);
                res.json(respond);
                return;
            }else{
                dbService.updateProgressWeekAfterReject(projectId,weekNumber,plans,late)
                .then(function(result){
                    res.json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log('\n',e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}