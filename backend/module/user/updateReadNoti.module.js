var checkAuth = require('../../db/checkAuth');
var updateReadNotiDb = require('../../db/updateReadNoti.db');

exports.initialize = (app)=>{
    app.post('/update/read/noti',updateReadNoti);
}

async function updateReadNoti(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
            return;
        }else{
            var notiId = req.body.notiId;
            if(notiId == ""){
                console.log("data: invalid notification id.");
                res.json({	
                    success:false,
                    data:"invalid notification id.",
                    obj:null
                });
                return;
            }else{
                updateReadNotiDb.updateReadNoti(notiId)
                .then(function(result){
                    res.status(200).json(result);
                    return;
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
        return;
    }
}