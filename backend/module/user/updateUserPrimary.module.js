var checkAuth = require('../../db/checkAuth');
var dbService = require('../../db/updateUserPrimary.db');
exports.initialize = (app) =>{
    app.post('/updateuserprimary',updateUserPrimary);
}

async function updateUserPrimary(req,res){
    try{
        var resultCheckAuth = await checkAuth.checkAuth(req.body.authorization,req.body.userId);
        if(resultCheckAuth.success === false){
            res.status(200),json({
                success:false,
                data:'authorization fail.',
                obj:null
            });
        }else{
            var userId = req.body.userId;
            var fristName = req.body.fristName;
            var lastName = req.body.lastName;
            var phone = req.body.phone;
            var position = req.body.position;
            var profileImage = req.body.profileImage;
            if(fristName == "" || lastName == "" || phone == "" || position == ""){
                var respond = {data:"invalid data.",status:false};
                res.json(respond);
            }else{
                dbService.updateUserPrimary(userId,fristName,lastName,phone,position,profileImage)
                .then(function(result){
                    res.json(result);
                });
            }
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            success:false,
            data:e,
            obj:null
        });
    }
}